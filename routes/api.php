<?php

use App\Http\Controllers\SSSController;
use App\Mail\SignUp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Auth\LoginController');
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('me', 'Auth\MeController');
        Route::post('logout', 'Auth\LogoutController');
        Route::get('logout', 'Auth\LogoutController@logout');
    });
});
Route::group([
    'prefix' => 'app'
], function () {
});

Route::group(
    ['middleware' => 'auth:api'],
    function () {
        Route::group(
            ['prefix' => 'schedules'],
            function () {
                Route::post('/', 'ScheduleController@store');
                Route::get('/', 'ScheduleController@index');

                Route::group(
                    ['prefix' => '{scheduleId}'],
                    function () {
                        Route::get('/', 'ScheduleController@show');
                        Route::put('/', 'ScheduleController@update');
                        Route::delete('/', 'ScheduleController@destroy');

                        Route::group(
                            ['prefix' => 'schedule-items'],
                            function () {
                                Route::post('/', 'ScheduleItemController@store');
                                Route::get('/', 'ScheduleItemController@index');
                            }
                        );

                        Route::group(
                            ['prefix' => 'schedule-employees'],
                            function () {
                                Route::post('/', 'ScheduleEmployeeController@store');
                                Route::get('/', 'ScheduleEmployeeController@index');
                            }
                        );
                    }
                );
            }
        );
        // Route::resource('schedules', 'ScheduleController');
        // Route::resource('schedule-items', 'ScheduleItemController');
        // Route::resource('schedule-employees', 'ScheduleEmployeeController');
        // Route::resource('schedule-employee-items', 'ScheduleEmployeeItemController');
    }
);

Route::resource('employees', 'EmployeeController');
Route::get('/employees/email-validation/{data}', 'EmployeeController@validationemail');
Route::get('/employees/employeeno-validation/{data}/{email}', 'EmployeeController@validationemployeeno');
Route::post('employees/import', 'ImportController');
Route::resource('/employee-salary', 'EmployeeSalaryController');
Route::post('/employee-salary/{salary_id}/allowance', 'EmployeeAllowanceController@store');
Route::get('/employee-salary/{salary_id}/allowance', 'EmployeeAllowanceController@index');
Route::get('/employee-salary/{salary_id}/allowance/{allowance_id}', 'EmployeeAllowanceController@show');
Route::put('/employee-salary/{salary_id}/allowance/{allowance_id}', 'EmployeeAllowanceController@update');
Route::post('operation_materials/import', 'ImportController');
Route::post('premise_equipment/import', 'ImportController');
Route::resource('system', 'SystemController');
Route::resource('users', 'UserController');
Route::resource('designation', 'DesignationController');
Route::get('organization', 'DepartmentController@organization');
Route::resource('department', 'DepartmentController');
Route::resource('reimbursement', 'ReimbursementController');
Route::put('adjustment/approve/{id}', 'AdjustmentController@ApproveAdju');
Route::resource('adjustment', 'AdjustmentController');
Route::resource('interview', 'InterviewController');
Route::resource('interview-type', 'InterviewTypeController');
Route::put('reimbursement/approve/{id}', 'ReimbursementController@ApproveReim');
Route::resource('modules', 'ModuleController');
Route::resource('roles', 'RoleController');
Route::get('/module/get_modules/{system_id}', 'ModuleController@getModules');
Route::get('/module/get_modules/permission/{system_id}', 'ModuleController@getModulesPermission');
Route::get('/system/permissions/{id}', 'PermissionController@getPermissionByApplication');
Route::resource('permissions', 'PermissionController');
Route::get('permissions/edit', 'PermissionController@edit');
Route::resource('employee-overtime', 'EmployeeOvertimeController');
Route::resource('job-openings', 'JobOpeningController');
Route::resource('employee_leave', 'EmployeeLeaveController');
Route::resource('leave_type', 'LeaveTypeController');
Route::resource('employee-exit', 'EmployeeExitController');
Route::resource('onboarding', 'OnboardingController');
Route::resource('onboarding_requirement', 'OnboardingRequirementController');
Route::resource('employee_timesheet', 'EmployeeTimesheetController');
Route::resource('restrictionmodule', 'RestrictionModuleController');
Route::resource('restriction', 'RestrictionController');
Route::resource('candidate', 'CandidateController');
Route::get('/systems/all', 'SystemController@getAllSystems');
Route::get('/restrictions/{name}', 'RestrictionController@getRestrictions');
Route::resource('holiday', 'HolidayController');
Route::resource('official_business', 'OBController');
Route::post('/inventories/validation', 'InventoriesController@validationInventory');
Route::post('/inventories/save-image', 'InventoriesController@saveImage');
Route::resource('inventories', 'InventoriesController');
Route::get('inventory/all', 'InventoriesController@catalog');
Route::post('inventory/adjust_quantity', 'InventoriesController@adjustQuantity');
Route::resource('customer-premise-equipment', 'CPEController');
Route::post('/customer-premise-equipment/save-image', 'CPEController@saveImage');
Route::resource('material-issuances', 'MaterialIssuancesController');
Route::get('material-issuances/return-use/{id}', 'MaterialIssuancesController@returnUseMaterial');
Route::resource('issuance-template', 'IssuanceTemplateController');
Route::post('issuance-template/return-used-materials', 'IssuanceTemplateController@returnusedmaterials');
Route::resource('team', 'TeamController');
Route::resource('assets', 'OfficeSuppliesAndEquipmentController');
Route::get('asset-inventory', 'OfficeSuppliesAndEquipmentController@getAssetInventories');
Route::get('assets/{id}/employee', 'OfficeSuppliesAndEquipmentController@getEmployeeAssignedAssets');
Route::get('assets/{id}/inventory', 'OfficeSuppliesAndEquipmentController@getAllAssets');
Route::post('assets/saveAllAssets', 'OfficeSuppliesAndEquipmentController@updateAssets');
Route::post('/assets/save-image', 'InventoriesController@saveImage');
Route::post('consumables', 'ConsumablesController@store');
Route::get('assets/consumables/{id}', 'OfficeSuppliesAndEquipmentController@showConsumables');
Route::post('/assets/validation', 'OfficeSuppliesAndEquipmentController@validationInventory');
Route::resource('return_slip', 'ReturnSlipController');
Route::get('return_slip/getasset_tag/{id}', 'ReturnSlipController@findAssetTag');
Route::put('return_slip/{id}/updateall', 'ReturnSlipController@updateAllColumns');
Route::get('elements/list-elements', 'ElementsController@listAddElements');
Route::get('elements/create-elements', 'ElementsController@createAddElements');
Route::post('elements/store-elements', 'ElementsController@storeAddElements');
Route::get('elements/view-elements/{id}', 'ElementsController@showAddElements');
Route::get('elements/list-elements-type', 'ElementsController@listElementsType');
Route::post('elements/store-elements-type', 'ElementsController@storeElementsType');
Route::resource('elements', 'ElementsController');
Route::resource('purchase_requests', 'PurchaseRequestController');
Route::resource('accountability_requisition', 'AccountabilityRequisitionController');
Route::put('accountability_requisition/{id}/updateall', 'AccountabilityRequisitionController@updateAllColumns');
Route::resource('accountability_issuance', 'AccountabilityIssuanceController');
Route::put('accountability_issuance/{id}/updateall', 'AccountabilityIssuanceController@updateAllColumns');
Route::resource('attendance', 'AttendanceController');
Route::get('attendance/{id}/get_attendances', 'AttendanceController@getAllAttendancePerDate');
Route::resource('approval_process', 'ApprovalProcessController');
route::resource('approvals', 'ApprovalController');
route::resource('audit_trails', 'AuditTrailController');
route::get('approve/all', 'ApprovalController@approveAllEmployees');
Route::post('attendance/breaks', 'AttendanceController@breakinsert');
Route::resource('transmittal-slip', 'TransmittalSlipController');
Route::resource('incident-reports', 'IncidentReportsController');
Route::get('transmittal-slip/getasset_tag/{id}', 'TransmittalSlipController@findAssetTag');
Route::get('material-issuances/getasset_tag/{id}', 'MaterialIssuancesController@findAssetTag');
Route::resource('service-orders', 'ServiceOrdersController');
Route::post('service-orders/service-charges', 'ServiceOrdersController@storeServiceCharges');
Route::get('service-orders/team-member/{id}', 'ServiceOrdersController@findTeamMembers');
Route::post('service-orders/service-payments/{id}', 'ServiceOrdersController@storeServiceOrderPayment');
Route::post('service-orders/subscribers', 'ServiceOrdersController@storeSubscribers');
Route::post('service-orders/service-info', 'ServiceOrdersController@insertServiceinfo');
Route::get('service-orders/find-serviceinfo/{id}', 'ServiceOrdersController@findServiceinfo');
Route::post('service-orders/material-uses', 'ServiceOrdersController@storeMaterialUses');
Route::get('material-issuances/get_onhand/{team}/template/{template}', 'MaterialIssuancesController@getOnhand');
Route::get('timelog/verify/{employee_no}', 'TimelogController@verifyEmployee');
Route::post('timelog/store', 'TimelogController@store');
Route::post('timelog/time-out', 'TimelogController@timeOut');
Route::post('timelog/breaks', 'TimelogController@addBreakAttendance');
Route::post('timelog/breaks/time-in', 'TimelogController@timeinBreakAttendance');
Route::post('timelog/qr/time-in/{employee_no}', 'TimelogController@qrTimeIn');
Route::post('timelog/qr/time-out/{employee_no}', 'TimelogController@qrTimeOut');
Route::post('incident-reports/incident-action', 'IncidentReportsController@storeAction');
Route::post('incident-reports/incident-statements', 'IncidentReportsController@storeStatements');
Route::post('incident-reports/incident-deducted/{id}', 'IncidentReportsController@storeDeducted');
Route::get('incident-reports/incident-statements/{id}', 'IncidentReportsController@getStatements');
Route::get('incident-reports/incident-action/{id}', 'IncidentReportsController@getAction');
Route::get('incident-reports/incident-deducted/{id}', 'IncidentReportsController@getDedcuted');
Route::get('ot-type/{type}', 'OvertimeTypeController@otPerType');
Route::get('ot-type', 'OvertimeTypeController@index');
route::resource('daily_time_record', 'DailyTimeRecordController');
Route::put('daily_time_record/massverify/{id}', 'DailyTimeRecordController@massverify');
route::post('user/change-password', 'UserController@changePassword');

route::post('daily_time_record/verify', 'DailyTimeRecordController@verifyDate');
route::resource('hr_dashboard', 'HRDashboardController');
route::get('departments/{id}/approvals', 'DepartmentController@getApprovals');
route::resource('biometrics', 'BiometricsController');
route::post('biometrics/attendance', 'BiometricsController@getAttendance');
route::post('biometrics/attendance/import', 'BiometricsController@importAttendance');

// Mass Deleting
Route::delete('department/massdelete/{id}', 'DepartmentController@massdelete');
Route::delete('employees/massdelete/{id}', 'EmployeeController@massdelete');
Route::delete('designation/massdelete/{id}', 'DesignationController@massdelete');
Route::delete('employee-exit/massdelete/{id}', 'EmployeeExitController@massdelete');
Route::delete('employee_leave/massdelete/{id}', 'EmployeeLeaveController@massdelete');
Route::delete('employee-overtime/massdelete/{id}', 'EmployeeOvertimeController@massdelete');
Route::delete('employee_timesheet/massdelete/{id}', 'EmployeeTimesheetController@massdelete');
Route::delete('schedules/massdelete/{id}', 'ScheduleController@massdelete');
Route::delete('adjustment/massdelete/{id}', 'AdjustmentController@massdelete');
Route::delete('reimbursement/massdelete/{id}', 'ReimbursementController@massdelete');
Route::delete('job-openings/massdelete/{id}', 'JobOpeningController@massdelete');
Route::delete('candidate/massdelete/{id}', 'CandidateController@massdelete');
Route::delete('interview/massdelete/{id}', 'InterviewController@massdelete');
Route::delete('onboarding/massdelete/{id}', 'OnboardingController@massdelete');
Route::delete('leave_type/massdelete/{id}', 'LeaveTypeController@massdelete');
Route::delete('user/massdelete/{id}', 'UserController@massdelete');
Route::delete('official_business/massdelete/{id}', 'OBController@massdelete');
Route::delete('file_manager/delete/{filemanager}', 'FileManagerController@deletefile')->name('file.delete');
// route::delete('file_manager/delete/{filemanager}','filemanagercontroller@del');
Route::get('file_manager/download/{id}', 'FileManagerController@downloadFile')->name('file.download');
Route::get('file_manager/get_attachments/{id}', 'FileManagerController@getAttachments')->name('file.attachments');
Route::get('users/first_login/{token}', 'UserController@firstResetPassword');
Route::post('user-verify', 'UserController@verifyUser');
Route::get('notification/all', 'NotificationController@getAllNotification');
Route::get('notification/unread-count', 'NotificationController@getUnreadNotificationCount');
Route::put('notification/mark-read', 'NotificationController@readAllNotification');
Route::put('notification/{id}', 'NotificationController@markAsViewNotification');
Route::resource('receivings', 'ReceivingController');

Route::get('employee/shifts/{id}', 'ScheduleController@getShiftSchedulebyEmployee');
Route::get('employee/overtime/{id}', 'EmployeeOvertimeController@getTotalOvertime');
Route::get('employee/search', 'EmployeeController@searchEmployee');
Route::get('user/search', 'UserController@searchUser');
Route::get('employee/qr-code/{id}', 'EmployeeController@generateQR');

require 'payroll.php'; // payroll routes
require 'hris.php'; // payroll routes

/**
 * Contribution Templates Routes
 * SSS, HDMF, PHIC, BIR
 */

Route::prefix('contributions/templates')->group(function () {
    Route::prefix('SSS')->group(function () {
        Route::get('/', 'SSSController@index');
        Route::get('/{id}', 'SSSController@show');
        Route::post('/', 'SSSController@store');
        Route::put('{id}/update', 'SSSController@update');
        Route::delete('/{id}', 'SSSController@destroy');
        Route::delete('/', 'SSSController@massdelete');
    });
    Route::prefix('SSSContribution')->group(function () {
        Route::get('/', 'SSSController@indexContribution');
        Route::get('/{id}', 'SSSController@showContributionTemplate');
        Route::get('/{id}/template', 'SSSController@showContributionTemplatebyTemplateID');
        Route::post('/', 'SSSController@storeContribution');
        Route::put('{id}/update', 'SSSController@updateContribution');
        Route::delete('/{id}', 'SSSController@destroyContribution');
        Route::delete('/', 'SSSController@massdelete');
    });

    Route::prefix('PHIC')->group(function () {
        Route::get('/', 'PHICController@index');
        Route::get('/{id}', 'PHICController@show');
        Route::post('/', 'PHICController@store');
        Route::put('{id}/update', 'PHICController@update');
        Route::delete('/{id}', 'PHICController@destroy');
    });

    Route::prefix('HDMF')->group(function () {
        Route::get('/', 'HDMIFController@index');
        Route::get('/{id}', 'HDMIFController@show');
        Route::post('/', 'HDMIFController@store');
        Route::put('{id}/update', 'HDMIFController@update');
        Route::delete('/{id}', 'HDMIFController@destroy');
    });

    Route::prefix('BIR')->group(function () {
        Route::prefix('Categories')->group(function () {
            Route::get('/', 'BIRController@showCategories');
            Route::get('/{id}', 'BIRController@showCategory');
            Route::post('/', 'BIRController@storeCategory');
            Route::put('{id}/update', 'BIRController@updateCategory');
            Route::delete('/{id}', 'BIRController@deleteCategory');
        });

        Route::prefix('Periods')->group(function () {
            Route::get('/', 'BIRController@showPeriods');
            Route::get('/{id}', 'BIRController@showPeriod');
            Route::post('/', 'BIRController@storePeriod');
            Route::put('{id}/update', 'BIRController@updatePeriod');
            Route::delete('/{id}', 'BIRController@deletePeriod');
        });

        Route::prefix('CategoryDeductions')->group(function () {
            Route::get('/', 'BIRController@showCategoryDeductions');
            Route::get('/{id}', 'BIRController@showCategoryDeduction');
            Route::post('/', 'BIRController@storeCategoryDeduction');
            Route::put('{id}/update', 'BIRController@updateCategoryDeduction');
            Route::delete('/{id}', 'BIRController@deleteCategoryDeduction');
        });

        Route::prefix('Exemptions')->group(function () {
            Route::get('/', 'BIRController@indexBirExemption');
            Route::get('/{id}', 'BIRController@showBirExemption');
            Route::post('/', 'BIRController@storeBIRExemption');
            Route::put('{id}/update', 'BIRController@updateBIRExemption');
            Route::delete('/{id}', 'BIRController@deleteBIRExemption');
        });

        Route::prefix('Salaries')->group(function () {
            Route::get('/', 'BIRController@indexBIRSalary');
            Route::get('/{id}', 'BIRController@showBIRSalary');
            Route::post('/', 'BIRController@storeBIRSalary');
            Route::put('{id}/update', 'BIRController@updateBIRSalary');
            Route::delete('/{id}', 'BIRController@deleteBIRSalary');
        });

        Route::prefix('TaxCategories')->group(function () {
            Route::get('/', 'BIRController@indexTaxCategories');
            Route::get('/{id}', 'BIRController@showTaxCategories');
            Route::post('/', 'BIRController@storeTaxCategories');
            Route::put('{id}/update', 'BIRController@updateTaxCategories');
            Route::delete('/{id}', 'BIRController@deleteTaxCategories');
        });

        Route::prefix('TaxCriterias')->group(function () {
            Route::get('/', 'BIRController@indexTaxCriterias');
            Route::get('/{id}', 'BIRController@showTaxCriterias');
            Route::post('/', 'BIRController@storeTaxCriterias');
            Route::put('{id}/update', 'BIRController@updateTaxCriterias');
            Route::delete('/{id}', 'BIRController@deleteTaxCriterias');
        });
    });
});

/**
 * End Contribution Templates Route
 */


route::resource('category', 'CategoryController');
Route::resource('deduction', 'DeductionController');
Route::get('thirteenthmonth/history', 'ThirteenthmonthController@history');
Route::resource('thirteenthmonth', 'ThirteenthmonthController');
Route::get('thirteenthmonth/getDetails/{employee_id}', 'ThirteenthmonthController@getDetails');
Route::get('reports', 'ReportsController@index');
Route::get('employee-type', 'EmployeeController@getEmployeeType');
Route::get('employee-years-join', 'EmployeeController@getYearJoined');
Route::get('reports/export', 'ReportsController@export');
Route::get('attendance-years', 'AttendanceController@getAttendanceYear');

// Route::get('/signupmail',function(){
//     Mail::to('admin.mmri@gmail.com')->send(new SignUp('hasjfka'));
//     return new SignUp('hasjfka');
// });


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

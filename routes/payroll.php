<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'payruns'
], function () {
    Route::post('/', 'PayRunController@store');
    Route::get('/history', 'PayRunController@getPayrollHistory');
    Route::get('/lates-absent/{start}/{end}', 'PayRunController@getLatesAbsence');
    Route::get('/employee-leave/{start}/{end}', 'PayRunController@getLeave');
    Route::get('/restday-holiday/{start}/{end}', 'PayRunController@getRestDay');
    Route::get('/attendance/{start}/{end}', 'PayRunController@getAttendance');
    Route::get('/timesheet/{start}/{end}', 'PayRunController@getTimesheet');
    Route::get('/ob/{start}/{end}', 'PayRunController@getOfficialBusiness');
    Route::get('/overtime/{start}/{end}', 'PayRunController@getOvertime');
    Route::get('/{start}/{end}/{pay_date}', 'PayRunController@getViewPayrollPeriod');
    Route::get('/form-payroll/{start}/{end}/{pay_date}', 'PayRunController@getFormPayroll');
    Route::get('/computation/{id}', 'PayRunController@computationSummary');
    Route::resource('', 'PayRunController');

    Route::group([
        'prefix' => '{payrunId}'
    ], function () {
        Route::get('/', 'PayRunController@show');
        Route::get('/payrun-items', 'PayRunItemController@show');
        Route::post('/payrun-items', 'PayRunItemController@store');
    });
    Route::group([
        'prefix' => '{payrunItemId}'
    ], function () {
        Route::post('/payrun-overtime', 'PayRunItemOvertimeController@store');
        Route::get('/payrun-overtime', 'PayRunItemOvertimeController@show');
    });

    Route::group([
        'prefix' => 'activity'
    ], function () {
        Route::get('/lates-absent/{start}/{end}', 'PayRunController@activityLateAbsence');
        Route::get('/employee-leave/{start}/{end}', 'PayRunController@activityLeave');
        Route::get('/restday-holiday/{start}/{end}', 'PayRunController@activityRestdayHoliday');
        Route::get('/attendance/{start}/{end}', 'PayRunController@activityAttendance');
        //Route::get('/timesheet/{start}/{end}', 'PayRunController@activityTimesheet');
        Route::get('/overtime/{start}/{end}', 'PayRunController@activityOvertime');
        Route::get('/ob/{start}/{end}', 'PayRunController@activityOfficialBusiness');
    });
});

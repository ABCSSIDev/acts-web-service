#Composer install
composer install

#Run migration
php artisan migrate

#Run seeder
# php artisan db:seed

#passport install
php artisan passport:install

#Run pm2 queue workders
# /usr/bin/pm2 start queue-workers.yml

#Run php-fpm
php-fpm -d variables_order="EGPCS" && exec nginx -g "daemon off;"

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PHICDetails extends Model
{
    protected $table = 'hr_employee_phic_details';
}

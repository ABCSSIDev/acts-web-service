<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ElementsType extends Model
{
    protected $table = 'kamalig_element_types';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    // use SoftDeletes;
}

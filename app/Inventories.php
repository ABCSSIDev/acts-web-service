<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventories extends Model
{
    use SoftDeletes;

    protected $table = 'kamalig_inventories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'inventory_description_id',
        'unit_id', 
        'category_id', 
        'image', 
        'quantity', 
        'type',
        'unit_price',
        'min_stock'
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['getInventoriesNames', 'getUnit', 'getCategories'];

    public function getInventoriesNames(){
        return $this->hasOne('App\InventoriesNames','id', 'inventory_description_id');
    }
    public function getCategories(){
        return $this->hasOne('App\Categories','id', 'category_id');
    }
    public function getUnit(){
        return $this->hasOne('App\Unit','id', 'unit_id');
    }
    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }

    public function issued()
    {
        return $this->hasManyThrough(
            IssuanceItems::class,
            OfficeSuppliesAndEquipment::class,
            'inventory_id',
            'asset_id',
            'id',
            'id'
        );
    }
}

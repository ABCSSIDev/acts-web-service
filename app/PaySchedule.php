<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaySchedule extends Model
{
    protected $table = "hr_pay_schedules";
    protected $primaryKey = 'id';

    public function getPayScheduleBreak()
    {
        return $this->hasMany(PayScheduleCutOff::class, 'pay_schedule_id', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayRunItem extends Model
{
    use SoftDeletes;
    protected $table = "hr_pay_run_items";
    protected $primaryKey = 'id';
    protected $fillable = [
        'employee_id',
        'pay_run_id',
        'total_late_absent_hours',
        'billable_leave_hours',
        'non_billable_leave_hours',
        'total_billable_hours',
        'total_non_billable_hours'
    ];

    /**
     * employee relationship
     *
     * @return void
     */
    public function getEmployee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    /**
     * payroll overtime relationship
     *
     * @return HasMany
     */
    public function payrollOt(): HasMany
    {
        return $this->hasMany(PayRunItemOvertime::class, 'pay_run_item_id');
    }

    public function payRun(): BelongsTo
    {
        return $this->belongsTo(PayRun::class, 'pay_run_id');
    }
}

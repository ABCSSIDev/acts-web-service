<?php

namespace App\Exports;

use App\EmployeeOvertime;
use App\Http\Resources\EmployeeOvertime as ResourcesEmployeeOvertime;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class EmployeeOvertimesExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping
{
    public function __construct($employee_id, $overtime_type, $year, $month, $status)
    {
        $this->employee_id = $employee_id;
        $this->overtime_type = $overtime_type;
        $this->year = $year;
        $this->month = $month;
        $this->status = $status;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $overtimes = EmployeeOvertime::where('deleted_at', null);

        if ($this->employee_id != null) {
            $overtimes->where('employee_id', $this->employee_id);
        }

        if ($this->overtime_type != null) {
            $overtimes->where('ot_id', $this->overtime_type);
        }

        if ($this->year != null) {
            $overtimes->whereYear('overtime_date', $this->year);
        }

        if ($this->month != null) {
            $overtimes->whereMonth('overtime_date', $this->month);
        }

        $overtimes = $overtimes->get();

        return ResourcesEmployeeOvertime::collection($overtimes);
    }

    public function headings(): array
    {
        return [
            'ID',
            'Employee Name',
            'Overtime Type',
            'Overtime Date',
            'Purpose',
            'Date Filed',
            'Start Time',
            'End Time',
            'Rendered Hours',
            'Status'
        ];
    }

    public function map($overtime): array
    {
        $full_name = str_replace('&ntilde;', 'ñ', $overtime->getEmployee->fullNameLastNameFirst());
        $full_name = html_entity_decode(htmlentities($full_name));

        if ($this->status != null && model_approval_status(check_approver($overtime)) == $this->status) {
            return [
                $overtime->id,
                $full_name,
                $overtime->getOtType->name,
                Carbon::createFromDate($overtime->overtime_date)->format('m/d/Y'),
                $overtime->purpose,
                Carbon::createFromDate($overtime->file_date)->format('m/d/Y'),
                date('h:i A', strtotime($overtime->start_time)),
                date('h:i A', strtotime($overtime->end_time)),
                date('h.i ', strtotime($overtime->rendered_hours)),
                model_approval_status(check_approver($overtime))
            ];
        }

        if ($this->status != null) {
            return [];
        } else {
            return [
                $overtime->id,
                $full_name,
                $overtime->getOtType->name,
                Carbon::createFromDate($overtime->overtime_date)->format('m/d/Y'),
                $overtime->purpose,
                Carbon::createFromDate($overtime->file_date)->format('m/d/Y'),
                date('h:i A', strtotime($overtime->start_time)),
                date('h:i A', strtotime($overtime->end_time)),
                date('h.i ', strtotime($overtime->rendered_hours)),
                model_approval_status(check_approver($overtime))
            ];
        }
    }
}

<?php

namespace App\Exports;

use App\EmployeeLeave;
use App\Http\Resources\EmployeeLeave as ResourcesEmployeeLeave;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class EmployeeLeavesExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping
{
    public function __construct($employee_id, $leave_type, $year, $month, $status)
    {
        $this->employee_id = $employee_id;
        $this->leave_type = $leave_type;
        $this->year = $year;
        $this->month = $month;
        $this->status = $status;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $leaves = EmployeeLeave::where('deleted_at', null);

        if ($this->employee_id != null) {
            $leaves->where('employee_id', $this->employee_id);
        }

        if ($this->leave_type != null) {
            $leaves->where('leave_id', $this->leave_type);
        }

        if ($this->year != null) {
            $leaves->whereYear('start_date', $this->year);
        }

        if ($this->month != null) {
            $leaves->whereMonth('start_date', $this->month);
        }

        $leaves = $leaves->get();

        return ResourcesEmployeeLeave::collection($leaves);
    }

    public function headings(): array
    {
        return [
            'ID',
            'Employee Name',
            'Leave Type',
            'Reason',
            'Start Date',
            'End Date',
            'Status'
        ];
    }

    public function map($leave): array
    {
        $full_name = str_replace('&ntilde;', 'ñ', $leave->getEmployee->fullNameLastNameFirst());
        $full_name = html_entity_decode(htmlentities($full_name));

        if ($this->status != null && model_approval_status(check_approver($leave)) == $this->status) {
            return [
                $leave->id,
                $full_name,
                $leave->getLeaveType->name,
                $leave->reason,
                Carbon::createFromDate($leave->start_date)->format('m/d/Y'),
                Carbon::createFromDate($leave->end_date)->format('m/d/Y'),
                model_approval_status(check_approver($leave))
            ];
        }

        if ($this->status != null) {
            return [];
        } else {
            return [
                $leave->id,
                $full_name,
                $leave->getLeaveType->name,
                $leave->reason,
                Carbon::createFromDate($leave->start_date)->format('m/d/Y'),
                Carbon::createFromDate($leave->end_date)->format('m/d/Y'),
                model_approval_status(check_approver($leave))
            ];
        }
    }
}

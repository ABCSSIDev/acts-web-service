<?php

namespace App\Exports;

use App\Assets;
use App\OfficeSuppliesAndEquipment;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AssetsExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping
{
    public function __construct($inventory_id, $status)
    {
        $this->inventory_id = $inventory_id;
        $this->status = $status;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $assets = OfficeSuppliesAndEquipment::where('deleted_at', null);

        if ($this->inventory_id != null) {
            $assets->where('inventory_id', $this->inventory_id);
        }

        if ($this->status != null) {
            $assets->where('asset_status', $this->status);
        }

        $assets = $assets->get();

        return $assets;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Asset Tag',
            'Serial Number',
            'Date Added',
            'Assigned To',
            'Status'
        ];
    }

    public function map($assets): array
    {
        $status = ['', 'Used', 'New', 'Damaged'];

        return [
            $assets->getInventories->getInventoriesNames->description,
            $assets->asset_tag,
            $assets->serial_model_no,
            $assets->created_at,
            $assets->assigned_to,
            $status[$assets->asset_status]
        ];
    }
}

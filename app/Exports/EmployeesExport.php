<?php

namespace App\Exports;

use App\Employee;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class EmployeesExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping
{
    public function __construct($department, $type, $joined, $designation)
    {
        $this->department = $department;
        $this->type = $type;
        $this->joined = $joined;
        $this->designation = $designation;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Employee::department($this->department)
                    ->type($this->type)
                    ->joined($this->joined)
                    ->designation($this->designation)
                ->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Employee Number',
            'Name',
            'Department',
            'Designation',
            'Date Hired',
            'Status'
        ];
    }

    public function map($employee): array
    {
        $first_name = str_replace('&ntilde;', 'ñ', $employee->first_name);
        $first_name = html_entity_decode(htmlentities($first_name));

        $last_name = str_replace('&ntilde;', 'ñ', $employee->last_name);
        $last_name = html_entity_decode(htmlentities($last_name));
        return [
            $employee->id,
            $employee->employee_no,
            $last_name . ' ' . $first_name,
            $employee->getJobDetails->getDesignation->getDepartment->name,
            $employee->getJobDetails->getDesignation->name,
            Carbon::createFromDate($employee->getJobDetails->joined_date)->format('m/d/Y'),
            model_approval_status(check_approver($employee))
        ];
    }
}

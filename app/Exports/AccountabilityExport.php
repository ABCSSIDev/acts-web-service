<?php

namespace App\Exports;

use App\AccountabilityIssuance;
use App\Assets;
use App\OfficeSuppliesAndEquipment;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AccountabilityExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping
{
    public function __construct($inventory_id, $department_id, $employee_id)
    {
        $this->inventory_id = $inventory_id;
        $this->department_id = $department_id;
        $this->employee_id = $employee_id;
 
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $accountabilities = AccountabilityIssuance::where('deleted_at', null);

        if ($this->employee_id != null) {
            $accountabilities->where('employee_id', $this->employee_id);
        }

        if ($this->department_id != null) {
            $department_id = $this->department_id;
            $accountabilities->whereHas('employee', function ($q) use ($department_id) {
                $q->whereHas('getJobDetails', function ($q) use ($department_id) {
                    $q->whereHas('getDesignation', function ($q) use ($department_id) {
                        $q->whereHas('getDepartment', function ($q) use ($department_id) {
                            $q->where('department_id', $department_id);
                        });
                    });
                });
            });
        }

        if ($this->inventory_id != null) {
            $inventory_id = $this->inventory_id;
            $accountabilities->whereHas('issuanceItems', function ($q) use ($inventory_id) {
                $q->whereHas('asset', function ($q) use ($inventory_id) {
                    $q->where('inventory_id', $inventory_id);
                });
            });
        }

        $accountabilities = $accountabilities->get();

        $accountability_array = [];
        foreach ($accountabilities as $accountability) {
            foreach ($accountability->issuanceItems as $item) {
                $data = (object) [
                    'employee_name' => $accountability->employee->first_name . " " . $accountability->employee->last_name,
                    'inventory_name' => $item->asset->inventory->getInventoriesNames->description,
                    'asset_tag' => $item->asset->asset_tag,
                    'serial_number' => $item->asset->serial_model_no,
                    'date_added' => date('F d, Y', strtotime($accountability->date)),
                    'status' => $item->asset->asset_status
                ];
                array_push($accountability_array, $data);
            }
        }
        $accountability_collection = collect($accountability_array);
        return $accountability_collection;
    }

    public function headings(): array
    {
        return [
            'Employee Name',
            'Inventory Name',
            'Asset Tag',
            'Serial Number',
            'Date Issued',
            'Status'
        ];
    }

    public function map($accountability): array
    {
        $status = ['', 'Used', 'New', 'Damaged'];

        return [
            $accountability->employee_name,
            $accountability->inventory_name,
            $accountability->asset_tag,
            $accountability->serial_number,
            $accountability->date_added,
            $status[$accountability->status]
        ];
    }
}

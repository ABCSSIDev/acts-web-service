<?php

namespace App\Exports;

use App\EmployeeSalary;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class EmployeeSalariesExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return EmployeeSalary::all();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Name',
            'Rate',
            'Date Effective',
            'Monthly Salary',
            'Daily Salary',
            'Date Created'
        ];
    }

    public function map($salaries): array
    {
        $first_name = str_replace('&ntilde;', 'ñ', $salaries->employee->first_name);
        $first_name = html_entity_decode(htmlentities($first_name));

        $last_name = str_replace('&ntilde;', 'ñ', $salaries->employee->last_name);
        $last_name = html_entity_decode(htmlentities($last_name));
        return [
            $salaries->id,
            $last_name . ' ' . $first_name,
            ($salaries->type == 0 ? 'Fixed Rate' : 'Daily Rate'),
            Carbon::createFromDate($salaries->effective_date)->format('m/d/Y'),
            $salaries->monthly_salary,
            $salaries->rate_per_day,
            Carbon::createFromDate($salaries->created_at->toDateTimeString())->format('m/d/Y'),
        ];
    }
}

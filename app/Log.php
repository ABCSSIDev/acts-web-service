<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = "logs";
    protected $primaryKey = 'id';
    protected $fillable = [
        'reference_id', 
        'reference_type', 
        'reference_relate_id', 
        'reference_relate_type', 
        'user_id',
        'description'
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['user'];

    public function reference()
    {
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo(User::class, "user_id", "id");
    }
}

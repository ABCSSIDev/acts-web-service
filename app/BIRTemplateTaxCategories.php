<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BIRTemplateTaxCategories extends Model
{
    //
    use SoftDeletes;

    protected $table = 'hr_bir_template_tax_categories';
    protected $dates = ['created_at','deleted_at','updated_at'];
    protected $fillable = ['title'];

    public function criterias()
    {
        return $this->hasMany(BIRTemplateTaxCriterias::class, 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assets extends Model
{
    use SoftDeletes;
    protected $table = "kamalig_assets";
    protected $primaryKey = 'id';
    protected $fillable = ['inventory_id', 'inventory_status_id','image', 'assigned_to', 'asset_tag', 'serial_model_no', 'asset_status'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['inventory', 'inventoryStatus', 'assignedTo' ];

    public function inventory(){
        return $this->hasOne(Inventories::class, 'id', 'inventory_id');
    }

    public function inventoryStatus(){
        return $this->hasOne(InventoryStatus::class, 'id', 'inventory_status_id');
    }

    public function assignedTo(){
        return $this->hasOne(Employee::class, 'id', 'assigned_to');
    }
}

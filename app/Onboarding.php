<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Onboarding extends Model
{
    protected $table = "hr_onboardings";
    protected $primaryKey = 'id';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getCandidate(){
        return $this->hasOne('App\Candidate', 'id', 'candidate_id');
    }

    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }  
}
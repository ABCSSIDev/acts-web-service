<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class UserAccess extends Model
{
    protected $with = ['getUser'];

    /**
     * role relationship
     *
     * @return BelongsTo
     */
    public function getRole(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    /**
     * permission relationship
     *
     * @return BelongsTo
     */
    public function getPermission(): BelongsTo
    {
        return $this->belongsTo(Permission::class, 'permission_id');
    }

    /**
     * permission module relationship
     *
     * @return HasMany
     */
    public function getPermissionModules(): HasMany
    {
        return $this->hasMany(PermissionModule::class, 'permission_id', 'permission_id');
    }

    /**
     * user relationship
     *
     * @return BelongsTo
     */
    public function getUser(): BelongsTo
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function logs()
    {
        return $this->morphMany(Log::class, "reference");
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receiving extends Model
{
    use SoftDeletes;

    protected $table = 'kamalig_receivings';
    protected $primaryKey = 'id';
    protected $fillable = ['received_by_id', 'received_at'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['items', 'user'];

    public function items()
    {
        return $this->hasMany(ReceivingItems::class, 'receiving_id', 'id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'received_by_id');
    }
}

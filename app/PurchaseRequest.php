<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequest extends Model
{
    protected $table = 'kamalig_purchase_requests';
    protected $with = ['department', 'prItems'];


    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function prItems()
    {
        return $this->hasMany(PurchaseRequestItems::class, 'purchase_request_id', 'id');
    }
}

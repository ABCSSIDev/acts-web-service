<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayRun extends Model
{
    use SoftDeletes;

    protected $table = "hr_pay_runs";
    protected $primaryKey = 'id';
    protected $fillable = [
        'start_period',
        'end_period',
        'pay_date'
    ];


    /**
     * logs polymorphic
     *
     * @return MorphMany
     */
    public function logs(): MorphMany
    {
        return $this->morphMany(Log::class, "reference");
    }

    /**
     * payroll item relationship
     *
     * @return HasMany
     */
    public function payrollItem(): HasMany
    {
        return $this->hasMany(PayRunItem::class, 'pay_run_id', 'id');
    }
}

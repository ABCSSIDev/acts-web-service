<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IncidentReports extends Model
{
    protected $table = 'kamalig_incident_reports';
    protected $primaryKey = 'id';
    protected $fillable = ['department_id', 'reported_by_employee','employee_deducted', 'asset_id', 'reported_by', 'incident_date', 'reported_date', 'incident_type', 'witnesses', 'incident_location', 'incident_report', 'deduction_amount', 'deduction_per_period','team_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;
    public function getTeam(){
        return $this->hasOne('App\Team','id', 'team_id');
    }
    public function getDepartment(){
        return $this->hasOne('App\Department','id', 'department_id');
    }
    public function getAssets(){
        return $this->hasOne('App\Assets','id', 'asset_id');
    }
    public function getReportedBy(){
        return $this->hasOne('App\Employee','id', 'reported_by_employee');
    }
    public function getEmployeeDeducted(){
        return $this->hasOne('App\Employee','id', 'employee_deducted');
    }
    public function getIncidentStatement(){
        return $this->hasMany('App\IncidentStatement','incident_id', 'id');
    }
    public function getIncidentAction(){
        return $this->hasMany('App\IncidentAction','incident_id', 'id');
    }
    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }
}

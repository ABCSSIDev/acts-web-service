<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BIRTemplateCategoryDeductions extends Model
{
    //
    use SoftDeletes;

    protected $table = 'hr_bir_template_category_deductions';
    protected $dates = ['created_at','deleted_at','updated_at'];
    protected $fillable = ['period_id','category_id','deduction_value','deduction_percent'];

    public function category()
    {
        return $this->belongsTo(BIRTemplateCategories::class, 'category_id');
    }

    public function period()
    {
        return $this->belongsTo(BIRTemplatePeriods::class, 'period_id');
    }
}

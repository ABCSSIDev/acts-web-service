<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Adjustment extends Model
{
    protected $table = 'hr_adjustments';
    protected $primaryKey = 'id';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;
    
    public function getEmployee(){
        return $this->hasOne('App\Employee','id', 'employee_id');
    }

    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }
}

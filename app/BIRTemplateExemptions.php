<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BIRTemplateExemptions extends Model
{
    //
    use SoftDeletes;

    protected $table = 'hr_bir_template_exemptions';
    protected $dates = ['created_at','deleted_at','updated_at'];
    protected $fillable = ['exemption_status','exemption_values','dependent'];

    public function salaries()
    {
        return $this->hasMany(BIRTemplateSalaries::class, 'id');
    }
}

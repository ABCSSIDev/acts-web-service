<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransmittalSlip extends Model
{
    protected $table = 'kamalig_transmittal_slips';
    protected $primaryKey = 'id';
    protected $fillable = ['employee_id', 'location','date'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getEmployee(){
        return $this->belongsTo(Employee::class,'employee_id');
    }
    public function getTransmittalSlipItem(){
        return $this->hasMany('App\TransmittalItemsSlip','transmittal_id', 'id');
    }
    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }
}

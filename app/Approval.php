<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    protected $table = "approvals";
    protected $primaryKey = 'id';
    protected $fillable = ['reference_id', 'reference_type', 'user_id', 'approved_status', 'remarks'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}

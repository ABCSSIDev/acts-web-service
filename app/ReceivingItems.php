<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceivingItems extends Model
{
    protected $table = 'kamalig_receiving_items';
    protected $primaryKey = 'id';
    protected $fillable = ['receiving_id', 'inventory_id', 'quantity', 'old_quantity'];
    protected $dates = ['created_at', 'updated_at'];
    protected $with = ['inventory_description'];

    public function inventory_description()
    {
        return $this->hasOne(Inventories::class, 'id', 'inventory_id');
    }
}

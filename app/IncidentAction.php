<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class IncidentAction extends Model
{
    protected $table = 'kamalig_incident_actions';
    protected $primaryKey = 'id';
    protected $fillable = ['incident_id', 'actions', 'responsibility', 'completion_date'];
    protected $dates = ['created_at', 'updated_at'];
    // use SoftDeletes;
    
    public function getIncidentReports(){
        return $this->hasOne('App\IncidentReports','id', 'incident_id');
    }
}

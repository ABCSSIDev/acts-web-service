<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Elements extends Model
{
    protected $table = 'kamalig_add_elements';
    protected $primaryKey = 'id';
    protected $fillable = ['element_type_id', 'category_id', 'inventory_id', 'caption'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    // use SoftDeletes;

    public function getElementsType(){
        return $this->hasOne('App\ElementsType','id', 'element_type_id');
    }

    public function getCategories(){
        return $this->hasOne('App\Categories','id', 'category_id');
    }
    
    public function getInventories(){
        return $this->hasOne('App\Inventories','id', 'inventory_id');
    }
}

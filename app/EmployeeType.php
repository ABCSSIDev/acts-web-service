<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeType extends Model
{
    protected $table = 'hr_employee_types';
    
    public static function getEmployeeTypeIDByName($name){
        $type = self::where('name','like',$name)->first();
        if($type){
            return $type->id;
        }
        return null;
    }
}

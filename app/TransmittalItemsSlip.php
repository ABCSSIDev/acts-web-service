<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransmittalItemsSlip extends Model
{
    protected $table = 'kamalig_transmittal_items';
    protected $primaryKey = 'id';
    protected $fillable = ['transmittal_id', 'inventory_id','asset_id', 'remarks','quantity'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getTransmittalSlip(){
        return $this->belongsTo(TransmittalSlip::class,'transmittal_id');
    }
    public function getInventories(){
        return $this->belongsTo(Inventories::class,'inventory_id');
    }
}

<?php

namespace App;

use App\Scopes\RoleScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Monolog\Handler\DeduplicationHandler;

class Employee extends Model
{
    use SoftDeletes;

    protected $table = 'hr_employees';
    protected $primaryKey = 'id';
    protected $fillable = [
        'employee_no',
        'user_id',
        'biometrics_number',
        'image',
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'birth_date',
        'permanent_address',
        'current_address',
        'marital_status'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    private static $employee_array = [];

    //Global Scope on Filtering Query to fetch only users/employees that are subordinates or peers of current user
    protected static function booted()
    {
        // if (Auth::user()->id != 1){
            // static::addGlobalScope(new RoleScope());
        //}
    }

    /**
     * leaves relationship
     *
     * @return HasMany
     */
    public function leaves(): HasMany
    {
        return $this->hasMany('App\EmployeeLeave', 'employee_id', 'id');
    }

    /**
     * job details relationship
     *
     * @return HasOne
     */
    public function getJobDetails(): HasOne
    {
        return $this->hasOne(JobDetail::class, 'employee_id');
    }

    /**
     * user relationship
     *
     * @return HasOne
     */
    public function getUser(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * work details relationship
     *
     * @return HasMany
     */
    public function getWorkDetails(): HasMany
    {
        return $this->hasMany(WorkDetails::class, 'employee_id');
    }

    /**
     * education details relationship
     *
     * @return HasMany
     */
    public function getEducationalDetails(): HasMany
    {
        return $this->hasMany(EducationalDetails::class, 'employee_id');
    }

    /**
     * contact details relationship
     *
     * @return HasOne
     */
    public function getContactDetails(): HasOne
    {
        return $this->hasOne(EmployeeContact::class, 'employee_id');
    }

    /**
     * fullname
     *
     * @return string
     */
    public function fullname(): string
    {
        return htmlentities($this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name, ENT_QUOTES, 'UTF-8');
    }

    public function fullNameWithoutMiddleName(): string
    {
        return htmlentities($this->first_name . ' ' . $this->last_name, ENT_QUOTES, 'UTF-8');
    }

    public function fullNameLastNameFirst(): string
    {
        return htmlentities($this->last_name . ', ' . $this->first_name, ENT_QUOTES, 'UTF-8');
    }

    /**
     * sss details relationship
     *
     * @return HasOne
     */
    public function getSSSDetails(): HasOne
    {
        return $this->hasOne(SSSDetails::class, 'employee_id');
    }

    /**
     * BIR details relationship
     *
     * @return HasOne
     */
    public function getBIRDetails(): HasOne
    {
        return $this->hasOne(BIRDetails::class, 'employee_id');
    }

    /**
     * PHIC details relationship
     *
     * @return HasOne
     */
    public function getPHICDetails(): HasOne
    {
        return $this->hasOne(PHICDetails::class, 'employee_id');
    }

    /**
     * HDMF details relationship
     *
     * @return void
     */
    public function getHDMFDetails(): HasOne
    {
        return $this->hasOne(HDMFDetails::class, 'employee_id');
    }

    /**
     * subordinate relationship
     *
     * @return HasMany
     */
    public function getSubordinates(): HasMany
    {
        return $this->hasMany(JobDetail::class, 'reporting_to');
    }

    /**
     * attachment relationship
     *
     * @return MorphMany
     */
    public function getAttachments(): MorphMany
    {
        return $this->morphMany(FileManager::class, 'reference');
    }

    /**
     * logs relationship
     *
     * @return MorphMany
     */
    public function getLogs(): MorphMany
    {
        return $this->morphMany(Log::class, "reference");
    }

    /**
     * employee status relationships
     *
     * @return HasMany
     */
    public function statuses(): HasMany
    {
        return $this->hasMany(EmployeeType::class, 'employee_id');
    }

    public function attendances(): HasMany
    {
        return $this->hasMany(Attendance::class, 'employee_id');
    }

    public function adjustments(): HasMany
    {
        return $this->hasMany(Adjustment::class, 'employee_id');
    }

    public function deductions(): HasMany
    {
        return $this->hasMany(Deduction::class, 'employee_id');
    }

    public function scopeRole($query, $model_name = null)
    {
        $user_id = Auth::id();
        if ($user_id != 1) {
            $user_access = UserAccess::where('user_id', $user_id)->first();
            if (self::checkModule($user_access->bypass_role_systems, $model_name)  && self::checkIfApprover($model_name)) {
                $roles = Role::where('id', $user_access->role_id)->get();
                self::loopRoles($roles, $user_access->role_id);
                return $query->whereIn('id', self::$employee_array);
            }
        }

        return $query;
    }

    public static function loopRoles($roles, $parentRoleId = null, $child = false)
    {
        foreach ($roles as $role) {
            if (( !empty($parentRoleId) && $role->is_shared != null ) || $role->id == $parentRoleId || !empty($child)) {
                if (!empty($role->getUserAccess)) {
                    foreach ($role->getUserAccess as $access) {
                        $employee = Employee::where('user_id', $access->user_id)->first();
                        if (!empty($employee)) {
                            array_push(self::$employee_array, $employee->id);
                        }
                    }
                }
                if (!empty($role->getChildren)) {
                    self::loopRoles($role->getChildren, null, true);
                }
            }
        }
    }

    public static function checkModule($system_list, $module_name)
    {
        $system_count = 0;
        if (!empty($system_list)) {
            $systems = json_decode($system_list);
            foreach ($systems as $system) {
                $modules = Module::where('system_id', '=', $system)->where('model_type', $module_name)->get();
                if ($modules->count() > 0) {
                    $system_count++;
                }
            }
        }
        return $system_count == 0;
    }

    public static function checkIfApprover($model_name)
    {
        $approval_users = [];
        $get_process = ApprovalProcess::where('reference_type', $model_name)->where('status', 1)->first();
        if (!empty($get_process)) {
            $approval_users = ApprovalUser::where('approval_process_id', $get_process->id)->where('user_id', Auth::user()->id)->get();
        }
        return count($approval_users) <= 0;
    }

    public function scopeDepartment($query, $department)
    {
        if (!empty($department)) {
            return $query->whereHas('getJobDetails', function ($q) use ($department) {
                $q->whereHas('getDesignation', function ($designation_query) use ($department) {
                    $designation_query->where('department_id', '=', $department);
                });
            });
        }
        return $query;
    }

    public function scopeType($query, $type)
    {
        if (!empty($type)) {
            return $query->whereHas('getJobDetails', function ($q) use ($type) {
                $q->whereHas('getDesignation', function ($designation_query) use ($type) {
                    $designation_query->where('employee_type_id', '=', $type);
                });
            });
        }
        return $query;
    }

    public function scopeJoined($query, $date_joined)
    {
        if (!empty($date_joined)) {
            return $query->whereHas('getJobDetails', function ($q) use ($date_joined) {
                $q->whereHas('getDesignation', function ($designation_query) use ($date_joined) {
                    $designation_query->whereYear('joined_date', $date_joined);
                });
            });
        }
        return $query;
    }

    public function scopeDesignation($query, $designation)
    {
        if (!empty($designation)) {
            return $query->whereHas('getJobDetails', function ($q) use ($designation) {
                $q->whereHas('getDesignation', function ($designation_query) use ($designation) {
                    $designation_query->where('designation_id', $designation);
                });
            });
        }
        return $query;
    }
}

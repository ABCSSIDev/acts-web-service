<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterviewType extends Model
{
    protected $table = 'hr_interview_types';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'description', 'is_enabled'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}

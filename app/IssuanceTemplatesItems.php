<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IssuanceTemplatesItems extends Model
{
    use SoftDeletes;

    protected $table = 'kamalig_issuance_template_item';
    protected $primaryKey = 'id';
    protected $fillable = ['template_id', 'inventory_id','quantity'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['getInventories'];

    public function getInventories()
    {
        return $this->belongsTo(Inventories::class, 'inventory_id');
    }
}

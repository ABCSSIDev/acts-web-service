<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaterialIssuances extends Model
{
    use SoftDeletes;

    protected $table = 'kamalig_material_issuances';
    protected $primaryKey = 'id';
    protected $fillable = ['team_id', 'tech1', 'tech2','template_id', 'date_issue'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getTeam(){
        return $this->belongsTo(Team::class,'team_id');
    }
    public function getTechnicians1(){
        return $this->belongsTo(Employee::class,'tech1');
    }
    public function getTechnicians2(){
        return $this->belongsTo(Employee::class,'tech2');
    }
    public function getIssuanceTemplates(){
        return $this->belongsTo(IssuanceTemplates::class,'template_id');
    }

    public function getMaterialIssuancesItems(){
        return $this->hasMany('App\MaterialIssuancesItems','issuance_id', 'id');
    }

    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }


}

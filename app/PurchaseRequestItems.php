<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequestItems extends Model
{
    protected $table = 'kamalig_purchase_request_items';
    protected $with = ['inventory'];

    public function inventory()
    {
        return $this->hasOne(Inventories::class, 'id', 'inventory_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestrictionModule extends Model
{
    public function getModule(){
        return $this->belongsTo(Module::class,'module_id');
    }
}

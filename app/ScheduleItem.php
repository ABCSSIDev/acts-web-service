<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleItem extends Model
{
    use SoftDeletes;

    protected $table = "hr_schedule_items";

    /**
     * @var array
     */
    protected $guarded = [];
    
    /**
     * get Schedule relationship
     *
     * @return BelongsTo
     */
    public function getSchedule(): BelongsTo
    {
        return $this->belongsTo(Schedule::class, 'schedule_id');
    }
    
    /**
     * employee relationship
     *
     * @return BelongsTo
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountabilityRequisition extends Model
{
    use SoftDeletes;
    protected $table = "kamalig_accountability_requisitions";
    protected $primaryKey = 'id';
    protected $fillable = ['employee_id', 'from_employee_id', 'type', 'date'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['employee', 'fromEmployee', 'requisitionItems', 'logs'];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function fromEmployee()
    {
        return $this->belongsTo(Employee::class, 'from_employee_id', 'id');
    }

    public function requisitionItems()
    {
        return $this->hasMany(RequisitionItems::class, 'requisition_id', 'id');
    }

    public function issuance()
    {
        return $this->hasOne(AccountabilityIssuance::class, 'requisition_id', 'id');
    }

    public function logs()
    {
        return $this->morphMany(Log::class,"reference");
    }
}

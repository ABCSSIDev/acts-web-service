<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\InterviewAttachments;

class Interview extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'interview_type_name' => $this->getInterviewType->name,
            'interview_type_id' => $this->interview_type_id,
            'interview_from' => date('F d, Y',strtotime($this->interview_from)),
            'interview_to' => date('F d, Y',strtotime($this->interview_to)),
            'edit_interviewfrom' => $this->interview_from,
            'edit_interviewto' => $this->interview_to,
            'candidate_name' => $this->getCandidate->name,
            'candidate_data' => $this->getCandidate->getJobOpening,
            'candidate_id' => $this->candidate_id,
            'department_id' => $this->getCandidate->getDesignation->getDepartment,
            'interviewer_name' => ($this->getInterviewer?$this->getInterviewer->first_name.' '.$this->getInterviewer->middle_name.' '.$this->getInterviewer->last_name:'N/A'),
            'interviewer_id' => $this->interviewer_id,
            'location' => $this->location,
            'comments' => $this->comments,
            'attachments' => InterviewAttachments::collection($this->getInterviewAttachment)
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

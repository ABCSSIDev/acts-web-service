<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeContact extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'email' => $this->email,
            'phone_number' => $this->phone_number,
            'telephone_number' => $this->telephone_number,
            'work_phone' => $this->work_phone,
            'emergency_contact' => $this->emergency_contact,
            'emergency_contact_number' => $this->emergency_contact_number,
        ];
    }
}

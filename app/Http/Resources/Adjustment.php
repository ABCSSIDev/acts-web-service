<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
class Adjustment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => date('F d, Y',strtotime($this->date)),
            'date_from' => $this->date,
            'applicable_date' => date('F d, Y',strtotime($this->applicable_date)),
            'date_to' => $this->applicable_date,
            'employee_id' => $this->getEmployee->id,
            'employee_name' => ($this->getEmployee?$this->getEmployee->first_name.' '.$this->getEmployee->middle_name.' '.$this->getEmployee->last_name:'N/A'),
            'amount' => $this->amount,
            'reason' => $this->reason,
            'status' => $this->status,            
            'approved_status' => model_approval_status(check_approver($this))
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

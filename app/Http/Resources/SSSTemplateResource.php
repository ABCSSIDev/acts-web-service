<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SSSTemplateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date_effective_from' => $this->date_effective_from,
            'date_effective_to' => $this->date_effective_to,
            'salary_from' => $this->SSSTemplateContribution->salary_from,
            'salary_to' => $this->SSSTemplateContribution->salary_to,
            'monthly_salary_credit' => $this->SSSTemplateContribution->monthly_salary_credit,
            'employer_contribution' => $this->SSSTemplateContribution->employer_contribution,
            'employee_contribution' => $this->SSSTemplateContribution->employee_contribution,
            'employee_compensation' => $this->SSSTemplateContribution->employee_compensation,
            'active' => $this->active == 1 ? 'Yes' : 'No',
        ];
    }
}

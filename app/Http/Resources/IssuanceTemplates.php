<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Log as LogResource;
use App\Http\Resources\IssuanceTemplatesItems as IssuanceTemplatesItemsResource;



class IssuanceTemplates extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'template_name' => $this->template_name, 
            'issuanceitem' => IssuanceTemplatesItemsResource::collection($this->getIssuanceTemplatesItems),
            'history_logs' => new LogResource(IssuanceTemplates::findOrFail($this->id)->getLogs()->orderBy('created_at','DESC')->first()),
            
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

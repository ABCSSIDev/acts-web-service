<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AttendanceSimple extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'time_in' => date('Y-m-d H:i A', strtotime($this->start_time)),
            'time_out' => $this->end_time != null ? date('Y-m-d H:i A', strtotime($this->end_time)) : '',
            'rendered_hours' => $this->getTotalHours($this->start_time, $this->end_time)
        ];
    }

    public function getTotalHours($start, $end)
    {
        if ($end == null) {
            return 0;
        }

        return round((strtotime($end) -
        strtotime($start)) / 3600, 2);
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Subscribers extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'service_id' => $this->getServiceOrders,
            'subscriber_name' => $this->subscriber_name,
            'email' => $this->email,
            'home_no' => $this->home_no,
            'other_no' => $this->other_no,
            'mobile_no' => $this->mobile_no,
            'gender' => $this->gender,
            'birthdate' => $this->birthdate,
            'civil_status' => $this->civil_status,
            'household_held' => $this->household_held,
            'service_address' => $this->service_address,
            'billing_address' => $this->billing_address,
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

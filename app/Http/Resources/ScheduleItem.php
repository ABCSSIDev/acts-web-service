<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ScheduleItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'schedule_id' => $this->schedule_id,
            'employee_id' => $this->employee_id,
            'time_in' => $this->time_in,
            'time_out' => $this->time_out,
            'restdays' => $this->restdays,
            'employee_name' => $this->employee->fullNameLastNameFirst(),
            'department' => $this->employee->getJobDetails->getDesignation->getDepartment->name,
            '_parse_restdays' => $this->dayOfWeek($this->restdays),
            '_parse_time_in' => date('h:i A', strtotime($this->time_in)),
            '_parse_time_out' => date('h:i A', strtotime($this->time_out)),
        ];
    }

    public function dayOfWeek($days)
    {
        $dayOfWeek = [];
        $dow = [
            0 => 'Sunday',
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday'
        ];

        $days = json_decode($days);

        foreach ($days as $day) {
            $dayOfWeek[] = $dow[$day];
        }

        return implode(',', $dayOfWeek);
    }
}

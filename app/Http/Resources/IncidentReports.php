<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\IncidentStatement as IncidentReportsResource;

class IncidentReports extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'department_data' => $this->getDepartment,
            'reported_by' => $this->reported_by,
            'asset_data' => $this->getAssets,
            'vehicle_plate_no'  => $this->vehicle_plate_no,
            'reported_by_employee' => $this->getReportedBy,
            'reported_date'  => date('F d, Y h:i A',strtotime($this->reported_date)),
            'incident_date'  => date('F d, Y h:i A',strtotime($this->incident_date)),
            'incident_type'  => ($this->incident_type == 0?'Vehicular':($this->incident_type == 1?'Equipment/Property Damage':'Equipment/Property Lost')),
            'witnesses'  => json_decode($this->witnesses),
            'incident_location'  => $this->incident_location,
            'incident_report'  => $this->incident_report,
            'team_id'  => $this->getTeam,
            'incident_actions'  => $this->getIncidentAction,
            'incident_statements'  => IncidentReportsResource::collection($this->getIncidentStatement),
            'employee_deducted'  => $this->getEmployeeDeducted,
            'deduction_amount'  => $this->deduction_amount,
            'deduction_per_period'  => $this->deduction_per_period

        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

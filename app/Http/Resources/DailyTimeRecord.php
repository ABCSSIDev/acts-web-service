<?php

namespace App\Http\Resources;

use App\Attendance;
use App\DailyTimeRecord as DailyTimeRecordModel;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class DailyTimeRecord extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'schedule_employee_id' => $this->schedule_employee_id,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee->fullNameLastNameFirst(),
            'resched_employee_id' => $this->resched_employee_id,
            'parse_date' => date('d m, Y M (D)', strtotime($this->scheduleEmployee->date)),
            'verified_attendance' => $this->verified_attendance,
            '_date' => $this->scheduleEmployee->date,
            '_attendance' => $this->getAttendance($this->scheduleEmployee->date, $this->employee_id) 
        ];
    }

    /**
     * getAttendance
     *
     * @param string $date
     * @param int $employee_id
     *
     * @return void
     */
    public function getAttendance(string $date, int $employee_id)
    {
        $attendance = Attendance::where(DB::raw('date(start_time)'), $date)
            ->where('employee_id', $employee_id)
            ->get();

        return $attendance;
    }
}

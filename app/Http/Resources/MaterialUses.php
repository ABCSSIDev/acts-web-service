<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MaterialUses extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'service_id' => $this->getServiceOrders,
            'inventory_id' => $this->getInventories,
            'type' => $this->type,
            'qty_used' => $this->qty_used,
            'brandandmodel' => $this->brandandmodel,
            'serial_no' => $this->serial_no
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

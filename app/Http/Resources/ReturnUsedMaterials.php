<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Log as LogResource;



class ReturnUsedMaterials extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'issuance_detials' => $this->getIssuanceTemplatesItems, 
            'status' => $this->status, 
            'quantity' => $this->quantity, 
            'remarks' => $this->remarks, 
            'history_logs' => new LogResource(ReturnUsedMaterials::findOrFail($this->id)->getLogs()->orderBy('created_at','DESC')->first()),
            
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

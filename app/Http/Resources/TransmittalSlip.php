<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Log as LogResource;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use App\Employee;
use App\Http\Resources\TransmittalItemsSlip as TransmittalItemsSlipResource;


class TransmittalSlip extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_id' => $this->getEmployee, 
            'location' => $this->location, 
            'transmittalslipitem' => TransmittalItemsSlip::collection($this->getTransmittalSlipItem),
            'approved_by' =>  $this->approve_by(TransmittalSlip::findOrFail($this->id)->first()),   //pending, waiting order approver,approved
            'history_logs' => new LogResource(TransmittalSlip::findOrFail($this->id)->getLogs()->orderBy('created_at','ASC')->first()),
            // "history_logs" => $this->getlogs()->oldest()->first(),
            'date' => $this->date  
        ];
    }
    public static function approve_by($data){
        $check_approver = check_approver($data);
        $model_approval_status = model_approval_status($data);
        $approval_status = $check_approver['approval_status'];
        if($check_approver != null){
            foreach($approval_status as $data){
                $validation = $data['status'];
            }
            if($validation != null){
                $return = Employee::findOrFail($model_approval_status['employee_id'])->first();
                
            }else{
                $return = Null;
            }
        }else{
            $return = Null;
        }
        return $return;
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BIRTemplateCategoryDeductionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'period_id' => $this->period_id,
            'deduction_value' => $this->deduction_value,
            'deduction_percent' => $this->deduction_percent
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Designation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'department_name' => ($this->getDepartment?$this->getDepartment->name:'N/A'),
            'department_id' => ($this->getDepartment?$this->getDepartment->id:'N/A'),
            'name' => $this->name,
            'description' => $this->description,
            'status' => $this->status
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

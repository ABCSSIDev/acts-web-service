<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Log as LogResource;
use App\Http\Resources\MaterialIssuancesItems as MaterialIssuancesItemsResource;



class Technicians extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'teams_id' => $this->teams_id, 
            'employee_name' => $this->getTechnicians->first_name.' '.$this->getTechnicians->last_name, 
            'employee_id' => $this->getTechnicians->id, 
            'technician' => $this->technician
            
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use App\Module;
use App\Http\Resources\Module as ModuleResource;
use App\PermissionModule;
use App\UserAccess;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class System extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

       
        return [
            'id' => $this->id,
            'name' => $this->name,
            'icon' => $this->icon,
            'route' => $this->route,
            'value' => $this->id,
            'text' => $this->name,
            'theme' => $this->system_theme,
            'description' => $this->description,
            'modules'  => ModuleResource::collection($this->getModules)

        ];
    }

    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
}

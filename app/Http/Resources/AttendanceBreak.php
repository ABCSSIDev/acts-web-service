<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
class AttendanceBreak extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    { 
        return [
            'id' => $this->id,
            'attendance_id' => $this->attendance_id,
            'break_id' => $this->getBreak->name,
            'type' => ($this->type == 0 ?'out':'in'),
            'time' => date('h:i A',strtotime($this->time)),
        ];
    }
    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
}

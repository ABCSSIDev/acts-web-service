<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountabilityRequisition extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                => $this->id,
            "employee_id"       => $this->employee,
            "from_employee_id"  => $this->fromEmployees,
            "type"              => $this->type,
            "date"              => date('F d, Y',strtotime($this->date)),
            'time'              => date('h:i A',strtotime($this->date)),
            "item_count"        => count($this->requisitionItems),
            "created"           => $this->logs()->oldest()->first(),
            "requisition_items" => $this->requisitionItems,
            "issuance"          => $this->issuance
        ];
    }

    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Log as LogResource;

class InventoriesNames extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'mat_code' => $this->mat_code,
            'category_id' => $this->getCategories,
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ScheduleBreak as BreakResource;
use App\Department;
use App\Employee;

class Schedule extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'schedule_date_end' => $this->schedule_date_end,
            'schedule_date_start,' => $this->schedule_date_start,
            'is_activate' => $this->is_activate,
            '_parse_schedule_date_start' => date('F d, Y', strtotime($this->schedule_date_start)),
            '_parse_schedule_date_end' => date('F d, Y', strtotime($this->schedule_date_end))
        ];
        // return [
        //     'id' => $this->id,
        //     'name' => $this->name,
        //     'working_days' => json_decode($this->working_days),
        //     'working_days_full' => $this->dayOfWeek($this->working_days),
        //     'departments' => json_decode($this->departments),
        //     'departments_name' => json_decode($this->departments),
        //     'edit_departments_name' => $this->departmentname(json_decode($this->departments)),
        //     'exceptions' => json_decode($this->exceptions),
        //     'exempted_name' => $this->employee_name(json_decode($this->exceptions)),
        //     'schedule_date_start' => date('F d, Y',strtotime($this->schedule_date_start)),
        //     'schedule_date_from' => $this->schedule_date_from != null ? date('F d, Y', strtotime($this->schedule_date_from)) : 'N/A',
        //     'edit_sheduledate_start' => $this->schedule_date_start,
        //     'edit_scheduledate_from' => $this->schedule_date_from,
        //     'start_full_day' => date('h:i A',strtotime($this->start_full_day)),
        //     'end_full_day' => date('h:i A',strtotime($this->end_full_day)),
        //     'edit_startfull_day' => $this->start_full_day,
        //     'edit_endfull_day' => $this->end_full_day,
        //     'start_half_day' => date('h:i A',strtotime($this->start_half_day)),
        //     'end_half_day' => date('h:i A',strtotime($this->end_half_day)),
        //     'edit_starthalf_day' => $this->start_half_day,
        //     'edit_endhalf_day' => $this->end_half_day,
        //     'break_time' => $this->when($request->route()->uri == "api/schedules/{schedule}" || $request->route()->uri == "api/schedules/{schedule}/edit",BreakResource::collection($this->getBreaks))
        // ];
    }
    // public function departmentname($data){
    //     $departments = Department::whereIn('id', $data)->get();
    //     $datas = array();
    //     foreach($departments as $data){
    //         $datas[] = $data->name;
    //     }
    //     return $datas;
    // }
    // public function employee_name($data){
    //     $employees = Employee::whereIn('id', $data)->get();
    //     $employee_data = array();
    //     foreach($employees as $data){
    //         $employee_data[] = $data->first_name.' '.$data->last_name;
    //     }
    //     return $employee_data;
    // }
    // public static function MsgResponse($type,$description){
    //     return [ "error" => $type, "desc" => $description];
    // }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RestdayHoliday extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'ot_id' => $this->ot_id,
            'attendance_id' => $this->attendance_id,
            'holidays' => $this->holidays,
            'rate' => $this->rate,
            'restday_holiday_date' => $this->restday_holiday_date,
            'file_date' => $this->file_date,
            'start_time' => $this->start_time,
            'rendered_hours' => $this->rendered_hours,
            'purpose' => $this->purpose
        ];
    }
}

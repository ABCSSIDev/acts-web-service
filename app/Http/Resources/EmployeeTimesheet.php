<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class EmployeeTimesheet extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [ 
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->getEmployee->first_name.' '.$this->getEmployee->last_name,
            'timesheet_date' => date('F d, Y',strtotime($this->timesheet_date)),
            'edit_timesheet_date' => $this->timesheet_date,
            'description' => $this->description,
            'start_time' => date('h:i A',strtotime($this->start_time)),
            'edit_start_time' => $this->start_time,
            'rendered_hours' => date('H.i',strtotime($this->rendered_hours)),
            'is_billable' => $this->is_billable == 1 ? 'Billable' : 'Non-Billable',
            'edit_is_billable' => $this->is_billable,
            'approver' => check_approver(EmployeeTimesheet::find($this->id)), //option if user is approver and already approved
            'approved_status' =>  model_approval_status(check_approver(EmployeeTimesheet::find($this->id))),   //pending, waiting order approver,approved
        ];
    }

    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

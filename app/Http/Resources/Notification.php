<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use function App\Notification\notificationDate;

class Notification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'notifiable_type' => $this->notifiable_type,
            'notifiable_id' => $this->notifiable_id,
            'data' => $this->data,
            'read_at' => $this->read_at,
            'view_at' => $this->view_at,
            'other_info' => $this->other_info,
            'created_at' => date('Y-m-d h:i:s', strtotime($this->created_at)),
            'time_diff' => notificationDate(date('Y-m-d H:i:s',strtotime($this->created_at))),
        ];
    }
}

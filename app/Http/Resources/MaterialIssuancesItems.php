<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Inventories as InventoriesResource;
use App\ReturnUsedMaterials;


class MaterialIssuancesItems extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'issuance_id' => $this->issuance_id, 
            'inventory_id' => $this->getInventories, 
            // 'inventory_name' => $this->getInventories->getInventoriesNames->description, 
            'issued' => $this->quantity, 
            'return' => ReturnUsedMaterials::where('issuance_item_id',$this->id)->where('status',0)->first(), 
            'use' => ReturnUsedMaterials::where('issuance_item_id',$this->id)->where('status',1)->first(), 
            
            
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}
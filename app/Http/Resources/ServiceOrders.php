<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Employee;
use App\ServiceCharges;
use App\Subscribers;
use App\MaterialUses;
use App\Http\Resources\MaterialUses as MaterialUsesResource;

class ServiceOrders extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'team_id' => $this->getTeam,
            'subscribers_data' => $this->getSubscribers,
            'team_member' => $this->teamMemberData($this->team_member),
            'account_no' => $this->account_no,
            'service_no' => $this->service_no,
            'payment_mode' => $this->payment_mode,
            'services_data' => ServiceCharges::where('service_id', $this->id)->first(),
            'billing_address' => json_decode($this->getSubscribers->billing_address),
            'service_address' => json_decode($this->getSubscribers->service_address),
            'materialUses_data' => MaterialUses::where('service_id', $this->id)->first(),
            'check_or_no' => json_decode($this->check_or_no),
            'bank' => $this->bank,
            'branch' => $this->branch,
            'so_no' => $this->so_no,
            'type' => $this->type,
            'completion_date' => date('F d, Y', strtotime($this->completion_date)),
            'time_start' => date('h:i A', strtotime($this->time_start)),
            'time_end' => date('h:i A', strtotime($this->time_end)),
            'remarks' => $this->remarks,
            'with_pob' => $this->with_pob,
            'without_pob' => $this->without_pob,
            'type_pob' => $this->type_pob,
            'subscribers_list' => $this->getSubscribers,
            'serviceCharges_list' => $this->getServiceCharges,
            'materialUses_list' =>  MaterialUsesResource::collection($this->getMaterialUses),
            "created"  => $this->logs()->oldest()->first(),
        ];
    }
    public static function teamMemberData($data)
    {
        $array = array();
        foreach (json_decode($data) as $val) {
            $employee_data = Employee::where('id', $val)->first();
            $array[] = [
                'id' => $val,
                'name' => $employee_data->first_name . ' ' . $employee_data->last_name
            ];
        }
        return $array;
    }
    public static function msgResponse($type, $description)
    {
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use App\Http\Resources\Log as LogResource;
use Illuminate\Http\Resources\Json\JsonResource;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class PayRun extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                 'id' => $this->id,
                 'status' => model_approval_status(check_approver($this)),
                 'payroll_period' => $this->periodRange($this->start_period, $this->end_period),
                 'cutoff_start_period' => $this->start_period,
                 'cutoff_end_period' => $this->end_period,
                 'pay_date' => date('F d, Y', strtotime($this->pay_date)),
                 'history_logs' => LogResource::collection($this->logs()->orderBy('created_at', 'ASC')->get())
        ];
    }

    public function periodRange($start, $end)
    {
        return date('F d', strtotime($start)) . ' - ' . date('d, Y', strtotime($end));
    }
}

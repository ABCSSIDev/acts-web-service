<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HDMFTemplateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'salary_from' => $this->salary_from,
            'salary_to' => $this->salary_to,
            'employer_percent' => $this->employer_percent * 100,
            'employee_percent' => $this->employee_percent * 100
        ];
    }
}

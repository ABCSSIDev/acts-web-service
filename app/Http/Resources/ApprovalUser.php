<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApprovalUser extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "approval_process_id" => $this->approval_process_id,
            "user_name" => $this->getUser->name,
            "role" => $this->getUser->getUserAccess->getRole->role_name,
            "user_id" => $this->user_id,
            "seq_order" => $this->seq_order
        ];
    }
}

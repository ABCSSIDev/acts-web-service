<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Restriction as RestrictionResource;
use App\Restriction;
use App\PermissionModule;

class RestrictionModule extends JsonResource
{
    protected $has_permission = false;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'restrictions' => ($this->getRestrictionSubModule ? $this->convertToRestriction($this->getRestrictionSubModule->restrictions) : null),
            'permission' => $this->getPermissionModule($this->permission_id, $this->id),
            'has_permission' => $this->getHasPermission()
        ];
    }

    public function convertToRestriction($restriction)
    {
        $ids = array();
        $arr_rest = json_decode($restriction);
        foreach ($arr_rest as $key => $id) {
            $ids[] = $id;
        }
        $rest_data = Restriction::whereIn('id', $ids)->get();

        return RestrictionResource::collection($rest_data);
    }

    public function getPermissionModule($permission, $module_id)
    {
        $data = PermissionModule::where('permission_id', $permission)
            ->where('module_id', $module_id)->get();
        if (count($data) > 0) {
            $this->has_permission = true;
            return $this->convertToRestriction($data[0]->restriction_id);
        }
    }

    public function getHasPermission()
    {
        return $this->has_permission;
    }

}

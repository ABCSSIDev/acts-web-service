<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Log as LogResource;
use App\Http\Resources\MaterialIssuancesItems as MaterialIssuancesItemsResource;
use App\Http\Resources\Team as TeamResource;

class MaterialIssuances extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'team_details' => $this->getTeam, 
            'issuance_detials' => $this->getIssuanceTemplates, 
            'tech1' => $this->getTechnicians1->first_name.' '.$this->getTechnicians1->last_name, 
            'tech2' => $this->getTechnicians2->first_name.' '.$this->getTechnicians2->last_name,  
            'date_issue' => date('F d, Y',strtotime($this->date_issue)),
            'materialitem' => MaterialIssuancesItemsResource::collection($this->getMaterialIssuancesItems),
            'history_logs' => new LogResource(MaterialIssuances::findOrFail($this->id)->getLogs()->orderBy('created_at','DESC')->first()),
            // 'history_logs' => $this->getlogs()->oldest()->first()
            
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

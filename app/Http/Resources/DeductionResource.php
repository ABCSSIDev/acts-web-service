<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

use function App\Approval\check_approver;

class DeductionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reason' => $this->reason,
            'employee_id' => $this->employee_id,
            'employee_name' => ($this->getEmployee ? $this->getEmployee->first_name . ' ' . $this->getEmployee->middle_name . ' ' . $this->getEmployee->last_name : 'N/A'),
            'status' => $this->getStatus($this->approved_status),
            'effective_date' => $this->effective_date,
            'cutoff' => $this->cutoff,
            'amount' => $this->amount,
            'asset' => $this->getAsset,
            'approver' => check_approver($this), //option if user is approver and already approved
            '_parse_effective_date' => date('F d, Y', strtotime($this->effective_date)),
            '_parse_effective_until' => date('F d, Y', strtotime($this->effective_until)),
            '_parse_amount' => number_format($this->amount, 2),
            '_parse_created_at' => date('F d, Y', strtotime($this->created_at)),
            '_parse_amount_per_cutoff' => number_format($this->amount / $this->cutoff, 2)
        ];
    }

    public function getStatus($status)
    {
        if ($status == 1) {
            return 'Approved';
        } elseif ($status == 2) {
            return 'Disapproved';
        }

        return 'Pending';
    }
}

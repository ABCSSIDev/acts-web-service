<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LeaveType extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'balance' => $this->balance,
            'type' => $this->type,
            'description' => $this->description,
            'encashment_percentage' => $this->encashment_percentage,
            'effective_value' => $this->effective_value,
            'effective_type' => $this->effective_type,
            'carry_forward_percentage' => $this->carry_forward_percentage,
            'gender' => $this->gender,
            'marital_status' => $this->marital_status,
            'office_type' => $this->office_type,
            'department' => json_decode($this->department),
            'designation' => json_decode($this->designation)
        ];
    }

    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
    
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Role extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'role_id' => $this->role_id,
            'role_name' => $this->role_name,
            'description' => $this->role_description,
            'is_shared' => $this->is_shared,
            'is_enabled' => $this->is_enabled,
            'all_children' =>  $this->when($request->route()->uri ==  "api/roles" || $request->route()->uri ==  "api/roles/edit", Role::collection($this->getChildren)),
            'immediate_children' => $this->when($request->route()->uri ==  "api/roles/{role}" || $request->route()->uri ==  "api/roles/{role}/edit", $this->getChildren)
        ];
    }

    public static function errorResponse($description) {
        return [ "error" => true, "desc" => $description];
    }
}

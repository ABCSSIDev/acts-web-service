<?php

namespace App\Http\Resources;

use function App\Helpers\change_date_format;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class FileManager extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'file_name' => $this->file_name,
            'url' => asset(Storage::url($this->url)),
            'attached_by' => $this->getAttacher->first_name.' '.$this->getAttacher->last_name,
            'created_at' => change_date_format('F d, Y',$this->created_at)
        ];
    }
    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
}

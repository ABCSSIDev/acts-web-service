<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PayRunOvertime extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'pay_run_item_id' => $this->pay_run_item_id,
            'ot_type' => $this->getOvertimeType->code,
            'rate' => $this->getOvertimeType->rate,
            'ot_type_id' => $this->ot_type_id,
            'billable_hours' => $this->billable_hours,
            'non_billable_hours' => $this->non_billable_hours
        ];
    }
}

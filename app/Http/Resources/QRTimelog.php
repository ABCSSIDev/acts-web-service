<?php

namespace App\Http\Resources;

use DateTime;
use Illuminate\Http\Resources\Json\JsonResource;

class QRTimelog extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'attendance_id' => $this->id,
            'employee_id' => $this->employee_id,
            'date' => date('D jS \of F Y'),
            'first_name' => $this->getEmployee->first_name,
            'last_name' => $this->getEmployee->last_name,
            'name' => $this->getEmployee->first_name . ' ' . $this->getEmployee->last_name,
            'designation'   => $this->getEmployee->getJobDetails->getDesignation->name .
                ' (' . $this->getEmployee->getJobDetails->getDesignation->getDepartment->name . ')',
            'start_time' => $this->start_time,
            'time_in' => 'Started At ' . date('h:i A', strtotime($this->start_time)),
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'location_address' => $this->getCoordinates($this->longitude, $this->latitude),
            'time_out' => $this->end_time != null ? date('h:i A', strtotime($this->end_time)) : null,
            'hours_worked'   => $this->getHoursWorked($this->start_time, $this->end_time)
        ];
    }

    /**
     * get Coordinates user location
     *
     * @param string $lat
     * @param string $long
     *
     * @return void
     */
    public function getCoordinates(
        string $long = null,
        string $lat = null
    ) {
        $coordinate = null;

        if ($lat != null && $long != null) {
            $coordinate = $this->googleMap($long, $lat);
        }

        return $coordinate;
    }

    /**
     * get Coordinates via google map
     *
     * @param string $address
     *
     * @return void
     */
    public function googleMap(string $long, string $lat)
    {
        if ($lat != null && $long != null) {
            $googleAPIURL = 'https://maps.google.com/maps/api/geocode/json?latlng='
                . $lat . ',' . $long . '&key=AIzaSyBTh1yXElPijMym4BL3buDO09WOf7DGw9U';
            $json = file_get_contents($googleAPIURL);

            $json = json_decode($json);

            if ($json->{'results'} != []) {
                return $json->{'results'}[0]->{'formatted_address'};
            }
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getHoursWorked($start_time, $end_time)
    {
        if ($end_time != null) {
            $datetimeObj1 = new DateTime(date('H:i'));
            $datetimeObj2 = new DateTime(date('H:i', strtotime($start_time)));
            $interval =  $datetimeObj1->diff($datetimeObj2);
            return $interval->format('%H:%I');
        } else {
            return '0:00';
        }
    }
}

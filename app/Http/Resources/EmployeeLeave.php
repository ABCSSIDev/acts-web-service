<?php

namespace App\Http\Resources;

use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\LeaveType as LeaveTypeResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
class EmployeeLeave extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_id' => $this->getEmployee->id,
            'employee_name' => $this->getEmployee->fullNameLastNameFirst(),
            'leave_type' => new LeaveTypeResource($this->getLeaveType),
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'total_days' => $this->total_days,
            'reason' => $this->reason,
            'status' => model_approval_status(check_approver($this)),
            '_parse_start_date' => Carbon::createFromDate($this->start_date)->format('F d, Y'),
            '_parse_end_date' => Carbon::createFromDate($this->end_date)->format('F d, Y')
        ];
    }

    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }

}

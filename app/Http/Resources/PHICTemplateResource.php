<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PHICTemplateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'year_effective' => $this->year_effective,
            'premium_rate' => $this->premium_rate
        ];
    }
}

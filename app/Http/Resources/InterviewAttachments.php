<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InterviewAttachments extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'interview_id' => $this->interview_id,
            'attached_by' => $this->attached_by,
            'filepath' => $this->filepath,
            'description' => $this->description
        ];
    }
}

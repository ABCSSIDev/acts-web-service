<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Log as LogResource;

class IncidentStatement extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'incident_id' => $this->incident_id,
            'statement_type' => $this->statement_type,
            'statement' => $this->statement,
            'injury_statement' => $this->injury_statement,
            'history_logs' => new LogResource(IncidentStatement::findOrFail($this->id)->getLogs()->orderBy('created_at','DESC')->first()),
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

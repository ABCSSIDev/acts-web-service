<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeDeducted extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_deducted' => $this->getEmployeeDeducted,
            'deduction_amount' => $this->deduction_amount,
            'deduction_per_period' => $this->deduction_per_period
        ];
    }
    public static function msgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use App\OnboardingRequirement;
use App\Http\Resources\OnboardingRequirement as OnboardingRequirementResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Candidate;
use App\Http\Resources\JobOpening as JobOpeningResource;
use App\Http\Resources\Candidate as CandidateResource;
use App\Http\Resources\CandidateAttachment as CandidateAttachmentResource;

class Onboarding extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'candidate_id' => $this->getCandidate->id,
            'candidate_name' => $this->getCandidate->name,
            'candidate_attachment' => CandidateAttachmentResource::collection($this->getCandidate->getCandidateAttachment),
            'candidate' => $this->getCandidate,
            'designation' => $this->getCandidate->getDesignation,
            'salary_offered' => $this->salary_offered,
            'status' => $this->status,
            'starting_date' => date('F d, Y',strtotime($this->starting_date)),
            'hired_date' => date('F d, Y',strtotime($this->hired_date)),
            'edit_startingdate' => $this->starting_date,
            'edit_datehired' => $this->hired_date,
            'job_offer' => !$this->getCandidate->getJobOpening?null:$this->getCandidate->getJobOpening,
            'interviewstatus' => !$this->getCandidate->getJobOpening?null:$this->getCandidate->getJobOpening->getInterviews,
            'requirements' =>$this->when($request->route()->uri ==  "api/onboarding/{onboarding}",!empty($this->requirements)?OnboardingRequirementResource::collection(OnboardingRequirement::whereIn('id',json_decode($this->requirements))->get()):null)  
        ];
    }
    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use App\Http\Resources\AttendanceBreak as AttendanceBreakResource;
use App\Http\Resources\Employee as EmployeeResource;

class Attendance extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_data' => new EmployeeResource($this->getEmployee),
            'project_site_data' => $this->getProjectSite,
            'start_time' => date('h:i A',strtotime($this->start_time)),
            'end_time' => ($this->end_time == null ? null:date('h:i A',strtotime($this->end_time))),
            'date' => date('F d, Y',strtotime($this->start_time)),
            'location' => $this->location,
            'meta_data' => $this->meta_data,
            'render_hrs' => $this->renderhrs($this->start_time,$this->end_time),
            'image_path' => $this->image_path,
            'break' => AttendanceBreakResource::collection($this->getAttendanceBreak)
        ];
    }
    public static function renderhrs($from,$to){
        $start = Carbon::parse($from);
        $end = Carbon::parse($to);
        $hours = $end->diff($start)->format('%H:%i');

        return $hours;
    }
    // public static function breaklist($data){
    //     $array = array();
    //     foreach($data as $val){
    //         $breaksdata = 1;
    //         $array = [
    //             'name' =>$val->break_id == $val->break_id,
    //             'time' =>$breaksdata,
    //             'renderhrs' =>$breaksdata,
    //         ];
    //     }

    //     return $array;
    // }
    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Technicians as TechniciansResource;



class Team extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'team_id' => $this->team_id, 
            'team_name' => $this->team_name, 
            'team_type' => $this->team_type, 
            'active' => $this->active, 
            'vehicle_plateno' => $this->vehicle_plateno, 
            'team_technicians' => TechniciansResource::collection($this->getTechnicians), 
            "created"  => $this->logs()->oldest()->first(),

            
            
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

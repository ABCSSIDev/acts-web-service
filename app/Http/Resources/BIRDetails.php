<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BIRDetails extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'membership_code' => $this->membership_code,
            'account_number' => $this->account_number,
            'joined_date' => date('F d, Y', strtotime($this->joined_date)),
            'separation_date' => date('F d, Y', strtotime($this->separation_date)),
            'status' => $this->status
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

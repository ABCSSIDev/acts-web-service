<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BIRTemplateSalaryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'period_id' => $this->period_id,
            'category_id' => $this->category_id,
            'exemption_id' => $this->exemption_id,
            'salary_value' => $this->salary_value
        ];
    }
}

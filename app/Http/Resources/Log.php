<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Employee;
use App\Role;
use App\User;
use App\UserAccess;

class Log extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => date('F d, Y', strtotime($this->created_at)),
            'time' => date('h:i A', strtotime($this->created_at)),
            'subject' => $this->description,
            'user' => $this->userName($this->user_id),
            'log_info' => $this->reference,
            'image' => $this->userImage($this->user_id)
        ];
    }

    /**
     * user image
     *
     * @param string $id
     *
     * @return void
     */
    public function userImage(string $id)
    {
        $data = Employee::where('user_id', $id)->first();
        
        if (!$data) {
            $image = null;
        } else {
            $image = $data->image;
        }

        return $image;
    }
    
    /**
     * get user name
     *
     * @param string $id
     *
     * @return string
     */
    public function userName(string $id): string
    {
        $name = Employee::where('user_id', $id)->first();
        if (!$name) {
            $user = User::select('name')->where('id', $id)->first();
            $approver_name = $user->name;
        } else {
            $approver_name = $name->first_name . ' ' . $name->last_name;
        }
        return $approver_name;
    }
}

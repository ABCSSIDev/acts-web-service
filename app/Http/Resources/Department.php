<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
class Department extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'employee_id' => $this->employee_id,
            'employee_name' => ($this->getEmployee?$this->getEmployee->first_name.' '.$this->getEmployee->middle_name.' '.$this->getEmployee->last_name:'N/A'),
            'status' => $this->status,
            'designations' => $this->getDesignations
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use App\Attendance;
use App\EmployeeOvertime as AppEmployeeOvertime;
use App\Schedule;
use DB;
use App\Http\Resources\Schedule as ScheduleResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class EmployeeOvertime extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_name' => $this->getEmployee->fullNameLastNameFirst(),
            'employee_id' => $this->employee_id,
            'ot_id' => $this->ot_id,
            'ot_type' => $this->getOtType->code,
            'ot_type_name' => $this->getOtType->name,
            'file_date' => $this->file_date,
            'overtime_date' => $this->overtime_date,
            'start_time' => date('h:i A',strtotime($this->start_time)),
            'end_time' => date('h:i A',strtotime($this->end_time)),
            'edit_start_time' => $this->start_time,
            'edit_end_time' => $this->end_time,
            'rendered_hours' => date('h.i ',strtotime($this->rendered_hours)),
            'purpose' => $this->purpose,
            'reason' => $this->reason,
            'approved_status' => model_approval_status(check_approver($this)),
            // 'schedule' => $this->getSchedule($this->employee_id),
            'cutoff' => $this->getCutoff(),
            'total_overtime' => $this->getTotalOTHours($this->employee_id),
            '_parse_overtime_date' => Carbon::createFromDate($this->overtime_date)->format('F d, Y')
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }

    public function getSchedule($employee_id) {
        $schedule = Schedule::getSchedulebyDepartment($employee_id);
        return new ScheduleResource($schedule);
    }

    /**
     * Get Cutoff Date
     *
     * @return void
     */
    public function getCutoff()
    {
        $today = Carbon::now();
        $start_date = Carbon::createFromFormat('Y-m-d', $today->year . '-' . $today->month . '-6');
        $end_date = Carbon::createFromFormat('Y-m-d', $today->year . '-' . $today->month . '-19');
        if ($today->day <= 5) {
            $start_date2 = Carbon::createFromFormat('Y-m-d', $today->year . '-' . $today->sub(1, 'month')->format('m') . '-20');
        } else {
            $start_date2 = Carbon::createFromFormat('Y-m-d', $today->year . '-' . $today->month . '-20');
        }
        $end_date2 = Carbon::createFromFormat('Y-m-d', $today->year . '-' . ($today->month == 12 ? 01 : $today->month + 1) . '-5');
        $cutoff1 = $today->between($start_date, $end_date);
        if (!$cutoff1) {
            $start_date = $start_date2;
            $end_date = $end_date2;
        }
        return $start_date->format('F d') . " - " . $end_date->format('F d');
    }

    /**
     * Get Total Overtime
     *
     * @param [id] $employee_id
     *
     * @return void
     */
    public function getTotalOTHours($employee_id)
    {
        $today = Carbon::now();
        $start_date = Carbon::createFromFormat('Y-m-d', $today->year . '-' . $today->month . '-6');
        $end_date = Carbon::createFromFormat('Y-m-d', $today->year . '-' . $today->month . '-19');
        if ($today->day <= 5) {
            $start_date2 = Carbon::createFromFormat('Y-m-d', $today->year . '-' . $today->sub(1, 'month')->format('m') . '-20');
        } else {
            $start_date2 = Carbon::createFromFormat('Y-m-d', $today->year . '-' . $today->month . '-20');
        }
        $end_date2 = Carbon::createFromFormat('Y-m-d', $today->year . '-' . ($today->month == 12 ? 01 : $today->month + 1) . '-5');
        $cutoff1 = $today->between($start_date, $end_date);
        if (!$cutoff1) {
            $start_date = $start_date2;
            $end_date = $end_date2;
        }

        $employee_overtime = AppEmployeeOvertime::where('employee_id', $employee_id)
                    ->whereBetween(
                        DB::raw('date(overtime_date)'),
                        [
                            $start_date->format('Y-m-d'),
                            $end_date->format('Y-m-d')
                        ]
                    )
                    ->orderBy('overtime_date', 'asc')
                    ->get();
        $total_hours = 0;
        foreach ($employee_overtime as $key => $overtime) {
            if (
                check_approver($overtime) == null
                || model_approval_status(check_approver($overtime)) == 'Approved'
            ) {
                $attendance = Attendance::where('employee_id', $employee_id)
                              ->whereDate('start_time', $overtime->overtime_date)
                              ->orderBy('start_time', 'asc')->first();
                if ($attendance) {
                    $approve_id[] = $attendance;
                    $end_time = date('H:i:s', strtotime($attendance->end_time));
                    $compute = (strtotime($end_time) - strtotime($overtime->start_time)) / 3600;
                    $compute = abs($compute);
                    if ($compute > $overtime->rendered_hours) {
                        $total_hours += $overtime->rendered_hours;
                    } else {
                        $total_hours += $compute;
                    }
                }
            }
        }
        return $total_hours;
    }
}

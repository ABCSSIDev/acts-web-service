<?php

namespace App\Http\Resources;

use App\AttendanceBreak;
use DateTime;
use Illuminate\Http\Resources\Json\JsonResource;

class TimelogBreak extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'attendance_id' => $this->attendance_id,
            'break' => $this->name,
            'billed' => $this->is_billable == 1 ? 'Paid' : 'Unpaid',
            'duration' => $this->attendance($this->attendance_id, $this->id),
            'status' => $this->checkStatus($this->attendance_id, $this->id)
        ];
    }

    public function attendance($attendance_id, $break_id)
    {
        $time_in = $this->attendanceBreakIn($attendance_id, $break_id);
        $time_out  = $this->attendanceBreakOut($attendance_id, $break_id);
        if ($time_in && $time_out) {
            $datetimeObj1 = new DateTime($time_in->time);
            $datetimeObj2 = new DateTime($time_out->time);
            $interval =  $datetimeObj1->diff($datetimeObj2);
            $diff = $interval->format('%H:%i');
            $duration = explode(':', $diff);
            $total_mins = ($duration[0] * 60 ) + $duration[1];
            return $total_mins . ' minutes';
        } elseif ($time_out) {
            return 'Started at ' . date('h:i A', strtotime($time_out->time));
        } else {
            return null;
        }
    }

    public function checkStatus($attendance_id, $break_id)
    {
        return ($this->attendanceBreakIn($attendance_id, $break_id) &&
        $this->attendanceBreakOut($attendance_id, $break_id) || $this->attendanceBreakOut);
    }

    public function attendanceBreakIn($attendance_id, $break_id)
    {
        return AttendanceBreak::where('attendance_id', $attendance_id)
            ->where('break_id', $break_id)->where('type', 1)->first();
    }

    public function attendanceBreakOut($attendance_id, $break_id)
    {
        return AttendanceBreak::where('attendance_id', $attendance_id)
            ->where('break_id', $break_id)->where('type', 0)->first();
    }
}

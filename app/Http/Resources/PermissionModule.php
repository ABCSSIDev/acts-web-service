<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Restriction as RestrictionResource;
use App\Restriction;

class PermissionModule extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'permission_id' => $this->permission_id,
            'module' => $this->getModule->name,
            'module_id' => $this->module_id,
            'restriction' => $this->convertToRestriction($this->restriction_id)
        ];
    }

    public function convertToRestriction($restriction)
    {
        $ids = array();
        $arr_rest = json_decode($restriction);
        foreach ($arr_rest as $key => $id) {
            $ids[] = $id;
        }
        $rest_data = Restriction::whereIn('id',$ids)->get();

        return RestrictionResource::collection($rest_data);
    }

    
}

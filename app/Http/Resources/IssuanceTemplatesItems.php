<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;



class IssuanceTemplatesItems extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'template_id' => $this->template_id, 
            'inventory_id' => $this->getInventories, 
            'quantity' =>  $this->quantity
            
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use function App\Helpers\change_date_format;
use Illuminate\Http\Resources\Json\JsonResource;

class JobDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'designation_id' => $this->designation_id,
            'joined_date' => change_date_format('F d, Y',$this->joined_date),
            'employee_type_id' => $this->employee_type_id,
            'employee_status' => $this->employee_status,
            'source_hire' => $this->source_of_hire,
            'employees_type' => $this->getEmployeeType,
            'employee_designation' => $this->getDesignation,
            'designation' => $this->when($request->route()->uri == 'api/employees' || $request->route()->uri == "api/employees/{employee}",$this->getDesignation->name),
            'department' => $this->when($request->route()->uri == 'api/employees' || $request->route()->uri == "api/employees/{employee}",$this->getDesignation->getDepartment->name),
            'employee_type' => $this->when($request->route()->uri == 'api/employees'  || $request->route()->uri == "api/employees/{employee}",$this->getEmployeeType->name),
            'source_of_hire' => $this->when($request->route()->uri == 'api/employees'  || $request->route()->uri == "api/employees/{employee}",$this->source_of_hire),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Employee;
use App\Http\Resources\Employee as EmployeeResource;
use App\System;
use App\Http\Resources\System as SystemResource;
class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $bypass = !empty($this->getUserAccess->bypass_role_systems)?json_decode($this->getUserAccess->bypass_role_systems):null;
        $systems = "";
        if(!empty($bypass)){
            $systems = System::whereIn('id', $bypass)->get();
        }
        return [
            'id' => $this->id,
            'avatar' => $this->avatar,
            'name' => html_entity_decode($this->name),
            'email' => $this->email,
            'role' => ($this->getUserAccess != null ? $this->getUserAccess->getRole->role_name  : 'None'),
            'permission' => ($this->getUserAccess != null ? $this->getUserAccess->getPermission->permission_name : 'None'),
            'role_exemptions' => $systems
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }


}

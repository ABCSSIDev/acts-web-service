<?php

namespace App\Http\Resources;

use function App\Helpers\change_date_format;
use Illuminate\Http\Resources\Json\JsonResource;

class WorkDetails extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'company' => $this->company,
            'start_date' => change_date_format('F d, Y',$this->start_date),
            'end_date' => change_date_format('F d, Y',$this->end_date),
            'description' => $this->description,
        ];
    }
}

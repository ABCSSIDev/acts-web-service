<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Employee as EmployeeResource;

class EmployeeExit extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee' => new EmployeeResource($this->getEmployee),
            'interviewer' => new EmployeeResource($this->getInterviewer),
            'separation_date' => date('F d, Y',strtotime($this->separation_date)),
            'edit_separation_date' => $this->separation_date,
            'reason' => $this->reason,
            'exit_type' => $this->exit_type
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

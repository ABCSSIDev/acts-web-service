<?php

namespace App\Http\Resources;

use App\SSSTemplates;
use Illuminate\Http\Resources\Json\JsonResource;

class SSSTemplateContributionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'salary_from' => $this->salary_from,
            'salary_to' => $this->salary_to,
            'monthly_salary_credit' => $this->monthly_salary_credit,
            'employer_contribution' => $this->employer_contribution,
            'employee_contribution' => $this->employee_contribution,
            'employee_compensation' => $this->employee_compensation
        ];
    }
}

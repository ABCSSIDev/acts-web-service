<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Elements extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'element_type_data' => ($this->getElementsType?$this->getElementsType:'N/A'),
            'category_data' => ($this->getCategories?$this->getCategories:'N/A'),
            'inventory_data' => ($this->getInventories?$this->getInventories:'N/A'),
            'caption' => $this->description
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CandidateAttachment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id, 
            'candidate_id' => $this->getCandidateid->first_name.' '.$this->getCandidateid->last_name, 
            'attached_by' => $this->getAttachedby->first_name.' '.$this->getAttachedby->last_name, 
            'filepath' => $this->filepath, 
            'description' => $this->description, 
            'created_at' => date('F d, Y',strtotime($this->created_at))
        ];
    }

}

<?php

namespace App\Http\Resources;

use App\BirTemplate;
use App\HDMFTemplates;
use App\Http\Resources\Log as LogResource;
use App\PHICTemplates;
use App\SSSTemplateContributions;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeSalary extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee->first_name . ' ' . $this->employee->last_name,
            'type' => $this->type,
            'classification' => $this->classification,
            'effective_date' => date('F d, Y', strtotime($this->effective_date)),
            'monthly_salary' => $this->monthly_salary,
            'rate_per_day' => $this->rate_per_day,
            'history_logs' => LogResource::collection($this->getLogs()->orderBy('created_at', 'DESC')->get()),
            'total_allowance' => $this->allowances->sum('amount'),
            'allowance' => $this->allowances,
            'monthly_daily_rate' => $this->getDailyIfMonthlyRate($this->monthly_salary),
            'rate_per_hour' => $this->getRatePerHour($this->monthly_salary, $this->rate_per_day),
            'daily_monthly_rate' => $this->getMonthlyIfDailyRate($this->rate_per_day),
            'sss' => $this->getSSSContribution(($this->monthly_salary == null ? $this->getMonthlyIfDailyRate($this->rate_per_day) : $this->monthly_salary)),
            'pagibig' => $this->getPagibigContri(($this->monthly_salary == null ? $this->getMonthlyIfDailyRate($this->rate_per_day) : $this->monthly_salary)),
            'bir' => $this->getBIRContributions(($this->monthly_salary == null ? $this->getMonthlyIfDailyRate($this->rate_per_day) : $this->monthly_salary))
        ];
    }

    public function getBIRContributions($rate)
    {
        $bir = 0;
        $percent_rate = 0;
        $birTemplate = BirTemplate::whereDate('date_effective_from', '<=', date('Y-m-d'))
                ->whereDate('date_effective_to', '>=', date('Y-m-d'))->where('is_activate', 1)
                ->first();

        $semi_monthly_rate = $rate / 2;
        
        foreach ($birTemplate->birTemplateItems as $item) {
            if ($item->rate_type == 'SEMI-MONTHLY') {
                if ($item->salary_range_to == '0.00') {
                    if ($item->salary_range_from < round($semi_monthly_rate, 2)) {
                        $percent_rate = round($semi_monthly_rate, 2) / ($item->withholding_tax_percent * 100);
                        $bir = $percent_rate + $item->withholding_tax_value;
                    }
                } elseif (round($semi_monthly_rate, 2) <= $item->salary_range_to && round($semi_monthly_rate, 2) >= $item->salary_range_from) {
                    if ($item->withholding_tax_percent != '0.00') {
                        $percent_rate = round($semi_monthly_rate, 2) / ($item->withholding_tax_percent * 100);
                    }
                    
                    $bir = $percent_rate + $item->withholding_tax_value;
                }
            }
        }
        return number_format($bir, 2);
    }

    /**
     * Get Daily Rate for Monthly Salary
     *
     * @return void
     */
    public function getDailyIfMonthlyRate($monthly)
    {
        if ($monthly != null) {
            $rate = ($monthly * 12) / 313;
            return number_format($rate, 2);
        }
    }

    public function getRatePerHour($monthly, $daily_rate)
    {
        if ($monthly != null) {
            $rate = (($monthly * 12) / 313) / 8;
            return number_format($rate, 2);
        }
        return $daily_rate / 8;
    }

    public function getMonthlyIfDailyRate($daily)
    {
        $rate = ($daily * 313) / 12;
        return $rate;
    }

    public function getSSSContribution($rate)
    {
        $contributions = SSSTemplateContributions::all();
        foreach ($contributions as $contribution) {
            if ($rate <= $contribution->salary_to && $rate >= $contribution->salary_from) {
                $sss = $contribution->employee_contribution + $contribution->employee_compensation;
                return number_format($sss);
            }
        }
    }

    public function getPagibigContri($rate) {
        $contributions = HDMFTemplates::all();
        foreach ($contributions as $contribution) {
            if ($rate <= $contribution->salary_to && $rate >= $contribution->salary_from) {
                $pagibig = $rate * ($contribution->employee_percent / 100);
                return number_format($pagibig);
            }

            if ($contribution->salary_to == '0.00') {
                if ($rate > $contribution->salary_from) {
                    $pagibig = 100;

                    return number_format($pagibig);
                }
            }
        }
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Designation as DesignationResource;
use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\Interview as InterviewResource;

class JobOpening extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name, 
            'assigned_recruiter' => new EmployeeResource($this->getRecruiter),
            'target_date' => date('F d, Y',strtotime($this->target_date)),
            'edit_target_date' => $this->target_date,
            'job_opening_status' => $this->job_opening_status,
            'designation' => new DesignationResource($this->getDesignation),
            'manpower_required' => $this->when($request->route()->uri ==  "api/job-openings/{job_opening}" || $request->route()->uri ==  "api/job-openings/{job_opening}/edit", $this->manpower_required),
            'expected_salary' => $this->when($request->route()->uri ==  "api/job-openings/{job_opening}" || $request->route()->uri ==  "api/job-openings/{job_opening}/edit", $this->expected_salary),
            'hiring_manager' => $this->when($request->route()->uri ==  "api/job-openings/{job_opening}" || $request->route()->uri ==  "api/job-openings/{job_opening}/edit", new EmployeeResource($this->getHiringManager)),
            'opened_date' => $this->when($request->route()->uri ==  "api/job-openings/{job_opening}",date('F d, Y',strtotime($this->opened_date))),
            'edit_opened_date' => $this->opened_date,
            'job_type' => $this->when($request->route()->uri ==  "api/job-openings/{job_opening}" || $request->route()->uri ==  "api/job-openings/{job_opening}/edit", $this->job_type),
            'work_experience' => $this->when($request->route()->uri ==  "api/job-openings/{job_opening}" || $request->route()->uri ==  "api/job-openings/{job_opening}/edit", $this->work_experience),
            'job_description' => $this->when($request->route()->uri ==  "api/job-openings/{job_opening}" || $request->route()->uri ==  "api/job-openings/{job_opening}/edit", $this->job_description),
            'job_requirements' => $this->when($request->route()->uri ==  "api/job-openings/{job_opening}" || $request->route()->uri ==  "api/job-openings/{job_opening}/edit", $this->job_requirements),
            'candidates' => $this->when($request->route()->uri ==  "api/job-openings/{job_opening}" || $request->route()->uri ==  "api/job-openings/{job_opening}/edit", $this->getCandidates),
            'interviews' => $this->when($request->route()->uri ==  "api/job-openings/{job_opening}" || $request->route()->uri ==  "api/job-openings/{job_opening}/edit", InterviewResource::collection($this->getInterviews))
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ReimbersementItems;

class Reimbursement extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'employee_name' => ($this->getEmployee?$this->getEmployee->first_name.' '.$this->getEmployee->middle_name.' '.$this->getEmployee->last_name:'N/A'),
            'employee_id' => $this->employee_id,
            'filed_date' => date('F d, Y',strtotime($this->filed_date)),
            'description' => $this->description,
            'approved_status' => ($this->approved_status == 0 ?'Pending':($this->approved_status == 1 ?'Approve':'Disappriove')),
            'approved_by' => ($this->getEmployeeApprover?$this->getEmployeeApprover->first_name.' '.$this->getEmployeeApprover->middle_name.' '.$this->getEmployeeApprover->last_name:'N/A'),
            'approved_by_id' => $this->approved_by,
            'approved_date' => date('F d, Y',strtotime($this->approved_date)),
            'total_amount' => $this->TotalAmount($this->getReimbursementItem),
            'reimbursmentitem' => ReimbersementItems::collection($this->getReimbursementItem)
        ];
    }
    public static function TotalAmount($data){
        $total = 0;
        foreach($data as $key => $value){
            $total+= $value->amount;
        }
        return number_format($total, 2);
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BIRTemplateExemptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'exemption_status' => $this->exemption_status,
            'exemption_values' => $this->exemption_values,
            'dependent' => $this->dependent
        ];
    }
}

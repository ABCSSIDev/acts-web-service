<?php

namespace App\Http\Resources;

use App\Department;
use App\Module;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ApprovalUser as ApprovalUserResource;

class ApprovalProcess extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference_type' => $this->reference_type,
            'module_name' => $this->getModuleDescription($this->reference_type),
            'name' => $this->name,
            'description' => $this->description,
            'execute' => $this->execute,
            'execute_msg' => ($this->execute == 0 ? 'Record Creation' :
                ($this->execute == 0 ? 'Record Edit' : 'Record Creation and Edit')),
            'status' => $this->status,
            'status_msg' => ($this->status == 0 ? 'inactive' : 'active'),
            'approved_setup' => $this->approved_setup,
            'approved_setup_msg' => ($this->approved_setup == 0 ? 'Anyone' : 'Everyone'),
            'order' => $this->order,
            'order_msg' => ($this->order == 0 ? 'Sequential' : ($this->order == 1 ? 'Parallel' : null)),
            'approvers' => ApprovalUserResource::collection($this->getApprovalUser),
            'departments' => Department::whereIn('id', json_decode($this->departments))->get()
        ];
    }

    public static function errorResponse($description)
    {
        return [ "error" => true, "desc" => $description];
    }
    
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }

    /**
     * get module name
     *
     * @param string $referenceType
     *
     * @return string
     */
    public function getModuleDescription(string $referenceType): string
    {
        $module = Module::where('model_type', $referenceType)->first();

        return $module->name;
    }
}

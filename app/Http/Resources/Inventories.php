<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Log as LogResource;
use Illuminate\Support\Facades\Storage;

class Inventories extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => ($this->type == 0 ? 'Operation' : ($this->type == 1 ? 'CPE' : ($this->type == 2 ? 'Accountable' : 'Consumable' ))),
            'type_number' => $this->type,
            'inventory_description_data' => $this->getInventoriesNames,
            'unit_id' => $this->getUnit,
            'category_id' => $this->getCategories,
            'image' => ($this->image == null ? null : asset(Storage::url($this->image))),
            'image_src' => ($this->image == null ? null : $this->image),
            'quantity' => $this->quantity,
            'unit_price' => $this->unit_price,
            'min_stock' => $this->min_stock,
            'issued_count' => $this->issued_count,
            'issued' => $this->issued,
            'created_at' => date('F d, Y', strtotime($this->created_at)),
            'time' => date('h:i A', strtotime($this->created_at)),
            'history_logs' => new LogResource(Inventories::findOrFail($this->id)->getLogs()->orderBy('created_at','DESC')->first()),
        ];
    }
    public static function MsgResponse($type, $description) {
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Approval extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference_id' => $this->reference_id,
            'reference_type' => $this->reference_type,
            'user_id' => $this->user_id,
            'approved_status' => $this->approved_status,
            'description' => $this->remarks,
            'approved_date' => $this->created_at
        ];
    }
    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
}

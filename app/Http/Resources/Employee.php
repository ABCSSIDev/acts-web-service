<?php

namespace App\Http\Resources;

use function App\Helpers\change_date_format;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\JobDetail as JobDetailResource;
use App\Http\Resources\EmployeeContact as EmployeeContactResource;
use App\Http\Resources\SSSDetails as SSSDetailsResource;
use App\Http\Resources\BIRDetails as BIRDetailsResource;
use App\Http\Resources\PHICDetails as PHICDetailsResource;
use App\Http\Resources\HDMFDetails as HDMFDetailsResource;
use App\Http\Resources\FileManager as FileManagerResource;
use Illuminate\Support\Facades\Storage;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
class Employee extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'employee_no' => $this->employee_no,
            'image' => ($this->image == null ? null : asset(Storage::url($this->image))),
            'first_name' => html_entity_decode($this->first_name),
            'last_name' => html_entity_decode($this->last_name),
            'middle_name' =>  html_entity_decode($this->middle_name),
            'birth_date' => change_date_format('F d, Y', $this->birth_date),
            'gender' => $this->gender,
            'marital_status' => $this->marital_status,
            'email' => (!empty($this->getContactDetails) ? $this->getContactDetails->email : ""),
            'current_address' => $this->current_address,
            'work_details' => $this->getWorkDetails,
            'educational_details' => $this->getEducationalDetails,
            'permanent_address' => $this->permanent_address,
            'user_access' =>($this->getUser && $this->getUser->getUserAccess ? $this->getUser->getUserAccess : 'Null'),
            'job_details' => new JobDetailResource($this->getJobDetails),
            'contact_details' => $this->when($request->route()->uri == "api/employees/{employee}" || $request->route()->uri == "api/employees/{employee}/edit",new EmployeeContactResource($this->getContactDetails)),
            'sss_details' => $this->when($request->route()->uri == "api/employees/{employee}" || $request->route()->uri == "api/employees/{employee}/edit",new SSSDetailsResource($this->getSSSDetails)),
            'bir_details' => $this->when($request->route()->uri == "api/employees/{employee}" || $request->route()->uri == "api/employees/{employee}/edit",new BIRDetailsResource($this->getBIRDetails)),
            'phic_details' => $this->when($request->route()->uri == "api/employees/{employee}" || $request->route()->uri == "api/employees/{employee}/edit",new PHICDetailsResource($this->getPHICDetails)),
            'hdmf_details' => $this->when($request->route()->uri == "api/employees/{employee}" || $request->route()->uri == "api/employees/{employee}/edit",new HDMFDetailsResource($this->getHDMFDetails)),
            'attachments' => FileManagerResource::collection($this->getAttachments),
            'status' => model_approval_status(check_approver($this)),
            '_parse_name' => html_entity_decode($this->last_name) . ', ' . html_entity_decode($this->first_name)
        ];
    }


    public static function msgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }

}

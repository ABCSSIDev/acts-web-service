<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BIRTemplateTaxCriteriasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tax_category_id' => $this->tax_category_id,
            'compensation_level' => $this->compensation_level,
            'presribed_tax' => $this->prescribed_tax,
            'percent_over' => $this->percent_over
        ];
    }
}

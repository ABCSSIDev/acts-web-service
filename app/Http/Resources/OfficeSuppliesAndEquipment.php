<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Log as LogResource;
use App\Http\Resources\Inventories as InventoriesResource;
use App\Http\Resources\Employee as EmployeeResource;

use Illuminate\Support\Facades\Storage;

class OfficeSuppliesAndEquipment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'inventory_data' => new InventoriesResource($this->getInventories),
            'inventory_status_data' => $this->getInventoryStatus,
            'asset_tag' => $this->asset_tag,
            'image_src' => ($this->image == null ? null : asset(Storage::url($this->image))),
            'image' => ($this->image == null ? null : $this->image),
            'assigned_to' =>  new EmployeeResource($this->getEmployee),
            'serial_model_no' => $this->serial_model_no,
            'asset_status' => $this->asset_status,
            'created_at' => $this->created_at,
            'history_logs' => new LogResource(OfficeSuppliesAndEquipment::findOrFail($this->id)->getLogs()->orderBy('created_at','DESC')->first()),
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

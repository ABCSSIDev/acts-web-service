<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IncidentAction extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'incident_id' => $this->incident_id,
            'actions' => $this->actions,
            'responsibility' => $this->responsibility,
            'completion_date'  => $this->completion_date
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

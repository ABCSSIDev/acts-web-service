<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Candidate extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id, 
            'name' => $this->name, 
            'contact_number' => $this->contact_number, 
            'email' => $this->email, 
            'address' => $this->address, 
            'source' => $this->source, 
            'recruiter_id' => empty($this->getRecruiter)?null:$this->getRecruiter,
            'potential_designation' => empty($this->getDesignation)?null:$this->getDesignation, 
            'job_opening_id' => empty($this->getJobOpening->id)?null:$this->getJobOpening->id,
            'job_opening_name' => empty($this->getJobOpening->name)?null:$this->getJobOpening->name
        ];
    }

    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }

    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

<?php

namespace App\Http\Resources;

use App\Module as AppModule;
use App\PermissionModule;
use App\UserAccess;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use App\Restriction;
use App\Http\Resources\Restriction as RestrictionResource;

use function App\Helpers\get_restrictions;

class Module extends JsonResource
{
    protected $has_permission = false;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (Auth::user()->id !== 1) {
            $permissions = UserAccess::where('user_id', Auth::user()->id)->first();
            $permission_mod = PermissionModule::where('permission_id', $permissions->permission_id)
                ->pluck('module_id')->toArray();
            return [
                'id' => $this->id,
                'name' => $this->name,
                'icon' => ($this->icon == null ? 'None' : $this->icon),
                'route' => ($this->route == null ? 'None' : $this->route),
                'status' => $this->is_enabled,
                'system_name' => $this->getParentSystem->name,
                'parent_module' => ($this->getParentModule == null ? 'None' : $this->getParentModule->name),
                'sub_modules' => Module::collection($this->getFilteredModules($permission_mod)->get()),
                'module_functions' => ($this->getRestrictionSubModule != null ?
                    get_restrictions($this->getRestrictionSubModule->restrictions) : null),
                'module_permission' => ($this->permission_id != null ?
                    $this->getPermissionModule($this->permission_id, $this->id) : null),
                'has_permission' => $this->getHasPermission()
            ];
        } else {
            return [
                'id' => $this->id,
                'name' => $this->name,
                'icon' => ($this->icon == null ? 'None' : $this->icon),
                'route' => ($this->route == null ? 'None' : $this->route),
                'status' => $this->is_enabled,
                'system_name' => $this->getParentSystem->name,
                'parent_module' => ($this->getParentModule == null ? 'None' : $this->getParentModule->name),
                'sub_modules' => Module::collection($this->getSubmodules),
                'module_functions' => ($this->getRestrictionSubModule != null ?
                    get_restrictions($this->getRestrictionSubModule->restrictions) : null),
                'module_permission' => ($this->permission_id != null ?
                    $this->getPermissionModule($this->permission_id, $this->id) : null),
                'has_permission' => $this->getHasPermission()
            ];
        }
    }

    public function getPermissionModule($permission, $module_id)
    {
        $data = PermissionModule::where('permission_id', $permission)
            ->where('module_id', $module_id)->get();
        if (count($data) > 0) {
            $this->has_permission = true;
            return $this->convertToRestriction($data[0]->restriction_id);
        }
    }

    public function convertToRestriction($restriction)
    {
        $ids = array();
        $arr_rest = json_decode($restriction);
        foreach ($arr_rest as $key => $id) {
            $ids[] = $id;
        }
        $rest_data = Restriction::whereIn('id', $ids)->get();

        return RestrictionResource::collection($rest_data);
    }

    public function getHasPermission()
    {
        return $this->has_permission;
    }
}

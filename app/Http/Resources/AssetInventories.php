<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Log as LogResource;
use App\OfficeSuppliesAndEquipment;
use Illuminate\Support\Facades\Storage;

class AssetInventories extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'inventory_description_data' => $this->getInventoriesNames,
            'unit_id' => $this->getUnit,
            'category_id' => $this->getCategories,
            'image' => ($this->image == null ? null : asset(Storage::url($this->image))),
            'image_src' => ($this->image == null ? null : $this->image),
            'quantity' => $this->quantity,
            'unit_price' => $this->unit_price,
            'min_stock' => $this->min_stock,
            'created_at' => date('F d, Y', strtotime($this->created_at)),
            'issued_count' => $this->issued_count,
            'issued' => $this->issued,
            'time' => date('h:i A', strtotime($this->created_at)),
        ];
    }
    public static function MsgResponse($type, $description) {
        return [ "error" => $type, "desc" => $description];
    }
}

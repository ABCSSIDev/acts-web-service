<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
class OB extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee' => $this->getEmployee,
            'date' => date('F d, Y',strtotime($this->date)),
            'edit_date' => $this->date,
            'time_in' => date('h:i A',strtotime($this->time_in)),
            'edit_timein' => $this->time_in,
            'time_out' => date('h:i A',strtotime($this->time_out)),
            'edit_timeout' => $this->time_out,
            'rendered_hours' => date('h.i ',strtotime($this->rendered_hours)),
            'purpose' => $this->purpose,
            'employee_name' => $this->getEmployee->first_name.' '.$this->getEmployee->last_name,
            'approved_status' => model_approval_status(check_approver($this))
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}

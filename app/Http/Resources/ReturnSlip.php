<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ReturnSlip extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $info_of_create = $this->logs()->oldest()->first();
        return [
            'id' => $this->id,
            'employee' => $this->employee,
            'date' => $this->date,
            'created' => $info_of_create,
            'item_count' => count($this->returnedItems),
            'return_items' => $this->returnedItems

        ];
    }

    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
}

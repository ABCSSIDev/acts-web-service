<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Inventories as InventoriesResource;

    
class TransmittalItemsSlip extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'transmittal_id' => $this->getTransmittalSlip, 
            'inventory_id' => New InventoriesResource($this->getInventories), 
            'asset_id' => $this->asset_id, 
            'remarks' => $this->remarks, 
            'quantity' => $this->quantity, 
            
        ];
    }
    public static function MsgResponse($type,$description){
        return [ "error" => $type, "desc" => $description];
    }
}
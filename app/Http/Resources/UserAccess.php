<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserAccess extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            "user_name" => $this->getUser->name,
            "role" => $this->getRole->role_name,
            "employee_id" => $this->getUser->getEmployee ? $this->getUser->getEmployee->id : null
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\System as SystemResource;
use App\System;

class Permission extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $systems = json_decode($this->system_id);
        $system_arr = array();
        foreach ($systems as $system) {
            $sys_info = System::findOrFail($system);
            $system_arr[] = array(
                'name' => substr($sys_info->name, 0, 2),
                'full_name' => $sys_info->name,
                'color' => $sys_info->system_theme,
                'description' => $sys_info->description
            );
        }

        return [
            'id' => $this->id,
            'permission_name' => $this->permission_name,
            'description' => $this->description,
            'created_by' => '',
            'created_at' => $this->created_at,
            'systems' => $system_arr,
            'history_logs' => $this->logs
        ];
    }

    // public function convertToSystem($systems)
    // {
    //     $ids = array();
    //     $arr_sys = json_decode($systems);
    //     foreach ($arr_sys as $key => $id) {
    //         $ids[] = $id;
    //     }
    //     $sys_data = System::whereIn('id',$ids)->get();

    //     return SystemResource::collection($sys_data);
    // }
}

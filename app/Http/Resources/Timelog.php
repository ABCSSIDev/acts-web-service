<?php

namespace App\Http\Resources;

use App\Attendance;
use App\AttendanceBreak;
use App\Http\Resources\TimelogBreak as TimelogBreakResource;
use App\Schedule;
use DateTime;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class Timelog extends JsonResource
{
    protected $status = false;
    protected $attendance;
    protected $attendance_id;
    protected $time_out;
    protected $start_time;
    protected $image;
    protected $location;
    protected $longitude;
    protected $latitude;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'employee_id'   => $this->id,
            'date'          => date('D jS \of F Y'),
            'name'          => $this->first_name . ' ' . $this->last_name,
            'designation'   => $this->getJobDetails->getDesignation->name .
                ' (' . $this->getJobDetails->getDesignation->getDepartment->name . ')',
            'time_in'       => $this->getTimein(date('Y-m-d'), $this->id),
            'image'         => $this->image,
            'location'      => $this->location,
            'longitude'     => $this->longitude,
            'latitude'      => $this->latitude,
            'location_address' => $this->getCoordinates($this->longitude, $this->latitude),
            'break_status' => $this->getBreakStatus(),
            //'breaks_status' => $this->getBreakStatus($this->id, $this->getJobDetails->getDesignation
                //->getDepartment->id),
            'status'        => $this->statusAttendance(),
            'hours_worked'   => $this->getHoursWorked(),
            'attendance_id' => $this->attendance_id,
            'time_out' => $this->time_out != null ? date('h:i A', strtotime($this->time_out)) : null,
            'status_work' => $this->getStatusWork()
        ];
    }


    public function getTimein($date, $employee_id)
    {
        $attendance = Attendance::where('employee_id', $employee_id)
        ->whereDate('start_time', $date)->first();
        if ($attendance) {
            $this->status = true;
            $this->attendance = $attendance;
            $this->attendance_id = $attendance->id;
            $this->start_time = $attendance->start_time;
            $this->location = $attendance->location;
            $this->time_out = $attendance->end_time;
            $this->longitude = $attendance->longitude;
            $this->latitude = $attendance->latitude;
            $this->image = $attendance->image_path;
            return 'Started At ' . date('h:i A', strtotime($attendance->start_time));
        }
        return null;
    }

    public function getStatusWork()
    {
        $status = "";
        if ($this->attendance) {
            $break = $this->attendance->getAttendanceBreak()->orderBy('created_at', 'DESC')->first();
            if ($break) {
                if ($break->type == 0) {
                    $status = 'On Break';
                    return $status;
                }
            }
            
            if ($this->attendance->time_out == null) {
                $status = 'End Shift';
            } else {
                $status = 'At Work';
            }
        } else {
            $status = 'No Time-in';
        }

        return $status;
    }

    public function getBreakStatus()
    {
        if ($this->attendance) {
            $break = $this->attendance->getAttendanceBreak()->orderBy('created_at', 'DESC')->first();
            if ($break) {
                return $break->type == 0;
            }
        }

        return false;
    }

    // public function getBreakStatus($dept_id, $employee_id)
    // {
    //     if ($this->attendance) {
    //         // $schedule = Schedule::whereJsonContains('departments', $dept_id)
    //         //     ->where(function ($exceptions) use ($employee_id) {
    //         //         $exceptions->whereJsonDoesntContain('exceptions', $employee_id)
    //         //         ->orWhere('exceptions', null);
    //         //     })->first();
    //         $schedule = Schedule::where('departments', 'like', "%$dept_id%")
    //             ->where(function ($exceptions) use ($employee_id) {
    //                 $exceptions->where('exceptions', 'not like', "%$employee_id%")
    //                 ->orWhere('exceptions', null);
    //             })->first();
    //         return $schedule ?
    //         TimelogBreakResource::collection($schedule->getBreaks()->select('hr_schedule_breaks.*')
    //         ->addSelect(DB::raw($this->attendance->id . ' as attendance_id'))->get()) : null;
    //     } else {
    //         return null;
    //     }
    // }

    public function getHoursWorked()
    {
        if ($this->status) {
            $datetimeObj1 = new DateTime(date('H:i'));
            $datetimeObj2 = new DateTime(date('H:i', strtotime($this->start_time)));
            $interval =  $datetimeObj1->diff($datetimeObj2);
            return $interval->format('%H:%I');
        } else {
            return '0:00';
        }
    }

    public static function msgResponse($type, $description)
    {
        return [ "error" => $type, "message" => $description];
    }

    public function statusAttendance()
    {
        $status = true;
        if ($this->attendance) {
            if ($this->attendance->end_time != null) {
                $status = false;
            }
            // if (count($this->attendance->getAttendanceBreak) >  0) {
            //     foreach ($this->attendance->getAttendanceBreak as $key => $value) {
            //         if ($value->type == 0) {
            //             $break = AttendanceBreak::where('break_id', $value->break_id)
            //                 ->where('attendance_id', $value->attendance_id)->where('type', 1)->first();
            //             if ($break) {
            //                 $status = false;
            //             }
            //             // } else {
            //             //     $status = false;
            //             // }
            //         }
            //     }
            // } elseif ($this->attendance->end_time != null) {
            //     return false;
            // }
        } else {
            $status = false;
        }

        return $status;
    }

    /**
     * get Coordinates user location
     *
     * @param string $lat
     * @param string $long
     *
     * @return void
     */
    public function getCoordinates(
        string $long = null,
        string $lat = null
    ) {
        $coordinate = null;

        if ($lat != null && $long != null) {
            $coordinate = $this->googleMap($long, $lat);
        }

        return $coordinate;
    }

    /**
     * get Coordinates via google map
     *
     * @param string $address
     *
     * @return void
     */
    public function googleMap(string $long, string $lat)
    {
        if ($lat != null && $long != null) {
            $googleAPIURL = 'https://maps.google.com/maps/api/geocode/json?latlng=' . $lat . ',' . $long . '&key=AIzaSyBTh1yXElPijMym4BL3buDO09WOf7DGw9U';
            // $googleAPIURL = env('GOOGLE_MAP') .
            // '?latlng=' . $lat . ',' . $long .
            // '&key=' . env('API_KEY');
            $json = file_get_contents($googleAPIURL);

            $json = json_decode($json);

            if ($json->{'results'} != []) {
                return $json->{'results'}[0]->{'formatted_address'};
            }
        }
    }
}

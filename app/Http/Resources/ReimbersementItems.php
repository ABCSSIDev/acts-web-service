<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReimbersementItems extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reimbursement_id' => $this->reimbursement_id,
            'date' => date('F d, Y',strtotime($this->date)),
            'edit_date' => $this->date,
            'type' => $this->type,
            'particulars' => $this->particulars,
            'receipt_number' => $this->receipt_number,
            'amount' => $this->amount,
        ];
    }
}

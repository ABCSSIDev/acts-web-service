<?php

namespace App\Http\Resources;

use App\Http\Resources\Log as LogResource;
use Illuminate\Http\Resources\Json\JsonResource;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class ThirteenthMonth extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee->last_name . ', ' . $this->employee->first_name,
            'period' => $this->period,
            'thirteenth_month_year' => $this->thirteenth_month_year,
            'category' => $this->category,
            'total_months' => $this->total_months,
            'hours_render' => $this->hours_render,
            'amount' => $this->amount,
            'status' => model_approval_status(check_approver($this)),
            'history_logs' => LogResource::collection($this->logs()->orderBy('created_at', 'ASC')->get())
        ];
    }
}

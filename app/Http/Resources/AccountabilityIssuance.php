<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountabilityIssuance extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'requisition_id'    => $this->requisition_id, 
            'employee'          => $this->employee, 
            'from_employee'     => $this->fromEmployee, 
            'type'              => $this->type, 
            'date'              => date('F d, Y', strtotime($this->date)),
            'item_count'        => count($this->issuanceItems),
            "created"           => $this->logs()->oldest()->first(),
            "issuance_items"    => $this->issuanceItems
        ];
    }

    public static function errorResponse($description)
    {
        return [ "error" => true, "desc" => $description];
    }
}


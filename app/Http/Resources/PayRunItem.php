<?php

namespace App\Http\Resources;

use App\EmployeeSalary;
use App\Adjustment;
use App\Deduction;
use DB;
use App\Http\Resources\EmployeeSalary as SalaryResource;
use App\Http\Resources\PayRunOvertime as PayRunItemOvertimeResource;
use App\PHICTemplates;
use App\SSSTemplateContributions;
use Illuminate\Http\Resources\Json\JsonResource;

class PayRunItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->getEmployee->first_name . ' ' . $this->getEmployee->last_name,
            'pay_date' => date('d', strtotime($this->payRun->pay_date)),
            'pay_run_id' => $this->pay_run_id,
            'total_late_absent_hours' => $this->total_late_absent_hours,
            'billable_leave_hours' => $this->billable_leave_hours,
            'non_billable_leave_hours' => $this->non_billable_leave_hours,
            'total_billable_hours' => $this->total_billable_hours,
            'total_non_billable_hours' => $this->total_non_billable_hours,
            'ot' => PayRunItemOvertimeResource::collection($this->payrollOt),
            'rate' => $this->getRate($this->employee_id),
            'adjustments' => $this->getAdjustments($this->payRun->start_period, $this->payRun->end_period, $this->employee_id),
            'deductions' => $this->getDeductions($this->payRun->start_period, $this->payRun->end_period, $this->employee_id),
            '_parse_deductions' => number_format($this->getDeductions($this->payRun->start_period, $this->payRun->end_period, $this->employee_id), 2),
            '_parse_adjustments' => number_format($this->getAdjustments($this->payRun->start_period, $this->payRun->end_period, $this->employee_id), 2),
            'philhealth' => $this->getPhilHealthContri($this->getRate($this->employee_id), date('Y', strtotime($this->payRun->start_period)))
        ];
    }

    public function getRate($employeeId) {
        $rate = EmployeeSalary::where('employee_id', $employeeId)->first();
        if ($rate) {
            return new SalaryResource($rate);
        }
        return null;
    }

    public function getPhilHealthContri($rate, $year) {
        $phic = PHICTemplates::where('year_effective', $year)->first();
        if (!$phic) {
            return false;
        }
        if ($rate) {
            $total = (($rate->monthly_salary == null ? $rate->daily_monthly_rate : $rate->monthly_salary)
            * ($phic->premium_rate / 100)) / 2;
            return round($total, 2);
        }
        
        return 0;
    }

    public function getAdjustments($start, $end, $employee_id)
    {
        $adjustments = Adjustment::where('employee_id', $employee_id)
        ->select('amount')
        ->where('approved_status', 1)
        ->whereBetween(
            DB::raw('applicable_date'),
            [
                date('Y-m-d', strtotime($start)),
                date('Y-m-d', strtotime($end))
            ]
        )
        ->sum('amount');
        return $adjustments;
    }

    public function getDeductions($start, $end, $employee_id)
    {
        $deductions = Deduction::where('employee_id', $employee_id)
        ->select('amount', 'cutoff')
        ->where('approved_status', 1)
        ->whereBetween(
            DB::raw('effective_date'),
            [
                date('Y-m-d', strtotime($start)),
                date('Y-m-d', strtotime($end))
            ]
        )
        ->orwhereBetween(
            DB::raw('effective_until'),
            [
                date('Y-m-d', strtotime($start)),
                date('Y-m-d', strtotime($end))
            ]
        )
        ->get();

        $deductions_total = 0;

        foreach ($deductions as $deduction) {
            if ($deduction->cutoff > 0) {
                $deductions_total += $deduction->amount / $deduction->cutoff;
            }
        }

        return $deductions_total;
    }
}

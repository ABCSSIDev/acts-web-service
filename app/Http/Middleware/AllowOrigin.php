<?php
/**
 * Created by PhpStorm.
 * User: Ad
 * Date: 7/27/2020
 * Time: 11:05 AM
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;

class AllowOrigin
{
    public function handle($request,Closure $next){
        $response = $next($request);

        $response = $response instanceof RedirectResponse ? $response : response($response);

        return $response->header('Access-Control-Allow-Origin','*');
    }
}
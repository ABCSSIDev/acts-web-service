<?php

namespace App\Http\Middleware;

use App\Module;
use Closure;
use Illuminate\Support\Facades\Route;

class Restriction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $module_name = self::stripString(request()->route()->getName());
        $module = Module::where('name', 'LIKE', '%' . $module_name . '%')->first();
        // return response()->json(['error' => 'Permission is not granted'])->setStatusCode(418);
        return $next($request);
    }

    public static function stripString($string){
        $spliced = explode('.', $string);
        $raw = explode($spliced[0], '_'); 
        array_push($raw, explode($spliced[0], '-'));
        $replace_spaces =$spliced[0];
        if($raw){
            $replace_spaces = str_replace("_"," ",$spliced[0]);
            $replace_spaces = str_replace("-"," ",$replace_spaces);
        }
        return $replace_spaces;
    }
}

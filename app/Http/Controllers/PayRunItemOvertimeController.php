<?php

namespace App\Http\Controllers;

use App\Http\Resources\PayRunOvertime as PayRunItemOvertimeResource;
use App\PayRunItem;
use App\PayRunItemOvertime;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PayRunItemOvertimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store
     *
     * @param Request $request
     * @param string $payrunItemID
     *
     * @return JsonResource
     */
    public function store(
        Request $request,
        string $payrunItemID
    ): JsonResource {
        $request->merge(['pay_run_item_id' => $payrunItemID]);
        $payroll_ot = PayRunItemOvertime::create($this->payload($request));

        return new PayRunItemOvertimeResource($payroll_ot);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PayRunItemOvertime  $payRunItemOvertime
     * @return \Illuminate\Http\Response
     */
    public function show(string $payrunItemId)
    {
        $payrollItemOt = PayRunItem::findOrFail($payrunItemId)
            ->payrollOt()
            ->get();

        return PayRunItemOvertimeResource::collection($payrollItemOt);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PayRunItemOvertime  $payRunItemOvertime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PayRunItemOvertime $payRunItemOvertime)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PayRunItemOvertime  $payRunItemOvertime
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayRunItemOvertime $payRunItemOvertime)
    {
        //
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    private function payload(Request $request)
    {
        return $this->validate(
            $request,
            [
                'pay_run_item_id' => 'required',
                'ot_type_id' => 'required',
                'billable_hours' => 'required',
                'non_billable_hours' => 'nullable'
            ]
        );
    }
}

<?php

namespace App\Http\Controllers;

use App\ReturnItems;
use Illuminate\Http\Request;

class ReturnItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReturnItems  $returnItems
     * @return \Illuminate\Http\Response
     */
    public function show(ReturnItems $returnItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReturnItems  $returnItems
     * @return \Illuminate\Http\Response
     */
    public function edit(ReturnItems $returnItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReturnItems  $returnItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReturnItems $returnItems)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReturnItems  $returnItems
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReturnItems $returnItems)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeTimesheet;
use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\EmployeeTimesheet as EmployeeTimesheetResource;
use App\Http\Resources\Log as LogResource;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use function App\Log\log_created;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeTimesheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $timesheet_array = array();
        $group_date = DB::table('hr_employee_timesheets')->select(DB::raw('id as id'),DB::raw('DATE(timesheet_date) as date'), DB::raw('count(*) as count'), DB::raw('sum(rendered_hours) as total'))->groupBy('date')->where('deleted_at',Null)->get();
        foreach($group_date as $key => $timesheet_date){
            $timesheets = EmployeeTimesheet::whereDate('timesheet_date', $timesheet_date->date)->where('deleted_at',Null)->get();
            $timesheet_array[$key] = ['id'=> $timesheet_date->id ,'Date' => date('F d, Y',strtotime($timesheet_date->date)), 'total_rendered_hours'=> $timesheet_date->total, 'timesheets' => EmployeeTimesheetResource::collection($timesheets)];
        }
        return response()->json($timesheet_array); 
    }

    public function create(){
        return response()->json([
            'employees' => Employee::select('id', 'first_name', 'last_name')->get()
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $timesheet = new EmployeeTimesheet;
            $timesheet->employee_id = $request->input('employee_id');
            $timesheet->timesheet_date = $request->input('timesheet_date');
            $timesheet->start_time = $request->input('start_time');
            $timesheet->description = $request->input('description');
            $timesheet->rendered_hours = $request->input('rendered_hours');
            $timesheet->is_billable = ($request->input('is_billable') == null ? 0 : $request->input('is_billable'));
            $timesheet->save();
            log_created($timesheet); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new EmployeeTimesheetResource($timesheet);
        } catch (\Exception $exception) {
            return EmployeeTimesheetResource::errorResponse($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeTimesheet  $employeeTimesheet
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return (new EmployeeTimesheetResource(EmployeeTimesheet::find($id)))->additional([
            'approver' => check_approver(EmployeeTimesheet::find($id)), //option if user is approver and already approved
            'approved_status' =>  model_approval_status(check_approver(EmployeeTimesheet::find($id))),   //pending, waiting order approver,approved
            'history_logs' => LogResource::collection(EmployeeTimesheet::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeTimesheet  $employeeTimesheet
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $employee = Employee::all();
        $employee_timesheet = EmployeeTimesheet::findOrFail($id);
        $timesheet = new EmployeeTimesheetResource($employee_timesheet);
        $collections = [
            'timesheet' => $timesheet,
            'employee' => $employee,
        ];
        return $collections;
    }

    public function update(Request $request, $id)
    {
        try{
            $employee_timesheet = EmployeeTimesheet::where('id', $id)->update([ 
                'timesheet_date' => $request->input('timesheet_date'),
                'description' => $request->input('description'),
                'start_time' => $request->input('start_time'),
                'rendered_hours' => $request->input('rendered_hours'),
                'is_billable' => ($request->input('is_billable') == null ? 0 : $request->input('is_billable')),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            // log_created($employee_timesheet); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            $query = EmployeeTimesheet::where('id', $id)->first();
            return new EmployeeTimesheetResource($query);
            
        }catch(\Exception $exception){
            return EmployeeTimesheetResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeTimesheet  $employeeTimesheet
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        try {
            $EmployeeTimesheetDelete = EmployeeTimesheet::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return EmployeeTimesheetResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return EmployeeTimesheetResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

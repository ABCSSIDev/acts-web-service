<?php

namespace App\Http\Controllers;

use App\PermissionModule;
use Illuminate\Http\Request;

class PermissionModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PermissionModule  $permissionModule
     * @return \Illuminate\Http\Response
     */
    public function show(PermissionModule $permissionModule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PermissionModule  $permissionModule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PermissionModule $permissionModule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PermissionModule  $permissionModule
     * @return \Illuminate\Http\Response
     */
    public function destroy(PermissionModule $permissionModule)
    {
        //
    }
}

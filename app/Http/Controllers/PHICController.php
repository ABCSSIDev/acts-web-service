<?php

namespace App\Http\Controllers;

use App\Http\Resources\PHICTemplateResource;
use App\PHICTemplates;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;
use PHPUnit\Util\Json;

class PHICController extends Controller
{
    //

    /**
     * Shows PHIC Contribution Collection
     *
     * @return response
     */
    public function index(): ResourceCollection
    {
        $templates = PHICTemplates::all();
        return PHICTemplateResource::collection($templates);
    }

    /**
     * Add PHIC Contribution Details
     *
     * @return respone
     * @param Request $request
     */
    public function store(Request $request): JsonResource
    {
        $validate = $this->validation($request);
        $phic_template = PHICTemplates::create($validate);
        return new PHICTemplateResource($phic_template);
    }

    /**
     * Show PHIC Contribution Details
     *
     * @param int $id
     *
     * @return response
     */
    public function show($id): JsonResource
    {
        $phic_template = PHICTemplates::findORFail($id);
        return new PHICTemplateResource($phic_template);
    }

    /**
     * Update PHIC Contribution Details
     *
     * @param Request $request
     * @param int $id
     *
     * @return repsonse
     */
    public function update(Request $request, $id): JsonResource
    {
        $validation = $this->validation($request);
        $phic_template = PHICTemplates::findORFail($id);
        $phic_template->update($validation);
        return new PHICTemplateResource($phic_template);
    }

    /**
     * Delete PHIC Contribution Details
     *
     * @param int $id
     *
     * @return response
     */
    public function destroy($id): Response
    {
        $phic_template = PHICTemplates::findORFail($id);
        $phic_template->delete();
        return response(null, 204);
    }

    public function validation(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'year_effective' => 'required|numeric',
                'premium_rate' => 'required|numeric'
            ]
        );
    }
}

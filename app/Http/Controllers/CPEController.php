<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Inventories;
use App\Unit;
use App\Categories;
use App\InventoriesNames;
use App\Http\Resources\Inventories as InventoriesResource;

use function App\Log\log_created;
use function App\Helpers\save_image;

class CPEController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $Inventories = Inventories::where('type', 1)->get();
        return InventoriesResource::collection($Inventories);
    }
    public function create()
    {
        $response = array();
        $Unit =  Unit::all();
        $Categories =  Categories::all();
        $response['Unit'] = $Unit;
        $response['Categories'] = $Categories;

        return response()->json($response);
    }

    public function store(Request $request)
    {
        try {
            $data =  $request->input('item');
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $result = InventoriesNames::where('description', 'LIKE', '%'. $value['description']. '%')->where('mat_code',$value['mat_code'])->first();
                    if (!$result) {
                        $InventoriesNames = New InventoriesNames;
                        $InventoriesNames->category_id = $value['category_id'];
                        $InventoriesNames->description = $value['description'];
                        $InventoriesNames->mat_code = $value['mat_code'];
                        $InventoriesNames->save();

                        $Inventories = New Inventories;
                        $Inventories->inventory_description_id = $InventoriesNames->id;
                        $Inventories->unit_id = $value['unit_id'];
                        $Inventories->category_id = $value['category_id'];
                        $Inventories->image = $value['image'];
                        $Inventories->quantity = $value['quantity'];
                        $Inventories->unit_price = $value['unit_price'];
                        $Inventories->min_stock = $value['min_stock'];
                        $Inventories->type = 1;
                        $Inventories->save();
                        // log_created($Inventories);
                    } else {
                        // return InventoriesResource::MsgResponse(true,'this Inventories already use!');
                    }
                }
                return InventoriesResource::MsgResponse(false, 'Inventories Successfully Created!');
            }
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }
    public function saveImage(Request $request)
    {
        try{
            $count =  Inventories::where('type', 1)->count();
            if (request()->hasFile('image')) {//save image
                $file_name = 'Inventories/CPE'.time().$count.'s.jpg';
                $file = \request()->file('image');
                $array = array();
                if (save_image($file, $file_name)) {
                    $response['image'] = $file_name;
                    $response['image_display'] = asset(Storage::url($file_name));
            
                    return response()->json($response);
                }
            }
        }catch (\Exception $exception){
            return InventoriesResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function show($id)
    {
        $Inventories = Inventories::findOrFail($id);
        return new InventoriesResource($Inventories);
    }
    public function edit($id)
    {
        $response = array();
        $Unit =  Unit::all();
        $Categories =  Categories::all();
        $response['Unit'] = $Unit;
        $response['Categories'] = $Categories;
        $response['data'] = new InventoriesResource(Inventories::findOrFail($id));
        return response()->json($response);
    }
    public function update(Request $request, $id)
    {
        // return $request;
        try {
            $result = InventoriesNames::where('description', 'LIKE', '%' . $request->input('description'). '%')->where('id','!=', $request->input('inventoies_name_id'))->where('mat_code',$request->input('mat_code'))->first();
            if (!$result) {
                if ($request->input('imageval') == 1) {
                    if (request()->hasFile('image')) {//save image
                        $count = Inventories::where('type',1)->count();
                        $file_name = 'Inventories/CPE'.time().$count.'s.jpg';
                        $file = \request()->file('image');
                        $array = array();
                        if (save_image($file, $file_name)) {
                            $file_name = $file_name;
                        }
                    }else{
                        $file_name = NULL;
                    }
                }else{
                    $file_name = $request->input('image');
                }
                $InventoriesNames = InventoriesNames::where('id', $request->input('inventoies_name_id'))->update([
                    'category_id' => $request->input('category'),
                    'description' => $request->input('description'),
                    'mat_code' => $request->input('mat_code'),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
                $Inventories = Inventories::where('id', $id)->update([
                    'unit_id' => $request->input('unit'),
                    'category_id' => $request->input('category'),
                    'image' => $file_name,
                    'quantity' => $request->input('quantity'),
                    'unit_price' => $request->input('unit_price'),
                    'min_stock' => $request->input('min_stock'),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
                return new InventoriesResource(Inventories::findOrFail($id));
            }
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }
    public function validationInventory($inventory,$modcode)
    {
        $result = InventoriesNames::where('description', 'LIKE', '%'. $inventory. '%')->where('mat_code',$modcode)->first();
        if($result){
            $msg = 'true';
        }else{
            $msg = 'false';
        }
        return $msg;
    }
    public function destroy($id)
    {
        try {
            $Inventories = Inventories::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return InventoriesResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return InventoriesResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Department;
use App\Designation;
use App\Http\Resources\Employee as EmployeeResource;
use App\Inventories;

use App\InventoriesNames;
use App\Unit;
use function App\Helpers\change_date_format;
use function App\Helpers\save_employee;
use function App\Helpers\save_employee_bir;
use function App\Helpers\save_employee_contacts;
use function App\Helpers\save_employee_hdmf;
use function App\Helpers\save_employee_phic;
use function App\Helpers\save_employee_sss;
use function App\Helpers\save_job_details;
use function App\Helpers\save_user_information;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function __invoke()
    {
        switch (request('type')) {
            case 'employees':
                return $this->importEmployees(request());
                break;
            case 'operation-material':
                return $this->importOperationMaterials(request());
                break;
            case 'premise-equipment':
                return $this->importCPEMaterials(request());
                break;
            default:
                break;
        }
    }
    
    public function importEmployees($request)
    {
        for ($x = 0; $x < $request['count']; $x++) {
            try {
                // $check = \App\Employee::where('id', $request['EmployeeID*' . "_" . $x])->first();
                // if (!$check) {
                    $employee_array = array();
                    $employee_array['employee_no']       = $request['EmployeeID*' . "_" . $x];
                    $employee_array['first_name']        = $request['FirstName*' . "_" . $x];
                    $employee_array['middle_name']       = $request['MiddleName' . "_" . $x];
                    $employee_array['last_name']         = $request['LastName*' . "_" . $x];
                    $employee_array['gender']            = $request['Gender*' . "_" . $x] == 'M' ? 'male' : 'female';
                    $employee_array['birth_date']        = change_date_format('Y-m-d', $request['BirthDate*' . "_" . $x]);
                    $employee_array['current_address']   = $request['CurrentAddress*' . "_" . $x];
                    $employee_array['permanent_address'] = $request['PermanentAddress*' . "_" . $x];
                    $employee_array['marital_status']    = $request['CivilStatus*' . "_" . $x];
                    $employee_info = save_employee($employee_array);

                    // $user_array = array();
                    // $user_array['username'] = $request['UserName*' . "_" . $x];
                    // $user_array['name'] = $request['FirstName*' . "_" . $x] . ' ' . $request['LastName*' . "_" . $x];
                    // $user_array['password'] = bcrypt('password123');
                    // $user_array['role'] = \App\Role::getRoleIDByName($request['Role*' . "_" . $x]);
                    // $user_array['permission'] = \App\Permission::getPermissionIdByName($request['Permission*' .
                    // "_" . $x]);
                    // $user_array['employee_id'] = $employee_info->id;
                    
                    // if ($request['PhoneNumber*' . "_" . $x] == '') {
                        $employee_contacts = array();
                        $employee_contacts['employee_id'] = $employee_info->id;
                        $employee_contacts['email'] = $request['Email*' . "_" . $x];
                        $employee_contacts['phone_number'] = $request['PhoneNumber*' . "_" . $x];
                        $employee_contacts['telephone_number'] = $request['TelephoneNumber*' . "_" . $x];
                        $employee_contacts['work_phone'] = $request['WorkPhone*' . "_" . $x];
                        $employee_contacts['emergency_contact'] = $request['EmergencyContact*' . "_" . $x];
                        $employee_contacts['emergency_contact_number'] = $request['ContactNumber*' . "_" . $x];
                        save_employee_contacts($employee_contacts);
                    // }

                    $job_detail = array();
                    $job_detail['employee_id'] = $employee_info->id;
                    $department_id = $this->getDepartment($request['Department*' . "_" . $x]);
                    $job_detail['designation_id'] = $this->getDesignation($department_id, $request['Designation*' . "_" . $x]);
                    $job_detail['employee_type_id'] = \App\EmployeeType::getEmployeeTypeIDByName($request[
                        'EmployeeType*' . "_" . $x]);
                    $job_detail['employee_status'] = ($request['EmployeeStatus*' . "_" . $x] == 'Active' ? 1 : 0);
                    $job_detail['source_of_hire'] = 1;
                    $job_detail['joined_date'] = change_date_format('Y-m-d', $request['DateHired*' . "_" . $x]);

                    $benefits_sss = array();
                    $benefits_sss['employee_id'] = $employee_info->id;
                    $benefits_sss['membership_code'] = $request['SSS#*' . "_" . $x];
                    $benefits_sss['joined_date'] = null;
                    $benefits_sss['separation_date'] = null;
                    $benefits_phic = array();
                    $benefits_phic['employee_id'] = $employee_info->id;
                    $benefits_phic['membership_code'] = $request['PHIC#*' . "_" . $x];
                    $benefits_phic['joined_date'] = null;
                    $benefits_phic['separation_date'] = null;
                    $benefits_hdmf = array();
                    $benefits_hdmf['employee_id'] = $employee_info->id;
                    $benefits_hdmf['membership_code'] = $request['HDMF#*' . "_" . $x];
                    $benefits_hdmf['joined_date'] = null;
                    $benefits_hdmf['separation_date'] = null;
                    $benefits_bir = array();
                    $benefits_bir['employee_id'] = $employee_info->id;
                    $benefits_bir['membership_code'] = $request['TIN#*' . "_" . $x];
                    $benefits_bir['joined_date'] = null;
                    $benefits_bir['separation_date'] = null;
                    save_employee_sss($benefits_sss);
                    save_employee_phic($benefits_phic);
                    save_employee_bir($benefits_bir);
                    save_employee_hdmf($benefits_hdmf);
                    //save_user_information($user_array);
                    save_job_details($job_detail);
                //}
            } catch (Exception $ex) {
                return EmployeeResource::msgResponse(true, $ex->getMessage());
            }
        }
        return  response()->json(array(
            'error' => false,
            'message' => "Data imported success!"
        ));
    }

    /**
     * getting department id
     *
     * @param [type] $deptName
     *
     * @return string
     */
    public function getDepartment($deptName): string
    {
        $department = Department::where('name', 'like', $deptName)->first();
        if ($department) {
            return $department->id;
        } else {
            $new_dept = new Department();
            $new_dept->name = $deptName;
            $new_dept->status = 1;
            $new_dept->save();
            return $new_dept->id;
        }
    }
    /**
     * getting designation id with two params
     *
     * @param [type] $deptId
     * @param [type] $designationName
     *
     * @return string
     */
    public function getDesignation($deptId, $designationName): string
    {
        $designation = Designation::where('department_id', $deptId)
                            ->where('name', 'like', $designationName)->first();
        if ($designation) {
            return $designation->id;
        } else {
            $new_designation = new Designation();
            $new_designation->department_id = $deptId;
            $new_designation->name = $designationName;
            $new_designation->status = 1;
            $new_designation->save();
            return $new_designation->id;
        }
    }

    public function importOperationMaterials($request)
    {
        for ($index = 0; $index < $request['count']; $index++) {
            try {
                $operation_material = new Inventories();
                $operation_material->inventory_description_id = $this->checkDescription(
                    $request['ItemDescription*' . "_" . $index],
                    $request['Category*' . "_" . $index],
                    $request['MatCode*' . "_" . $index]
                );
                $operation_material->unit_id = $this->checkUnit($request['Unit*' . "_" . $index]);
                $operation_material->category_id = $this->checkCategory($request['Category*' . "_" . $index]);
                $operation_material->type = 0;
                $operation_material->quantity = $request['Quantity*' . "_" . $index];
                $operation_material->unit_price = $request['UnitPrice' . "_" . $index];
                $operation_material->min_stock = $request['MinimumStock*' . "_" . $index];
                $operation_material->save();
            } catch (Exception $ex) {
                return response()->json(array(
                    'error' => true,
                    'message' => $ex->getMessage()
                ));
            }
        }
        return  response()->json(array(
            'error' => false,
            'message' => "Data imported success!"
        ));
    }

    public function checkDescription($description, $category, $matCode): string
    {
        $cat = $this->checkCategory($category);
        $desc = InventoriesNames::where('description', 'like', $description)
            ->where('category_id', $cat)
            ->where('mat_code', $matCode)
            ->first();
        
        if ($desc) {
            return $desc->id;
        } else {
            $new_description = new InventoriesNames();
            $new_description->category_id = $cat;
            $new_description->description = $description;
            $new_description->mat_code = $matCode;
            $new_description->save();
            return $new_description->id;
        }
    }

    public function checkCategory($category)
    {
        $cat = Categories::where('category_name', 'like', $category)->first();
        if ($cat) {
            return $cat->id;
        } else {
            $new_cat = new Categories();
            $new_cat->category_id = null;
            $new_cat->category_name = $category;
            $new_cat->enabled = 1;
            $new_cat->save();
            return $new_cat->id;
        }
    }

    public function checkUnit($units)
    {
        $unit = Unit::where('unit_name', 'like', $units)
            ->whereOr('unit_code', 'like', $units)->first();
        if ($unit) {
            return $unit->id;
        } else {
            $new_unit = new Unit();
            $new_unit->unit_name = $units;
            $new_unit->unit_code = $units;
            $new_unit->enabled = 1;
            $new_unit->save();
            return $new_unit->id;
        }
    }

    public function importCPEMaterials($request)
    {
        for ($index = 0; $index < $request['count']; $index++) {
            try {
                $operation_material = new Inventories();
                $operation_material->inventory_description_id = $this->checkDescription(
                    $request['ItemDescription*' . "_" . $index],
                    $request['Category*' . "_" . $index],
                    $request['MatCode*' . "_" . $index]
                );
                $operation_material->unit_id = $this->checkUnit($request['Unit*' . "_" . $index]);
                $operation_material->category_id = $this->checkCategory($request['Category*' . "_" . $index]);
                $operation_material->type = 1;
                $operation_material->quantity = $request['Quantity*' . "_" . $index];
                $operation_material->unit_price = $request['UnitPrice' . "_" . $index];
                $operation_material->min_stock = $request['MinimumStock*' . "_" . $index];
                $operation_material->save();
            } catch (Exception $ex) {
                return response()->json(array(
                    'error' => true,
                    'message' => $ex->getMessage()
                ));
            }
        }
        return  response()->json(array(
            'error' => false,
            'message' => "Data imported success!"
        ));
    }
}

<?php

namespace App\Http\Controllers;

use App\AccountabilityRequisition;
use App\Assets;
use App\Employee;
use App\Http\Resources\AccountabilityRequisition as ResourcesAccountabilityRequisition;
use App\RequisitionItems;
use Illuminate\Http\Request;
use function App\Log\log_created;
use App\Inventories;

class AccountabilityRequisitionController extends Controller
{
    
    public function __construct()
    {
       $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ResourcesAccountabilityRequisition::collection( AccountabilityRequisition::all() );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            // 'assets'    => Assets::all(),
            'inventories'    => Inventories::where('type', 2)->get()
            // 'employees' => Employee::role(AccountabilityRequisition::class)->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $requisition_query = AccountabilityRequisition::create([
                'employee_id'       => $request->employee_id,
                'from_employee_id'  => $request->from_employee_id,
                'type'              => $request->type,
                'date'              => $request->date
            ]);

            if ($request->has('requisition_items')) {
                foreach ($request->requisition_items as $item) {
                    $requisition_item                   = new RequisitionItems();
                    $requisition_item->requisition_id   = $requisition_query->id;
                    $requisition_item->asset_id     = $item['asset_id'];
                    $requisition_item->quantity         = $item['quantity'];
                    $requisition_item->save();
                }
            }
            // log_created($requisition_query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new ResourcesAccountabilityRequisition($requisition_query);
        } catch (\Exception $exception) {
            return ResourcesAccountabilityRequisition::errorResponse($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $requisition = AccountabilityRequisition::findOrFail($id);
            return new ResourcesAccountabilityRequisition($requisition);
        }catch (\Exception $exception) {
            return ResourcesAccountabilityRequisition::errorResponse($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AccountabilityRequisition  $accountabilityRequisition
     * @return \Illuminate\Http\Response
     */
    public function edit(AccountabilityRequisition $accountabilityRequisition)
    {
        return response()->json([
            'data'      => $accountabilityRequisition,
            'assets'    => Assets::all(),
            'employees' => Employee::role(AccountabilityRequisition::class)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AccountabilityRequisition  $accountabilityRequisition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccountabilityRequisition $accountabilityRequisition)
    {

        try{
            $updated = AccountabilityRequisition::where('id', $accountabilityRequisition->id)->update(
                [$request->column_name => $request->new_value]
            );
            if(!empty($updated)){
                $query = AccountabilityRequisition::where('id', $accountabilityRequisition->id)->first();
                return new ResourcesAccountabilityRequisition($query);
            }
        }catch(\Exception $exception){
            return ResourcesAccountabilityRequisition::errorResponse(true,$exception->getMessage());
        }

    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAllColumns(Request $request, $id)
    {
        try {
            $update_query = AccountabilityRequisition::where('id', $id)->update([
                'employee_id'       => $request->employee_id, 
                'from_employee_id'  => $request->from_employee_id, 
                'type'              => $request->type, 
                'date'              => $request->date
            ]);

            if(!empty($update_query)){
                $query = AccountabilityRequisition::where('id', $id)->first();
                return new ResourcesAccountabilityRequisition($query);
            }
        } catch (\Exception $exception) {
            return ResourcesAccountabilityRequisition::errorResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccountabilityRequisition  $accountabilityRequisition
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $delete_query = AccountabilityRequisition::destroy(explode(',', $id));
            return ResourcesAccountabilityRequisition::errorResponse('Successfully Deleted!');
        }
        catch(\Exception $exception){
            return ResourcesAccountabilityRequisition::errorResponse($exception->getMessage());
        }
    }
}

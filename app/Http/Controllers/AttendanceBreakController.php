<?php

namespace App\Http\Controllers;

use App\AttendanceBreak;
use Illuminate\Http\Request;

class AttendanceBreakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AttendanceBreak  $attendanceBreak
     * @return \Illuminate\Http\Response
     */
    public function show(AttendanceBreak $attendanceBreak)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AttendanceBreak  $attendanceBreak
     * @return \Illuminate\Http\Response
     */
    public function edit(AttendanceBreak $attendanceBreak)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AttendanceBreak  $attendanceBreak
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AttendanceBreak $attendanceBreak)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AttendanceBreak  $attendanceBreak
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttendanceBreak $attendanceBreak)
    {
        //
    }
}

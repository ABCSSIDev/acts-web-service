<?php

namespace App\Http\Controllers;

use App\RequisitionItems;
use Illuminate\Http\Request;

class RequisitionItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequisitionItems  $requisitionItems
     * @return \Illuminate\Http\Response
     */
    public function show(RequisitionItems $requisitionItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequisitionItems  $requisitionItems
     * @return \Illuminate\Http\Response
     */
    public function edit(RequisitionItems $requisitionItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequisitionItems  $requisitionItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequisitionItems $requisitionItems)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequisitionItems  $requisitionItems
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequisitionItems $requisitionItems)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Resources\Inventories as InventoriesResource;
use App\Http\Resources\Receiving as ResourcesReceiving;
use App\Inventories;
use App\OfficeSuppliesAndEquipment;
use App\Receiving;
use App\ReceivingItems;
use Illuminate\Http\Request;

class ReceivingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ResourcesReceiving::collection(Receiving::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return InventoriesResource::collection(Inventories::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only(['inventories', 'received_by_id', 'received_at', 'pr_reference_number']);

        $receiving = new Receiving();
        $receiving->received_by_id = $data['received_by_id'];
        $receiving->received_at = $data['received_at'];
        $receiving->purchase_request_id = $data['pr_reference_number'];
        $receiving->save();

        foreach ($data['inventories'] as $key => $inventory) {
            if (!empty($inventory['inventory_id'])) {
                $inventory_query = Inventories::find($inventory['inventory_id']);

                $receiving_item = new ReceivingItems();
                $receiving_item->inventory_id = $inventory['inventory_id'];
                $receiving_item->receiving_id = $receiving->id;
                $receiving_item->old_quantity = $inventory_query->quantity;
                $receiving_item->quantity = $inventory['quantity'];
                $receiving_item->save();

                $new_quantity = $inventory['quantity'] + $inventory_query->quantity;
                $inventory_query->update([
                    'quantity' => $new_quantity
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Receiving  $receiving
     * @return \Illuminate\Http\Response
     */
    public function show(Receiving $receiving)
    {
        return new ResourcesReceiving($receiving);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Receiving  $receiving
     * @return \Illuminate\Http\Response
     */
    public function edit(Receiving $receiving)
    {
        return new ResourcesReceiving($receiving);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Receiving  $receiving
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Receiving $receiving)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Receiving  $receiving
     * @return \Illuminate\Http\Response
     */
    public function destroy(Receiving $receiving)
    {
        //
    }
}

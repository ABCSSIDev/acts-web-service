<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Designation;
use App\Employee;
use App\Http\Resources\Candidate as CandidateResource;
use App\Http\Resources\Log as LogResource;
use App\JobOpening;
use Illuminate\Http\Request;
use function App\Log\log_created;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $Candidate = Candidate::all();
        return CandidateResource::collection($Candidate);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'unique:hr_candidates'
            ]);
            $new_candidate = new Candidate;
            $new_candidate->name = $request->input('name');
            $new_candidate->contact_number = $request->input('contact_number');
            $new_candidate->email = empty($request->input('email')) ? 'N/A' : $request->input('email');
            $new_candidate->address = $request->input('address');
            $new_candidate->source = $request->input('source');
            $new_candidate->recruiter_id = $request->input('recruiter_id');
            $new_candidate->potential_designation = $request->input('potential_designation');
            $new_candidate->job_opening_id = $request->input('job_opening_id');
            $new_candidate->save();
            log_created($new_candidate);
            return new CandidateResource($new_candidate);
        } catch (\Exception $exception) {
            return CandidateResource::errorResponse($exception->getMessage());
        }
    }

    public function create(){
        $employees = Employee::select('id', 'first_name', 'middle_name', 'last_name')->role(Candidate::class)->get();
        $designation = Designation::select('id', 'name')->get();
        $job_opening = JobOpening::select('id', 'name')->get();
        return response()->json([
            'sources' => $employees,
            'potential_designations' => $designation,
            'job_openings' => $job_opening
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $candidate = Candidate::findOrFail($id);
        return (new CandidateResource($candidate))->additional([
            'approver' => check_approver($candidate), //option if user is approver and already approved
            'approved_status' =>  model_approval_status(check_approver($candidate)),   //pending, waiting order approver,approved
            'history_logs' => LogResource::collection(Candidate::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::findOrFail($id);
        $designation = Designation::all();
        $job_opening = JobOpening::all();
        $employee_candidate = new CandidateResource($candidate);
        $employees = Employee::select('id', 'first_name', 'middle_name', 'last_name')->role(Candidate::class)->get();
        $collections = [
            'designation' => $designation,
            'job_opening' => $job_opening,
            'candidate' => $employee_candidate,
            'employees' => $employees,
        ];
        return $collections;
    }
    public function update(Request $request, $id)
    {
        try{
            $candidate = Candidate::where('id', $id)->update([
                'name' => $request->input('name'),
                'contact_number' => $request->input('contact_number'),
                'email' => empty($request->input('email')) ? 'N/A' : $request->input('email'),
                'address' => $request->input('address'),
                'source' => $request->input('source'),
                'recruiter_id' => $request->input('recruiter_id'),
                'potential_designation' => $request->input('potential_designation'),
                'job_opening_id' => $request->input('job_opening_id'),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            $query = Candidate::where('id', $id)->first();
            log_created($query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new CandidateResource($query);

        }catch(\Exception $exception){
            return CandidateResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
        try {
            $CandidateDelete = Candidate::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return CandidateResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return CandidateResource::MsgResponse(true,$exception->getMessage());
        }
    }

}

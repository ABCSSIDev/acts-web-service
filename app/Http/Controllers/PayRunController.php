<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\AttendanceBreak;
use App\Employee;
use App\EmployeeLeave;
use App\EmployeeOvertime;
use App\EmployeeTimesheet;
use App\EmployeeSalary;
use App\Helpers\Payruns\AttendanceHelper;
use App\Helpers\Payruns\EmployeeLeaveHelper;
use App\Helpers\Payruns\EmployeeOvertimeHelper;
use App\Helpers\Payruns\HolidayRestdayHelper;
use App\Helpers\Payruns\LateAbsenceHelper;
use App\Helpers\Payruns\OBHelper;
use App\Helpers\Payruns\PayRunHelper;
use App\Holiday;
use App\Http\Resources\EmployeeLeave as EmployeeLeaveResource;
use App\Http\Resources\EmployeeOvertime as EmployeeOvertimeResource;
use App\Http\Resources\OB as ResourcesOB;
use App\Http\Resources\OvertimeType as OvertimeTypeResource;
use App\Http\Resources\PayRun as PayRunResource;
use App\Http\Resources\RestdayHoliday as RestdayHolidayResource;
use App\JobDetail;
use App\Notifications\Payrun as PayrunNotification;
use App\OB;
use App\OvertimeType;
use App\PayRun;
use App\PayRunItem;
use App\PayRunItemOvertime;
use App\RestdayHoliday;
use App\Schedule;
use App\ScheduleBreak;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use function App\Log\log_created;

class PayRunController extends Controller
{
    /**
     * @var float
     */
    protected $total_hrs;

    /**
     * @var float
     */
    protected $total_ot_hrs;

    /**
     * @var int
     */
    protected $payable_days;

    /**
     * @var int
     */
    protected $total_employee;

    /**
     * @var PayRunHelper
     */
    private $payrunHelper;

    /**
     * @var AttendanceHelper
     */
    private $attendanceHelper;

    /**
     * @var LateAbsenceHelper
     */
    private $lateAbsenceHelper;

    /**
     * @var EmployeeLeaveHelper
     */
    private $employeeLeaveHelper;

    /**
     * @var EmployeeOvertimeHelper
     */
    private $employeeOvertimeHelper;

    /**
     * @var OBHelper
     */
    private $obHelper;

    /**
     * @var HolidayRestdayHelper
     */
    private $holidayRestdayHelper;

    /**
     * Construct API
     */
    public function __construct(
        PayRunHelper $payrunHelper,
        AttendanceHelper $attendanceHelper,
        LateAbsenceHelper $lateAbsenceHelper,
        EmployeeLeaveHelper $employeeLeaveHelper,
        EmployeeOvertimeHelper $employeeOvertimeHelper,
        OBHelper $obHelper,
        HolidayRestdayHelper $holidayRestdayHelper
    ) {
        $this->middleware('auth:api');

        $this->payrunHelper = $payrunHelper;
        $this->attendanceHelper = $attendanceHelper;
        $this->lateAbsenceHelper = $lateAbsenceHelper;
        $this->employeeLeaveHelper = $employeeLeaveHelper;
        $this->employeeOvertimeHelper = $employeeOvertimeHelper;
        $this->obHelper = $obHelper;
        $this->holidayRestdayHelper = $holidayRestdayHelper;
    }

    /**
     * Index
     *
     * @param Request $request
     *
     * @return array
     */
    public function index(Request $request): array
    {
        return $this->payrunHelper->getPayruns($request);
    }

    /**
     * Store
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function store(Request $request): JsonResource
    {
        $payroll = Payrun::create($this->payload($request));
        log_created($payroll);
        return new PayRunResource($payroll);
    }

    /**
     * Form Payroll
     *
     * @param string $start
     * @param string $end
     * @param string $pay_date
     *
     * @return void
     */
    public function getFormPayroll(string $start, string $end, string $pay_date)
    {
        $form_attendance = array();
        $ob = $this->getOfficialBusiness($start, $end);
        foreach ($ob as $key => $value) {
            $form_attendance[] = [
                "employee_id" => $value['employee_id'],
                "total_late_absent_hours" => 0.00,
                "billable_leave_hours" => 0.00,
                "non_billable_leave_hours" => 0.00,
                "total_billable_hours" => $value['rendered_hours'],
                "total_non_billable_hours" => 0.00
            ];
        }

        $attendance = $this->attendanceHelper->getAttendance($start, $end);
        foreach ($attendance['data'] as $key => $value) {
            if (in_array($value['employee_id'], array_column($form_attendance, 'employee_id'))) {
                foreach ($form_attendance as $key => $form) {
                    if ($form['employee_id'] == $value['employee_id']) {
                        $index = $key;
                        $form_attendance[$index]['total_non_billable_hours'] += $value['rendered_hours'];
                        $form_attendance[$index]['total_billable_hours'] += $value['billable_hrs'];
                        break;
                    }
                }
            } else {
                $form_attendance[] = [
                    "employee_id" => $value['employee_id'],
                    "total_late_absent_hours" => 0.00,
                    "billable_leave_hours" => 0.00,
                    "non_billable_leave_hours" => 0.00,
                    "total_billable_hours" => $value['billable_hrs'],
                    "total_non_billable_hours" => $value['rendered_hours']
                ];
            }
        }

        $lateAbsent = $this->getLatesAbsence($start, $end);
        foreach ($lateAbsent as $key => $value) {
            if (in_array($value['employee_id'], array_column($form_attendance, 'employee_id'))) {
                foreach ($form_attendance as $key => $form) {
                    if ($form['employee_id'] == $value['employee_id']) {
                        $index = $key;
                        break;
                    }
                }
                $form_attendance[$index]['total_late_absent_hours']
                    += $value['lates_absences']['hrs_late'] + $value['lates_absences']['hrs_absent'];
            } else {
                $form_attendance[] = [
                    "employee_id" => $value['employee_id'],
                    "total_late_absent_hours" =>
                        $value['lates_absences']['hrs_late'] + $value['lates_absences']['hrs_absent'],
                    "billable_leave_hours" => 0.00,
                    "non_billable_leave_hours" => 0.00,
                    "total_billable_hours" => 0.00,
                    "total_non_billable_hours" => 0.00
                ];
            }
        }

        $leaves = $this->getLeave($start, $end);
        foreach ($leaves as $key => $leave) {
            if (in_array($leave['employee_id'], array_column($form_attendance, 'employee_id'))) {
                foreach ($form_attendance as $key => $form) {
                    if ($form['employee_id'] == $leave['employee_id']) {
                        $index = $key;
                        break;
                    }
                }
                $form_attendance[$index]['billable_leave_hours']
                    += $leave['billable'] == 'Yes' ? $leave['leave_hours'] : 0.00;
                $form_attendance[$index]['non_billable_leave_hours']
                    += $leave['billable'] == 'No' ? $leave['leave_hours'] : 0.00;
            } else {
                $form_attendance[] = [
                    "employee_id" => $leave['employee_id'],
                    "total_late_absent_hours" => 0.00,
                    "billable_leave_hours" => $leave['billable'] == 'Yes' ? $leave['leave_hours'] : 0.00,
                    "non_billable_leave_hours" => $leave['billable'] == 'No' ? $leave['leave_hours'] : 0.00,
                    "total_billable_hours" => 0.00,
                    "total_non_billable_hours" => 0.00
                ];
            }
        }

        foreach ($form_attendance as $key => $emp_attendance) {
            $rate = EmployeeSalary::where('employee_id', $emp_attendance['employee_id'])->first();
            if (!$rate) {
                unset($form_attendance[$key]);
            }
        }

        return [
            "attendance" => array_values($form_attendance),
            "overtime" => $this->getOvertime($start, $end),
            "restday_holiday" => $this->getRestDay($start, $end)
        ];
    }

    /**
     * Show
     *
     * @param string $payrunId
     *
     * @return void
     */
    public function show(string $payrunId)
    {
        $payroll = PayRun::findOrFail($payrunId);

        return (new PayRunResource($payroll))->additional([
            'approver' => check_approver($payroll),
            'approved_status' =>  model_approval_status(check_approver($payroll))
        ]);
    }

    /**
     * PayrollHistory
     *
     * @return JsonResource
     */
    public function getPayrollHistory(): JsonResource
    {
        $payroll = Payrun::orderBy('id', 'DESC')->get();
        return PayRunResource::collection($payroll);
    }

    /**
     * Payload
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    private function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'start_period' => 'required',
                'end_period' => 'required',
                'pay_date' => 'required'
            ]
        );
    }

    /**
     * get Attendance
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function getAttendance(string $start, string $end): array
    {
        $data = $this->attendanceHelper->getAttendance($start, $end);

        $this->totalBillableHrs($data['data']);  //compute total hours per cutoff period

        $this->employee = $data['total_employee'];

        return $data['data'];
    }

    /**
     * Activity Attendance
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function activityAttendance(string $start, string $end): array
    {
        $data = $this->attendanceHelper->getActivityAttendance($start, $end);

        return $data;
    }

    /**
     * Payables depend on days
     *
     * @param string $start
     * @param string $end
     *
     * @return int
     */
    public function payableDays(string $start, string $end): int
    {
        $start = new DateTime($start);
        $end = new DateTime($end);
        // otherwise the  end date is excluded (bug?)
        $end->modify('+1 day');

        $interval = $end->diff($start);

        // total days
        $days = $interval->days;

        // create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);

        // best stored as array, so you can add more than one
        $holidays = array();

        foreach ($period as $dt) {
            $curr = $dt->format('D');

            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--; // substract if Saturday or Sunday
            } elseif (in_array($dt->format('Y-m-d'), $holidays)) {
                $days--; // (optional) for the updated question
            }
        }
        return $days;
    }

    /**
     * Computation for OT
     *
     * @param string $start
     * @param string $end
     * @param string $employee_id
     * @param string $payitemid
     *
     * @return void
     */
    public function computeOtTypeCreate(
        string $start,
        string $end,
        string $employee_id,
        string $payitemid
    ) {
        $hours_ot = 0;
        $break_hours = 0;
        $overtime = EmployeeOvertime::where('employee_id', $employee_id)
            ->whereBetween(
                DB::raw('date(overtime_date)'),
                [date('Y-m-d', strtotime($start)),
                date('Y-m-d', strtotime($end))]
            )->get();

        foreach ($overtime as $key => $ot) {
            $attendance = Attendance::where(
                DB::raw('date(start_time)'),
                date('Y-m-d', strtotime($ot->overtime_date))
            )->first();

            if ($attendance) {
                $hours_ot += (strtotime($attendance->end_time) -
                    strtotime($attendance->start_time)) / 3600; // computation of total ot hours per OT type
                $break_hours = $this->computeBreakHours($attendance->getAttendanceBreak);
                $hours_ot = $hours_ot - round($break_hours, 2);
                $PayRunItemOvertime = new PayRunItemOvertime();
                $PayRunItemOvertime->pay_run_item_id = $payitemid;
                $PayRunItemOvertime->overtime_id = $ot->id;
                $PayRunItemOvertime->billable_hours = ($ot->getOtType->id == 3
                    || $ot->getOtType->id ==  4 ? 0 : $hours_ot);
                $PayRunItemOvertime->non_billable_hours = ($ot->getOtType->id == 3
                    || $ot->getOtType->id == 4 ? $hours_ot : 0);
                $PayRunItemOvertime->save();
            }
        }

        return 'Created!';
    }

    /**
     * Getting attandance Create
     *
     * @param string $start
     * @param string $end
     * @param string $employee_id
     *
     * @return array
     */
    public function getAttendanceCreate(
        string $start,
        string $end,
        string $employee_id
    ): array {
        $data = array();
        $employee_attendance = $this->attendanceHelper->getPeriodAttendance($start, $end);
        foreach ($employee_attendance->groupBy('employee_id')->get() as $key => $attendance) {
            if ($attendance->getEmployee->id == $employee_id) {
                $data = [
                    'no_billable_hrs' => $this->computeTotalWorkingHrsWithType($start, $end, $attendance, 0),
                    'no_working_hours'  => $this->computeTotalWorkingHrsWithType($start, $end, $attendance, 1)
                ];
            }
        }
        return $data;
    }

    /**
     * totalworking hours
     *
     * @param string $object
     *
     * @return float
     */
    public function computeTotalWorkingHrs(object $attendance): float
    {
        $total_hours = 0;
        $break_hours = 0;
        $att = Attendance::where('employee_id', $attendance->employee_id)->whereNotNull('end_time')->get();
        foreach ($att as $key => $time_att) {
                $total_hours += (strtotime($time_att->end_time) -
                    strtotime($time_att->start_time)) / 3600; // computation of total working hours per employee
                $break_hours = $this->computeBreakHours($time_att->getAttendanceBreak);
                $total_hours = $total_hours - $break_hours;
        }
        return round($total_hours, 2);
    }

    /**
     * get Leave Create
     *
     * @param string $start
     * @param string $end
     * @param string $employee_id
     *
     * @return array
     */
    public function getLeaveCreate(
        string $start,
        string $end,
        string $employee_id
    ): array {

        $data = array();
        $employee_leave = $this->leaves($start, $end)->get();
        foreach ($employee_leave as $key => $leave) {
            $stats = check_approver($leave);
            if (model_approval_status($stats) == 'approved' && $leave->employee_id == $employee_id) {
                $data = [
                    'no_working_hours' => ($leave->getLeaveType->type == 1 ? ($leave->total_days * 8.00) : 0),
                    'no_billable_hrs' => ($leave->getLeaveType->type == 1 ? 0 : ($leave->total_days * 8.00)),
                ];
            }
        }

        return $data;
    }

    /**
     * Getting Timesheet Create
     *
     * @param string $start
     * @param string $end
     * @param string $employe_id
     *
     * @return array
     */
    public function getTimesheetCreate(
        string $start,
        string $end,
        string $employee_id
    ): array {

        $data = array();
        $employee_timesheets = EmployeeTimesheet::whereBetween(
            DB::raw('timesheet_date'),
            [date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))]
        );

        foreach ($employee_timesheets->groupBy('employee_id')->get() as $key => $timesheet) {
            if ($timesheet->employee_id == $employee_id) {
                $data = [
                    'no_working_hours' => EmployeeTimesheet::where('employee_id', $timesheet->employee_id)
                                            ->where('approved_status', 1)
                                            ->where('is_billable', 1)
                                            ->sum('rendered_hours'),
                    'no_billable_hrs'  => EmployeeTimesheet::where('employee_id', $timesheet->employee_id)
                                          ->where('approved_status', 1)
                                          ->where('is_billable', 0)
                                          ->sum('rendered_hours'),
                ];
            }
        }
        return $data;
    }

    /**
     * Compute total working days
     *
     * @param string $start
     * @param string $end
     * @param string $employee_id
     *
     * @return int
     */
    public function computeTotalWorkingDays(
        string $start,
        string $end,
        string $employee_id
    ): int {
        $attendance = Attendance::whereBetween(
            DB::raw('date(start_time)'),
            [date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))]
        )->whereNotNull('end_time')->where('employee_id', $employee_id)->groupBy(DB::raw('date(start_time)'))->get();
        

        // $JobDetail = JobDetail::where('employee_id', $employee_id)->first();
        // $Schedule = Schedule::all();
        // $dayslist = 0;
        // foreach ($Schedule as $data => $value) {
        //     if (in_array($JobDetail->getDesignation->department_id, json_decode($value->departments))) {
        //         $time1 = strtotime($value->end_full_day);
        //         $time2 = strtotime($value->start_full_day);
        //         $noofhour = round((abs($time2 - $time1) / 3600) - 2, 2);
        //     }
        // }

        return count($attendance);
    }

    /**
     * View Payroll Period
     *
     * @param string $start
     * @param string $end
     * @param string $pay_date
     *
     * @return array
     */
    public function getViewPayrollPeriod(
        string $start,
        string $end,
        string $pay_date
    ): array {
        $this->total_hrs = 0.00;
        $this->total_ot_hrs = 0.00;
        $this->payable_days = date('d F, Y', strtotime($pay_date));
        $this->total_employee = 0;

        $data = $this->attendanceHelper->getAttendance($start, $end);

        $this->total_employee = $data['total_employee'];

        if (date('F', strtotime($end)) == date('F', strtotime($start))) {
            $to_period = date('d, Y', strtotime($end));
        } else {
            $to_period = date('F d, Y', strtotime($end));
        }

        return [
            'from_period' => date('F d', strtotime($start)),
            'to_period' => $to_period,
            'total_hrs' => round($this->total_hrs, 2),
            'total_ot_hrs' => $this->employeeOvertimeHelper->getTotalOtHours($start, $end),
            'payable_days' => $this->payable_days,
            'total_employee' => $this->total_employee
        ];
    }

    /**
     * GET OB
     * @param string $start
     * @param string $end
     * @return void
     */
    public function getOfficialBusiness(string $start, string $end)
    {
        return $this->obHelper->getOfficialBusiness($start, $end);
    }

    /**
     * get Activity OB
     * @param string $start
     * @param string $end
     * @return void
     */
    public function activityOfficialBusiness(string $start, string $end)
    {
        return $this->obHelper->activityOfficialBusiness($start, $end);
    }
    
    /**
     * get Overtime
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function getOvertime(string $start, string $end)
    {
        return $this->employeeOvertimeHelper->getOvertimeEmployee($start, $end);
    }

    /**
     * activity OT
     *
     * @param string $start
     * @param string $end
     *
     * @return ResourceCollection
     */
    public function activityOvertime(string $start, string $end)
    {
        $data = array();
        $data = $this->employeeOvertimeHelper->getActivityOvertime($start, $end);
    
        return $data;
    }

    /**
     * Get Leave
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function getLeave(string $start, string $end): array
    {
        return $this->employeeLeaveHelper->getLeaveEmployee($start, $end);
    }

    /**
     * activity Leave
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function activityLeave(string $start, string $end): array
    {
        return $this->employeeLeaveHelper->activityLeave($start, $end);
    }

    /**
     * Get Late and Absent
     * @param string $start
     * @param string $end
     * @return array
     */
    public function getLatesAbsence(string $start, string $end): array
    {
        $data = array();
        $data = $this->lateAbsenceHelper->getLatesAbsence($start, $end);

        return $data;
    }

    /**
     * Activity Absence
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function activityLateAbsence(string $start, string $end): array
    {
        $data = array();
        
        $data = $this->lateAbsenceHelper->activityLateAbsence($start, $end);

        return $data;
    }

    /**
     * check holidays
     *
     * @param string $date
     *
     * @return bool
     */
    public function holidays(string $date): bool
    {

        $yr = date('y', strtotime($date));
        $month = date('m', strtotime($date));
        $day = date('d', strtotime($date));

        $isHoliday = Holiday::whereMonth('holiday_date', $month)->whereDay('holiday_date', $day)->first();

        if ($isHoliday) {
            $checkRecurring = Holiday::whereMonth('holiday_date', $month)
                ->whereDay('holiday_date', $day)
                ->where('is_recurring', 1)
                ->first();
            if (!$checkRecurring) {
                $isHoliday = Holiday::whereDate('holiday_date', date('Y-m-d', strtotime($date)))->first();
            }
        }
        return $isHoliday != null;
    }

    /**
     * Total Billable Hours
     *
     * @param [type] $data
     *
     * @return void
     */
    public function totalBillableHrs($data)
    {
        foreach ($data as $key => $value) {
            $this->total_hrs += $value['billable_hrs'];
        }
    }

    /**
     * getRestday
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function getRestDay(string $start, string $end): array
    {
        return $this->holidayRestdayHelper->getRestDay($start, $end);
    }

    /**
     * activity restday/holiday
     *
     * @param string $start
     * @param string $end
     *
     * @return ResourceCollection
     */
    public function activityRestdayHoliday(string $start, string $end)
    {
        return $this->holidayRestdayHelper->activityRestdayHoliday($start, $end);
    }
}

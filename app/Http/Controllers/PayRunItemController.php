<?php

namespace App\Http\Controllers;

use App\Http\Resources\PayRunItem as PayRunItemResource;
use App\PayRun;
use App\PayRunItem;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PayRunItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param string $payrunId
     *
     * @return JsonResouce
     */
    public function store(
        Request $request,
        string $payrunId
    ): JsonResource {
        $request->merge(['pay_run_id' => $payrunId]);

        $payRunItem = PayRunItem::create($this->payload($request));

        return new PayRunItemResource($payRunItem);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PayRunItem  $payRunItem
     * @return \Illuminate\Http\Response
     */
    public function show(string $payrunId)
    {
        $payrollItem = Payrun::findOrFail($payrunId)->payrollItem()->get();

        return PayRunItemResource::collection($payrollItem);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PayRunItem  $payRunItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PayRunItem $payRunItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PayRunItem  $payRunItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayRunItem $payRunItem)
    {
        //
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    private function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'employee_id' => 'required',
                'pay_run_id' => 'required',
                'total_late_absent_hours' => 'required',
                'billable_leave_hours' => 'required',
                'non_billable_leave_hours' => 'required',
                'total_billable_hours' => 'required',
                'total_non_billable_hours' => 'required'
            ]
        );
    }
}

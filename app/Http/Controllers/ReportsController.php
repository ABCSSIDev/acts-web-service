<?php

namespace App\Http\Controllers;

use App\AccountabilityIssuance;
use App\Assets;
use App\Attendance;
use App\Employee;
use App\EmployeeLeave;
use App\EmployeeOvertime;
use App\Exports\AccountabilityExport;
use App\Exports\AssetsExport;
use App\Exports\EmployeeLeavesExport;
use App\Exports\EmployeeOvertimesExport;
use App\Exports\EmployeeSalariesExport;
use App\Exports\EmployeesExport;
use App\Http\Resources\AccountabilityIssuance as ResourcesAccountabilityIssuance;
use App\Http\Resources\Employee as ResourcesEmployee;
use App\Http\Resources\EmployeeLeave as ResourcesEmployeeLeave;
use App\Http\Resources\EmployeeOvertime as ResourcesEmployeeOvertime;
use App\Http\Resources\LeaveType as LeaveTypeResource;
use App\Http\Resources\OfficeSuppliesAndEquipment as OfficeSuppliesAndEquipmentResource;
use App\OfficeSuppliesAndEquipment;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class ReportsController extends Controller
{
    //
    /**
     * Construct API
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $module = $request->module;

        switch ($module) {
            case 'employees':
                $employees = Employee::department($request->department)
                    ->type($request->employee_type)
                    ->joined($request->year)
                    ->designation($request->designation)
                    ->get();

                return ResourcesEmployee::collection($employees);
                break;
            case 'employee-leaves':
                $leaves = EmployeeLeave::where('deleted_at', null);

                if ($request->has('employee_id')) {
                    $leaves->where('employee_id', $request->employee_id);
                }

                if ($request->has('leave_type')) {
                    $leaves->where('leave_id', $request->leave_type);
                }

                if ($request->has('year')) {
                    $leaves->whereYear('start_date', $request->year);
                }

                if ($request->has('month')) {
                    $leaves->whereMonth('start_date', $request->month);
                }

                $leaves = $leaves->get();

                if ($request->has('status')) {
                    $data = [];

                    foreach ($leaves as $leave) {
                        $status = model_approval_status(check_approver($leave));

                        if ($status == $request->status) {
                            $data[] = array(
                                'id' => $leave->id,
                                'employee_id' => $leave->getEmployee->id,
                                'employee_name' => $leave->getEmployee->fullname(),
                                'leave_type' => new LeaveTypeResource($leave->getLeaveType),
                                'start_date' => $leave->start_date,
                                'end_date' => $leave->end_date,
                                'total_days' => $leave->total_days,
                                'reason' => $leave->reason,
                                'status' => $status,
                                '_parse_start_date' => Carbon::createFromDate($leave->start_date)->format('F d, Y'),
                                '_parse_end_date' => Carbon::createFromDate($leave->end_date)->format('F d, Y')
                            );
                        }
                    }

                    return $data;
                }
                return ResourcesEmployeeLeave::collection($leaves);
                break;
            case 'employee-overtimes':
                $overtimes = EmployeeOvertime::where('deleted_at', null);

                if ($request->has('employee_id')) {
                    $overtimes->where('employee_id', $request->employee_id);
                }

                if ($request->has('overtime_type')) {
                    $overtimes->where('ot_id', $request->overtime_type);
                }

                if ($request->has('year')) {
                    $overtimes->whereYear('overtime_date', $request->year);
                }

                if ($request->has('month')) {
                    $overtimes->whereMonth('overtime_date', $request->month);
                }

                $overtimes = $overtimes->get();

                if ($request->has('status')) {
                    $data = [];

                    foreach ($overtimes as $overtime) {
                        $status = model_approval_status(check_approver($overtime));

                        if ($status == $request->status) {
                            $data[] = array(
                                'id' => $overtime->id,
                                'employee_name' => $overtime->getEmployee->fullNameLastNameFirst(),
                                'employee_id' => $overtime->employee_id,
                                'ot_id' => $overtime->ot_id,
                                'ot_type' => $overtime->getOtType->code,
                                'ot_type_name' => $overtime->getOtType->name,
                                'file_date' => $overtime->file_date,
                                'overtime_date' => $overtime->overtime_date,
                                'start_time' => date('h:i A', strtotime($overtime->start_time)),
                                'end_time' => date('h:i A', strtotime($overtime->end_time)),
                                'edit_start_time' => $overtime->start_time,
                                'edit_end_time' => $overtime->end_time,
                                'rendered_hours' => date('h.i ', strtotime($overtime->rendered_hours)),
                                'purpose' => $overtime->purpose,
                                'reason' => $overtime->reason,
                                'approved_status' => model_approval_status(check_approver($overtime)),
                                '_parse_overtime_date' => Carbon::createFromDate($overtime->overtime_date)->format('F d, Y')
                            );
                        }
                    }
                    return $data;
                }

                return ResourcesEmployeeOvertime::collection($overtimes);

                break;
            case 'assets':
                $assets = OfficeSuppliesAndEquipment::where('deleted_at', null);

                if ($request->has('inventory_id')) {
                    $assets->where('inventory_id', $request->inventory_id);
                }

                if ($request->has('status')) {
                    $assets->where('asset_status', $request->status);
                }

                $assets = $assets->get();

                return OfficeSuppliesAndEquipmentResource::collection($assets);

                break;
            case 'accountability':
                $accountability = AccountabilityIssuance::where('deleted_at', null);

                if ($request->has('employee_id')) {
                    $accountability->where('employee_id', $request->employee_id);
                }

                if ($request->has('department_id')) {
                    $department_id = $request->department_id;
                    $accountability->whereHas('employee', function ($q) use ($department_id) {
                        $q->whereHas('getJobDetails', function ($q) use ($department_id) {
                            $q->whereHas('getDesignation', function ($q) use ($department_id) {
                                $q->whereHas('getDepartment', function ($q) use ($department_id) {
                                    $q->where('department_id', $department_id);
                                });
                            });
                        });
                    });
                }

                if ($request->has('inventory_id')) {
                    $inventory_id = $request->inventory_id;
                    $accountability->whereHas('issuanceItems', function ($q) use ($inventory_id) {
                        $q->whereHas('asset', function ($q) use ($inventory_id) {
                            $q->where('inventory_id', $inventory_id);
                        });
                    });
                }

                return ResourcesAccountabilityIssuance::collection($accountability->get());

                break;
            default:
                return response()->jsons([
                    'message' => 'Invalid module value'
                ], 400);
                break;
        }
    }

    public function export(Request $request)
    {
        $module = $request->module;

        $filename = '';

        switch ($module) {
            case 'employees':
                $filename = 'employee-report' . Carbon::now()->format('mdY') . '.csv';

                $export = Excel::store(new EmployeesExport($request->department, $request->employee_type, $request->year, $request->designation), $filename, 'reports');

                $file = Storage::disk('reports')->get($filename);

                if ($file) {
                    $fileLink = 'data:application/vnd.ms-excel;base64,' . base64_encode($file);
                    @chmod(Storage::disk('reports')->path($filename), 0755);
                    @unlink(Storage::disk('reports')->path($filename));
                }

                return response()->json([
                    'success' => true,
                    'data' => $fileLink,
                    'fileName' => $filename,
                    'message' => 'Employee Data are successfully exported.'
                ], 200);
                break;
            case 'employee-leaves':
                $filename = 'employee-leave-report' . Carbon::now()->format('mdY') . '.csv';

                $export = Excel::store(new EmployeeLeavesExport($request->employee_id, $request->leave_type, $request->year, $request->month, $request->status), $filename, 'reports');

                $file = Storage::disk('reports')->get($filename);

                if ($file) {
                    $fileLink = 'data:application/vnd.ms-excel;base64,' . base64_encode($file);
                    @chmod(Storage::disk('reports')->path($filename), 0755);
                    @unlink(Storage::disk('reports')->path($filename));
                }

                return response()->json([
                    'success' => true,
                    'data' => $fileLink,
                    'fileName' => $filename,
                    'message' => 'Employee Leave Data are successfully exported.'
                ], 200);
                break;
            case 'employee-overtimes':
                $filename = 'employee-overtime-report' . Carbon::now()->format('mdY') . '.csv';

                $export = Excel::store(new EmployeeOvertimesExport($request->employee_id, $request->overtime_type, $request->year, $request->month, $request->status), $filename, 'reports');

                $file = Storage::disk('reports')->get($filename);

                if ($file) {
                    $fileLink = 'data:application/vnd.ms-excel;base64,' . base64_encode($file);
                    @chmod(Storage::disk('reports')->path($filename), 0755);
                    @unlink(Storage::disk('reports')->path($filename));
                }

                return response()->json([
                    'success' => true,
                    'data' => $fileLink,
                    'fileName' => $filename,
                    'message' => 'Employee Overtime Data are successfully exported.'
                ], 200);
                break;
            case 'employee-salaries':
                $filename = 'employee-salaries-report' . Carbon::now()->format('mdY') . '.csv';

                $export = Excel::store(new EmployeeSalariesExport(), $filename, 'reports');

                $file = Storage::disk('reports')->get($filename);

                if ($file) {
                    $fileLink = 'data:application/vnd.ms-excel;base64,' . base64_encode($file);
                    @chmod(Storage::disk('reports')->path($filename), 0755);
                    @unlink(Storage::disk('reports')->path($filename));
                }

                return response()->json([
                    'success' => true,
                    'data' => $fileLink,
                    'fileName' => $filename,
                    'message' => 'Employee Salaries Data are successfully exported.'
                ], 200);
                break;
            case 'assets':
                    $filename = 'assets-report' . Carbon::now()->format('mdY') . '.csv';
    
                    $export = Excel::store(new AssetsExport($request->inventory_id, $request->status), $filename, 'reports');
    
                    $file = Storage::disk('reports')->get($filename);
    
                    if ($file) {
                        $fileLink = 'data:application/vnd.ms-excel;base64,' . base64_encode($file);
                        @chmod(Storage::disk('reports')->path($filename), 0755);
                        @unlink(Storage::disk('reports')->path($filename));
                    }
    
                    return response()->json([
                        'success' => true,
                        'data' => $fileLink,
                        'fileName' => $filename,
                        'message' => 'Assets Data are successfully exported.'
                    ], 200);
                    break;
            case 'accountability':
                $filename = 'accountability-report' . Carbon::now()->format('mdY') . '.csv';

                $export = Excel::store(
                    new AccountabilityExport(
                        $request->inventory_id,
                        $request->department_id,
                        $request->employee_id
                    ),
                    $filename,
                    'reports'
                );

                $file = Storage::disk('reports')->get($filename);

                if ($file) {
                    $fileLink = 'data:application/vnd.ms-excel;base64,' . base64_encode($file);
                    @chmod(Storage::disk('reports')->path($filename), 0755);
                    @unlink(Storage::disk('reports')->path($filename));
                }

                return response()->json([
                    'success' => true,
                    'data' => $fileLink,
                    'fileName' => $filename,
                    'message' => 'Assets Data are successfully exported.'
                ], 200);
                break;
            default:
                return response()->json([
                    'message' => 'Invalid module value'
                ], 400);
                break;
        }
    }
}

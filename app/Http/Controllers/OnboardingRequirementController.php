<?php

namespace App\Http\Controllers;

use App\Http\Resources\OnboardingRequirement as OnboardingRequirementResource;
use App\OnboardingRequirement;
use Illuminate\Http\Request;
use function App\Log\log_created;

class OnboardingRequirementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $onboarding = OnboardingRequirement::all();
        return $onboarding;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
            $request->validate([
                'name' => 'unique:hr_onboarding_requirements'
            ]);

            $new_requirement = new OnboardingRequirement;
            $new_requirement->name = $request->input('name');
            $new_requirement->is_enabled = 1;
            $new_requirement->save();
            log_created($new_requirement); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new OnboardingRequirementResource($new_requirement);
            
       } catch (\Exception $exception) {
            return OnboardingRequirementResource::errorResponse($exception->getMessage());
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OnboardingRequirement  $onboardingRequirement
     * @return \Illuminate\Http\Response
     */
    public function show(OnboardingRequirement $onboardingRequirement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OnboardingRequirement  $onboardingRequirement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OnboardingRequirement $onboardingRequirement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OnboardingRequirement  $onboardingRequirement
     * @return \Illuminate\Http\Response
     */
    public function destroy(OnboardingRequirement $onboardingRequirement)
    {
        //
    }
}

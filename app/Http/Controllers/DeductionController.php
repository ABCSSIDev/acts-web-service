<?php

namespace App\Http\Controllers;

use App\Deduction;
use Illuminate\Http\Request;
use App\Employee;
use App\Http\Resources\DeductionResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

use function App\Log\log_created;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class DeductionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deductions = Deduction::all();
        return DeductionResource::collection($deductions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = array();
        $employee = Employee::role(Deduction::class)->get();
        $response['employee'] = $employee;

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): JsonResource
    {
        $cutoff =  31 * ($request->cutoff / 2);
        $effective_date = Carbon::createFromDate($request->effective_date)->addDay($cutoff)->format('Y-m-d');
        $payload = $this->payload($request);
        $payload['effective_until'] = $effective_date;
        $deduction = Deduction::create($payload);
        // log_created($deduction);
        return new DeductionResource($deduction);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id): JsonResource
    {
        $deduction = Deduction::findOrFail($id);
        return new DeductionResource($deduction);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id): JsonResource
    {
        $cutoff =  31 * ($request->cutoff / 2);
        $effective_date = Carbon::createFromDate($request->effective_date)->addDay($cutoff)->format('Y-m-d');
        $payload = $this->payload($request);
        $deduction = Deduction::findOrFail($id);
        $payload = $request->except(['employee_id']);
        $payload['effective_until'] = $effective_date;
        $deduction->update($payload);
        // log_created($deduction);
        return new DeductionResource($deduction);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id): Response
    {
        $deduction = Deduction::findOrFail($id);
        $deduction->delete();
        // log_created($deduction);
        return response(null, 204);
    }

    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'employee_id' => 'required',
                'effective_date' => 'required',
                'amount' => 'required|numeric',
                'cutoff' => 'required|numeric',
                'reason' => 'required',
                'asset_id' => 'numeric'
            ]
        );
    }
}

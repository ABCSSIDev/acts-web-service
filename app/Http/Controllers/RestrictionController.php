<?php

namespace App\Http\Controllers;

use App\Module;
use App\PermissionModule;
use App\Restriction;
use App\UserAccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RestrictionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $Restriction = Restriction::all();
        return $Restriction;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Restriction  $restriction
     * @return \Illuminate\Http\Response
     */
    public function show(Restriction $restriction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Restriction  $restriction
     * @return \Illuminate\Http\Response
     */
    public function edit(Restriction $restriction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Restriction  $restriction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Restriction $restriction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Restriction  $restriction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restriction $restriction)
    {
        //
    }

    public function getRestrictions($name){
        if(Auth::user()->id === 1){
            $response = ['permitted' => true, 'admin' => true];
        }else{
            $spliced = array_change_key_case(explode('.', $name), CASE_LOWER);
            $module = Module::where('route','LIKE', '%'.$spliced[0].'%')->first();
            if(!empty($module)){
                $access = UserAccess::where('user_id', Auth::user()->id)->first();
                if(!empty($access)){
                    $restrictions = PermissionModule::where('permission_id', $access->permission_id)->where('module_id', $module->id)->first();
                    $restriction_names = array_map('strtolower',Restriction::whereIN('id', json_decode($restrictions->restriction_id))->pluck('description')->toArray());
                    $response = [];
                    if(empty($spliced[1])){
                        if(in_array('list',$restriction_names)){
                            $response = ['permitted' => true, 'index' => true];
                        }else{
                            $response = ['permitted' => false, 'index' => false];
                        }
                    }elseif( in_array($spliced[1], $restriction_names) ){
                        $response = ['permitted' => true];
                    }else{
                        $response = ['permitted' => false, 'feature' => true];
                    }
                }else{
                    $response = ['permitted' => false, 'access' => true];
                }
            }else{
                $response = ['permitted' => false];
            }
        }
        
       
        return response()->json($response);
    }

    
}

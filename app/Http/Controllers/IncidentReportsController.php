<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IncidentStatement;
use App\IncidentReports;
use App\IncidentAction;
use App\Employee;
use App\Assets;
use App\Team;
use App\Department;
use DateTime;
use App\Http\Resources\IncidentStatement as IncidentStatementResource;
use App\Http\Resources\IncidentAction as IncidentActionResource;
use App\Http\Resources\IncidentReports as IncidentReportsResource;
use function App\Log\log_created;
use App\Http\Resources\EmployeeDeducted as EmployeeDeductedResource;
class IncidentReportsController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $IncidentReports = IncidentReports::all();
        return IncidentReportsResource::collection($IncidentReports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = array();
        $Assets =  Assets::all();
        $Employee =  Employee::role(IncidentReports::class)->get();
        $Department =  Department::all();
        $Team =  Team::all();
        $response['Team'] = $Team;
        $response['Assets'] = $Assets;
        $response['Employee'] = $Employee;
        $response['Department'] = $Department;

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        try {
            $new_array = array();
            foreach ($request->input('witnesseslist') as $data) {
                $new_array[] = $data['witness'];
            }
            $incident_report_array = array();
            foreach ($request->input('incident_report') as $data) {
                $incident_report_array[] = $data;
            }
            $issuance_query = IncidentReports::create([
                'reported_by'       => $request->input('reported_by_employee'),
                'department_id'       => $request->input('department_id'),
                'reported_date'       => date('Y-m-d H:i:s', strtotime($request->input('reported_date'))),
                'incident_date'       => date('Y-m-d H:i:s', strtotime($request->input('incident_date'))),
                'witnesses'       => json_encode($new_array),
                'incident_type'       => $request->input('incident_type'),
                'reported_by_employee'       => ($request->input('reported_by') == 0?NULL:$request->input('reported_by')),
                'asset_id'       => ($request->input('asset_id') == null ?null:$request->input('asset_id')),
                'team_id'       => ($request->input('team_id') == null ?null:$request->input('team_id')),
                'incident_location'       => $request->input('incident_location'),
                'deduction_amount'       => $request->input('deduction_amount'),
                'deduction_per_period'       => $request->input('deduction_per_period'),
                'incident_report'       => json_encode($incident_report_array),
            ]);
            // log_created($issuance_query); 
            return new IncidentReportsResource($issuance_query);
        } catch (\Exception $exception) {
            return IncidentReportsResource::MsgResponse(true, $exception->getMessage());
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $IncidentReports = IncidentReports::findOrFail($id);
        return new IncidentReportsResource($IncidentReports);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \App\IncidentReports  $incidentReports
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team =  Team::all(); 
        $asset =  Assets::all();
        $employees =  Employee::role(IncidentReports::class)->get();
        $department =  Department::all();
        $incident_report = IncidentReports::findOrFail($id);    
        $incident_reports = new IncidentReportsResource($incident_report);
        $collections = [
            'assets' => $asset,
            'team' => $team,
            'employees' => $employees,
            'department' => $department,
            'incident_reports' => $incident_reports,
        ];
        return $collections;
    }
    public function update(Request $request, $id)
    {
        try{
            $new_array = array();
            foreach ($request->input('witnesseslist') as $data) {
                $new_array[] = $data;
            }
            $incident_report_array = array();
            foreach ($request->input('incident_report') as $data) {
                $incident_report_array[] = $data;
            }
            // return $incident_report_array;
            $incident_report = IncidentReports::where('id', $id)->update([ 
                'reported_by' => $request->input('reported_by'),
                'department_id' => $request->input('department_id'),
                'incident_date' => date('Y-m-d H:i:s', strtotime($request->input('incident_date'))),
                'reported_date' => date('Y-m-d H:i:s', strtotime($request->input('reported_date'))),
                'witnesses' => json_encode($new_array),
                'incident_type' => $request->input('incident_type'),
                'reported_by_employee' => ($request->input('reported_by_employee') == 0 ? null : $request->input('reported_by_employee')),
                'asset_id' => $request->input('asset_id'),
                'team_id' => $request->input('team_id'),
                'incident_location' => $request->input('incident_location'),
                'incident_report' => json_encode($incident_report_array),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            // log_created($Employee_leave); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            $query = IncidentReports::where('id', $id)->first();
            return new IncidentReportsResource($query);
            
        }catch(\Exception $exception){
            return IncidentReportsResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $IncidentReports = IncidentReports::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return IncidentReportsResource::MsgResponse($type, $msg);
        } catch (\Exception $exception) {
            return IncidentReportsResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function storeStatements(Request $request)
    {
        try {
            // foreach (request('item') as $key => $value){
            if (!empty($request->id) && $request->id != "null") {
                $IncidentStatement = IncidentStatement::where('id', $request->id)->update([
                    'statement' => $request->statement,
                    'injury_statement' => $request->injury_statement
                ]);
                $IncidentStatement = IncidentStatement::where('id', $request->id)->first();
            } else {
                $IncidentStatement = IncidentStatement::create([
                    'incident_id' => $request->input('incident_id'),
                    'statement_type' => $request->input('statement_type'),
                    'statement' => $request->input('statement'),
                    'injury_statement' => $request->input('injury_statement'),
                ]);
                log_created($IncidentStatement);
                // $IncidentStatement = new IncidentStatement();
                // $IncidentStatement->incident_id = $request->incident_id;
                // $IncidentStatement->statement_type = $request->statement_type;
                // $IncidentStatement->statement = $request->statement;
                // $IncidentStatement->injury_statement = $request->injury_statement;
                // $IncidentStatement->save();
            }
            return new IncidentStatementResource($IncidentStatement);
        } catch (\Exception $exception) {
            return IncidentStatementResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function getStatements($id)
    {
        $IncidentStatement = IncidentStatement::where('incident_id',$id)->get();
        return IncidentStatementResource::collection($IncidentStatement);
    }
    public function storeAction(Request $request)
    {
        try {
            foreach (request('item') as $key => $value) {
                if($value['id'] == 0){
                    $IncidentAction = New IncidentAction;
                    $IncidentAction->incident_id = $value['incident_id'];
                    $IncidentAction->actions = $value['actions'];
                    $IncidentAction->responsibility = $value['responsibility'];
                    $IncidentAction->completion_date = $value['completion_date'];
                    $IncidentAction->save();
                }
                
            }
            return IncidentActionResource::collection($IncidentAction);
        } catch (\Exception $exception) {
            return IncidentActionResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function getAction($id)
    {
        $IncidentAction = IncidentAction::findOrFail($id);
        return IncidentActionResource::collection($IncidentAction);
    }
    public function storeDeducted(Request $request, $id)
    {
        try{
            $IncidentReports = IncidentReports::where('id', $id)->update([ 
                'employee_deducted' => $request->input('employee_deducted'),
                'deduction_amount' => $request->input('deduction_amount'),
                'deduction_per_period' => $request->input('deduction_per_period')
            ]);
            $IncidentReports = IncidentReports::where('id', $id)->first();
            return new EmployeeDeductedResource($IncidentReports);
            // return EmployeeDeductedResource::MsgResponse(false, '');
        }catch(\Exception $exception){
            return EmployeeDeductedResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function getDedcuted($id)
    {
        $IncidentReports = IncidentReports::findOrFail($id);
        return new EmployeeDeductedResource($IncidentReports);
    }
}
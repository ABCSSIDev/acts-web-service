<?php

namespace App\Http\Controllers;

use App\Elements;
use App\Categories;
use App\ElementsType;
use App\ElementsList;
use App\Inventories;
use Illuminate\Http\Request;
use App\Http\Resources\Elements as ElementsResource;
use App\Http\Resources\ElementsList as ElementsListResource;

class ElementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ElementsList = ElementsList::all();
        return ElementsListResource::collection($ElementsList);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = array();
        $Elements = Elements::all();
        $response['Elements'] = $Elements;
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $Elements = New ElementsList;
            $Elements->element_id = $request->input('element_id');
            $Elements->name = $request->input('name');
            $Elements->value = $request->input('value');
            $Elements->save();            
            $type = false;
            $msg = 'Successfully New Elements Created!';
            return ElementsListResource::MsgResponse(false,$msg);
        }catch(\Exception $exception){
            return ElementsListResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ElementsList = ElementsList::findOrFail($id);
        return new ElementsListResource($ElementsList);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $ElementsList = ElementsList::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return ElementsListResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return ElementsListResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function listAddElements()
    {
        $Elements = Elements::all();
        return ElementsResource::collection($Elements);
    }
    public function createAddElements()
    {
        $response = array();
        $ElementsType = ElementsType::all();
        $Categories = Categories::all();
        $Inventories = Inventories::all();
        $response['ElementsType'] = $ElementsType;
        $response['Categories'] = $Categories;
        $response['Inventories'] = $Inventories;
        return response()->json($response);
    }
    public function storeAddElements(Request $request)
    {
        try{
            $Elements = New Elements;
            $Elements->element_type_id = $request->input('element_type_id');
            $Elements->category_id = $request->input('category_id');
            $Elements->inventory_id = $request->input('inventory_id');
            $Elements->caption = $request->input('caption');
            $Elements->save();            
            $type = false;
            $msg = 'Successfully New Elements Created!';
            return new ElementsResource($Elements);
        }catch(\Exception $exception){
            return ElementsResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function showAddElements($id)
    {
        $Elements = Elements::findOrFail($id);
        return new ElementsResource($Elements);
    }
    public function listElementsType()
    {
        $ElementsType = ElementsType::all();
        return $ElementsType;
    }
    public function storeElementsType(Request $request)
    {
        try{
            $Elements = New ElementsType;
            $Elements->name = $request->input('name');
            $Elements->save();            
            $type = false;
            $msg = 'Successfully New Elements Type Created!';
            return ElementsResource::MsgResponse(false,$msg);
        }catch(\Exception $exception){
            return ElementsResource::MsgResponse(true,$exception->getMessage());
        }
    }
}



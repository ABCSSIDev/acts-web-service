<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Resources\ReturnSlip as ResourcesReturnSlip;
use App\Inventories;
use App\ReturnItems;
use App\ReturnSlip;
use App\Assets;
use App\Unit;
use Illuminate\Http\Request;
use function App\Log\log_created;

class ReturnSlipController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $return_slips = ReturnSlip::all(); 
        return ResourcesReturnSlip::collection($return_slips);
        // return ResourcesReturnSlip::collection($return_slips);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'employees' => Employee::role(ReturnSlip::class)->get(),
            'inventories' => Inventories::all(),
            'units' => Unit::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        try {
            $slip_query = ReturnSlip::create([
                'employee_id' => $request->employee_id,
                'date' => $request->date,
            ]);
            
            $data =  $request->input('item');
            if($data){
                foreach( $data as $item){
                    $assets=Assets::where('inventory_id', $item['inventory_id'])->first();
                    $item_query = new ReturnItems();
                    $item_query->return_id      = $slip_query->id;
                    $item_query->inventory_id   = $item["inventory_id"]; //required
                    $item_query->asset_id       = ($assets ? $assets->id:null); //nullable
                    $item_query->remarks        = $item["remarks"]; //nullable
                    $item_query->qty_return     = $item["returned"]; //nullable
                    $item_query->qty_recieved   = $item["received"]; //nullable
                    $item_query->save();
                }
            }
            log_created($slip_query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return response()->json($slip_query);
        } catch (\Exception $exception) {
            return ResourcesReturnSlip::errorResponse($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReturnSlip  $returnSlip
     * @return \Illuminate\Http\Response
     */
    public function show(ReturnSlip $returnSlip)
    {
        return new ResourcesReturnSlip($returnSlip);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReturnSlip  $returnSlip
     * @return \Illuminate\Http\Response
     */
    public function edit(ReturnSlip $returnSlip)
    {
        return response()->json([
            'data' => $returnSlip,
            'employees' => Employee::role(ReturnSlip::class)->get(),
            'inventories' => Inventories::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReturnSlip  $returnSlip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReturnSlip $returnSlip)
    {
        try{
            $updated = ReturnSlip::where('id', $returnSlip->id)->update(
                [$request->column_name => $request->new_value]
            );
            if(!empty($updated)){
                $query = ReturnSlip::where('id', $returnSlip->id)->first();
                return new ResourcesReturnSlip($query);
            }
        }catch(\Exception $exception){
            return ResourcesReturnSlip::errorResponse(true,$exception->getMessage());
        }
    }

 /**
  * Update All Collumns
  *
  * @param Request $request
  * @param ReturnSlip $returnSlip
  *
  * @return void
  */
    public function updateAllColumns(Request $request, $id)
    {
        try{
            $updated = ReturnSlip::where('id', $id)->update([
                    'employee_id'   => $request->employee_id,
                    'date'          => $request->date
                ]);
            if(!empty($updated)){
                $query = ReturnSlip::where('id', $id)->first();
                return new ResourcesReturnSlip($query);
            }
        }catch(\Exception $exception){
            return ResourcesReturnSlip::errorResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $delete_query = ReturnSlip::destroy(explode(',', $id));
            return ResourcesReturnSlip::errorResponse('Successfully Deleted!');
        }
        catch(\Exception $exception){
            return ResourcesReturnSlip::errorResponse($exception->getMessage());
        }
    }

    public function findAssetTag($id)
    {
        $data = Assets::where('inventory_id', $id)->first();
        return $data->asset_tag;
    }
}

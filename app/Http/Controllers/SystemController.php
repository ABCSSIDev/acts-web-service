<?php

namespace App\Http\Controllers;

use App\Module;
use App\System;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
Use App\Http\Resources\System as SystemResource;
use App\Http\Resources\Module as ModuleResource;
use App\UserAccess;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //    $this->middleware('auth:api')->except(['index','getAllSystems','show']);
    // }

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        
        return SystemResource::collection(System::where('is_enabled', '1')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        try{
            $module = new Module();
            $module->system_id = \request('system_id');
            $module->name  = request('name');
            $module->save();

            return new ModuleResource($module);
        }catch (\Exception $exception){
            return SystemResource::errorResponse($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\System  $system
     * @return \Illuminate\Http\Response
     */
    public function show(System $system)
    {
        return new SystemResource($system);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\System  $system
     * @return \Illuminate\Http\Response
     */
    public function edit(System $system)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\System  $system
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, System $system)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\System  $system
     * @return \Illuminate\Http\Response
     */
    public function destroy(System $system)
    {
        //
    }

    public function getAllSystems()
    {
        if(!empty(FacadesAuth::user()->id)){
            if(FacadesAuth::user()->id == 1){
                return SystemResource::collection(System::where('is_enabled',1)->get());
            }
            else{
                $system = UserAccess::where('user_id', FacadesAuth::user()->id)->first();
                $getSystems = System::whereIn('id', json_decode($system->getPermission->system_id))->where('is_enabled', 1)->get();
                return SystemResource::collection($getSystems);
            }
        }else{
            return SystemResource::collection(System::where('is_enabled',1)->get());
        }
        
    }

}

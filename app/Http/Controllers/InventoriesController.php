<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventories;
use App\Unit;
use App\Categories;
use App\InventoriesNames;
use App\MaterialIssuancesItems;
use App\Http\Resources\InventoriesNames as InventoriesNamesResource;
use App\Http\Resources\Inventories as InventoriesResource;
use App\OfficeSuppliesAndEquipment;

use function App\Log\log_created;
use function App\Helpers\save_image;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class InventoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index()
    {
        $Inventories = Inventories::where('type', 0)->get();
        return InventoriesResource::collection($Inventories);
    }
    public function create()
    {
        $response = array();
        $Unit =  Unit::all();
        $Categories =  Categories::all();
        $response['Unit'] = $Unit;
        $response['Categories'] = $Categories;

        return response()->json($response);
    }
    public function store(Request $request)
    {

        try {
            $data =  $request->input('item');
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $result = InventoriesNames::where('description', 'LIKE', '%' . $value['description'] . '%')->where('mat_code', $value['mat_code'])->first();
                    if (!$result) {
                        $InventoriesNames = new InventoriesNames;
                        $InventoriesNames->category_id = $value['category_id'];
                        $InventoriesNames->description = $value['description'];
                        $InventoriesNames->mat_code = $value['mat_code'];
                        $InventoriesNames->save();

                        $Inventories = new Inventories;
                        $Inventories->inventory_description_id = $InventoriesNames->id;
                        $Inventories->unit_id = $value['unit_id'];
                        $Inventories->image = ($value['image'] ? $value['image'] : NULL);
                        $Inventories->category_id = $value['category_id'];
                        $Inventories->quantity = $value['quantity'];
                        $Inventories->unit_price = $value['unit_price'];
                        $Inventories->min_stock = $value['min_stock'];
                        $Inventories->type = 0;
                        $Inventories->save();

                        // log_created($Inventories);
                    } else {
                        // return InventoriesResource::MsgResponse(true,'this Inventories already use!');
                    }
                }
                return InventoriesResource::MsgResponse(false, 'Inventories Successfully Created!');
            }
        } catch (\Exception $exception) {
            return InventoriesResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function saveImage(Request $request)
    {
        try {
            $count = $Inventories = Inventories::where('type', 0)->count();
            if (request()->hasFile('image')) { //save image
                $file_name = 'Inventories/inventories_' . time() . $count . 's.jpg';
                $file = \request()->file('image');
                $array = array();
                if (save_image($file, $file_name)) {
                    $response['image'] = $file_name;
                    $response['image_display'] = asset(Storage::url($file_name));

                    return response()->json($response);
                }
            }
        } catch (\Exception $exception) {
            return InventoriesResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function show($id)
    {
        $Inventories = Inventories::findOrFail($id);
        $MaterialIssuancesItems = MaterialIssuancesItems::where('inventory_id', $id)->get();
        $collections = [
            'data' => new InventoriesResource($Inventories),
            'MaterialIssuancesItems' => MaterialIssuancesItems::where('inventory_id', $id)->get()
        ];

        return $collections;
    }
    public function edit($id)
    {
        $Inventories = Inventories::findOrFail($id);
        $MaterialIssuancesItems = MaterialIssuancesItems::where('inventory_id', $id)->get();
        $collections = [
            'data' => new InventoriesResource($Inventories),
            'Unit' => Unit::all(),
            'Categories' => Categories::all(),
        ];

        return $collections;
    }
    public function update(Request $request, $id)
    {
        // return $request;
        try {
            $data = Inventories::where('id', $id)->first();
            $result = InventoriesNames::where('id', '!=', $data->inventory_description_id)->where('description', $request->input('description'))->where('mat_code', $request->input('matcode'))->first();
            if (!$result) {
                $InventoriesNames = InventoriesNames::where('id', $data->inventory_description_id)->update([
                    'description' => $request->input('description'),
                    'category_id' => $request->input('category'),
                    'mat_code' => $request->input('matcode'),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
                if ($request->input('imageval') == 1) {
                    if (request()->hasFile('image')) { //save image
                        $count = $Inventories = Inventories::where('type', 0)->count();
                        $file_name = 'Inventories/inventories_' . time() . $count . 's.jpg';
                        $file = \request()->file('image');
                        $array = array();
                        if (save_image($file, $file_name)) {
                            $file_name = $file_name;
                        }
                    } else {
                        $file_name = NULL;
                    }
                } else {
                    $file_name = $request->input('image');
                }

                $Inventories = Inventories::where('id', $id)->update([
                    'unit_id' => $request->input('unit'),
                    'category_id' => $request->input('category'),
                    'quantity' => $request->input('quantity'),
                    'image' => $file_name,
                    'unit_price' => $request->input('unit_price'),
                    'min_stock' => $request->input('min_stock'),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
                $query = Inventories::where('id', $id)->first();
                return new InventoriesResource($query);
            } else {
                return InventoriesResource::MsgResponse(true, 'Sorry but this Description is already use');
            }
        } catch (\Exception $exception) {
            return InventoriesResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function validationInventory(Request $request)
    {
        // return $request;
        $result = InventoriesNames::where('description', $request->input('description'))->where('mat_code', $request->input('mat_code'))->first();
        if ($result) {
            $data = 'Error';
        } else {
            $data = 'No-Error';
        }
        return $data;
    }
    public function destroy($id)
    {
        try {
            $Inventories = Inventories::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return InventoriesResource::MsgResponse($type, $msg);
        } catch (\Exception $exception) {
            return InventoriesResource::MsgResponse(true, $exception->getMessage());
        }
    }

    public function catalog()
    {
        return InventoriesResource::collection(Inventories::whereNotNull('type')->withCount('issued')->get());
    }

    public function adjustQuantity(Request $request)
    {
        $inventory = Inventories::where('id', $request->input('inventory_id'))->withCount('issued')->first();
        $quantity = $request->input('adjusted_warehouse_quantity');
        if (!empty($inventory->issued_count)) {
            $quantity = $request->input('adjusted_warehouse_quantity') + $inventory->issued_count;
        }
        $inventory_update = Inventories::where('id', $request->input('inventory_id'))->update([
            'quantity' => $quantity
        ]);

        switch ($request->input('operation')) {
            case "addition":
                for ($i = 1; $i <= $request->input('quantity'); $i++) {
                    $asset = new OfficeSuppliesAndEquipment();
                    $asset->inventory_id = $request->input('inventory_id');
                    $asset->inventory_status_id = 1;
                    $asset->save();
                }
                break;
            case "deduction":
                $assets = OfficeSuppliesAndEquipment::where('inventory_id', $request->input('inventory_id'))
                            ->whereNull(['serial_model_no', 'asset_tag', 'assigned_to'])->get();
                for ($i = 1; $i <= $request->input('quantity'); $i++) {
                    $delete = OfficeSuppliesAndEquipment::where('id', $assets[$i]->id)->delete();
                }
                break;
            default:
                dd('default');
        }
        
        return new InventoriesResource(
            Inventories::where('id', $request->input('inventory_id'))
            ->withCount('issued')
            ->with('issued')
            ->first()
        );
    }
}

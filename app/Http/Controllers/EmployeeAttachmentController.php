<?php

namespace App\Http\Controllers;

use App\EmployeeAttachment;
use Illuminate\Http\Request;

class EmployeeAttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeAttachment  $employeeAttachment
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeAttachment $employeeAttachment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeAttachment  $employeeAttachment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeAttachment $employeeAttachment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeAttachment  $employeeAttachment
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeAttachment $employeeAttachment)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeOvertime;
use App\Http\Resources\Employee as ResourcesEmployee;
use App\Http\Resources\EmployeeOvertime as EmployeeOvertimeResource;
use App\Http\Resources\Log as LogResource;
use App\Http\Resources\OvertimeType as ResourcesOvertimeType;
use App\OvertimeType;
use App\RestdayHoliday;
use App\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use function App\Datetime\check_for_holidays;
use function App\Datetime\check_rest_day;
use function App\Datetime\get_overtime_rate;
use function App\Log\log_created;

class EmployeeOvertimeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resaource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $overtime = EmployeeOvertime::role(EmployeeOvertime::class)->get();
        return EmployeeOvertimeResource::collection($overtime);
    }

    public function create(){
        $employees = Employee::role(EmployeeOvertime::class)->get();
        $overtime_type = OvertimeType::all();

        $overtimeYears = EmployeeOvertime::select(DB::raw('YEAR(overtime_date) as overtime_year'))->groupBy('overtime_year')->get();
        $overtimeMonths = EmployeeOvertime::select(DB::raw('MONTHNAME(overtime_date) as overtime_month, MONTH(overtime_date) as overtime_month_value'))->groupBy('overtime_month')->get();

        return response()->json(
            [
                'employee_name' => ResourcesEmployee::collection($employees),
                'overtime_type' => ResourcesOvertimeType::collection($overtime_type),
                'overtimeYears' => $overtimeYears,
                'overtimeMonths' => $overtimeMonths

            ]
        );
    }

    /**
     * @param Request $request
     * @param $rendered_hours
     * @param $rate
     *
     * @return void
     */
    public function addRestDayHolidayWorks(Request $request, $rendered_hours, $rate)
    {
        $request->merge([
            'rendered_hours' => 8,
            'rate' => $rate['restday_rate'],
            'ot_id' => $rate['restday_id'],
            'restday_holiday_date' => $request->overtime_date
        ]);

        $restDayHolidayWork = RestdayHoliday::create($this->payload($request));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'employee_id' => 'required',
                'ot_id' => 'nullable',
                'attendance_id' => 'nullable',
                'holidays' => 'nullable',
                'rate' => 'nullable',
                'restday_holiday_date' => 'required',
                'file_date' => 'required',
                'start_time' => 'required',
                'rendered_hours' => 'required',
                'purpose' => 'nullable'
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date_filed = $request->overtime_date;
        //check rest day
        $rest_day = check_rest_day($date_filed);

        //check holidays
        $holiday = check_for_holidays($date_filed);

        $rate = get_overtime_rate($rest_day, $holiday);
        $overtime = new EmployeeOvertime();
        if ($request->purpose == 'Work on Holiday' || $request->purpose == 'Work on Restday') {
            $overtime->rendered_hours = $this->getTimeDifference($request);

            $this->addRestDayHolidayWorks($request, $overtime->rendered_hours, $rate);
        }

        if ($request->purpose == 'Extended Work' || $request->purpose == 'Temporary work on other department') {
            //$schedule = Schedule::getSchedulebyDepartment($request->employee_id);
            $overtime->rendered_hours = $this->getTimeDifference($request);
        }

        $overtime->employee_id = $request->employee_id;
        $overtime->start_time = $request->start_time;
        $overtime->ot_id = $rate['id'];
        $overtime->rate = $rate['rate'];
        $overtime->file_date = $request->file_date;
        $overtime->overtime_date = $request->overtime_date;
        $overtime->end_time = $request->end_time;
        $overtime->purpose = $request->purpose;
        $overtime->reason = $request->reason;
        $overtime->approved_status = 0;
        $overtime->save();
        log_created($overtime);

        return new EmployeeOvertimeResource($overtime);

        //apply ND if possible
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeOvertime  $employeeOvertime
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $overtime = EmployeeOvertime::findOrFail($id);
        return (new EmployeeOvertimeResource($overtime))->additional([
            'approver' => check_approver($overtime), //option if user is approver and already approved
            'history_logs' => LogResource::collection(EmployeeOvertime::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get()),
            'approved_status' =>  model_approval_status(check_approver($overtime))   //pending, waiting order approver,approved
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeOvertime  $employeeOvertime
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee_ot = EmployeeOvertime::findOrFail($id);
        $employee_overtime = new EmployeeOvertimeResource($employee_ot);
        $collections = [
            'employee_overtime' => $employee_overtime,
        ];
        return $collections;
    }
    public function update(Request $request, $id)
    {
        $date_filed = $request->overtime_date;
        $rest_day = check_rest_day($date_filed);
        $holiday = check_for_holidays($date_filed);
        $rate = get_overtime_rate($rest_day, $holiday);
        try {
            $overtime = EmployeeOvertime::findORFail($id);
            if ($request->purpose == 'Work on Holiday' || $request->purpose == 'Work on Restday') {
                $request->rendered_hours = $this->getTimeDifference($request);
            }

            if ($request->purpose == 'Extended Work' || $request->purpose == 'Temporary work on other department') {
                //$schedule = Schedule::getSchedulebyDepartment($overtime->employee_id);
                //$request->request->add(['start_time' => $schedule->end_full_day]);
                $request->rendered_hours = $this->getTimeDifference($request);
            }

            $overtime->update([
                'file_date' => date('Y-m-d', strtotime($request->file_date)),
                'overtime_date' => date('Y-m-d', strtotime($request->overtime_date)),
                'start_time' => $request->start_time,
                'ot_id' => $rate['id'],
                'rate' => $rate['rate'],
                'end_time' => $request->end_time,
                'rendered_hours' => $request->rendered_hours,
                'purpose' => $request->purpose,
                'reason' => $request->reason,
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            log_created($overtime); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            // $query = EmployeeOvertime::where('id', $id)->first();
            return new EmployeeOvertimeResource($overtime);
        }
        catch (\Exception $exception) {
            return EmployeeOvertimeResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeOvertime  $employeeOvertime
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $EmployeeOvertime = EmployeeOvertime::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return EmployeeOvertimeResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return EmployeeOvertimeResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Get Time Difference
     *
     * @param Request $request
     *
     * @return void
     */
    public function getTimeDifference(Request $request) {
        $overtime_start = strtotime($request->overtime_date . $request->start_time);
        $overtime_end = strtotime($request->overtime_date . $request->end_time);
        $diff = $overtime_start - $overtime_end;
        $hours = $diff / 3600;

        if (((int) date('H', strtotime($request->start_time))) < 13 && ((int) date('H', strtotime($request->end_time))) >= 13) {
            $hours = abs($hours) - 1;
        }
        return abs($hours);
    }
}

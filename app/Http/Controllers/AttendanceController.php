<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\AttendanceBreak;
use App\Employee;
use App\EmployeeLeave;
use App\Http\Resources\Attendance as AttendanceResource;
use App\Http\Resources\AttendanceSimple;
use Carbon\Carbon;
use function App\Approval\check_approver;

use function App\Approval\model_approval_status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
       $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $Attendance = Attendance::all();
        $history_attendance = AttendanceResource::collection($Attendance);
        $present_employee_today_list = AttendanceResource::collection(Attendance::whereDate('start_time', Carbon::today())->get());
        $present_employee_count = Attendance::whereDate('start_time', Carbon::today())->count();
        $onleave_count = $this->leavevalidation(EmployeeLeave::whereDate('start_date', Carbon::today())->get());
        $absent_count = Employee::count() - Attendance::whereDate('start_time', Carbon::today())->count();
        $attendance_array = array();
        $group_date = DB::table('hr_attendances')->select(DB::raw('id as id'),DB::raw('employee_id as employee_id'),DB::raw('start_time as start_time'),DB::raw('end_time as end_time'),DB::raw('DATE(start_time) as date'), DB::raw('count(*) as count'))->groupBy('date')->where('deleted_at',Null);

        if ($request->has('max')) {
            $group_date->orderBy('date', 'DESC')->limit($request->max);
        }

        $group_date = $group_date->get();

        // return $group_date;
        foreach($group_date as $key => $data){
            $Employee = Employee::whereDate('id', $data->employee_id)->first();
            $Attendance_data = Attendance::whereDate('start_time', $data->date)->get();
            $attendance_array[$key] = ['id' => $data->id, 'Date' => date('F d, Y',strtotime($data->date)),'attendance' => AttendanceResource::collection($Attendance_data)];
        }
        $collections = [
            'history_attendance' => $attendance_array,
            'present_employee_today_list' => $present_employee_today_list,
            'present_employee_count' => $present_employee_count,
            'onleave_count' => $onleave_count,
            'absent_count' => $absent_count,
        ];

        return $collections;
    }

    public function leavevalidation($data)
    {
        $count = 0;
        foreach($data as $key => $leave){
            $stats = check_approver($leave);
            if(model_approval_status($stats) != 'rejected' || model_approval_status($stats) != 'pending'){
                $count++;
            }
        }
        return $count;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->input('type') == 1){//1 = time in ,2 time out
                $Attendance = New Attendance;
                $Attendance->employee_id = $request->input('employee_id');
                $Attendance->project_site_id = $request->input('project_site_id');
                $Attendance->start_time = date('Y/m/d H:i:s');
                $Attendance->location = $request->input('location');
                $Attendance->image_path = $request->input('image_path');
                $Attendance->meta_data = $request->input('meta_data');
                $Attendance->save();
            }else{
                $Attendance = Attendance::where('employee_id', $request->input('employee_id'))->whereDate('start_time', Carbon::today())->where('end_time', Null)->update([
                    'end_time' => date('Y/m/d H:i:s')
                ]);

            }

            // return new AttendanceResource($Attendance);

        }catch(\Exception $exception){
            return AttendanceResource::errorResponse(true,$exception->getMessage());
        }
    }

    public function breakinsert(Request $request)
    {
        try{
            $Attendancedata = Attendance::where('employee_id', $request->input('employee_id'))->whereDate('start_time', Carbon::today())->first();
                $AttendanceBreak = New AttendanceBreak;
                $AttendanceBreak->attendance_id = $Attendancedata->id;
                $AttendanceBreak->break_id = $request->input('break_id');
                $AttendanceBreak->type = ($request->input('break_type') == 1?1:0);
                $AttendanceBreak->time = date('Y/m/d H:i:s');
                $AttendanceBreak->save();


        }catch(\Exception $exception){
            return AttendanceResource::errorResponse(true,$exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attendance_array = array();
        $Attendance = Attendance::findOrFail($id);
        $group_date = DB::table('hr_attendances')->select(DB::raw('id as id'),DB::raw('employee_id as employee_id'),DB::raw('start_time as start_time'),DB::raw('end_time as end_time'),DB::raw('DATE(start_time) as date'), DB::raw('count(*) as count'))->groupBy('date')->where('deleted_at',Null)->get();
        foreach ($group_date as $key => $data) {
            $Employee = Employee::whereDate('id', $data->employee_id)->first();
            $Attendance_data = Attendance::whereDate('start_time', $data->date)->get();
            $attendance_array[$key] = ['id' => $data->id, 'Date' => date('F d, Y',strtotime($data->date)),'attendance' => AttendanceResource::collection($Attendance_data)];
        }
        $collections = [
            'history_attendance' => $attendance_array,
            'data' => new AttendanceResource($Attendance),
        ];

        return $collections;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        //
    }

    /**
     * get attendance per date
     *
     * @param int $id
     *
     * @return void
     */
    public function getAllAttendancePerDate(int $id)
    {
        $attendance = Attendance::findOrFail($id);

        $getperDate = Attendance::where(DB::raw('date(start_time)'), date('Y-m-d', strtotime($attendance->start_time)))
            ->orderBy('created_at', 'ASC')->get();

        return AttendanceSimple::collection($getperDate);
    }

    public function getAttendanceYear()
    {
        $years = Attendance::select(DB::raw('YEAR(start_time) as year'))
        ->groupBy('year')
        ->orderBy('year', 'ASC')
        ->get();

        $array = [];

        foreach ($years as $year) {
            $array[]['year'] = (string) $year->year;
        }

        if (!in_array(date('Y'), $array)) {
            $array[]['year'] = date('Y');
        }

        return $array;
    }
}

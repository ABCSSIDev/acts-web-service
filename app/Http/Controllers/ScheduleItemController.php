<?php

namespace App\Http\Controllers;

use App\Http\Resources\ScheduleItem as ScheduleItemResource;
use App\Schedule;
use App\ScheduleItem;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ScheduleItemController extends Controller
{
    /**
     * list
     *
     * @return ResourceCollection
     */
    public function index($scheduleId): ResourceCollection
    {
        $scheduleItems = Schedule::findOrFail($scheduleId)
            ->scheduleItems()->get();

        return ScheduleItemResource::collection($scheduleItems);
    }

    /**
     * store data
     *
     * @param Request $request
     * @param int $scheduleId
     *
     * @return JsonResponse
     */
    public function store(
        Request $request,
        int $scheduleId
    ): JsonResponse {
        $data = [];
        $attaches = $request->input('employee_id');

        foreach ($attaches as $attach) {
            ScheduleItem::updateOrCreate(
                [
                    'schedule_id' => $scheduleId,
                    'employee_id' => $attach['employee_id']
                ],
                [
                    'schedule_id' => $scheduleId,
                    'employee_id' => $attach['employee_id'],
                    'time_in' => $request->time_in,
                    'time_out' => $request->time_out,
                    'restdays' => json_encode($request->restdays)
                ]
            );
        }

        return response()->json('Successfully insert data.');
    }

    /**
     * payload for store and update
     *
     * @param Request $request
     *
     * @return array
     */
    private function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'schedule_id' => 'required',
                'employee_id' => 'required',
                'time_in' => 'required',
                'time_out' => 'required',
                'working_days' => 'required'
            ]
        );
    }
}

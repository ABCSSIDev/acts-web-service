<?php

namespace App\Http\Controllers;

use App\Approval;
use App\Attendance;
use App\Department;
use App\Employee;
use App\EmployeeLeave;
use App\EmployeeOvertime;
use App\Holiday;
use App\JobDetail;
use App\LeaveType;
use App\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HRDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $scorecards = $this->fetchScorecards();
        return response()->json([
            "scorecards" => $scorecards,
            "employee_by_department" => $this->employeesByDepartment(),
            "leave" => $this->getLeave(),
            "absences" => $this->getConsecutiveAbsences(),
            "present_by_department" => $this->getPresentPerDepartment(),
            "birthdays" => $this->upcomingBirthdays(),
            "overtimes" => $this->getOvertime(),
            "holidays" => $this->getHolidays(),
            "logs" => $this->getLogs()
        ]);
    }

    public function fetchScorecards()
    {
        $all_employees = Employee::all();
        $date = Carbon::today()->format('Y-m-d');
        $present = [];
        $absent = [];
        $new_hires = [];
        $total_employee_count = 0;
        foreach ($all_employees as $employee) {
            $approval = Approval::where('reference_type', Employee::class)->where('reference_id', $employee->id)->first();
            if ($approval && $approval->approved_status == 1) {
                $total_employee_count++;
            }

            $new_hire = JobDetail::where('employee_id', $employee->id)->whereDate("joined_date", $date)->first();
            if ($new_hire) {
                array_push($new_hires, $employee);
            }

            $attendance = Attendance::where('employee_id', $employee->id)->whereDate("start_time", $date)->first();
            if ($attendance && ($approval && $approval->approved_status == 1)) {
                array_push($present, $employee);
                continue;
            }
            if ($approval && $approval->approved_status == 1) {
                array_push($absent, $employee);
            }
        }

        return [
            'total_employees' => $total_employee_count,
            'total_present' => count($present),
            'total_absent' => count($absent),
            'new_hire' => count($new_hires)
        ];
    }
    public function employeesByDepartment()
    {
        $departments = Department::all();
        $label = [];
        $dataCount = [];
        foreach ($departments as $department) {
            $label[] = $department->name;
            $employee = DB::table('hr_employees')
                        ->join('hr_job_details', 'hr_employees.id', '=', 'hr_job_details.employee_id')
                        ->join('hr_designations', 'hr_job_details.designation_id', '=', 'hr_designations.id')
                        ->join('hr_departments', 'hr_designations.department_id', '=', 'hr_departments.id')
                        ->select('hr_employees.id')
                        ->where('hr_departments.id', '=', $department->id)
                        ->count();
            $dataCount[] = $employee;
        }

        $data = [
            'label' => $label,
            'data' => $dataCount
        ];

        return $data;
    }
    public function getLeave()
    {
        $leave_types = LeaveType::all();
        $types = [];
        $total = 0;
        foreach ($leave_types as $leave_type) {
            $leave = EmployeeLeave::where('leave_id', $leave_type->id)
                            ->whereDate('end_date', '>=', Carbon::today()->toDateString())
                            ->whereDate('start_date', '<=', Carbon::today()->toDateString())
                            ->get();
            $types[] = array(
                'name' => $leave_type->name,
                'count' => $leave->count()
            );
            $total += $leave->count();
        }
        return $types;
    }
    public function getConsecutiveAbsences()
    {
        $employees = Employee::all();
        $date_today = Carbon::today();
        $employeeList = [];
        foreach ($employees as $employee) {
            $count = 0;
            $first_day = Carbon::today()->firstOfMonth();
            while ($first_day->lessThanOrEqualTo($date_today)) {
                $attendance = Attendance::where('employee_id', $employee->id)
                                ->whereDate('start_time', $first_day->format('Y-m-d'))->first();
                if (!$attendance) {
                    $count++;
                }
                $first_day->add('1 day');
            }
            if ($count >= 3) {
                $employeeList[] = array(
                    'name' => $employee->fullname(),
                    'count' => $count
                );
            }
            if (count($employeeList) == 5) {
                break;
            }
        }
        return $employeeList;
    }

    public function getPresentPerDepartment() {
        $departments = Department::all();
        $label = [];
        $dataCount = [];
        foreach ($departments as $department) {
            $label[] = $department->name;
            // $employee = Employee::whereHas('getJobDetails', function ($q) use ($department) {
            //     $q->whereHas('getDesignation', function ($designation_query) use ($department) {
            //         $designation_query->where('department_id', $department);
            //     });
            // })->whereHas('attendances', function ($attendance) {
            //     $attendance->whereDate('start_time', Carbon::today()->format('Y-m-d'));
            // })->count();
            $employee = DB::table('hr_employees')
                        ->join('hr_job_details', 'hr_employees.id', '=', 'hr_job_details.employee_id')
                        ->join('hr_designations', 'hr_job_details.designation_id', '=', 'hr_designations.id')
                        ->join('hr_departments', 'hr_designations.department_id', '=', 'hr_departments.id')
                        ->join('hr_attendances', 'hr_attendances.employee_id', '=', 'hr_employees.id')
                        ->select('hr_employees.id')
                        ->where('hr_departments.id', '=', $department->id)
                        ->whereDate('hr_attendances.start_time', Carbon::today()->format('Y-m-d'))
                        ->count();
            $dataCount[] = $employee;
        }
        $data = [
            'label' => $label,
            'data' => $dataCount
        ];
        return $data;
    }

    public function upcomingBirthdays()
    {
        $birthdays = [];
        $employees = Employee::whereMonth('birth_date', '=', Carbon::today()->format('m'))
        ->whereDay('birth_date', '>=', Carbon::today()->format('d'))
        ->select('first_name', 'middle_name', 'last_name', 'birth_date')
        ->orderByRaw('DATE_FORMAT(birth_date, "%m-%d")', 'ASC')
        ->get();

        foreach ($employees as $employee) {
            $birthdays[] = array(
                'name' => $employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name,
                'birthday' => Carbon::createFromDate($employee->birth_date)->format('F d')
            );
        }
        return $birthdays;
    }

    public function getOvertime(){
         $overtimes = EmployeeOvertime::all()->take(10);
         $formatted_response = [];
         foreach($overtimes as $overtime){
             $message = $overtime->getEmployee->fullname() . " filed an overtime at " . date('F d, Y', strtotime($overtime->overtime_date));
             array_push($formatted_response, $message);
         }

         return $formatted_response;
    }

    public function getHolidays(){
        $holiday_arr = [];
        $holidays = Holiday::whereMonth('holiday_date', '=', Carbon::today()->format('m'))
        ->whereDay('holiday_date', '>=', Carbon::today()->format('d'))
        ->orderByRaw('DATE_FORMAT(holiday_date, "%m-%d")', 'ASC')
        ->get();
        foreach($holidays as $holiday){
            if(date('Y', strtotime($holiday->holiday_date) == Carbon::today()->format('Y')) || $holiday->is_recurring == 1 ){
                array_push($holiday_arr, [
                    'name' => $holiday->name,
                    'date' => date('F d', strtotime($holiday->holiday_date)),
                    'type' => ($holiday->holiday_type == 1?"Regular":"Special Non-Working")
                ]);
            }
        }
        return $holiday_arr;
    }

    public function getLogs(){
        $logs = Log::latest()->get()->take(10);
        $log_arr = [];
        foreach($logs as $log){
            $full_name = (!empty($log->user->getEmployee)?$log->user->getEmployee->fullname():($log->user->id == 1?"System Administrator":$log->user->username));
            array_push($log_arr, $log->description . ' by ' . $full_name);
        }
        return $log_arr;
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Employee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MeController extends Controller
{
    public function __invoke(Request $request)
    {
        $user = $request->user();
        $role = "";
        $employee = Employee::where('user_id', $user->id)->first();
        $employee_info = [
            'isEmployee' => false
        ];

        if (!empty($employee)) {
            $employee_info = [
                'isEmployee' => true,
                'id' => $employee->id,
                'employee_no' => $employee->employee_no,
                'name' => $employee->first_name . (empty($employee->middle_name)? " " : " " . $employee->middle_name . " ") . $employee->last_name
            ];
        }
        
        if ($user->id === 1) {
            $role = "Administrator";
        } else {
            $role = $user->getUserAccess->getRole->role_name;
        }

        return response()->json([
            'id' => $user->id,
            'email' => $user->email,
            'name' => $user->name,
            'role' => $role,
            'employee' => $employee_info
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Resources\ScheduleEmployee as ScheduleEmployeeResource;
use App\Schedule;
use App\ScheduleEmployee;
use App\ScheduleEmployeeItem;
use DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

use function App\TimeAttendance\check_shift_schedule;

class ScheduleEmployeeController extends Controller
{
    /**
     * list
     *
     * @return ResourceCollection
     */
    public function index($scheduleId): ResourceCollection
    {
        $scheduleEmployee = ScheduleEmployee::all();

        return ScheduleEmployeeResource::collection($scheduleEmployee);
    }

    /**
     * store data
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request, $scheduleId): JsonResponse
    {
        
        $schedule = Schedule::findOrFail($scheduleId);

        $begin = new DateTime($schedule->schedule_date_start);

        $ends = new DateTime($schedule->schedule_date_end);

        for ($i = $begin; $i <= $ends; $i->modify('+1 day')) {
            $schedule_employee = ScheduleEmployee::create([
                'schedule_id' => $scheduleId,
                'date' => $i->format("Y-m-d"),
            ]);

            foreach ($schedule->scheduleItems as $employee) {
                $date_schedule = date("N", strtotime($i->format("Y-m-d")));
                $rest_days = json_decode($employee->restdays);
                if (!in_array($date_schedule, $rest_days)) {
                    $schedule_employee_items = ScheduleEmployeeItem::create([
                        'schedule_employee_id' => $schedule_employee->id,
                        'employee_id' => $employee->employee_id
                    ]);
                }
            }
        }

        $schedule->update([
            'is_activate' => 1
        ]);

        return response()->json('Successfully Activate Employee Schedule.');
    }

    /**
     * payload for store and update
     *
     * @param Request $request
     *
     * @return array
     */
    private function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'schedule_id' => 'required',
                'date' => 'required'
            ]
        );
    }
}

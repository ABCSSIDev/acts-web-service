<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Resources\MaterialUses as MaterialUsesResource;
use App\Http\Resources\ServiceCharges as ServiceChargesResource;
use App\Http\Resources\ServiceOrders as ServiceOrdersResource;
use App\Http\Resources\Subscribers as SubscribersResource;
use App\Http\Resources\Technicians as TechniciansResource;
use App\Inventories;
use App\MaterialUses;
use App\ServiceCharges;
use App\ServiceOrders;
use App\Subscribers;
use App\Team;
use App\Technicians;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function App\Log\log_created;

class ServiceOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $ServiceOrders = ServiceOrders::all();
        return ServiceOrdersResource::collection($ServiceOrders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = array();
        $Team = Team::all();
        $Subscribers = Subscribers::all();
        $response['Team'] = $Team;
        $response['Subscribers'] = $Subscribers;

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('team_member') != []) {
            foreach ($request->input('team_member') as $key => $value) {
                $Department = Employee::where(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', '%' . $value . '%')->first();
                $employeeid[] = $Department->id;
            }
        }
        try {
            $slip_query = ServiceOrders::create([
                'team_id' => $request->input('team_code'),
                'subscribers_id' => $request->input('subscriber_id'),
                'team_member' => json_encode($employeeid),
                'account_no' => $request->input('account_no'),
                'service_no' => $request->input('service_no'),
                'so_no' => $request->input('so_no'),
                'type' => $request->input('type'),
                'completion_date' => $request->input('date'),
                'time_start' => $request->input('time_start'),
                'time_end' => $request->input('time_end'),
                'remarks' => $request->input('remarks'),
                'with_pob' => $request->input('with_pob'),
                'without_pob' => $request->input('without_pob'),
                'type_pob' => $request->input('type_pob'),

            ]);
            log_created($slip_query);
            return new ServiceOrdersResource($slip_query);
        } catch (\Exception $exception) {
            return ServiceOrdersResource::MsgResponse(true, $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ServiceOrders = ServiceOrders::findOrFail($id);
        return (new ServiceOrdersResource($ServiceOrders))->additional([
            'invetory' => Inventories::where('type', 0)->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $ServiceOrdersDelete = ServiceOrders::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return ServiceOrdersResource::MsgResponse($type, $msg);
        } catch (\Exception $exception) {
            return ServiceOrdersResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function findTeamMembers($id)
    {
        $data = Technicians::where('teams_id', $id)->get();
        return TechniciansResource::collection($data);
    }
    public function storeServiceCharges(Request $request)
    {
        // return $request;
        try {
            $slip_query = ServiceCharges::create([
                'service_id' => $request->input('service_id'),
                'type' => $request->input('type'),
                'description' => $request->input('description'),
                'amount' => $request->input('amount'),
            ]);
            return ServiceChargesResource::MsgResponse(false, 'Successfully Created!');
        } catch (\Exception $exception) {
            return ServiceChargesResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function storeServiceOrderPayment(Request $request, $id)
    {
        // return $request;
        try {
            
            $slip_query = ServiceOrders::where('id', $id)->update([
                'payment_mode' => $request->input('payment_mode'),
                'check_or_no' => ($request->input('payment_mode') == 'Cash' ? $request->input('or_no') : $request->input('check_no') ),
                'bank' => $request->input('bank'),
                'branch' => $request->input('branch'),
            ]);
            $ServiceOrders = ServiceOrders::where('id', $id)->first();
            return new  ServiceOrdersResource($ServiceOrders);
        } catch (\Exception $exception) {
            return ServiceOrdersResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function storeSubscribers(Request $request)
    {
        try {
            $slip_query = Subscribers::create([
                'subscriber_name' => $request->input('name'),
                'email' => $request->input('email'),
                'home_no' => $request->input('home_code') . '' . $request->input('home_no'),
                'other_no' => $request->input('other_code') . '' . $request->input('other_no'),
                'mobile_no' => $request->input('mobile_prefix') . '' . $request->input('mobile_no'),
                'gender' => $request->input('gender'),
                'birthdate' => $request->input('birth_date'),
                'civil_status' => $request->input('civil_status'),
                'household_held' => $request->input('household'),
            ]);
            return $slip_query;
            $Subscribers = Subscribers::all();
            return SubscribersResource::collection($Subscribers);
        } catch (\Exception $exception) {
            return SubscribersResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function findServiceinfo($id)
    {
        $data = Subscribers::where('id', $id)->first();
        return $data;
    }
    public function insertServiceinfo(Request $request)
    {
        try {
            $service = array();
            $billings = array();
            $service[] = [
                'house_no' => $request->input('ser_home_house'),
                'building_name' => $request->input('ser_building_name'),
                'sub_vill_brgy' => $request->input('ser_sub_vill_brgy'),
                'city_town' => $request->input('ser_city_town'),
                'province' => $request->input('ser_province'),
                'zip_code' => $request->input('ser_zip_code'),
            ];
            $billings[] = [
                'house_no' => $request->input('bill_home_house'),
                'building_name' => $request->input('bill_building_name'),
                'sub_vill_brgy' => $request->input('bill_sub_vill_brgy'),
                'city_town' => $request->input('bill_city_town'),
                'province' => $request->input('bill_province'),
                'zip_code' => $request->input('bill_zip_code'),
            ];
            $Subscribers = Subscribers::where('id', $request->input('id'))->update([
                'service_address' => json_encode($service),
                'billing_address' => json_encode(($request->input('billing') == true ? $service : $billings)),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            $Subscribers = Subscribers::where('id', $request->input('id'))->first();
            return new SubscribersResource($Subscribers);
        } catch (\Exception $exception) {
            return SubscribersResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function storeMaterialUses(Request $request)
    {
        try {
            $Inventories = Inventories::where('id', $request->input('inventory'))->first();
            $slip_query = MaterialUses::create([
                'service_id' => $request->input('service_id'),
                'inventory_id' => $request->input('inventory'),
                'type' => $Inventories->type,
                'qty_used' => $request->input('quantity'),
                'brandandmodel' => $request->input('brandandmodel'),
                'serial_no' => $request->input('serial_no')
            ]);
            return new MaterialUsesResource($slip_query);
        } catch (\Exception $exception) {
            return MaterialUsesResource::MsgResponse(true, $exception->getMessage());
        }
    }
}

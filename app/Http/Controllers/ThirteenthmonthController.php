<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use App\EmployeeSalary;
use App\Helpers\Payruns\AttendanceHelper;
use App\Http\Resources\Employee as ResourcesEmployee;
use App\Http\Resources\ThirteenthMonth as ThirteenthMonthResource;
use App\ThirteenthMonth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function App\Log\log_created;

class ThirteenthmonthController extends Controller
{

    /**
     * @var AttendanceHelper
     */
    private $attendanceHelper;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');

        $this->attendanceHelper = new AttendanceHelper();
    }

    public function index(Request $request)
    {
        $data = [];
        $employees = Employee::all();

        if ($request->year != null) {
            $current_year = $request->year;
        } else {
            $current_year = Carbon::now()->format('Y');
        }

        foreach ($employees as $employee) {
            $first_period = 0;
            $second_period = 0;
            $first_attendance = Attendance::where('employee_id', $employee->id)
                                    ->select('employee_id', DB::raw('MONTH(start_time) as attendance_month'))
                                    ->whereBetween(DB::raw('date(start_time)'), [
                                        date('Y-m-d', strtotime($current_year . '-01-01')),
                                        date('Y-m-d', strtotime($current_year . '-05-31'))
                                    ])
                                    ->whereNotNull('end_time')
                                    ->groupBy('attendance_month')
                                    ->get();

            foreach ($first_attendance as $first) {
                $first_period++;
            }

            $second_attendance = Attendance::where('employee_id', $employee->id)
                                    ->select('employee_id', DB::raw('MONTH(start_time) as attendance_month'))
                                    ->whereBetween(DB::raw('date(start_time)'), [
                                        date('Y-m-d', strtotime($current_year . '-06-01')),
                                        date('Y-m-d', strtotime($current_year . '-12-31'))
                                    ])
                                    ->whereNotNull('end_time')
                                    ->groupBy('attendance_month')
                                    ->get();

            foreach ($second_attendance as $second) {
                $second_period++;
            }

            $data[] = array(
                '_parse_name' => html_entity_decode($employee->last_name) . ', ' . html_entity_decode($employee->first_name),
                'first_period' => $first_period,
                'second_period' => $second_period,
                'status_first' => 'Pending',
                'status_second' => 'Pending'
            );
        }

        return $data;
    }

    public function create()
    {
        $response = array();
        $employee = Employee::role(ThirteenthMonth::class)->get();
        $response['employee'] = $employee;

        return response()->json($response);
    }

    /**
     * store
     *
     * @param Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $thirteenth_month = ThirteenthMonth::create($this->payload($request));

        log_created($thirteenth_month);

        return new ThirteenthMonthResource($thirteenth_month);
    }

    public function getDetails($id, Request $request)
    {
        $employee = Employee::findOrFail($id);

        if ($request->year != null) {
            $current_year = $request->year;
        } else {
            $current_year = Carbon::now()->format('Y');
        }
        
        $date1 = $current_year . ($request->period == 1 ? '-01-01' : '-06-01');
        $date2 = $current_year . ($request->period == 1 ?  '-05-31' : '-12-31');

        $total_hours = 0;
        $total_days = 0;

        $rendered_hours = Attendance::where('employee_id', $id)
                                    ->select('start_time', 'end_time')
                                    ->whereBetween(DB::raw('date(start_time)'), [
                                        date('Y-m-d', strtotime($date1)),
                                        date('Y-m-d', strtotime($date2))
                                    ])
                                    ->whereNotNull('end_time')
                                    ->get();

        $dates = array();

        foreach ($rendered_hours as $hours) {
            $date = date('Y-m-d', strtotime($hours->start_time));

            if (!in_array($date, $dates)) {
                $dates[] = $date;
                $attendance_emp = Attendance::where(
                    DB::raw('date(start_time)'),
                    $date
                )->where('employee_id', $id);
                
    
                $check = $this->attendanceHelper->checkSameDayTimeinAttendance($attendance_emp, $date);
                
                if ($check['ifSameDayAttendance']) {
                    $total_hours += $check['total_hours'];
                } else {
                    $total_hours += (strtotime($hours->end_time) -
                             strtotime($hours->start_time)) / 3600;
                }

                $total_days++;
            }
        }

        $months_worked = 0;
        $month = [];

        $months = Attendance::where('employee_id', $employee->id)
                                    ->select('employee_id', DB::raw('MONTHNAME(start_time) as attendance_month'), DB::raw('COUNT(start_time) as attendance_count'))
                                    ->whereBetween(DB::raw('date(start_time)'), [
                                        date('Y-m-d', strtotime($date1)),
                                        date('Y-m-d', strtotime($date2))
                                    ])
                                    ->whereNotNull('end_time')
                                    ->orderBy('start_time', 'ASC')
                                    ->groupBy('attendance_month')
                                    ->get();

        $salary = EmployeeSalary::where('employee_id', $id)->first();
        $monthly = $salary->monthly_salary;
        $monthly_to_daily = ($salary->rate_per_day ? $salary->rate_per_day : ($monthly * 12) / 313);
        $computation = 0;


        $computation = ($monthly / 12) * $months_worked;

        $computation = $computation / 26.08;

        $attendance_count = [];

        foreach ($months as $month_data) {
            $months_worked++;
            $month[] = $month_data->attendance_month;
            $attendance_count[] = ($month_data->attendance_count * ($monthly / 12) / 26.08);
        }

        $response = array(
            'employee_id' => $employee->id,
            'employee_name' => $employee->fullNameLastNameFirst(),
            'rendered_hours' => round($total_hours, 2),
            'months_worked' => $months_worked,
            'total_days_worked' => $total_days,
            'months' => implode(',', $month),
            'amount' => array_sum($attendance_count),
            'year' => $current_year
        );

        return response()->json($response);
    }

    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'employee_id' => ['required'],
                'thirteenth_month_year' => ['required'],
                'period' => ['required'],
                'category' => ['required'],
                'months_worked' => ['required'],
                'total_months' => ['required'],
                'hours_render' => ['required'],
                'amount' => ['required']
            ]
        );
    }

    /**
     * history
     *
     * @param Request $request
     *
     * @return void
     */
    public function history(Request $request)
    {
        $thirteenth_month = ThirteenthMonth::all();

        return ThirteenthMonthResource::collection($thirteenth_month);
    }

    public function show($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\AccountabilityIssuance;
use App\AccountabilityRequisition;
use App\Assets;
use App\Employee;
use App\Http\Resources\AccountabilityIssuance as ResourcesAccountabilityIssuance;
use App\IssuanceItems;
use Illuminate\Http\Request;
use function App\Approval\check_approver;
use App\Http\Resources\Log as LogResource;
use function App\Approval\model_approval_status;

use function App\Log\log_created;

class AccountabilityIssuanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ResourcesAccountabilityIssuance::collection(AccountabilityIssuance::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'requisitions'  => AccountabilityRequisition::all(),
            'employees'     => Employee::role(AccountabilityRequisition::class)->get(),
            'assets'        => Assets::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $issuance_query = AccountabilityIssuance::create([
                'requisition_id'       => $request->requisition_id,
                'employee_id'       => $request->employee_id,
                'from_employee_id'  => $request->from_employee_id,
                // 'type'              => $request->type,
                'date'              => $request->date
            ]);

            if ($request->has('issuance_items')) {
                foreach ($request->issuance_items as $item) {
                    foreach ($item['asset_id'] as $key => $asset) {
                        $issuance_item                = new IssuanceItems();
                        $issuance_item->issuance_id   = $issuance_query->id;
                        $issuance_item->asset_id      = $asset['asset_id'];
                        $issuance_item->save();
                    }
                }
            }
            // log_created($issuance_query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new ResourcesAccountabilityIssuance($issuance_query);
        } catch (\Exception $exception) {
            return ResourcesAccountabilityIssuance::errorResponse($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AccountabilityIssuance  $accountabilityIssuance
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $issuance = AccountabilityIssuance::findOrFail($id);
            return (new ResourcesAccountabilityIssuance($issuance))->additional([
                'approver' => check_approver($issuance), //option if user is approver and already approved
                'approved_status' =>  model_approval_status(check_approver($issuance)),   //pending, waiting order approver,approved 
                // 'history_logs' => LogResource::collection(AccountabilityIssuance::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
            ]);
        }catch (\Exception $exception) {
            return ResourcesAccountabilityIssuance::errorResponse($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AccountabilityIssuance  $accountabilityIssuance
     * @return \Illuminate\Http\Response
     */
    public function edit(AccountabilityIssuance $accountabilityIssuance)
    {
        return response()->json([
            'data'          => $accountabilityIssuance,
            'requisitions'  => AccountabilityRequisition::all(),
            'employees'     => Employee::role(AccountabilityIssuance::class)->get(),
            'assets'        => Assets::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AccountabilityIssuance  $accountabilityIssuance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccountabilityIssuance $accountabilityIssuance)
    {
        try {
            $updated = AccountabilityIssuance::where('id', $accountabilityIssuance->id)->update(
                [$request->column_name => $request->new_value]
            );
            if (!empty($updated)) {
                $query = AccountabilityIssuance::where('id', $accountabilityIssuance->id)->first();
                return new ResourcesAccountabilityIssuance($query);
            }
        } catch (\Exception $exception) {
            return ResourcesAccountabilityIssuance::errorResponse(true, $exception->getMessage());
        }
    }

    /**
     * updateAllColumns
     *
     * @param Request $request
     * @param [string] $id
     *
     * @return void
     */
    public function updateAllColumns(Request $request, $id): array
    {
        try {
            $update_query = AccountabilityIssuance::where('id', $id)->update([
                'requisition_id'    => $request->requisition_id,
                'employee_id'       => $request->employee_id,
                'from_employee_id'  => $request->from_employee_id,
                'type'              => $request->type,
                'date'              => $request->date
            ]);

            if (!empty($update_query)) {
                $query = AccountabilityIssuance::where('id', $id)->first();
                return new ResourcesAccountabilityIssuance($query);
            }
        } catch (\Exception $exception) {
            return ResourcesAccountabilityIssuance::errorResponse(true, $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $delete_query = AccountabilityIssuance::destroy(explode(',', $id));
            return ResourcesAccountabilityIssuance::errorResponse('Successfully Deleted!');
        } catch (\Exception $exception) {
            return ResourcesAccountabilityIssuance::errorResponse($exception->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\MaterialIssuances as MaterialIssuancesResource;
use App\MaterialIssuancesItems;
use App\MaterialIssuances;
use App\Technicians;
use App\Team;
use App\IssuanceTemplates;
use App\Inventories;
use function App\Log\log_created;
use App\Http\Resources\Technicians as TechniciansResource;
use App\ReturnUsedMaterials;
use App\Http\Resources\ReturnUsedMaterials as ReturnUsedMaterialsResource;
use App\Assets;
use App\IssuanceTemplatesItems;

class MaterialIssuancesController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $MaterialIssuances = MaterialIssuances::all();
        return MaterialIssuancesResource::collection($MaterialIssuances);
    }
    
    public function create(){
        $response = array();
        $Team = Team::all();
        $Technicians = TechniciansResource::collection(Technicians::all());
        $IssuanceTemplates = IssuanceTemplates::all();
        $Inventories = Inventories::where('type', 0)->get();
        $response['Team'] = $Team;
        $response['Technicians'] = $Technicians;
        $response['IssuanceTemplates'] = $IssuanceTemplates;
        $response['Inventories'] = $Inventories;

        return response()->json($response);
    }
    public function findAssetTag($id)
    {
        $data = Assets::where('inventory_id', $id)->first();
        return $data->asset_tag;
    }
    public function store(Request $request)
    {
        try {
            $MaterialIssuances = New MaterialIssuances;
            $MaterialIssuances->team_id = $request->input('team_id');
            $MaterialIssuances->tech1 = $request->input('tech1');
            $MaterialIssuances->tech2 = $request->input('tech2');
            $MaterialIssuances->template_id = $request->input('template_id');
            $MaterialIssuances->date_issue = $request->input('date_issue');
            $MaterialIssuances->save();
            
            log_created($MaterialIssuances); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            $data =  $request->input('item');
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $MaterialIssuancesItems = New MaterialIssuancesItems;
                    $MaterialIssuancesItems->issuance_id = $MaterialIssuances->id;
                    $MaterialIssuancesItems->inventory_id = $value['inventory_id'];
                    $MaterialIssuancesItems->quantity = $value['quantity'];
                    $MaterialIssuancesItems->save();
                }
            }
            
            return new MaterialIssuancesResource($MaterialIssuances);
        } catch (\Exception $exception) {
            return MaterialIssuancesResource::MsgResponse(true, $exception->getMessage());
        }
    }

    public function show($id)
    {
        $MaterialIssuances = MaterialIssuances::findOrFail($id);
        return new MaterialIssuancesResource($MaterialIssuances);
    }

    public function destroy($id)
    {
        try {
            $MaterialIssuancesDelete = MaterialIssuances::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return MaterialIssuancesResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return MaterialIssuancesResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function returnUseMaterial($id)
    {
        $ReturnUsedMaterials = ReturnUsedMaterials::findOrFail($id);
        return new ReturnUsedMaterialsResource($MaterialIssuances);
    }

    public function getOnHand($team_id, $template_id)
    {
        if (!empty($team_id) && !empty($template_id)) {
            $issued = MaterialIssuances::where('team_id', $team_id)->get();
            $template_item = IssuanceTemplatesItems::select('inventory_id')->where('template_id', $template_id)->get()->map(function ($row) {
                return $row->inventory_id;
            });
            if (!empty($issued) && count($issued) > 0) {
                $item_quantity_array = [];
                $item_issued = [];
                $item_returned = [];
                $issuance_items = [];
                $item_onhand = [];

                foreach ($issued as $key => $item) {
                    $items = MaterialIssuancesItems::where('issuance_id', $item->id)
                        ->whereIN('inventory_id', $template_item)
                        ->get();

                    $grouped_by_inventory = $items->groupBy('inventory_id');

                    foreach ($grouped_by_inventory->toArray() as $key => $value) {
                        if (empty($issuance_items[$key])) {
                            $issuance_items[$key] = [$value[0]["id"]];
                        } else {
                            array_push($issuance_items[$key], $value[0]["id"]);
                        }
                    }

                    $item_quantity = $grouped_by_inventory->map(function ($row) {
                        return $row->sum('quantity');
                    });

                    foreach ($item_quantity as $key => $item) {
                        if (empty($item_quantity_array[$key])) {
                            $item_quantity_array[$key] = $item;
                        } else {
                            $item_quantity_array[$key] += $item;
                        }
                    }
                }

                foreach ($issuance_items as $key => $issuance_item) {
                    foreach ($issuance_item as $item_key => $item) {
                        $materials = ReturnUsedMaterials::where('issuance_item_id', $item)->get();
                        if (!empty($materials) || count($materials) > 0) {
                            $grouped_by_status = $materials->groupBy('status');

                            $item_issued[$key] = 0;
                            $item_returned[$key] = 0;

                            $quantity = $grouped_by_status->map(function ($row) {
                                return $row->sum('quantity');
                            });
    
                           
                                $item_issued[$key] += $quantity[1];
    
                                $item_returned[$key] += $quantity[0];
                        }
                    }
                }
            
                foreach ($item_quantity_array as $key => $value) {
                    $item_onhand[$key] = $value - abs($item_issued[$key] - $item_returned[$key]);
                }

                $template_items = IssuanceTemplatesItems::where('template_id', $template_id)->get();
                $response = [];
                foreach ($template_items as $key => $template_item) {
                    $response[$key] = [
                        "inventory_id" => $template_item->inventory_id,
                        "id" => $template_item->id,
                        "inventory_description_id" => $template_item->getInventories->getInventoriesNames->description,
                        "onhand"  => $item_onhand[$template_item->inventory_id],
                        "buffer_stock" => $template_item->quantity,
                        "unit" => $template_item->getInventories->getUnit->unit_code
                    ];
                }
                return response()->json($response);
            }
        }
        return false;
    }
}
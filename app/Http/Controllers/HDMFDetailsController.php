<?php

namespace App\Http\Controllers;

use App\HDMFDetails;
use Illuminate\Http\Request;

class HDMFDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HDMFDetails  $hDMFDetails
     * @return \Illuminate\Http\Response
     */
    public function show(HDMFDetails $hDMFDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HDMFDetails  $hDMFDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HDMFDetails $hDMFDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HDMFDetails  $hDMFDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(HDMFDetails $hDMFDetails)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeExit;
use App\Http\Resources\Employee as EmployeeResource;
use Illuminate\Http\Request;
use App\Http\Resources\EmployeeExit as EmployeeExitResource;
use function App\Approval\check_approver;
use App\Http\Resources\Log as LogResource;
use function App\Approval\model_approval_status;
use function App\Log\log_created;

class EmployeeExitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $EmployeeExit = EmployeeExit::role(EmployeeExit::class)->get();
        return EmployeeExitResource::collection($EmployeeExit);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function create(){
        return EmployeeResource::collection(Employee::role(EmployeeExit::class)->get());
    }

    public function store(Request $request)
    {
        try{
            $employee_exit = New EmployeeExit;
            $employee_exit->employee_id = $request->input('employee');
            $employee_exit->interviewer_id = $request->input('interviewer');
            $employee_exit->separation_date = $request->input('separation_date');
            $employee_exit->reason = $request->input('reason');
            $employee_exit->exit_type = $request->input('exit_type');
            $employee_exit->save();
            log_created($employee_exit); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new EmployeeExitResource($employee_exit);
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeExit  $employeeExit
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employeeExit = EmployeeExit::findOrFail($id);    
        return (new EmployeeExitResource($employeeExit))->additional([
            'approver' => check_approver($employeeExit), //option if user is approver and already approved
            'approved_status' =>  model_approval_status(check_approver($employeeExit)),   //pending, waiting order approver,approved 
            'history_logs' => LogResource::collection(EmployeeExit::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeExit  $employeeExit
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::role(EmployeeExit::class)->get();
        $emp_exit = EmployeeExit::findOrFail($id);     
        $employee_exit = new EmployeeExitResource($emp_exit);
        $collections = [
            'employee_exit' => $employee_exit,
            'employee' => $employee,
        ];
        return $collections;
    }
    public function update(Request $request, $id)
    {
        $employee_exit = EmployeeExit::where('id', $id)->update([ 
            'interviewer_id' => $request->input('interviewer'),
            'separation_date' => $request->input('separation_date'),
            'reason' => $request->input('reason'),
            'exit_type' => $request->input('exit_type') ,
            'updated_at' => date('Y/m/d H:i:s')
        ]);
        $query = EmployeeExit::where('id', $id)->first();
        return new EmployeeExitResource($query);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeExit  $employeeExit
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $EmployeeExitDelete = EmployeeExit::whereIn('id', explode(',', $id))->update([
            'deleted_at' => date('Y/m/d H:i:s')
        ]);
        $type = false;
        $msg = 'Successfully Deleted!';
        return EmployeeExitResource::MsgResponse($type,$msg);
    }
    public function massdelete(Request $request, $id)
    {
        $EmployeeExit = EmployeeExit::whereIn('id',$request)->update([
            'deleted_at' => date('Y/m/d H:i:s')
        ]);
        try {
            $EmployeeExitDelete = EmployeeExit::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return EmployeeExitResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return EmployeeExitResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

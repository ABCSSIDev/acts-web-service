<?php

namespace App\Http\Controllers;

use App\BIRTemplateCategories;
use App\BIRTemplateCategoryDeductions;
use App\BIRTemplateExemptions;
use App\BIRTemplatePeriods;
use App\BIRTemplateSalaries;
use App\BIRTemplateTaxCategories;
use App\BIRTemplateTaxCriterias;
use App\Http\Resources\BIRTemplateCategoryDeductionResource;
use App\Http\Resources\BIRTemplateCategoryResource;
use App\Http\Resources\BIRTemplateExemptionResource;
use App\Http\Resources\BIRTemplatePeriodResource;
use App\Http\Resources\BIRTemplateSalaryResource;
use App\Http\Resources\BIRTemplateTaxCategoryResource;
use App\Http\Resources\BIRTemplateTaxCriteriasResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class BIRController extends Controller
{
    //

    /**
     * Show BIR Categories
     *
     * @return ResourceCollection
     */
    public function showCategories(): ResourceCollection
    {
        $category = BIRTemplateCategories::all();
        return BIRTemplateCategoryResource::collection($category);
    }

    /**
     * Show BIR Category
     *
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function showCategory($id): JsonResource
    {
        $category = BIRTemplateCategories::findORFail($id);
        return new BIRTemplateCategoryResource($category);
    }

    /**
     * Store BIR Category
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function storeCategory(Request $request): JsonResource
    {
        $validate = $this->categoryValidation($request);
        $category = BIRTemplateCategories::create($validate);
        return new BIRTemplateCategoryResource($category);
    }

    /**
     * Update BIR Category
     *
     * @param Request $request
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function updateCategory(Request $request, $id): JsonResource
    {
        $validate = $this->categoryValidation($request);
        $category = BIRTemplateCategories::findORFail($id);
        $category->update($validate);
        return new BIRTemplateCategoryResource($category);
    }

    /**
     * Delete BIR Category
     *
     * @param [int] $id
     *
     * @return Response
     */
    public function deleteCategory($id): Response
    {
        $category = BIRTemplateCategories::findORFail($id);
        $category->delete();
        return response(null, 204);
    }

    /**
     * BIR Category Payload
     *
     * @param Request $request
     *
     * @return array
     */
    public function categoryValidation(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'category' => 'required|string'
            ]
        );
    }

    /**
     * Show BIR Periods
     *
     * @return ResourceCollection
     */
    public function showPeriods(): ResourceCollection
    {
        $periods = BIRTemplatePeriods::all();
        return BIRTemplatePeriodResource::collection($periods);
    }

    /**
     * Show BIR Period
     *
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function showPeriod($id): JsonResource
    {
        $period = BIRTemplatePeriods::findORFail($id);
        return new BIRTemplatePeriodResource($period);
    }

    /**
     * Create BIR Period
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function storePeriod(Request $request): JsonResource
    {
        $validate = $this->periodValidate($request);
        $period = BIRTemplatePeriods::create($validate);
        return new BIRTemplatePeriodResource($period);
    }

    /**
     * Update BIR Period
     *
     * @param Request $request
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function updatePeriod(Request $request, $id): JsonResource
    {
        $validate = $this->periodValidate($request);
        $period = BIRTemplatePeriods::findORFail($id);
        $period->update($validate);
        return new BIRTemplatePeriodResource($period);
    }

    /**
     * Delete BIR Period
     *
     * @param [int] $id
     *
     * @return Response
     */
    public function deletePeriod($id): Response
    {
        $period = BIRTemplatePeriods::findORFail($id);
        $period->delete();
        return response(null, 204);
    }

    /**
     * BIR Period Validate
     *
     * @param Request $request
     *
     * @return array
     */
    public function periodValidate(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'period' => 'required|string'
            ]
        );
    }

    /**
     * Show BIR Category Deductions
     *
     * @return ResourceCollection
     */
    public function showCategoryDeductions(): ResourceCollection
    {
        $category_deductions = BIRTemplateCategoryDeductions::all();
        return BIRTemplateCategoryDeductionResource::collection($category_deductions);
    }

    /**
     * Show BIR Category Deduction
     *
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function showCategoryDeduction($id): JsonResource
    {
        $category_deduction = BIRTemplateCategoryDeductions::findORFail($id);
        return new BIRTemplateCategoryDeductionResource($category_deduction);
    }

    /**
     * Store BIR Category Deduction
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function storeCategoryDeduction(Request $request): JsonResource
    {
        $validate = $this->categoryDeductionValidate($request);
        $category_deduction = BIRTemplateCategoryDeductions::create($validate);
        return new BIRTemplateCategoryDeductionResource($category_deduction);
    }

    /**
     * Update BIR Category Deduction
     *
     * @param Request $request
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function updateCategoryDeduction(Request $request, $id): JsonResource
    {
        $validate = $this->categoryDeductionValidate($request);
        $category_deduction = BIRTemplateCategoryDeductions::findORFail($id);
        $category_deduction->update($validate);
        return new BIRTemplateCategoryDeductionResource($category_deduction);
    }

    /**
     * Delete BIR Category Deduction
     *
     * @param [type] $id
     *
     * @return Response
     */
    public function deleteCategoryDeduction($id): Response
    {
        $category_deduction = BIRTemplateCategoryDeductions::findORFail($id);
        $category_deduction->delete();
        return response(null, 204);
    }

    /**
     * Validate Category Deduction
     *
     * @param Request $request
     *
     * @return array
     */
    public function categoryDeductionValidate(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'category_id' => 'required|numeric',
                'period_id' => 'required|numeric',
                'deduction_value' => 'required|numeric',
                'deduction_percent' => 'required|numeric'
            ]
        );
    }

    /**
     * Index BIR Template Exemptions
     *
     * @return ResourceCollection
     */
    public function indexBirExemption(): ResourceCollection
    {
        $bir_exemption = BIRTemplateExemptions::all();
        return BIRTemplateExemptionResource::collection($bir_exemption);
    }

    /**
     * Show BIR Template Exemption
     *
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function showBirExemption($id): JsonResource
    {
        $bir_exemption = BIRTemplateExemptions::findORFail($id);
        return new BIRTemplateExemptionResource($bir_exemption);
    }

    /**
     * Store BIR Exemption
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function storeBIRExemption(Request $request): JsonResource
    {
        $validate = $this->birExemptionValidate($request);
        $bir_exemption = BIRTemplateExemptions::create($validate);
        return new BIRTemplateExemptionResource($bir_exemption);
    }

    /**
     * Update BIR Template Exemption
     *
     * @param Request $request
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function updateBIRExemption(Request $request, $id): JsonResource
    {
        $validate = $this->birExemptionValidate($request);
        $bir_exemption = BIRTemplateExemptions::findORFail($id);
        $bir_exemption->update($validate);
        return new BIRTemplateExemptionResource($bir_exemption);
    }

    /**
     * Delete BIR Template Exemption
     *
     * @param [int] $id
     *
     * @return Response
     */
    public function deleteBIRExemption($id): Response
    {
        $bir_exemption = BIRTemplateExemptions::findORFail($id);
        $bir_exemption->delete();
        return response(null, 204);
    }

    /**
     * BIR Template Exemption Validate
     *
     * @param Request $request
     *
     * @return array
     */
    public function birExemptionValidate(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'exemption_status' => 'required|numeric',
                'exemption_values' => 'required|numeric',
                'dependent' => 'required|numeric'
            ]
        );
    }

    /**
     * Show all BIR Template Salaries
     *
     * @return ResourceCollection
     */
    public function indexBIRSalary(): ResourceCollection
    {
        $bir_salary = BIRTemplateSalaries::all();
        return BIRTemplateSalaryResource::collection($bir_salary);
    }

    /**
     * Show BIR Template Salary
     *
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function showBIRSalary($id): JsonResource
    {
        $bir_salary = BIRTemplateSalaries::findORFail($id);
        return new BIRTemplateSalaryResource($bir_salary);
    }

    /**
     * Store BIR Template Salary
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function storeBIRSalary(Request $request): JsonResource
    {
        $validate = $this->birSalaryValidate($request);
        $bir_salary = BIRTemplateSalaries::create($validate);
        return new BIRTemplateSalaryResource($bir_salary);
    }

    /**
     * Update BIR Template Salary
     *
     * @param Request $request
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function updateBIRSalary(Request $request, $id): JsonResource
    {
        $validate = $this->birSalaryValidate($request);
        $bir_salary = BIRTemplateSalaries::findORFail($id);
        $bir_salary->update($validate);
        return new BIRTemplateSalaryResource($bir_salary);
    }

    /**
     * Delete BIR Template Salary
     *
     * @param [int] $id
     *
     * @return Response
     */
    public function deleteBIRSalary($id): Response
    {
        $bir_salary = BIRTemplateSalaries::findORFail($id);
        $bir_salary->delete();
        return response(null, 204);
    }

    /**
     * BIR Template Salary Validation
     *
     * @param Request $request
     *
     * @return array
     */
    public function birSalaryValidate(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'period_id' => 'required|numeric',
                'category_id' => 'required|numeric',
                'exemption_id' => 'required|numeric',
                'salary_value' => 'required|numeric'
            ]
        );
    }

    /**
     * Show All BIR Template Tax Categories
     *
     * @return ResourceCollection
     */
    public function indexTaxCategories(): ResourceCollection
    {
        $tax_category = BIRTemplateTaxCategories::all();
        return BIRTemplateTaxCategoryResource::collection($tax_category);
    }

    /**
     * Show BIR Template Tax Category
     *
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function showTaxCategories($id): JsonResource
    {
        $tax_category = BIRTemplateTaxCategories::findORFail($id);
        return new BIRTemplateTaxCategoryResource($tax_category);
    }

    /**
     * Store BIR Template Tax Category
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function storeTaxCategories(Request $request): JsonResource
    {
        $validate = $this->taxCategoriesValidate($request);
        $tax_category = BIRTemplateTaxCategories::create($validate);
        return new BIRTemplateTaxCategoryResource($tax_category);
    }

    /**
     * Update BIR Template Tax Categiry
     *
     * @param Request $request
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function updateTaxCategories(Request $request, $id): JsonResource
    {
        $validate = $this->taxCategoriesValidate($request);
        $tax_category = BIRTemplateTaxCategories::findORFail($id);
        $tax_category->update($validate);
        return new BIRTemplateTaxCategoryResource($tax_category);
    }

    /**
     * Delete BIR Template Tax Category
     *
     * @param [int] $id
     *
     * @return Response
     */
    public function deleteTaxCategories($id): Response
    {
        $tax_category = BIRTemplateTaxCategories::findORFail($id);
        $tax_category->delete();
        return response(null, 204);
    }

    /**
     * BIR Template Tax Category Validate
     *
     * @param Request $request
     *
     * @return array
     */
    public function taxCategoriesValidate(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'title' => 'required'
            ]
        );
    }

    /**
     * Show All BIR Template Tax Criterias
     *
     * @return ResourceCollection
     */
    public function indexTaxCriterias(): ResourceCollection
    {
        $tax_criterias = BIRTemplateTaxCriterias::all();
        return BIRTemplateTaxCriteriasResource::collection($tax_criterias);
    }

    /**
     * Show BIR Template Tax Criteria
     *
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function showTaxCriterias($id): JsonResource
    {
        $tax_criterias = BIRTemplateTaxCriterias::findORFail($id);
        return new BIRTemplateTaxCriteriasResource($tax_criterias);
    }

    /**
     * Store BIR Template Tax Criteria
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function storeTaxCriterias(Request $request): JsonResource
    {
        $validate = $this->taxCriteriasValidation($request);
        $tax_criterias = BIRTemplateTaxCriterias::create($validate);
        return new BIRTemplateTaxCriteriasResource($tax_criterias);
    }

    /**
     * Update BIR Template Tax Criteria
     *
     * @param Request $request
     * @param [int] $id
     *
     * @return JsonResource
     */
    public function updateTaxCriterias(Request $request, $id): JsonResource
    {
        $validate = $this->taxCriteriasValidation($request);
        $tax_criterias = BIRTemplateTaxCriterias::findORFail($id);
        $tax_criterias->update($validate);
        return new BIRTemplateTaxCriteriasResource($tax_criterias);
    }

    /**
     * Delete BIR Template Tax Criteria
     *
     * @param [int] $id
     *
     * @return Response
     */
    public function deleteTaxCriterias($id): Response
    {
        $tax_criterias = BIRTemplateTaxCriterias::findORFail($id);
        $tax_criterias->delete();
        return response(null, 204);
    }

    /**
     * BIR Template Tax Criteria Validation
     *
     * @param Request $request
     *
     * @return array
     */
    public function taxCriteriasValidation(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'tax_category_id' => 'required|numeric',
                'compensation_level' => 'required|numeric',
                'prescribed_tax' => 'required|numeric',
                'percent_over' => 'required|numeric'
            ]
        );
    }
}

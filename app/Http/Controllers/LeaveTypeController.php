<?php

namespace App\Http\Controllers;

use App\Department;
use App\Designation;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Resources\Designation as DesignationResource;
use App\Http\Resources\LeaveType as LeaveTypeResource;
use App\LeaveType;
use Illuminate\Http\Request;
use function App\Log\log_created;

class LeaveTypeController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leavetype = LeaveType::where('deleted_at',Null)->get();
        return LeaveTypeResource::collection($leavetype);
    }

    public function create(){
        $departments = Department::all();
        $designations = Designation::all();

        return response()->json([
            'departments' => DepartmentResource::collection($departments), 
            'designations' => DesignationResource::collection($designations)
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // try {
            $request->validate([
                'name' => 'unique:hr_leave_types'
            ]);

            $new_type = new LeaveType;
            $new_type->name = $request->input('name');
            $new_type->code = $request->input('code');
            $new_type->balance = $request->input('balance');
            $new_type->type = $request->input('type');
            $new_type->description = $request->input('description');
            $new_type->encashment_percentage = $request->input('encashment_percentage');
            $new_type->effective_value = $request->input('effective_value');
            $new_type->effective_type = $request->input('effective_type');
            $new_type->carry_forward_percentage = $request->input('carry_forward_percentage');
            $new_type->gender = $request->input('gender');
            $new_type->marital_status = $request->input('marital_status');
            $new_type->department = json_encode($request->input('departments'));
            $new_type->designation = json_encode($request->input('designations'));
            $new_type->office_type = $request->input('office_type');
            $new_type->save();
            log_created($new_type); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new LeaveTypeResource($new_type);


        // } catch (\Exception $exception) {
        //     return LeaveTypeResource::errorResponse($exception->getMessage());
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LeaveType  $leaveType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $leave_type = LeaveType::where('id', $id)->first();
            return new LeaveTypeResource($leave_type);
        } catch (\Exception $exception) {
            return LeaveTypeResource::errorResponse($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeaveType  $leaveType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveType $leaveType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeaveType  $leaveType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {       
        try {
            $LeaveTypeDelete = LeaveType::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return LeaveTypeResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return LeaveTypeResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\DailyTimeRecord;
use App\Employee;
use App\EmployeeLeave;
use App\EmployeeOvertime;
use App\Http\Resources\DailyTimeRecord as DailyTimeRecordResource;
use App\OB;
use App\ScheduleEmployeeItem;
use Carbon\CarbonPeriod;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class DailyTimeRecordController extends Controller
{
    private $verified_cutoff = 0;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * index
     *
     * @param Request $request
     *
     * @return ResourceCollection
     */
    public function index(Request $request): ResourceCollection
    {
        $payload = $this->filterPayload($request);

        $user = Auth::user();

        if ($user->id == 1) {
            $employee_schedule = ScheduleEmployeeItem::filter($payload)->get();
        } else {
            $employee_schedule = ScheduleEmployeeItem::filter($payload)
                ->where('employee_id', $user->getEmployee->id)
                ->get();
        }

        return DailyTimeRecordResource::collection($employee_schedule);
    }

    /**
     * show
     *
     * @param int $scheduleEmployeeItemId
     *
     * @return JsonResource
     */
    public function show(int $scheduleEmployeeItemId): JsonResource
    {
        $item = ScheduleEmployeeItem::findOrFail($scheduleEmployeeItemId);

        return new DailyTimeRecordResource($item);
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $scheduleEmployeeItemId
     *
     * @return void
     */
    public function update(Request $request, int $scheduleEmployeeItemId)
    {
        $scheduleEmployeeItem = ScheduleEmployeeItem::findOrFail($scheduleEmployeeItemId);

        $scheduleEmployeeItem->update([
            'verified_attendance' => 1
        ]);
        
        return new DailyTimeRecordResource($scheduleEmployeeItem);
    }

    /**
     * massverify
     *
     * @param Request $request
     * @param string $scheduleEmployeeItemId
     *
     * @return void
     */
    public function massVerify(Request $request, string $scheduleEmployeeItemId)
    {
        ScheduleEmployeeItem::whereIn('id', explode(',', $scheduleEmployeeItemId))
            ->update([
                'verified_attendance' => 1
            ]);
        
        return response()->json('Successfully Verified!');
    }

    /**
     * show
     *
     * @param $employee_id
     *
     * @return void
     */
    // public function show($employee_id)
    // {
    //     if (Auth::user()->id != 1) {
    //         $employee_id = Auth::user()->getEmployee->id;
    //     }
        
    //     $current_month = intval(date('m'));
    //     $year = intval(date('Y'));
    //     $month_arr = [];
    //     $year_arr = [$year];
    //     $cutoff_data = [];
    //     $count = 0;
    //     for ($i = $current_month; $i > 0; $i--) {
    //         array_push($month_arr, $i);
    //         for ($j = 2; $j >= 1; $j--) {
    //             $start_date = date("Y-m-d", mktime(0, 0, 0, $i, ($j == 1 ? '6' : '20' ), $year));
    //             $end_date = ($j == 1 ? date("Y-m-d", mktime(0, 0, 0, $i, 19, $year)) :
    //                         $this->addMonth(date("Y-m", strtotime($year . '-' . $i)) . '-5') );
    //             array_push($cutoff_data, [
    //                 'start_date' => date("F d, Y", strtotime($start_date)),
    //                 'end_date' => date("F d, Y", strtotime($end_date)),
    //                 'cutoff_data' => $this->getData($employee_id, $start_date, $end_date),
    //                 'verified' => $this->verified_cutoff
    //             ]);
    //             $this->verified_cutoff = 0;
    //         }

    //         $count++;

    //         if ($i == 1) {
    //             $i = 13;
    //             array_push($year_arr, $year - 1);
    //             $year--;
    //         }

    //         if ($count == 12) {
    //             break;
    //         }
    //     }
    //     return $cutoff_data;
    // }

    public function addMonth($date)
    {
        $d = new DateTime($date);
        $d->add(new DateInterval('P1M'));

        return $d->format('Y-m-d');
    }

    public function getData(
        $employee_id,
        $start_date,
        $end_date
    ): array {

        $period = CarbonPeriod::create($start_date, $end_date);
        $dates = [];
        foreach ($period as $date) {
            $dtr = DailyTimeRecord::where('date', $date)->get();
            $dtr = DailyTimeRecord::where('date', $date)->where('employee_id', $employee_id)->get();
            $verified = 0;
            if (!empty($dtr) && count($dtr) > 0) {
                $verified = 1;
                $this->verified_cutoff++;
            }
            $data = [
                'date' => $date->format("F d, Y l"),
                'type' => 'N/A',
                'time_in' => '-',
                'time_out' => '-',
                'overtime' => '-',
                'remarks' => '-',
                'verified' => $verified
            ];
            $leave = EmployeeLeave::where('employee_id', $employee_id)
                        ->where('start_date', '<=', $date)
                        ->where('end_date', '>=', $date)->first();
            $leave_status = model_approval_status(check_approver($leave));

            if (!empty($leave)) {
                $message = 'Filed for ' . $leave->getLeaveType->name . ', Approval Status: ';
                $data['type'] = 'leave';
                $data['remarks'] =  empty($leave_status) ? $message . 'Approved' : $message . $leave_status;
                $data['status'] = $leave_status;
                array_push($dates, $data);
            }

            $official_business = OB::where('employee_id', $employee_id)->where('date', $date)->first();
            if (!empty($official_business)) {
                $ob_status = model_approval_status(check_approver($official_business));
                $data['type'] = 'official business';
                $data['time_in'] = date('h:i A', strtotime($official_business->time_in));
                $data['time_out'] = date('h:i A', strtotime($official_business->time_out));
                $data['overtime'] = $this->getOvertime($employee_id, $date);
                $data['remarks'] .=  ' Entry retrieved from Official Business, Approval Status: ' . $ob_status;
                $data['status'] = $ob_status;
                array_push($dates, $data);
                continue;
            }

            $attendance = Attendance::where('employee_id', $employee_id)->whereDate('start_time', $date)->first();
            // if($date == "2021-02-21 00:00:00.0 Asia/Manila (+08:00)"){
            // }
            if (!empty($attendance)) {
                $data['type'] = 'attendance';
                $data['time_in'] = date('h:i A', strtotime($attendance->start_time));
                $data['time_out'] = date('h:i A', strtotime($attendance->end_time));
                $data['overtime'] = $this->getOvertime($employee_id, $date);
                $data['remarks'] .= ' Entry retrieved from Attendance';
                array_push($dates, $data);
                continue;
            }

            if ($data['type'] == 'N/A') {
                array_push($dates, $data);
            }
        }

        return $dates;
    }

    public function getOvertime(
        $employee_id,
        $date
    ) {
        $overtime = EmployeeOvertime::where('employee_id', $employee_id)->where('overtime_date', $date)->first();
        if (empty($overtime) && model_approval_status(check_approver($overtime)) != 'Approved') {
            return 'N/A';
        }
        return  date('h:i A', strtotime($overtime->start_time)) . ' to ' . date('h:i A', strtotime($overtime->end_time));
    }

    public function verifyDate(Request $request) {
        if ($request->has('status') && $request->status != 'Approved') {
            return response()->json([
                'title' => 'Verification Error',
                'message' => 'Cannot verifiy this date due to no data/status is pending',
                'variant' => 'danger'
            ]);
        }
        $time = strtotime($request->date);
        $newformat = date('Y-m-d', $time);
        $daily_time_record = new DailyTimeRecord();
        $daily_time_record->date = $newformat;
        $daily_time_record->employee_id = $request->employee_id;
        $daily_time_record->verified_attendance = $request->verified_attendance;
        $daily_time_record->verified_leave = $request->verified_leave;
        $daily_time_record->verified_overtime = $request->verified_leave;
        $daily_time_record->save();

        return response()->json([
            'title' => 'Verification Successful',
            'message' => 'Time record succesfully verified',
            'variant' => 'success'
        ]);

        return $daily_time_record;
    }

    /**
     * Validation Rules for Filter
     *
     * @param Request $request
     *
     * @return array
     */
    private function filterPayload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'verify' => 'nullable',
                'date_from' => 'nullable',
                'date_to' => 'nullable',
                'search' => 'nullable'
            ]
        );
    }
}

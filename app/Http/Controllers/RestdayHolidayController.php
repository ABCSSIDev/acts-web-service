<?php

namespace App\Http\Controllers;

use App\Http\Resources\RestdayHoliday as RestdayHolidayResource;
use App\RestdayHoliday;
use Illuminate\Http\Request;

class RestdayHolidayController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $restdayHoliday = RestdayHoliday::all();

        return RestdayHolidayResource::collection($restdayHoliday);
    }

    /**
     * store
     *
     * @param Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $restdayHoliday = RestdayHoliday::create($this->payload($request));

        return new RestdayHolidayResource($restdayHoliday);
    }

    /**
     * show
     *
     * @param int $restdayId
     *
     * @return void
     */
    public function show(int $restdayId)
    {
        $restdayHoliday = RestdayHoliday::findOrFail($restdayId);

        return new RestdayHolidayResource($restdayHoliday);
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $restdayId
     *
     * @return void
     */
    public function update(Request $request, int $restdayId)
    {
        $restdayHoliday = RestdayHoliday::findOrFail($restdayId);

        $restdayHoliday->update($this->payload($request, $restdayId));

        return new RestdayHolidayResource($restdayHoliday);
    }

    /**
     * destroy
     *
     * @param int $restdayId
     *
     * @return void
     */
    public function destroy(int $restdayId)
    {
        $restdayHoliday = RestdayHoliday::findOrFail($restdayId);

        $restdayHoliday->delete();

        return response(null, 204);
    }

    /**
     * update store payload
     *
     * @param Request $request
     *
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'employee_id' => ['required'],
                'ot_id' => ['nullable'],
                'attendance_id' => ['nullable'],
                'holidays' => ['nullable'],
                'rate' => ['nullable'],
                'restday_holiday_date' => ['required'],
                'file_date' => ['required'],
                'start_time' => ['required'],
                'rendered_hours' => ['nullable'],
                'purpose' => ['nullable']
            ]
        );
    }
}

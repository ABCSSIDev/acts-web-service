<?php

namespace App\Http\Controllers;

use App\Http\Resources\SSSTemplateContributionResource;
use App\Http\Resources\SSSTemplateResource;
use App\SSSTemplateContributions;
use App\SSSTemplates;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class SSSController extends Controller
{
    //

    /**
     * Shows SSS Contribution Collection
     *
     * @return ResourceCollection
     */
    public function index(): ResourceCollection
    {
        $templates = SSSTemplateContributions::all();
        return SSSTemplateContributionResource::collection($templates);
    }

    /**
     * Add SSS Contribution Details
     *
     * @return JsonResource
     * @param Request $request
     */
    public function store(Request $request): JsonResource
    {
        $validate = $this->contributionValidation($request);
        $sss_template = SSSTemplateContributions::create($validate);
        return new SSSTemplateContributionResource($sss_template);
    }

    /**
     * Show SSS Template Contribution Details
     *
     * @param int $id
     *
     * @return JsonResource
     */
    public function show($id): JsonResource
    {
        $sss_template = SSSTemplateContributions::findORFail($id);
        return new SSSTemplateContributionResource($sss_template);
    }

    /**
     * Update SSS Template Contribution Details
     *
     * @param Request $request
     * @param int $id
     *
     * @return JsonResource
     */
    public function update(Request $request, int $id): JsonResource
    {
        $validate = $this->contributionValidation($request);
        $sss_contribution = SSSTemplateContributions::findORFail($id);
        $sss_contribution->update($validate);
        return new SSSTemplateContributionResource($sss_contribution);
    }

    /**
     * Delete SSS Template Details
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(int $id): Response
    {
        $sss_contribution = SSSTemplateContributions::findORFail($id);
        $sss_contribution->delete();
        return response(null, 204);
    }

    /**
     * Mass delete SSS Template
     *
     * @param string $id
     *
     * @return response
     */
    public function massdelete(string $id): Response
    {
        $templateDelete = SSSTemplates::whereIn('id', explode(',', $id))->update([
            'deleted_at' => date('Y/m/d H:i:s')
        ]);
        return response(null, 204);
    }

    /**
     * Undocumented function
     *
     * @param string $id
     *
     * @return Response
     */
    public function massdeleteContribution(string $id): Response
    {
        $templateDelete = SSSTemplateContributions::whereIn('id', explode(',', $id))->update([
            'deleted_at' => date('Y/m/d H:i:s')
        ]);
        return response(null, 204);
    }

    /**
     * SSS Template Validation
     *
     * @param Request $request
     *
     * @return array
     */
    public function validation(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'date_effective_from' => 'required|date',
                'date_effective_to' => 'required|date',
                'active' => 'in:0,1'
            ]
        );
    }

    /**
     * SSS Template Contribution Validation
     *
     * @param Request $request
     *
     * @return array
     */
    public function contributionValidation(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'salary_from' => 'required|numeric',
                'salary_to' => 'required|numeric',
                'monthly_salary_credit' => 'required|numeric',
                'employer_contribution' => 'required|numeric',
                'employee_contribution' => 'required|numeric',
                'employee_compensation' => 'required|numeric',
            ]
        );
    }
}

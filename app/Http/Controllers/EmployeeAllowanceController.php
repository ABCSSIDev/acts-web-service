<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeSalary;
use App\EmployeeSalaryAllowance;
use App\Http\Resources\EmployeeSalary as EmployeeSalaryResource;
use App\Http\Resources\EmployeeSalaryAllowance as EmployeeSalaryAllowanceResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EmployeeAllowanceController extends Controller
{

    /**
     * _construct
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * index
     *
     * @param string $salaryId
     *
     * @return ResourceCollection
     */
    public function index(string $salaryId): ResourceCollection
    {
        $allowance = EmployeeSalary::findOrFail($salaryId)
            ->allowances()
            ->get();
            
        return EmployeeSalaryAllowanceResource::collection($allowance);
    }

    /**
     * store Allowance
     *
     * @param Request $request
     * @param string $salaryId
     *
     * @return JsonResource
     */
    public function store(
        Request $request,
        string $salaryId
    ): JsonResource {
        $request->merge(['salary_id' => $salaryId]);

        $job = EmployeeSalaryAllowance::create($this->payload($request));

        return new EmployeeSalaryAllowanceResource($job);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(
        string $salaryId,
        string $allowanceId
    ): JsonResource {
        $allowance = EmployeeSalary::findOrFail($salaryId)
            ->allowances()
            ->findOrFail($allowanceId);

        return new EmployeeSalaryAllowanceResource($allowance);
    }

    /**
     * update
     *
     * @param Request $request
     * @param string $salaryId
     * @param string $allowanceId
     *
     * @return JsonResource
     */
    public function update(
        Request $request,
        string $salaryId,
        string $allowanceId
    ): JsonResource {
        $allowance = EmployeeSalary::findOrFail($salaryId)
            ->allowances()
            ->findOrFail($allowanceId);

        $request->merge(['salary_id' => $salaryId]);

        $allowance->update($this->payload($request));
        
        return new EmployeeSalaryAllowanceResource($allowance);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Validation Rules for Store and Update Allowances
     *
     * @param Request $request
     *
     * @return array
     */
    private function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'salary_id' => 'required',
                'allowance_name' => 'required',
                'amount' => 'nullable',
            ]
        );
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Resources\ScheduleEmployeeItem as ScheduleEmployeeItemResource;
use App\ScheduleEmployeeItem;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ScheduleEmployeeItemController extends Controller
{
    /**
     * list
     *
     * @return ResourceCollection
     */
    public function index(): ResourceCollection
    {
        $scheduleEmployeeItems = ScheduleEmployee::scheduleEmployeeItems()->get();

        return ScheduleEmployeeItem::collection($scheduleEmployeeItems);
    }

    /**
     * store data
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function store(Request $request): JsonResource
    {
        $scheduleEmployeeItem = ScheduleEmployeeItem::create($this->payload($request));

        return new ScheduleEmployeeItemResource($scheduleEmployeeItem);
    }

    /**
     * payload for store and update
     *
     * @param Request $request
     *
     * @return array
     */
    private function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'schedule_employee_id' => 'required',
                'employee_id' => 'required',
                'resched_employee_id' => 'nullable'
            ]
        );
    }
}

<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Designation;
use App\JobOpening;
use Illuminate\Http\Request;
use App\Http\Resources\JobOpening as JobOpeningResource;
use App\Http\Resources\Log as LogResource;
use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\Designation as DesignationResource;
use function App\Log\log_created;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class JobOpeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $JobOpening = JobOpening::all();
        return JobOpeningResource::collection($JobOpening);
    }

    public function create(){
        return EmployeeResource::collection(Employee::all())->additional([
            'designation' => DesignationResource::collection(Designation::all())
        ]);
    }

    public function store()
    {
        try{
           $job_opening = JobOpening::create(\request()->all());
           log_created($job_opening); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
           return new JobOpeningResource($job_opening);

        }catch (\Exception $exception){
            dd($exception->getMessage());
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobOpening  $jobOpening
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobOpening = JobOpening::findOrFail($id);
        return (new JobOpeningResource($jobOpening))->additional([
            'approver' => check_approver($jobOpening), //option if user is approver and already approved
            'approved_status' =>  model_approval_status(check_approver($jobOpening)),   //pending, waiting order approver,approved
            'history_logs' => LogResource::collection(JobOpening::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
        ]);
    }

    public function edit($id)
    {
        $designation = Designation::all();
        $employee = Employee::all();
        $job_opening = JobOpening::findOrFail($id);    
        $job_openings = new JobOpeningResource($job_opening);
        $collections = [
            'employees' => $employee,
            'designation' => $designation,
            'job_openings' => $job_openings,
        ];
        return $collections;       
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobOpening  $jobOpening
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $job_opening = JobOpening::where('id', $id)->update([
                'name' => $request->input('name'),
                'manpower_required' => $request->input('manpower_required'),
                'designation_id' => $request->input('designation_id'),
                'assigned_recruiter' => $request->input('assigned_recruiter'),
                'target_date' => $request->input('target_date'),
                'job_opening_status' => $request->input('job_opening_status'),
                'expected_salary' => $request->input('expected_salary'),
                'hiring_manager_id' => $request->input('hiring_manager_id'),
                'opened_date' => $request->input('opened_date'),
                'job_type' => $request->input('job_type'),
                'work_experience' => $request->input('work_experience'),
                'job_requirements' => $request->input('job_requirements')
            ]);
            $query = JobOpening::where('id', $id)->first();
            return new JobOpeningResource($query);
        }
        catch(\Exception $exception){
            return JobOpeningResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobOpening  $jobOpening
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $JobOpeningDelete = JobOpening::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return JobOpeningResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return JobOpeningResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

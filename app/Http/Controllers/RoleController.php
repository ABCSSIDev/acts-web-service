<?php

namespace App\Http\Controllers;

use App\Http\Resources\Role as RoleResource;
use App\Role;
use Illuminate\Http\Request;
use Auth;
use function App\Log\log_created;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RoleResource::collection(Role::all());
    }

    public function create(){
        try{
            $roles = Role::all();
            return RoleResource::collection($roles);
        }catch(\Exception $exception){
            return RoleResource::errorResponse($exception->getMessage());
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [ 'role_name' => 'unique:roles' ],
            ['role_name.unique' => 'Role name already taken']
        );
        
        try{
          

            $role = new Role();
            $role->role_id = $request->input('reports_to');
            $role->role_name = $request->input('role_name');
            $role->role_description = $request->input('role_description');
            $role->is_shared = (!empty($request->input('is_shared')) ? $request->input('is_shared') : '0');
            $role->save();
            log_created($role); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new RoleResource($role);

        }catch(\Exception $exception){
            return RoleResource::errorResponse($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::where('id', $id)->first();
        return new RoleResource($role);
    }

    public function edit($id)
    {
        $role_reports = Role::all(); 
        $role = Role::findOrFail($id);   
        $roles = new RoleResource($role);
        $collections = [
            'roles' => $roles,
            'role_reports' => $role_reports
        ];
        return $collections;       
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        try{
            $roles = Role::where('id', $id)->update([
                'role_id' => $request->input('role_id'),
                'role_name' => $request->input('role_name'),
                'role_description' => $request->input('role_description'),
                'is_shared' => $request->input('is_shared')
            ]);
            $query = Role::where('id', $id)->first();
            return new RoleResource($query);
        }
        catch(\Exception $exception){
            return RoleResource::errorResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }

    public function uploadFile(Request $request, $id){
        
    }

}

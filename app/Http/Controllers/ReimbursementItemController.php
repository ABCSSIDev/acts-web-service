<?php

namespace App\Http\Controllers;

use App\ReimbursementItem;
use Illuminate\Http\Request;

class ReimbursementItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReimbursementItem  $reimbursementItem
     * @return \Illuminate\Http\Response
     */
    public function show(ReimbursementItem $reimbursementItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReimbursementItem  $reimbursementItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReimbursementItem $reimbursementItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReimbursementItem  $reimbursementItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReimbursementItem $reimbursementItem)
    {
        //
    }
}

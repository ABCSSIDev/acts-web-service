<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeSalary;
use App\EmployeeSalaryAllowance;
use App\Http\Resources\EmployeeSalary as EmployeeSalaryResource;
use App\Http\Resources\EmployeeSalaryAllowance as EmployeeSalaryAllowanceResource;
use App\Http\Resources\Log as LogResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use function App\Log\log_created;

class EmployeeSalaryController extends Controller
{

    /**
     * _construct
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * index
     *
     * @param Request $request
     *
     * @return ResourceCollection
     */
    public function index(Request $request): ResourceCollection
    {
        $payload = $this->filterPayload($request);

        $employeeSalary = EmployeeSalary::filter($payload)->get();

        return EmployeeSalaryResource::collection($employeeSalary);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json(['employee_name' => Employee::all()]);
    }
    
    /**
     * store Salary
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function store(Request $request): JsonResource
    {
        $job = EmployeeSalary::create($this->payload($request));
        log_created($job);
        return new EmployeeSalaryResource($job);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($salaryId)
    {
        $salary = EmployeeSalary::findOrFail($salaryId);

        return (new EmployeeSalaryResource($salary))->additional([
            'approver' => check_approver($salary),
            'approved_status' =>  model_approval_status(check_approver($salary))
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($salaryId)
    {
        $salary = EmployeeSalary::findOrFail($salaryId);

        return new EmployeeSalaryResource($salary);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $salaryId)
    {
        $salary = EmployeeSalary::findOrFail($salaryId);

        $salary->update($this->payload($request));

        return new EmployeeSalaryResource($salary);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $salaryId)
    {
        $biller = EmployeeSalary::findOrFail($salaryId);

        $biller->delete();

        return response('', 204);
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    private function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'employee_id' => 'required',
                'effective_date' => 'required|date',
                'type' => 'required',
                'monthly_salary' => 'nullable',
                'rate_per_day' => 'nullable',
            ]
        );
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    private function filterPayload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'search' => 'nullable',
                'rate_type' => 'nullable'
            ]
        );
    }
}

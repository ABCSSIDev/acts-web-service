<?php

namespace App\Http\Controllers;

use App\WorkDetails;
use Illuminate\Http\Request;

class WorkDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkDetails  $workDetails
     * @return \Illuminate\Http\Response
     */
    public function show(WorkDetails $workDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkDetails  $workDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkDetails $workDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkDetails  $workDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkDetails $workDetails)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Adjustment;
use Illuminate\Http\Request;
use App\Http\Resources\Adjustment as AdjustmentResource;
use App\Http\Resources\Log as LogResource;
use function App\Log\log_created;
use App\Employee;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class AdjustmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $adjustment = Adjustment::all();
        return AdjustmentResource::collection($adjustment);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function create(){
        $response = array();
        $employee = Employee::role(Adjustment::class)->get();
        $response['employee'] = $employee;

        return response()->json($response);
    }
    public function store(Request $request)
    {
        try{
            $Adjustment = New Adjustment;
            $Adjustment->employee_id = $request->input('employee_id');
            $Adjustment->date = $request->input('date');
            $Adjustment->applicable_date = $request->input('applicable_date');
            $Adjustment->status = 0;
            $Adjustment->amount = $request->input('amount');
            $Adjustment->reason = $request->input('reason');
            $Adjustment->save();
            $type = false;
            $msg = 'Successfully New Adjustment Created!';
            log_created($Adjustment); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new AdjustmentResource($Adjustment);

        }catch(\Exception $exception){
            return AdjustmentResource::MsgResponse(true,$exception->getMessage());
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Adjustment  $adjustment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $Adjustment = Adjustment::findOrFail($id);
            return (new AdjustmentResource($Adjustment))->additional([
                'approver' => check_approver($Adjustment), //option if user is approver and already approved
                'approved_status' =>  model_approval_status(check_approver($Adjustment)),   //pending, waiting order approver,approved
                'history_logs' => LogResource::collection(Adjustment::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
            ]);
        }catch(\Exception $exception){
            return AdjustmentResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Adjustment  $adjustment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adjustment = Adjustment::findOrFail($id);     
        $employee_adjustment = new AdjustmentResource($adjustment);
        $collections = [
            'adjustment' => $employee_adjustment,
        ];
        return $collections;
    }
    public function update(Request $request, $id)
    {
        try{
            $adjustment = Adjustment::where('id', $id)->update([
                'date' => date('Y-m-d', strtotime($request->input('date'))),
                'applicable_date' => $request->input('applicable_date'),
                'amount' => $request->input('amount'),
                'reason' => $request->input('reason'),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            $query = Adjustment::where('id', $id)->first();
            log_created($query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new AdjustmentResource($query);
            
        }catch(\Exception $exception){
            return AdjustmentResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Adjustment  $adjustment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $AdjustmentDelete = Adjustment::whereIn('id', explode(',', $id))->update([
            'deleted_at' => date('Y/m/d H:i:s')
        ]);
        $type = false;
        $msg = 'Successfully Deleted!';
        return AdjustmentResource::MsgResponse($type,$msg);
    }
    public function massdelete(Request $request, $id)
    {
        try {
            $AdjustmentDelete = Adjustment::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return AdjustmentResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return AdjustmentResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

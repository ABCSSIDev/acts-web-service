<?php

namespace App\Http\Controllers;

use App\BIRDetails;
use App\Department;
use App\Designation;
use App\EducationalDetails;
use App\Employee;
use App\EmployeeContact;
use App\EmployeeType;
use App\HDMFDetails;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\EmployeeType as ResourcesEmployeeType;
use App\Http\Resources\JobDetail as ResourcesJobDetail;
use App\Http\Resources\Log as LogResource;
use App\JobDetail;
use App\Permission;
use App\PHICDetails;
use App\Role;
use App\SSSDetails;
use App\User;
use App\WorkDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Support\Facades\DB;
use ZipArchive;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use function App\Helpers\change_date_format;
use function App\Helpers\edit_educational_background;
use function App\Helpers\edit_employee;
use function App\Helpers\edit_employee_bir;
use function App\Helpers\edit_employee_contacts;
use function App\Helpers\edit_employee_hdmf;
use function App\Helpers\edit_employee_phic;
use function App\Helpers\edit_employee_sss;
use function App\Helpers\edit_employment_background;
use function App\Helpers\edit_job_details;
use function App\Helpers\edit_user_information;
use function App\Helpers\save_educational_background;
use function App\Helpers\save_employee;
use function App\Helpers\save_employee_bir;
use function App\Helpers\save_employee_contacts;
use function App\Helpers\save_employee_hdmf;
use function App\Helpers\save_employee_phic;
use function App\Helpers\save_employee_sss;
use function App\Helpers\save_employment_background;
use function App\Helpers\save_image;
use function App\Helpers\save_job_details;
use function App\Helpers\save_user_information;
use function App\Helpers\send_email;
use function App\Log\log_created;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index()
    {
        $employees = Employee::role(Employee::class)->get();
        return EmployeeResource::collection($employees)->additional([
            'departments' => DepartmentResource::collection(Department::all()),
            'employee_types' => EmployeeType::all(),
            'date_year' => range(1970, Carbon::now()->format("Y"))
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $response = array();
        $departments = DepartmentResource::collection(Department::all());
        $employee_types = EmployeeType::all();
        $designations = Designation::all();
        $roles = Role::all();
        $permissions = Permission::all();
        $response['departments'] = $departments;
        $response['employee_types'] = $employee_types;
        $response['designations'] = $designations;
        $response['roles'] = $roles;
        $response['permissions'] = $permissions;
        $response['count'] = count(Employee::all());
        return response()->json($response);
    }

    public function store()
    {
        try {
            $employee = array();
            $employee['employee_no'] = \request('employee_no');
            $employee['first_name'] = \request('first_name');
            $employee['middle_name'] = \request('middle_name');
            $employee['last_name'] = \request('last_name');
            $employee['gender'] = (\request('gender') == '0' ? 'female' : 'male');
            $employee['birth_date'] = \request('birth_date');
            $employee['current_address'] = \request('current_address');
            $employee['permanent_address'] = \request('permanent_address');
            $employee['marital_status'] = \request('marital_status');
            $employee_info = save_employee($employee);

            $user = array();
            $user['username'] = \request('acct_email');
            $user['name'] = \request('username');
            $user['password'] = Hash::make(\request('password'));
            $user['role'] = \request('role');
            $user['permission'] = \request('permission');
            $user['employee_id'] = $employee_info->id;
            $user_info = save_user_information($user);

            if (\request('send_notification') == '1') {
                send_email($user_info);
            }

            if (request()->hasFile('image')) {//save image
                $file_name = 'Employees/employee_number_' . $employee_info->id . '.jpg';
                $file = \request()->file('image');
                if (save_image($file, $file_name)) {
                    $employee_info->update([
                        'image' => $file_name
                    ]);
                }
            }
            //employee contact details
            $employee_contacts = array();
            $employee_contacts['employee_id'] = $employee_info->id;
            $employee_contacts['email'] = \request('email');
            $employee_contacts['phone_number'] = \request('phone_number');
            $employee_contacts['telephone_number'] = \request('telephone_number');
            $employee_contacts['work_phone'] = \request('work_phone');
            $employee_contacts['emergency_contact'] = \request('emergency_contact');
            $employee_contacts['emergency_contact_number'] = \request('emergency_contact_number');
            save_employee_contacts($employee_contacts);

            //employee job details
            $job_detail = array();
            $job_detail['employee_id'] = $employee_info->id;
            $job_detail['designation_id'] = \request('designation_id');
            $job_detail['employee_type_id'] = \request('employee_type_id');
            $job_detail['employee_status'] = \request('employee_status');
            $job_detail['source_of_hire'] = \request('source_of_hire');
            $job_detail['joined_date'] = \request('joined_date');
            save_job_details($job_detail);

//            employee educational background
            if (\request()->has('educational')) {
                $educational_arr = json_decode(request('educational'));
                foreach ($educational_arr as $key => $value) {
                    $educational_details = array();
                    $educational_details['employee_id'] = $employee_info->id;
                    $educational_details['level'] = \request('level_' . $value);
                    $educational_details['start_date'] = change_date_format('Y-m-d', request('start_date_' . $value));
                    $educational_details['end_date'] = change_date_format('Y-m-d', request('end_date_' . $value));
                    $educational_details['school'] = \request('school_name_' . $value);
//                    $educational_details['awards'] = \request('awards'.$value);
                    save_educational_background($educational_details);
                }
            }

//            employee employment background
            if (\request()->has('work_details')) {
                $arr = json_decode(request('work_details'));
                foreach ($arr as $key => $value) {
                    $work_details = array();
                    $work_details['employee_id'] = $employee_info->id;
                    $work_details['company'] = \request('company_' . $value);
                    $work_details['start_date'] = change_date_format('Y-m-d', request('start_date_' . $value));
                    $work_details['end_date'] = change_date_format('Y-m-d', request('end_date_' . $value));
                    $work_details['description'] = \request('description_' . $value);
                    save_employment_background($work_details);
                }
            }

            if (\request()->has('sss_details')) {
                $sss = array();
                $sss['employee_id'] = $employee_info->id;
                $sss['membership_code'] = \request('sss_membership_code');
                $sss['joined_date'] = \request('sss_joined_date');
                $sss['separation_date'] = \request('sss_separation_date');
//                $sss['status'] = \request('sss_status');
                save_employee_sss($sss);
            }
            if (\request()->has('bir_details')) {
                $bir = array();
                $bir['employee_id'] = $employee_info->id;
                $bir['membership_code'] = \request('bir_membership_code');
                $bir['account_number'] = \request('bir_account_number');
                $bir['joined_date'] = \request('bir_joined_date');
                $bir['separation_date'] = \request('bir_separation_date');
//                $bir['status'] = \request('bir_status');
                save_employee_bir($bir);
            }
            if (\request()->has('phic_details')) {
                $phic = array();
                $phic['employee_id'] = $employee_info->id;
                $phic['membership_code'] = \request('phic_membership_code');
                $phic['joined_date'] = \request('phic_joined_date');
                $phic['separation_date'] = \request('phic_separation_date');
//                $phic['status'] = \request('phic_status');
                save_employee_phic($phic);
            }
            if (\request()->has('hdmf_details')) {
                $hdmf = array();
                $hdmf['employee_id'] = $employee_info->id;
                $hdmf['membership_code'] = \request('hdmf_membership_code');
                $hdmf['account_number'] = \request('hdmf_account_number');
                $hdmf['joined_date'] = \request('hdmf_joined_date');
                $hdmf['separation_date'] = \request('hdmf_separation_date');
//                $hdmf['status'] = \request('hdmf_status');
                save_employee_hdmf($hdmf);
            }
            log_created($employee_info); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new EmployeeResource($employee_info);
        } catch (\Exception $exception) {
            return EmployeeResource::msgResponse(true, $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::findOrFail($id);

        return (new EmployeeResource($employee))->additional([
            'approver' => check_approver($employee), //option if user is approver and already approved
            'approved_status' =>  model_approval_status(check_approver($employee)),
            'history_logs' => LogResource::collection(Employee::findOrFail($id)->getLogs()
                                ->orderBy('created_at', 'DESC')->get())
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function validationemail($data)
    {
        $employee = User::where('email', $data)->first();
        if (!$employee) {
            return 'no-error';
        } else {
            return 'error';
        }
    }

    public function validationemployeeno($data, $email)
    {
        $employeeemail = User::where('email', $email)->first();
        $employee = Employee::where('employee_no', $data)->first();
        if (!$employee && !$employeeemail) {
            return 'no-error';
        } else {
            if ($employee) {
                return 'no-error-employeno';
            } else {
                return 'no-error-employenoemail';
            }
        }
    }
    public function update(Request $request, $id)
    {

        try {
            $employee = array();
            $employee['id'] = $id;
            $employee['employee_no'] = $request->input('employee_no');
            $employee['first_name'] = $request->input('first_name');
            $employee['middle_name'] = $request->input('middle_name');
            $employee['last_name'] = $request->input('last_name');
            $employee['gender'] = $request->input('gender');
            $employee['birth_date'] = $request->input('birth_date');
            $employee['current_address'] = $request->input('current_address');
            $employee['permanent_address'] = $request->input('permanent_address');
            $employee['marital_status'] = $request->input('marital_status');
            $employee_info = edit_employee($employee);

            //save user credentials
            // $user = array();
            // $user['email'] = $request->input('username');
            // $user['name'] = $request->input('first_name') . ' ' . $request->input('last_name');
            // $user['password'] = bcrypt($request->input('password'));
            // $user['role_id'] = $request->input('role');
            // $user['permission'] = $request->input('permission');
            // $user['employee_id'] = $id;
            // $user_info = edit_user_information($user);

            // if (\request()->has('send_notification')){
            //     send_email($user_info);
            // }

            $employee_contacts = array();
            $employee_contacts['employee_id'] = $id;
            $employee_contacts['email'] = $request->input('email');
            $employee_contacts['phone_number'] = $request->input('phone_number');
            $employee_contacts['telephone_number'] = $request->input('telephone_number');
            $employee_contacts['work_phone'] = $request->input('work_phone');
            $employee_contacts['emergency_contact'] = $request->input('emergency_contact');
            $employee_contacts['emergency_contact_number'] = $request->input('emergency_contact_number');
            $validation = EmployeeContact::where('employee_id', $id)->first();
            if ($validation) {
                edit_employee_contacts($employee_contacts);
            } else {
                save_employee_contacts($employee_contacts);
            }

             //employee job details
             $job_detail = array();
             $job_detail['employee_id'] = $id;
             $job_detail['designation_id'] = $request->input('designation_id');
             $job_detail['employee_type_id'] = $request->input('employee_type_id');
             $job_detail['employee_status'] = $request->input('employee_status');
             $job_detail['source_of_hire'] = $request->input('source_of_hire');
             $job_detail['joined_date'] = $request->input('joined_date');
             $job_detail['reporting_to'] = $request->input('reporting_to');
             $validation = JobDetail::where('employee_id', $id)->first();
            if ($validation) {
                edit_job_details($job_detail);
            } else {
                save_job_details($job_detail);
            }



             //employee educational
            $educational_arr = json_decode(\request('educational'), true);
            if (!empty($educational_arr)) {
                foreach ($educational_arr as $key => $value) {
                    $educational_details = array();
                    $educational_details['employee_id'] = $id;
                    $educational_details['id'] = $value['id'];
                    $educational_details['level'] = $value['level'];
                    $educational_details['start_date'] = change_date_format('Y-m-d', $value['start_date']);
                    $educational_details['end_date'] = change_date_format('Y-m-d', $value['end_date']);
                    $educational_details['school'] = $value['school'];
                    if (!empty($educational_details['id'])) {
                        edit_educational_background($educational_details);
                    } else {
                        save_educational_background($educational_details);
                    }
                }
            }

            //employee work_details
            $work_details_arr = json_decode(\request('work_details'), true);
            if (!empty($work_details_arr)) {
                foreach ($work_details_arr as $key => $value) {
                    $work_details = array();
                    $work_details['employee_id'] = $id;
                    $work_details['id'] = $value['id'];
                    $work_details['company'] = $value['company'];
                    $work_details['start_date'] = change_date_format('Y-m-d', $value['start_date']);
                    $work_details['end_date'] = change_date_format('Y-m-d', $value['end_date']);
                    $work_details['description'] = $value['description'];
                    $validation = WorkDetails::where('id', $value['id'])->first();
                    if (!empty($work_details['id'])) {
                        edit_employment_background($work_details);
                    } else {
                        save_employment_background($work_details);
                    }
                }
            }


            if ($request->input('sss_details')) {
                $sss = array();
                $sss['employee_id'] = $id;
                $sss['membership_code'] =  $request->input('membership_code');
                $sss['joined_date'] = $request->input('joined_date');
                $sss['separation_date'] = $request->input('separation_date');
                $sss['status'] = $request->input('status');
                $validation = SSSDetails::where('employee_id', $id)->first();
                if ($validation) {
                    edit_employee_sss($sss);
                } else {
                    save_employee_sss($sss);
                }
            }
            if ($request->input('bir_details')) {
                $bir = array();
                $bir['employee_id'] = $id;
                $bir['membership_code'] =  $request->input('bir_membership_code');
                $bir['account_number'] = $request->input('bir_account_number');
                $bir['separation_date'] = $request->input('bir_separation_date');
                $bir['joined_date'] = $request->input('bir_joined_date');
                $bir['status'] = $request->input('status');
                $validation = BIRDetails::where('employee_id', $id)->first();
                if ($validation) {
                    edit_employee_bir($bir);
                } else {
                    save_employee_bir($bir);
                }
            }
            if ($request->input('phic_details')) {
                $phic = array();
                $phic['employee_id'] = $id;
                $phic['membership_code'] =  $request->input('phic_membership_code');
                $phic['separation_date'] = $request->input('phic_separation_date');
                $phic['joined_date'] = $request->input('phic_joined_date');
                $phic['status'] = $request->input('phic_status');
                $validation = PHICDetails::where('employee_id', $id)->first();
                if ($validation) {
                    edit_employee_phic($phic);
                } else {
                    save_employee_phic($phic);
                }
            }
            if ($request->input('hdmf_details')) {
                $hdmf = array();
                $hdmf['employee_id'] = $id;
                $hdmf['membership_code'] =  $request->input('hdmf_membership_code');
                $hdmf['account_number'] = $request->input('hdmf_account_number');
                $hdmf['separation_date'] = $request->input('hdmf_separation_date');
                $hdmf['joined_date'] = $request->input('hdmf_joined_date');
                $hdmf['status'] = $request->input('hdmf_status');
                $validation = HDMFDetails::where('employee_id', $id)->first();
                if ($validation) {
                    edit_employee_hdmf($hdmf);
                } else {
                    save_employee_hdmf($hdmf);
                }
            }
            return EmployeeResource::msgResponse(false, 'Employee Successfully Edited!');
        } catch (\Exception $exception) {
            return EmployeeResource::msgResponse(true, $exception->getMessage());
        }
    }
    public function edit($id)
    {
        $response = array();
        $departments = DepartmentResource::collection(Department::all());
        $employee_types = EmployeeType::all();
        $designations = Designation::all();
        $roles = Role::all();
        $permissions = Permission::all();
        $employee = new EmployeeResource(Employee::findOrFail($id));
        $response['departments'] = $departments;
        $response['employee_types'] = $employee_types;
        $response['designations'] = $designations;
        $response['roles'] = $roles;
        $response['permissions'] = $permissions;
        $response['data'] = (new EmployeeResource($employee))->additional([
            'history_logs' => LogResource::collection(Employee::findOrFail($id)->getLogs()
                                ->orderBy('created_at', 'DESC')->get())
        ]);
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $EmployeeDelete = Employee::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return EmployeeResource::MsgResponse($type, $msg);
        } catch (\Exception $exception) {
            return EmployeeResource::MsgResponse(true, $exception->getMessage());
        }
    }

    public function searchEmployee(Request $request)
    {
        $query = Employee::where('first_name', 'LIKE', '%' . $request->query('search') . '%')
            ->orWhere('last_name', 'LIKE', '%' . $request->query('search') . '%')
            ->department($request->query('department'))
            ->type($request->query('employee_type'))
            ->joined($request->query('joined_date'))
            ->orderBy('last_name', 'ASC')
            ->get();
        return EmployeeResource::collection($query);
    }
    // public function generateQR($id) {
    //     $employee = Employee::findOrFail($id);
    //     $qr = QrCode::size('300')
    //           ->format('svg')
    //           ->generate($employee->employee_no);
    //     return $qr;
    // }


    // public function generateQR($id) {
    //     $employee = Employee::findOrFail($id);
    //     $qr = QrCode::format('png')
    //             ->size(300)
    //             ->generate($employee->employee_no);
    //     $filename = $employee->last_name . " " . $employee->full_name . '.jpg';
    //     Storage::disk('public')->put($filename, $qr);
    //     return response()->download(storage_path('app/public/' . $filename), $filename);
    // }

    public function generateQR(Request $request, $id)
    {
        if ($id == "single") {
            $employee = Employee::findOrFail($request->id[0]);
            $qr = $this->simpleQR($employee->employee_no);
            $filename = htmlentities($employee->last_name . ' ' . $employee->first_name . ' ' . $employee->middle_name, ENT_QUOTES, 'UTF-8') . '.jpg';
            $file = Storage::disk('public')->put('employee_qr_codes/' . $filename, $qr);
            return response()->download(storage_path('app/public/employee_qr_codes/' . $filename), $filename);
        } else {
            $employees = Employee::whereIN('id', $request->id)->get();
            $employees_inserted = [];
            foreach ($employees as $key => $employee) {
                $qr = $this->simpleQR($employee->employee_no);
                $filename = htmlentities($employee->last_name . ' ' . $employee->first_name . ' ' . $employee->middle_name, ENT_QUOTES, 'UTF-8') . '.jpg';
                $file = Storage::disk('public')->put('employee_qr_codes/' . $filename, $qr);
                if ($file) {
                    array_push($employees_inserted, $filename);
                }
            }
            $zipFileName = 'storage/Employee_QR_codes.zip';
            $zip = new ZipArchive();
            $zip->open($zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
            foreach ($employees_inserted as $key => $inserted) {
                $path = storage_path('app/public/employee_qr_codes/' . $inserted);
                $zip->addFile($path, $inserted);
            }
            $zip->close();

            return response()->download($zipFileName);
        }
    }

    public function simpleQR($employee_number)
    {
        return QrCode::format('png')
            ->size(300)
            ->margin(2)
            ->generate($employee_number);
    }

    public function getEmployeeType()
    {
        $employee_types = EmployeeType::all();

        return ResourcesEmployeeType::collection($employee_types);
    }

    public function getYearJoined()
    {
        $years = JobDetail::select(DB::raw('YEAR(joined_date) as joined_year'))
        ->groupBy('joined_year')
        ->orderBy('joined_year', 'ASC')
        ->get();

        return $years;
    }
}

<?php

namespace App\Http\Controllers;

use App\HDMFTemplates;
use App\Http\Resources\HDMFTemplateResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class HDMIFController extends Controller
{
    //

    /**
     * Shows HDMIF Contribution Collection
     *
     * @return response
     */
    public function index(): ResourceCollection
    {
        $all = HDMFTemplates::all();
        return HDMFTemplateResource::collection($all);
    }

    /**
     * Add HDMIF Contribution Details
     *
     * @return respone
     * @param Request $request
     */
    public function store(Request $request): JsonResource
    {
        $validate = $this->validation($request);
        $hdmf_template = HDMFTemplates::create($validate);
        return new HDMFTemplateResource($hdmf_template);
    }

    /**
     * Show HDMIF Contribution Details
     *
     * @param int $id
     *
     * @return response
     */
    public function show($id): JsonResource
    {
        $hdmf_template = HDMFTemplates::findORFail($id);
        return new HDMFTemplateResource($hdmf_template);
    }

    /**
     * Update HDMIF Contribution Details
     *
     * @param Request $request
     * @param int $id
     *
     * @return repsonse
     */
    public function update(Request $request, $id): JsonResource
    {
        $validate = $this->validation($request);
        $hdmf_template = HDMFTemplates::findORFail($id);
        $hdmf_template->update($validate);
        return new HDMFTemplateResource($hdmf_template);
    }

    /**
     * Delete HDMIF Contribution Details
     *
     * @param int $id
     *
     * @return response
     */
    public function destroy($id): Response
    {
        $hdmf_template = HDMFTemplates::findORFail($id);
        $hdmf_template->delete();
        return response(null, 204);
    }

    /**
     * Mass delete HDMF Template
     *
     * @param Request $request
     * @param int $id
     *
     * @return void
     */
    public function massdelete(Request $request, $id)
    {
        return 'response';
    }

    public function validation(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'salary_from' => 'required|numeric',
                'salary_to' => 'required|numeric',
                'employer_percent' => 'required|numeric',
                'employee_percent' => 'required|numeric',
            ]
        );
    }
}

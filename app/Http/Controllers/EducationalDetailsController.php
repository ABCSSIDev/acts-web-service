<?php

namespace App\Http\Controllers;

use App\EducationalDetails;
use Illuminate\Http\Request;

class EducationalDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EducationalDetails  $educationalDetails
     * @return \Illuminate\Http\Response
     */
    public function show(EducationalDetails $educationalDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EducationalDetails  $educationalDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EducationalDetails $educationalDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EducationalDetails  $educationalDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationalDetails $educationalDetails)
    {
        //
    }
}

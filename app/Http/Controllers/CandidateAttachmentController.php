<?php

namespace App\Http\Controllers;

use App\CandidateAttachment;
use Illuminate\Http\Request;

class CandidateAttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CandidateAttachment  $candidateAttachment
     * @return \Illuminate\Http\Response
     */
    public function show(CandidateAttachment $candidateAttachment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CandidateAttachment  $candidateAttachment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CandidateAttachment $candidateAttachment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CandidateAttachment  $candidateAttachment
     * @return \Illuminate\Http\Response
     */
    public function destroy(CandidateAttachment $candidateAttachment)
    {
        //
    }
}

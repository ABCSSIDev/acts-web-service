<?php

namespace App\Http\Controllers;

use App\Http\Resources\Module as ModuleResource;
use App\Http\Resources\Permission as PermissionResource;
use App\Http\Resources\PermissionModule as PermissionModuleResource;
use App\Http\Resources\RestrictionModule as RestrictionModuleResource;
use App\Http\Resources\System as ResourcesSystem;
use App\Module;
use App\Permission;
use App\PermissionModule;
use App\Restriction;
use App\RestrictionModule;
use App\System;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use function App\Log\log_created;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();
        return PermissionResource::collection($permissions);
    }

    public function create()
    {
        $permission = Permission::all();
        $system = ResourcesSystem::collection(System::where('is_enabled', '1')->with('getSubModules')->get());
        return [
            'permission' => PermissionResource::collection($permission),
            'application' => $system,
        ];
    }

    public function getPermissionByApplication($id)
    {
        $permission = Permission::findOrFail($id);
        $arr = array();
        $arr = json_decode($permission->system_id);
        $systems = System::whereIn('id', $arr)->get();
        $system_module = array();
        foreach ($systems as $key => $application_name) {
            $data = $application_name->getSubModules()->select('modules.*')->addSelect(DB::raw($id .
            ' as permission_id'))->get();
            $system_module[] = [
                'id' => $application_name->id,
                'title' => $application_name->name,
                'content' => ModuleResource::collection($data)
            ];
        }
        return response()->json([
            'systems' => $system_module,
            'permission_data' => new PermissionResource($permission)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $systems = [];
            $modules = [];
            foreach ($request->systems as $system) {
                array_push($systems, $system['id']);
                foreach ($system['modules'] as $module) {
                    if ($module['enabled'] && !empty($module['restrictions'])) {
                        $restriction_arr = [];
                        foreach ($module['restrictions'] as $restriction) {
                            array_push($restriction_arr, $restriction['id']);
                        }
                        array_push($modules, [
                            'module_id' => $module['id'],
                            'restriction_id' => json_encode($restriction_arr)
                        ]);
                    }
                }
            }
            $permission = new Permission();
            $permission->permission_name = $request->permission_name;
            $permission->description = !empty($request->description) ? $request->description : "";
            $permission->system_id = json_encode($systems);
            $permission->save();
            log_created($permission);
            foreach ($modules as $key => $module) {
                $permission_module = new PermissionModule();
                $permission_module->permission_id = $permission->id;
                $permission_module->module_id = $module['module_id'];
                $permission_module->restriction_id = $module['restriction_id'];
                $permission_module->save();
            }
            return new PermissionResource($permission);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::findOrFail($id);
        return (new PermissionResource($permission))
                ->additional(['system_module' => $this->systemModuleRestriction($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $systems = [];
            $system_id = [];
            $modules = [];
            $deletedmodules = [];
           
            foreach ($request->system as $system) {
                array_push($systems, $system['id']);
                foreach ($system['modules'] as $module) {
                    if ($module['enabled'] && !empty($module['restrictions'])) {
                        $restriction_arr = [];
                        foreach ($module['restrictions'] as $restriction) {
                            array_push($restriction_arr, $restriction['id']);
                        }
                        array_push($modules, [
                            // 'id' => $module['id'],
                            'module_id' => $module['id'],
                            'restriction_id' => json_encode($restriction_arr)
                        ]);
                    }if (!$module['enabled'] && !empty($module['restrictions'])) {
                        array_push($deletedmodules, $module['per_mod_id']);
                    }
                }
            };
            // return $modules;
            foreach ($request->system as $systemdata) {
                array_push($system_id, $systemdata['id']);
            }
            // return $system_id;
            $Permission = Permission::where('id', $id)->update([
                'permission_name' => $request->permission_name,
                'description' => !empty($request->description) ? $request->description : "",
                'system_id' => json_encode($system_id),
            ]);
            $query = Permission::where('id', $id)->first();
            log_created($query);
            foreach ($modules as $key => $module) {
                if(PermissionModule::withTrashed()->where('permission_id', $query->id)->where('module_id', $module['module_id'])->first()){
                    $PermissionModule = PermissionModule::withTrashed()->where('permission_id', $query->id)->where('module_id', $module['module_id'])->update([
                        'permission_id' => $query->id,
                        'module_id' => $module['module_id'],
                        'restriction_id' => $module['restriction_id'],
                        'deleted_at' => null,
                    ]);
                }else{
                    $permission_module = new PermissionModule();
                    $permission_module->permission_id = $query->id;
                    $permission_module->module_id = $module['module_id'];
                    $permission_module->restriction_id = $module['restriction_id'];
                    $permission_module->save();
                }
                
            }
            if (count($deletedmodules) > 0) {
                $ReimbursementDelete = PermissionModule::whereIn('module_id', $deletedmodules)->where('permission_id', $query->id)->update([
                    'deleted_at' => date('Y/m/d H:i:s')
                ]);
            }
            return new PermissionResource($query);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        $arr = array();
        $arr = json_decode($permission->system_id);
        $system = ResourcesSystem::collection(System::where('is_enabled', '1')->with('getSubModules')->get());
        $systems = System::whereIn('id', $arr)->get();
        $system_module = array();
        foreach ($systems as $key => $application_name) {
            $data = $application_name->getSubModules()->select('modules.*')->addSelect(DB::raw($id .
            ' as permission_id'))->get();
            $system_module[] = [
                'id' => $application_name->id,
                'title' => $application_name->name,
                'content' => ModuleResource::collection($data)
            ];
        }
        return (new PermissionResource($permission) )
        ->additional([
            'system_module' => $this->systemModuleRestriction($id),
            'permission' =>  PermissionResource::collection(Permission::all()),
            'system' => $system,
            'systems' => $system_module
        ]);
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        //
    }

    public function systemModuleRestriction($permission_id = null)
    {
        $system = System::where('is_enabled', '1')->get();
        
        $system_module = array();
        foreach ($system as $key => $application_name) {
            $data = $application_name->getSubModules()->select('modules.*')->addSelect(DB::raw($permission_id .
            ' as permission_id'))->get();
            $system_module[] = [
                'title' => $application_name->name,
                'data' =>  RestrictionModuleResource::collection($data),
            ];
        }
        return $system_module;
    }
}

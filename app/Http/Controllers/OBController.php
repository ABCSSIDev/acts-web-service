<?php

namespace App\Http\Controllers;

use App\OB;
use App\Employee;
use Illuminate\Http\Request;
use function App\Log\log_created;
use App\Http\Resources\Employee as ResourcesEmployee;
use App\Http\Resources\OB as OBResource;
use Illuminate\Support\Facades\DB;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use App\Http\Resources\Log as LogResource;
class OBController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index()
    {
        $ob_array = array();
        $group_date = DB::table('hr_ob')->select(DB::raw('id as id'),DB::raw('DATE(date) as date'), DB::raw('count(*) as count'))->groupBy('date')->where('deleted_at',Null)->get();
        foreach($group_date as $key => $ob_date){
            $ob = OB::whereDate('date', $ob_date->date)->where('deleted_at',Null)->get();
            $ob_array[$key] = ['id'=> $ob_date->id ,'Date' => date('F d, Y',strtotime($ob_date->date)), 'ob' => OBResource::collection($ob)];
        }
        return response()->json($ob_array); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            $employee = Employee::role(OB::class)->get();
            return ResourcesEmployee::collection($employee);
        }catch(\Exception $exception){
            return OBResource::MsgResponse(true, $exception->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $official_business = New OB;
            $official_business->employee_id = $request->input('employee_id');
            $official_business->date = $request->input('date');
            $official_business->time_in = $request->input('time_in');
            $official_business->time_out = $request->input('time_out');
            // $official_business->rendered_hours = $request->input('rendered_hours');
            $official_business->purpose = $request->input('purpose');
            $official_business->save();
            log_created($official_business); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new OBResource($official_business);
            
        }catch(\Exception $exception){
            return OBResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $official_business = OB::findOrFail($id);
            $collections = [
                'approver' => check_approver($official_business),
                'approved_status' => model_approval_status(check_approver($official_business)),
                'official_business' => new OBResource($official_business),
                'history_logs' => new LogResource (OB::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->first()),
            ];
            return $collections;

        }catch(\Exception $exception){
            return OBResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ob = OB::findOrFail($id);     
        $official_business = new OBResource($ob);
        $collections = [
            'official_business' => $official_business,
        ];
        return $collections;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $ob = OB::where('id', $id)->update([
                'date' => date('Y-m-d', strtotime($request->input('date'))),
                'time_in' => $request->input('time_in'),
                'time_out' => $request->input('time_out'),
                'purpose' => $request->input('purpose'),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            $query = OB::where('id', $id)->first();
            log_created($query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new OBResource($query);
            
        }catch(\Exception $exception){
            return OBResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $OBDelete = OB::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return OBResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return OBResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function massdelete(Request $request,$id)
    {        
        try {
            $UserDelete = OB::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return OBResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return OBResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

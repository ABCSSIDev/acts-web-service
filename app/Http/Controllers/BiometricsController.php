<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BiometricsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file("file"));
        $retrieved_file =$request->file('file')->path();
        // $file_url = storage_path('app/' . $retrieved_file);
        // $file_open = Storage::get($retrieved_file);
        // $removed_first = explode("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",Storage::get($retrieved_file));
        // $removed_secod = explode("\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",Storage::get($retrieved_file));
        // // dd($file_open);
        // $str_s = file_get_contents(storage_path('app/' . $retrieved_file));
        // dd($retrieved_file);
        // dd(preg_replace('/[0-9a-fA-F]{6}/', '', $str_s));
        $is = strval(file_get_contents($retrieved_file));
        $SS = mb_convert_encoding($is,"ASCII","UTF-8");
        $sad = preg_replace('/[\x00-\x1f]/', "-", $SS);
        $es = explode("-", $sad);
        $sc = preg_split('/\x/',  $SS);
        // $fin = array_search("",$sc);
        foreach($es as $key => $data){
            if($data == "" || strlen($data) <= 1){
                unset($es[$key]);
            }
        }
        // dd( strval(file_get_contents($retrieved_file)));
        dd($es);
        
        // dd(iconv('ASCII', 'UTF-8//IGNORE', $file_open));
        // dd(fopen($retrieved_file->filename, "r", true));
    }

    public function getAttendance(Request $request){
        $file_extension = $request->file('file')->getClientOriginalExtension();
        $response = null;
        switch($file_extension){
            case 'xls':
                $response = [
                    "attendance" =>  $this->handleExcel($request),
                    "success" => true
                ];
                break;
            case 'dat':
                $response = [
                    "attendance" =>  $this->handleDatFile($request),
                    "success" => true
                ];
                break;
            default:
                $response = [
                    "success" => false,
                    "error_desc" => "File Extension Not Supported"
                ];
        }

        return $response;
    }
    public function handleDatFile(Request $request){
        $retrieved_file =$request->file('file')->path();
        $is = str_replace(" ", "", file_get_contents($retrieved_file));
        $sis = str_replace("\"\"\"", "", file_get_contents($retrieved_file));
        $separated_per_line = preg_split("/\\r\\n/",  $sis);
        $attendances = [];
        foreach($separated_per_line as $attendance){
            $data = (preg_split("/\\t/", $attendance));
            if(!empty($data) && count($data) >= 6){
                $user = trim($data[0]);
                $date = date('Y-m-d', strtotime($data[1]));
                $employee = Employee::where('biometrics_number', $user)->first();
                if($employee){
                    $date_log = $data[1];
                    $emp_attendance = Attendance::where('employee_id', $employee->id)
                                                ->where(function ($query) use ($date_log) {
                                                    $query->where('start_time', '=', $date_log)
                                                        ->orWhere('end_time', '=', $date_log);
                                                })->get();
                    if(empty($attendances[$date][$user])){
                        $attendances[$date][$user] = [
                                "user_id" => $user,
                                "employee" => $employee->fullname(),
                                "time_in" => "",
                                "time_out" => "",
                                "verified" => ($emp_attendance->count() > 0),
                                "description" => (($emp_attendance->count() > 0)?"Uploaded":"Not Yet Uploaded" )                       
                            ];
                    }
               
                    
                }else{
                    if(empty($attendances[$date][$user])){
                        $attendances[$date][$user] = [
                                "user_id" => $user,
                                "employee" => "",
                                "time_in" => "",
                                "time_out" => "",
                                "verified" => false,
                                "description" => "Biometric number does not exist"              
                            ];
                    }
                }

                if($data[3] == 1){
                    if($attendances[$date][$user]["time_out"] == ""){
                        $attendances[$date][$user]["time_out"] = trim($data[1]);
                    }
                } else{
                    if($attendances[$date][$user]["time_in"] == ""){
                        $attendances[$date][$user]["time_in"] = trim($data[1]);
                    }
                }
            }
        }
        $remapped = array_map('array_values', $attendances);
        return $remapped;
    }

    public function handleExcel(Request $request){
        $retrieved_file =$request->file('file');
        $temp_file = Storage::putFileAs("temp_import", $retrieved_file, $retrieved_file->getClientOriginalName());
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile(storage_path('app/'.$temp_file));
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load(storage_path('app/'.$temp_file));
        $worksheet = $spreadsheet->getActiveSheet();
        $highest_row = $worksheet->getHighestRow();
        $hightest_column = $worksheet->getHighestColumn();
        $alphas = range('A', $hightest_column);
        $values  = [];
        $time_cells = [
            "IN" => [],
            "OUT" => []
        ];
        $formatted_value = [];
        $retrieved_in_out_pos = false;
        $employee_id = null;
        $employee_name = null;
        $employee_query = null;
        $status = "";

        //loop through each row in worksheet till last row with value
        for($y = 1; $y < $highest_row; $y++){
            $first_column = $worksheet->getCell($alphas[0] . strval($y))->getValue();
            $exploded = explode(' ', $first_column);
            $values[$y] = [];
            $string_date = strtotime(str_replace("/", "-", $exploded[0]));
            $date_format = date('Y-m-d', $string_date);
            $timelog = [
                'user_id' => $employee_id,
                'employee' => $employee_name,
                'time_in' => null,
                'time_out' => null,
                "verified" => false,
                'description' => $status
            ];
           
            if($string_date != false){
                //handles timein .and timeout data manipulations
                if( !empty($exploded[0]) && empty($formatted_value[$date_format])){
                    $formatted_value[$date_format] = [];
                }

                //loop through each column in worksheet till last column with value
                for($x = 0; $x < count($alphas); $x++){
                    $cell = strval($alphas[$x]) . strval($y);
                    $cell_value = $worksheet->getCell($cell)->getValue();
                    
                    if(in_array(strval($alphas[$x]), $time_cells["IN"])){
                        if(!empty($cell_value)){
                            $timelog['time_in'] = date('Y-m-d H:i:s', strtotime("$date_format $cell_value"));
                        }
                    }
                    if(in_array(strval($alphas[$x]), $time_cells["OUT"])){
                        if(!empty($cell_value)){
                            $timelog['time_out'] = date('Y-m-d H:i:s', strtotime("$date_format $cell_value"));
                        }
                    }
                }
            }else{
                if(empty($exploded[0])){
                    //handles empty cell
                    if(!$retrieved_in_out_pos){
                        for($x = 0; $x < count($alphas); $x++){
                            $cell = strval($alphas[$x]) . strval($y);
                            $cell_value = $worksheet->getCell($cell)->getValue();
                            if(in_array($cell_value, ["IN", "OUT"])){
                                array_push($time_cells[$cell_value], strval($alphas[$x]));
                                // array_push($values[$y], $cell);
                                $retrieved_in_out_pos = true;
                            }
                        }
                    }
                    array_push($values[$y], "empty cell");
                }else{
                    //handles strings that are not dates
                    $exploded_string = explode("(", str_replace(")", "", $first_column));
                    if(!empty($exploded_string) && count($exploded_string) > 1){
                        $cleaned_string = trim($exploded_string[1]);
                        $employee_query = Employee::where('employee_no', $cleaned_string)->first();
                        if(!empty($employee_query)){
                            $employee_name = $employee_query->fullname();
                            $employee_id = $cleaned_string;
                            $status = "";
                            continue;
                        }else{
                            $employee_id = $cleaned_string;
                            $employee_name = $exploded_string[0];
                            $status = "No Employee Records";
                        }
                    }
                }
            }
            if($employee_id!="None" && !empty($timelog['time_in']) && !empty($timelog['time_out'])){
                if($employee_query && !empty($employee_query)){
                    $emp_attendance = Attendance::where('employee_id', $employee_query->id)
                    ->where(function ($query) use ($date_format) {
                        $query->where('start_time', '=', $date_format)
                            ->orWhere('end_time', '=', $date_format);
                    })->get();
                    $timelog["verified"] = ($emp_attendance->count() > 0);
                    $timelog['description'] = (($emp_attendance->count() > 0)?"Uploaded":"Not Yet Uploaded" );
                }
               
                $formatted_value[$date_format][$employee_id] = $timelog;
            }
        }
        $remapped = array_map('array_values', $formatted_value);
        return array_filter($remapped);
    }

    public function importAttendance(Request $request){
        $data = json_decode($request->input('data'));
        foreach($data as $date_key => $date){
            foreach($date->attendance as $attendance_key => $attendance){
                $employee = Employee::where('biometrics_number', $attendance->user_id)->orWhere('employee_no', $attendance->user_id)->first();
                if($employee){
                    $attendance_new = new Attendance();
                    $attendance_new->employee_id = $employee->id;
                    $attendance_new->project_site_id = 1;
                    $attendance_new->start_time = $attendance->time_in;
                    $attendance_new->end_time = $attendance->time_out;
                    if(!$attendance->verified){
                        $attendance_new->save();
                    }
                    $data[$date_key]->attendance[$attendance_key]->verified = true;
                    $data[$date_key]->attendance[$attendance_key]->description = "Uploaded";
                    continue;
                }
                $data[$date_key]->attendance[$attendance_key]->verified = false;
                $data[$date_key]->attendance[$attendance_key]->description = "Biometric number does not exist";
            }
        }
        return $data;
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransmittalSlip;
use App\TransmittalItemsSlip;
use App\Employee;
use App\Http\Resources\TransmittalSlip as TransmittalSlipResource;
use function App\Log\log_created;
use App\Inventories;
use App\Unit;
use App\Assets;

class TransmittalSlipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $TransmittalSlip = TransmittalSlip::all();
        return TransmittalSlipResource::collection($TransmittalSlip);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'employees' => Employee::role(Transmittal::class)->get(),
            'inventories' => Inventories::all(),
            'units' => Unit::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function findAssetTag($id)
    {
        $data = Assets::where('inventory_id', $id)->first();
        return $data->asset_tag;
    }
    public function store(Request $request)
    {
        try {
            $TransmittalSlip = TransmittalSlip::create([
                'employee_id'       => $request->employee_id,
                'location'  => $request->location,
                'date'              => $request->date
            ]);
            $data =  $request->input('item');
            if(!empty($data)){
                foreach ($data as $key => $value){
                    $assets=Assets::where('inventory_id', $value['inventory_id'])->first();
                    
                    $TransmittalItemsSlip = New TransmittalItemsSlip;
                    $TransmittalItemsSlip->transmittal_id = $TransmittalSlip->id;
                    $TransmittalItemsSlip->inventory_id = $value['inventory_id'];
                    $TransmittalItemsSlip->asset_id = ($assets ? $assets->id:null);
                    $TransmittalItemsSlip->remarks = $value['remarks'];
                    $TransmittalItemsSlip->quantity = $value['quantity'];
                    $TransmittalItemsSlip->save();
                }
            }
            log_created($TransmittalSlip);
            return TransmittalSlipResource::MsgResponse(false, 'Successfully Created');
            // return new TransmittalSlipResource($TransmittalSlip);
        }catch(\Exception $exception){
            return TransmittalSlipResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $TransmittalSlip = TransmittalSlip::findOrFail($id);
        return new TransmittalSlipResource($TransmittalSlip);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $TransmittalSlip = TransmittalSlip::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return TransmittalSlipResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return TransmittalSlipResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

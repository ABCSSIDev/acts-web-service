<?php

namespace App\Http\Controllers;

use App\ApprovalProcess;
use App\ApprovalUser;
use App\Department;
use App\Http\Resources\ApprovalProcess as ApprovalProcessResource;
use App\Http\Resources\UserAccess as UserAccessResource;
use App\Role;
use App\System;
use App\UserAccess;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

use function App\Log\log_created;

class ApprovalProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ApprovalProcess = ApprovalProcess::all();
        return ApprovalProcessResource::collection($ApprovalProcess);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
  
        $system = System::where('is_enabled', '1')
            ->where('id', '1')
            ->get();
        
        $data = array();

        foreach ($system as $key => $systemModule) {
            $data[] = [
                'label' => $systemModule->name,
                'options' => $this->getModules($systemModule->getSubModules()->get()),
            ];
        }

        $approver = array();

        $roles = Role::all();
        foreach ($roles as $key => $role) {
            $user_access = UserAccess::where('role_id', $role->id)->get();
            if (count($user_access) > 0) {
                $approver[] = [
                    'role' => $role->role_name,
                    "users" => UserAccessResource::collection($user_access)
                ];
            }
        }

        return response()->json([
            'module' => $data,
            'approver' => $approver,
            'departments' => Department::select('id', 'name')->get()
        ]);
    }

    /**
     * get modules option
     *
     * @param object $modules
     *
     * @return array
     */
    public function getModules(object $modules): array
    {
        $options = array();
        foreach ($modules as $key => $module) {
            if ($module->model_type != null) {
                $options[] = [
                    "value" => $module->model_type,
                    "text"  => $module->name
                ];
            }
        }
        return $options;
    }

    /**
     * Store Approval Process function
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function store(Request $request): JsonResource
    {
        $department_array = [];
        foreach($request->departments as $department){
            array_push($department_array, $department['id']);
        }
        $approvalProcess = new ApprovalProcess();
        $approvalProcess->reference_type = $request->module;
        $approvalProcess->name = $request->name;
        $approvalProcess->description = $request->description;
        $approvalProcess->execute = count($request->execute) == 2 ? 2 : $request->execute[0];
        $approvalProcess->status = 1;
        $approvalProcess->approved_setup = $request->approver_setup;
        $approvalProcess->order = $request->approver_setup == 0 ? null : $request->order;
        $approvalProcess->departments = json_encode($department_array);
        $approvalProcess->save();

        
        $order = 1;
        if ($request->sorted == null) {
            foreach ($request->approver as $key => $approver) {
                $approvalUser = new ApprovalUser();
                $approvalUser->approval_process_id = $approvalProcess->id;
                $approvalUser->user_id = $approver['user_id'];
                $approvalUser->seq_order = $approvalProcess->order == 0 &&
                    $approvalProcess->order != null ? $order++ : null;

                $approvalUser->save();
            }
        } else {
            foreach ($request->sorted as $key => $approver) {
                $approvalUser = new ApprovalUser();
                $approvalUser->approval_process_id = $approvalProcess->id;
                $approvalUser->user_id = $approver['name']['user_id'];
                $approvalUser->seq_order =  $approvalProcess->order == 0 &&
                    $approvalProcess->order != null ? $order++ : null;

                $approvalUser->save();
            }
        }

        return new ApprovalProcessResource($approvalProcess);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ApprovalProcess = ApprovalProcess::findOrFail($id);
        return new ApprovalProcessResource($ApprovalProcess);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $ApprovalProcess = ApprovalProcess::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return ApprovalProcessResource::MsgResponse($type, $msg);
        } catch (\Exception $exception) {
            return ApprovalProcessResource::MsgResponse(true, $exception->getMessage());
        }
    }
}

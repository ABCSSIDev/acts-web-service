<?php

namespace App\Http\Controllers;

use App\Http\Resources\PurchaseRequest as ResourcesPurchaseRequest;
use App\PurchaseRequest;
use App\PurchaseRequestItems;
use Exception;
use Illuminate\Http\Request;

class PurchaseRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ResourcesPurchaseRequest::collection(PurchaseRequest::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'pr_number' => 'required|unique:kamalig_purchase_requests'
            ]);

            $data = $request->only([
                'pr_number',
                'date',
                'department_id',
                'purchase_request_items'
            ]);

            $purchase_request = new PurchaseRequest();
            $purchase_request->pr_number = $data['pr_number'];
            $purchase_request->date = $data['date'];
            $purchase_request->department_id = $data['department_id'];
            $purchase_request->save();

            foreach ($data['purchase_request_items'] as $key => $item) {
                $purchase_request_item = new PurchaseRequestItems();
                $purchase_request_item->purchase_request_id = $purchase_request->id;
                $purchase_request_item->inventory_id = $item['id'];
                $purchase_request_item->quantity = $item['quantity'];
                $purchase_request_item->save();
            }
            
            return new ResourcesPurchaseRequest($purchase_request);
        }
        catch (Exception $exception) {
            return ResourcesPurchaseRequest::errorResponse($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PurchaseRequest  $purchaseRequest
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseRequest $purchaseRequest)
    {
        return new ResourcesPurchaseRequest($purchaseRequest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PurchaseRequest  $purchaseRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseRequest $purchaseRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PurchaseRequest  $purchaseRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PurchaseRequest $purchaseRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseRequest  $purchaseRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseRequest $purchaseRequest)
    {
        //
    }
}

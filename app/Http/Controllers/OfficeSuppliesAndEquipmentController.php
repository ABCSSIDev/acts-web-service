<?php

namespace App\Http\Controllers;

use App\AccountabilityIssuance;
use App\AccountabilityRequisition;
use Illuminate\Http\Request;
use App\OfficeSuppliesAndEquipment;
use function App\Log\log_created;
use App\Http\Resources\OfficeSuppliesAndEquipment as OfficeSuppliesAndEquipmentResource;
use App\Inventories;
use App\InventoriesNames;
use App\Employee;
use App\Unit;
use App\Categories;
use App\Http\Resources\AccountabilityRequisition as ResourcesAccountabilityRequisition;
use App\Http\Resources\AssetInventories;
use App\InventoryStatus;
use App\Http\Resources\Inventories as InventoriesResource;
use Illuminate\Support\Facades\DB;

use function App\Helpers\save_image;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
class OfficeSuppliesAndEquipmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index()
    {
        $inventories = Inventories::where('type', 2)->withCount('issued')->get();
        $assetList = AssetInventories::collection($inventories);
        // $OfficeSuppliesAndEquipment = OfficeSuppliesAndEquipment::all();
        // $assetList = OfficeSuppliesAndEquipmentResource::collection($OfficeSuppliesAndEquipment);
        $consumableList = InventoriesResource::collection(Inventories::where('type', 3)->get());
        $collections = [
            'assets' => $assetList,
            'consumables' => $consumableList,
        ];
        return $collections;
    }
    public function create(){
        $response = array();
        $Unit = Unit::all();
        $Employee = Employee::role(OfficeSuppliesAndEquipment::class)->get();
        $Categories = Categories::all();
        $InventoryStatus = InventoryStatus::all();
        $response['Unit'] = $Unit;
        $response['Categories'] = $Categories;
        $response['Employees'] = $Employee;
        $response['InventoryStatus'] = $InventoryStatus;

        return response()->json($response);
    }
    public function store(Request $request){
        try{
            // return $request->input('item');
            $data = $request->input('item');
            $inventory_id = null;
            // [{"inventory_status_id":2,"asset_tag":"Cat-01","inventory_description_id":2,"unit_id":2,"category_id":2,"quantity":2},{"inventory_status_id":2,"asset_tag":"Cat-02","inventory_description_id":2,"unit_id":2,"category_id":2,"quantity":2}]
            if(!empty($data)){ 
                foreach ($data as $key => $value) {
                    $result = DB::table('kamalig_inventory_descriptions')
                            ->leftJoin('kamalig_inventories', 'kamalig_inventory_descriptions.id', '=', 'kamalig_inventories.inventory_description_id')
                            ->where('kamalig_inventory_descriptions.description',$value['description'])
                            ->select('kamalig_inventories.id')
                            ->where('kamalig_inventories.type', 2)
                            ->first();
                    if (!$result) {
                        $InventoriesNames = New InventoriesNames;
                        $InventoriesNames->category_id = $value['category_id'];
                        $InventoriesNames->description = $value['description'];
                        $InventoriesNames->save();

                        $Inventories = New Inventories;
                        $Inventories->inventory_description_id = $InventoriesNames->id;
                        $Inventories->unit_id = $value['unit_id'];
                        $Inventories->category_id = $value['category_id'];
                        $Inventories->quantity = $value['quantity'];
                        $Inventories->type = 2;
                        $Inventories->save();
                        $inventory_id = $Inventories->id;
                    } else {
                        $inventory_id = $result->id;
                    }
                        $OfficeSuppliesAndEquipment = New OfficeSuppliesAndEquipment;
                        $OfficeSuppliesAndEquipment->inventory_id = $inventory_id;
                        $OfficeSuppliesAndEquipment->inventory_status_id = $value['inventory_status_id'];
                        $OfficeSuppliesAndEquipment->asset_tag = $value['asset_tag'];
                        $OfficeSuppliesAndEquipment->assigned_to = $value['assigned_to'];
                        $OfficeSuppliesAndEquipment->serial_model_no = $value['serial_model_no'];
                        $OfficeSuppliesAndEquipment->save();
                        // log_created($OfficeSuppliesAndEquipment);
                }
            }
            return OfficeSuppliesAndEquipmentResource::MsgResponse(false, 'Created!');
        } catch (\Exception $exception) {
            return OfficeSuppliesAndEquipmentResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function saveImage(Request $request)
    {
        try{
            $count =  OfficeSuppliesAndEquipment::count();
            if (request()->hasFile('image')) {//save image
                $file_name = 'Inventories/Assets'.time().$count.'s.jpg';
                $file = \request()->file('image');
                $array = array();
                if (save_image($file, $file_name)) {
                    $response['image'] = $file_name;
                    $response['image_display'] = asset(Storage::url($file_name));

                    return response()->json($response);
                }
            }
        }catch (\Exception $exception){
            return InventoriesResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function show($id)
    {
        $inventories = Inventories::where('type', 2)->where('id', $id)->withCount('issued')->with('issued')->first();
        return new AssetInventories($inventories);
    }
    public function edit($id)
    {
        $response = array();
        $Unit = Unit::all();
        $Employee = Employee::role(OfficeSuppliesAndEquipment::class)->get();
        $Categories = Categories::all();
        $InventoryStatus = InventoryStatus::all();
        $response['data'] = new OfficeSuppliesAndEquipmentResource(OfficeSuppliesAndEquipment::findOrFail($id));
        $response['Unit'] = $Unit;
        $response['Categories'] = $Categories;
        $response['Employees'] = $Employee;
        $response['InventoryStatus'] = $InventoryStatus;

        return response()->json($response);
    }
    public function update(Request $request, $id)
    {
        // return $request->input('image'); 
        try{
            if($request->input('serial_model_no') != null && OfficeSuppliesAndEquipment::where('serial_model_no', $request->input('serial_model_no'))->where('id','!=', $id)->first()){
                return OfficeSuppliesAndEquipmentResource::MsgResponse(true,'This Serial No Already been use');
            }elseif(OfficeSuppliesAndEquipment::where('asset_tag', $request->input('asset_tag'))->where('id','!=', $id)->first()){
                return OfficeSuppliesAndEquipmentResource::MsgResponse(true,'This Assets Tag Already been use');
            }else{
                    if($request->input('imageval') == 1){
                            if (request()->hasFile('image')) {//save image
                            $count = OfficeSuppliesAndEquipment::count();
                            $file_name = 'Inventories/inventories_'.time().$count.'s.jpg';
                            $file = \request()->file('image');
                            $array = array();
                            if (save_image($file, $file_name)) {
                                $file_name = $file_name;
                            }
                        }else{
                            $file_name = NULL;
                        }
                    }else{
                        $file_name = $request->input('image');
                    }

                $OfficeSuppliesAndEquipment = OfficeSuppliesAndEquipment::where('id', $id)->update([ 
                    'inventory_status_id' => $request->input('inventory_status_id'),
                    'asset_tag' => $request->input('asset_tag'),
                    'image' => $file_name,
                    'assigned_to' => $request->input('assigned_to'),
                    'serial_model_no' => $request->input('serial_model_no'),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
                $query = OfficeSuppliesAndEquipment::where('id', $id)->first();
                $Inventories = Inventories::where('id', $query->inventory_id)->update([
                    'unit_id' => $request->input('unit'),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
                $query2 = Inventories::where('id', $query->inventory_id)->first();
                $Inventories = Inventories::where('id', $query2->inventory_description_id)->update([ 
                    'category_id' => $request->input('category'),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
                return new OfficeSuppliesAndEquipmentResource($query);
            }
        }catch(\Exception $exception){
            return OfficeSuppliesAndEquipmentResource::MsgResponse(true,$exception->getMessage());
        }
    }
     public function destroy($id)
    {
        // return $id;
        try {
            $data = OfficeSuppliesAndEquipment::where('id',$id)->first();
            if(OfficeSuppliesAndEquipment::where('inventory_id',$data->inventory_id)->count() == 1){
                $Inventories = Inventories::where('id',$data->inventory_id)->update([
                    'deleted_at' => date('Y/m/d H:i:s')
                ]);
            }
            
            $OfficeSuppliesAndEquipmentDelete = OfficeSuppliesAndEquipment::where('id', $id)->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return OfficeSuppliesAndEquipmentResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return OfficeSuppliesAndEquipmentResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function showConsumables($id)
    {
        $Inventories = Inventories::where('type',3)->where('id',$id)->first();
        return new InventoriesResource($Inventories);
    }
    public function insertConsumables(Request $request){
        try{
            $InventoriesNames = New InventoriesNames;
            $InventoriesNames->category_id = $request->input('category_id');
            $InventoriesNames->description = $request->input('description');
            $InventoriesNames->save();
            
            $Inventories = New Inventories;
            $Inventories->inventory_description_id =  $InventoriesNames->id;
            $Inventories->unit_id = $request->input('unit_id');
            $Inventories->category_id = $request->input('category_id');
            $Inventories->quantity = $request->input('quantity');
            // $Inventories->unit_price = $request->input('unit_price');
            $Inventories->min_stock =$request->input('min_stock');
            $Inventories->type = 3;
            $Inventories->save();

            $type = false;
            $msg = 'Successfully New Consumables Created!';
            return new InventoriesResource($Inventories);
        }catch (\Exception $exception){
            return InventoriesResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function validationInventory(Request $request)
    {
        // return $request;
        $result = DB::table('kamalig_inventory_descriptions')
        ->leftJoin('kamalig_inventories', 'kamalig_inventory_descriptions.id', '=', 'kamalig_inventories.inventory_description_id')
        ->where('kamalig_inventory_descriptions.description',$request->input('description'))
        ->where('kamalig_inventories.type',2)
        ->first();
        if($result){
            $data = 'Error';
        }else{
            $data = 'No-Error';
        }
        return $data;
    }

    public function newAsset(Request $request)
    {
        try {
            $value = $request->input("assets");
            $OfficeSuppliesAndEquipment = New OfficeSuppliesAndEquipment;
            $OfficeSuppliesAndEquipment->inventory_id = $inventory_id;
            $OfficeSuppliesAndEquipment->inventory_status_id = $value['inventory_status_id'];
            $OfficeSuppliesAndEquipment->asset_tag = $value['asset_tag'];
            $OfficeSuppliesAndEquipment->image = $value['image'];
            $OfficeSuppliesAndEquipment->assigned_to = $value['assigned_to'];
            $OfficeSuppliesAndEquipment->serial_model_no = $value['serial_model_no'];
            $OfficeSuppliesAndEquipment->save();
            return OfficeSuppliesAndEquipmentResource::MsgResponse(false, 'Created!');
        } catch (\Exception $exception) {
            return OfficeSuppliesAndEquipmentResource::MsgResponse(true, $exception->getMessage());
        }
    }

public function getAllAssets($inventory_id)
    {
        $assets = OfficeSuppliesAndEquipment::where('inventory_id', $inventory_id)->doesnthave('issuanceItem')->get();
        return OfficeSuppliesAndEquipmentResource::collection($assets);
    }

    public function updateAssets(Request $request)
    {
        $data = json_decode($request->input('assets'));
        foreach ($data as $key => $asset) {
            $asset_update = OfficeSuppliesAndEquipment::where('id', $asset->id)->update([
                'asset_tag' => $asset->asset_tag,
                'serial_model_no' => $asset->serial_model_number,
                'asset_status' => $asset->status
            ]);
        }

        return OfficeSuppliesAndEquipmentResource::MsgResponse(false, 'Assets Successfully Updated!');
    }

    public function getEmployeeAssignedAssets($employee_id)
    {
        $assets = AccountabilityIssuance::where('employee_id', $employee_id)->get();
        $assigned = [];
        foreach ($assets as $asset) {
            foreach ($asset->issuanceItems as $key => $item) {
                array_push($assigned, [
                    'id' => $item->id,
                    'accountability_issuance_id' => $asset->id,
                    'inventory_name' => $item->asset->inventory->getInventoriesNames->description,
                    'serial_model_number' => $item->asset->serial_model_no,
                    'amount' => $item->asset->amount
                ]);
            }
        }
        return response()->json($assigned);
    }

    public function getAssignedAssets(): HasMany
    {
        return $this->hasMany(AccountabilityIssuance::class, 'employee_id');
    }

    public function getAssetInventories()
    {
        $inventories = Inventories::where('type', 2)->withCount('issued')->get();
        return AssetInventories::collection($inventories);
    }
}



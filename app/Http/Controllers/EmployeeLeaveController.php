<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeLeave;
use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\EmployeeLeave as LeaveResource;
use App\Http\Resources\EmployeeLeave as EmployeeLeaveResource;
use App\Http\Resources\LeaveType as LeaveTypeResource;
use App\LeaveType;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Log as LogResource;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use function App\Log\log_created;

class EmployeeLeaveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $EmployeeLeave = EmployeeLeave::role(EmployeeLeave::class)->get();
        return EmployeeLeaveResource::collection($EmployeeLeave);
    }

    public function create() {
        try {
            $employees = Employee::role(EmployeeLeave::class)->get();
            $leave_types = LeaveType::all();

            $leaveYears = EmployeeLeave::select(DB::raw('YEAR(start_date) as leave_year'))->groupBy('leave_year')->get();
            $leaveMonths = EmployeeLeave::select(DB::raw('MONTHNAME(start_date) as leave_month, MONTH(start_date) as leave_month_value'))->groupBy('leave_month')->get();

            return response()->json([
                'employees' =>  EmployeeResource::collection($employees),
                'leave_type' => LeaveTypeResource::collection($leave_types),
                'leaveYears' => $leaveYears,
                'leaveMonths' => $leaveMonths
            ]);

        } catch(\Exception $exception) {
            return LeaveResource::errorResponse($exception->getMessage());
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $leave = new EmployeeLeave();
            $leave->employee_id = $request->input('employee_id');
            $leave->leave_id = $request->input('leave_id');
            $leave->start_date = date('Y-m-d', strtotime($request->input('start_date')));
            $leave->end_date = date('Y-m-d', strtotime($request->input('end_date')));
            $leave->total_days = date_diff(new DateTime($request->input('start_date')),new DateTime($request->input('end_date')))->format("%a")+1;
            $leave->reason = $request->input('reason');
            $leave->status = 0;
            $leave->save();
            log_created($leave); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new LeaveResource($leave);

        } catch(\Exception $exception) {
            return LeaveResource::errorResponse($exception->getMessage());
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $leave = EmployeeLeave::find($id);
        $all_leaves_employee = EmployeeLeave::select(DB::raw('YEAR(start_date) as date'), DB::raw('count(*) as total'), DB::raw('SUM(total_days) as total_days'))
                                            ->where('employee_id', $leave->employee_id)
                                            ->where('leave_id', $leave->leave_id)
                                            ->groupBy('date')
                                            ->get();
        $balances = self::computeLeaveBalance($leave);

        return (new LeaveResource($leave))->additional([
            'estimated_balance' => $balances['estimated_balance'],
            'available_balance' => $balances['available_balance'],
            'approver' => check_approver($leave, $leave->employee_id), //option if user is approver and already approved
            'approved_status' =>  model_approval_status(check_approver($leave)),   //pending, waiting order approver,approved
            'history_logs' => LogResource::collection(EmployeeLeave::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $leave_type = LeaveType::all();
        $employee_leave = EmployeeLeave::findOrFail($id);
        $employee_leaves = new EmployeeLeaveResource($employee_leave);
        $collections = [
            'leave_type' => $leave_type,
            'employee_leave' => $employee_leaves,
        ];
        return $collections;
    }
    public function update(Request $request, $id)
    {
        try{
            $d = EmployeeLeave::where('id', $id)->update([
                'leave_id' => $request->input('leave_id'),
                'start_date' => date('Y-m-d', strtotime($request->input('start_date'))),
                'end_date' => date('Y-m-d', strtotime($request->input('end_date'))),
                'total_days' => date_diff(new DateTime($request->input('start_date')),new DateTime($request->input('end_date')))->format("%a")+1,
                'reason' => $request->input('reason'),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            // log_created($Employee_leave); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            $query = EmployeeLeave::where('id', $id)->first();
            return new EmployeeLeaveResource($query);

        }catch(\Exception $exception){
            return EmployeeLeaveResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $EmployeeLeave = EmployeeLeave::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return EmployeeLeaveResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return EmployeeLeaveResource::MsgResponse(true,$exception->getMessage());
        }
    }


    public static function computeLeaveBalance($leave){
        $employee = $leave->getEmployee;
        $years = [];
        $date_joined = date('Y', strtotime(!empty($employee->getJobDetails)?$employee->getJobDetails->joined_date:now()));
        $total_balance = 0;
        $present_balance = 0;
        array_push($years, intval($date_joined));
        while($years[count($years)-1] != now()->year){
            array_push($years, $years[count($years)-1] + 1);
        }

        foreach($years as $year){
            $all_leaves_employee = EmployeeLeave::select(DB::raw('YEAR(start_date) as date'), DB::raw('count(*) as total'), DB::raw('SUM(total_days) as total_days'))
            ->where('employee_id', $leave->employee_id)
            ->where('leave_id', $leave->leave_id)
            ->whereYear('start_date', strval($year))
            ->groupBy('date')
            ->first();

            $leaves_current = EmployeeLeave::select(DB::raw('YEAR(start_date) as date'), DB::raw('count(*) as total'), DB::raw('SUM(total_days) as total_days'))
            ->where('employee_id', $leave->employee_id)
            ->where('leave_id', $leave->leave_id)
            ->whereYear('start_date', strval($year))
            ->whereDate('start_date', '<=', $leave->start_date)
            ->groupBy('date')
            ->first();

            if(!empty($all_leaves_employee) ){
                if($year == now()->year){
                    $past_balance = $leave->getLeaveType->balance  - $all_leaves_employee->total_days;
                    $current_balance = $leave->getLeaveType->balance  - $leaves_current->total_days;
                }else{
                    $past_balance = number_format(($leave->getLeaveType->balance * ($leave->getLeaveType->carry_forward_percentage / 100)), 2, '.', '')  - $all_leaves_employee->total_days;
                    $current_balance = number_format(($leave->getLeaveType->balance * ($leave->getLeaveType->carry_forward_percentage / 100)), 2, '.', '')  - $leaves_current->total_days;
                }
                $total_balance += $past_balance;
                $present_balance += $current_balance;

            }else{
                $total_balance += number_format(($leave->getLeaveType->balance * ($leave->getLeaveType->carry_forward_percentage / 100)), 2, '.', '');
                $present_balance  += number_format(($leave->getLeaveType->balance * ($leave->getLeaveType->carry_forward_percentage / 100)), 2, '.', '');
            }

        }
        return ['available_balance' => number_format($present_balance, 2, '.', ''), 'estimated_balance' => number_format($total_balance, 2, '.', '')];

    }
}

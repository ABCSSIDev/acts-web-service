<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\Technicians;
use App\Employee;
use function App\Log\log_created;
use App\Http\Resources\Team as TeamResource;

class TeamController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $Team = Team::all();
        return TeamResource::collection($Team);
    }
    public function create(){
        $response = array();
        $Employee = Employee::role(Team::class)->get();
        $Team = Team::all();
        $response['Employee'] = $Employee;
        $response['Team'] = $Team;

        return response()->json($response);
    }
    public function store(Request $request)
    {
        try {
            $Team = New Team;
            $Team->team_id = $request->input('team_id') == null ? '' : $request->input('team_id');
            $Team->team_name = $request->input('team_name');
            $Team->team_type = $request->input('team_type');
            $Team->active = 1;
            $Team->vehicle_plateno = $request->input('vehicle_plateno');
            $Team->save();
            if (!empty($request->input('tech1'))) {
                $Technicians = New Technicians;
                $Technicians->teams_id = $Team->id;
                $Technicians->employee_id = $request->input('tech1');
                $Technicians->technician = 1;
                $Technicians->save();
            }
            if (!empty($request->input('tech2'))) {
                $Technicians = New Technicians;
                $Technicians->teams_id = $Team->id;
                $Technicians->employee_id = $request->input('tech2');
                $Technicians->technician = 2;
                $Technicians->save();
            }
            log_created($Team); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new TeamResource($Team);
        } catch (\Exception $exception) {
            return TeamResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function show($id)
    {
        $Team = Team::findOrFail($id);
        return new TeamResource($Team);
    }
    public function destroy($id)
    {
        try {
            $Team = Team::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return TeamResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return TeamResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

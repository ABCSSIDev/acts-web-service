<?php

namespace App\Http\Controllers;

use App\Approval;
use App\Employee;
use App\Http\Resources\Approval as ApprovalResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function App\Log\log_approval;

class ApprovalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $approval = new Approval();
            $approval->reference_id = $request->input('reference_id');
            $approval->reference_type = $request->input('reference_type');
            $approval->user_id = Auth::user()->id;
            $approval->approved_status = $request->input('approved_status');
            $approval->remarks = !empty($request->input('remarks')) ? $request->input('remarks') : null;
            $approval->save();

            log_approval(
                $request->input('reference_id'),
                $request->input('reference_type'),
                Auth::user()->id,
                $request->input('approved_status')
            );
            
            return new ApprovalResource($approval);
        } catch (\Exception $exception) {
            return ApprovalResource::errorResponse($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approveAllEmployees()
    {
        $employee_approvals = Approval::where('reference_type', Employee::class)->select('reference_id')->get()->toArray();
        $reference_ids = [];
        foreach ($employee_approvals as $value) {
            array_push($reference_ids, $value['reference_id']);
        }
        $employee_not_approved = Employee::whereNotIn('id', $reference_ids)->get();
        foreach ($employee_not_approved as $key => $value) {
            $approval = new Approval();
            $approval->reference_id = $value->id;
            $approval->reference_type = Employee::class;
            $approval->user_id = 72;
            $approval->approved_status = 1;
            $approval->remarks = 'approved';
            $approval->save();
            log_approval(
                $value->id,
                Employee::class,
                72,
                1
            );
        }

        return response()->json([
            'code' => 200,
            'message' => "Approved All Employees"
        ]);
    }
}

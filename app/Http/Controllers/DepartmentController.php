<?php

namespace App\Http\Controllers;

use App\Department;
use App\JobDetail;
use Illuminate\Http\Request;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Resources\Log as LogResource;
use App\Employee;
use App\Http\Resources\Employee as ResourcesEmployee;
use function App\Log\log_created;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index()
    {
        $department = Department::all();
        return DepartmentResource::collection($department);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
            $employee = Employee::role(Department::class)->get();
            return ResourcesEmployee::collection($employee);
        }catch(\Exception $exception){
            return DepartmentResource::MsgResponse(true,$exception->getMessage());
        }
    }

    public function store(Request $request)
    {
        try{
            if(Department::where('name', $request->input('name'))->first()){
                return DepartmentResource::MsgResponse(true,'This Department Name Already been use');
            }else{
                $department = New Department;
                $department->name = $request->input('name');
                $department->description = $request->input('description');
                $department->employee_id = $request->input('employee_id');
                $department->status = 1;
                $department->save();
                log_created($department); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
                return new DepartmentResource($department);
            }
            
            
        }catch(\Exception $exception){
            return DepartmentResource::MsgResponse(true,$exception->getMessage());
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $department = Department::findOrFail($id);            
            return (new DepartmentResource($department))->additional([
                'approver' => check_approver($department), //option if user is approver and already approved
                'approved_status' =>  model_approval_status(check_approver($department)),   //pending, waiting order approver,approved 
                'history_logs' => LogResource::collection(Department::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
            ]);
        }catch(\Exception $exception){
            return DepartmentResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = array();
        $Employee = Employee::role(Department::class)->get();
        $department = Department::findOrFail($id);  
        $response['employee'] = $Employee;          
        $response['data'] = new DepartmentResource($department);
        return response()->json($response);
    }
    public function update(Request $request, $id)
    {
        try{
            if(Department::where('name', $request->input('name'))->where('id','!=', $id)->first()){
                return DepartmentResource::MsgResponse(true,'This Department Name Already been use');
            }else{
                $Department = Department::where('id', $id)->update([ 
                    'name' => $request->input('name'),
                    'description' => $request->input('description'),
                    'employee_id' => $request->input('employee_id'),
                    'status' => $request->input('status'),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
                log_created($Department); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
                // return DepartmentResource::MsgResponse(false,'Created');
                $query = Department::where('id', $id)->first();
                return new DepartmentResource($query);
            }
           
            
        }catch(\Exception $exception){
            return DepartmentResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $DepartmentDelete = Department::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return DepartmentResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return DepartmentResource::MsgResponse(true,$exception->getMessage());
        }
    }
    
    public function organization(){
        $response = array();
        $response[] = array(
            'id' => 'top-management',
            'tags' => ["top-management"]
        );
        $id = 1;
        $response[] = array(
            'id' => $id,
            'stpid' => "top-management",
            'name' =>  "Aries Alibangbang",
            'title' =>  "CEO",
            'img' => "https://cdn.balkan.app/shared/1.jpg",
            'tags' => ["seo-menu"]
        );
        $departments = Department::all();
        foreach ($departments as $department){
            $response[] = array(
                'id' => strtolower(str_replace(" ","-",$department->name)),
                'pid' =>  "top-management",
                'tags' => [strtolower(str_replace(" ","-",$department->name)), "department"],
                'name' => $department->name
            );
            if ($department->employee_id !=null){
                $id++;
                $response[] = array(
                    'id' => $id,
                    'stpid' => strtolower(str_replace(" ","-",$department->name)),
                    'name' => $department->getEmployee->first_name." ".$department->getEmployee->last_name,
                    'title' => ($department->getEmployee->getJobDetails->getDesignation != null ? $department->getEmployee->getJobDetails->getDesignation->name : ''),
                    'img' => "https://cdn.balkan.app/shared/4.jpg"
                );
                $sid = $id;
                foreach ($department->getEmployee->getSubordinates as $subordinate){
                    $id++;
                    $response[] = array(
                        'id' =>  $id,
                        'pid' => $sid,
                        'name' => $subordinate->getEmployee->first_name.' '.$subordinate->getEmployee->last_name,
                        'title' => $subordinate->getDesignation->name,
                        'img' => "https://cdn.balkan.app/shared/5.jpg"
                    );
                }
            }
        }
        return response()->json($response);
    }

    public function getApprovals($model_id){
        return Department::approvals($model_id)->get();
    }
}

<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\EmployeeContact;
use App\Http\Resources\JobDetail;
use App\Http\Resources\Permission as PermissionResource;
use App\Http\Resources\Permission;
use App\Http\Resources\Role as RoleResource;
use App\Http\Resources\User as UserResource;
use App\Mail\SignUp;
use App\Permission as PermissionUser;
use App\Role;
use App\System;
use App\User;
use App\UserAccess;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

use function App\Log\log_created;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('firstResetPassword');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id', '!=', '1')->get();
        return UserResource::collection($users)->additional([
            'roles' => Role::all(),
            'permissions' => PermissionUser::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return EmployeeResource::collection(Employee::where('id', '!=', '1')->where('user_id', null)->get())
            ->additional([
                'roles' => RoleResource::collection(Role::all()),
                'permissions' =>  PermissionResource::collection(\App\Permission::all())
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $email = User::where('email', $request->input('email'))->first();

            if(!$email) {
                $user = new User();
                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->password = Hash::make($request->input('password'));
                $user->save();

                if ($request->input('employee_id') != null) {
                    $update = Employee::where('id', $request->input('employee_id'))
                    ->update([
                        'user_id' => $user->id
                    ]);
                }

                // if (\Notification::send(Auth::user(), new UserNotification($user))) {
                //     return back();
                // }
                $systems = [];
                if( !empty($request->input('systems'))) {
                    foreach ($request->input('systems') as $system) {
                        array_push($systems, $system['id']);
                    }
                }

                $user_access = new UserAccess();
                $user_access->user_id = $user->id;
                $user_access->role_id = $request->input('role_id');
                $user_access->permission_id = $request->input('permission_id');
                $user_access->bypass_role_systems = !empty($systems) ? json_encode($systems) : null;
                $user_access->save();
                log_created($user_access);// Logs have to Parameter available(param1,param2) param1 = reference module,
                //param2 = reference relate module
                // if ($request->input('is_email') == 1) {
                //     Mail::to($user->email)->send(new SignUp($user));
                // }
                return new UserResource($user);
            }
            return UserResource::MsgResponse(true, 'Please try another email');
        } catch (Exception $exception) {
            return UserResource::MsgResponse(true, $exception->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return (new UserResource($user))
                ->additional([
                    'employee' => ($user->getEmployee == null ? null : new EmployeeResource($user->getEmployee)),
                    'contact_details' => ($user->getEmployee == null ?
                        null : ($user->getEmployee->getContactDetails == null ?
                        null : new EmployeeContact($user->getEmployee->getContactDetails))),
                    'job_details' => ($user->getEmployee == null ?
                        null : ($user->getEmployee->getJobDetails == null ?
                        null : new JobDetail($user->getEmployee->getJobDetails)))
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permissions = PermissionUser::all();
        $roles = Role::all();
        $user_access = UserAccess::where('user_id', $id)->first();
        $users = User::findOrFail($id);
        $user = new UserResource($users);
        $employees = Employee::where('user_id', $id)->first();
        $employee = empty($employees) ? "" : $employees;
        $bypass = !empty( $user_access->bypass_role_systems) ? json_decode( $user_access->bypass_role_systems) : null;
        $systems = "";
        if (!empty($bypass)) {
            $systems = System::whereIn('id', $bypass)->get();
        }
        $collections = [
            'users' => $user,
            'employees' => $employee,
            'user_access' => $user_access,
            'permissions' => $permissions,
            'roles' => $roles,
            'role_exemptions' => $systems
        ];

        return $collections;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            if (empty($request->password)) {
                $user = User::where('id', $id)->update([
                    'name' => $request->input('name'),
                    'email' => $request->input('email_address'),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
            } else {
                $user = User::where('id', $id)->update([
                    'name' => $request->input('name'),
                    'email' => $request->input('email_address'),
                    'password' => Hash::make($request->input('password')),
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
            }


            $user_access = UserAccess::where('user_id', $id);

            $systems = [];
            if (!empty($request->input('systems'))) {
                foreach ($request->input('systems') as $system) {
                    array_push($systems, $system['id']);
                }
            }

            if ($user_access->count()) {
                $user_access->update([
                    'role_id' => $request->input('role_id'),
                    'permission_id' => $request->input('permission_id'),
                    'bypass_role_systems' => !empty($systems) ? json_encode($systems) : null,
                    'updated_at' => date('Y/m/d H:i:s')
                ]);
            } else {
                $user_access = new UserAccess();
                $user_access->user_id = $id;
                $user_access->role_id = $request->input('role_id');
                $user_access->permission_id = $request->input('permission_id');
                $user_access->bypass_role_systems = !empty($systems) ? json_encode($systems) : null;
                $user_access->save();
            }

            $query = User::where('id', $id)->first();
            log_created($query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new UserResource($query);

        }catch(\Exception $exception){
            return UserResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $UserDelete = User::where('id', $id)->update([
            'email' => null,
            'deleted_at' => date('Y/m/d H:i:s')
        ]);
        $type = false;
        $msg = 'Successfully Deleted!';
        return UserResource::MsgResponse($type, $msg);
    }

    public function massdelete(Request $request, $id)
    {
        try {
            $UserDelete = User::whereIn('id', explode(',', $id))->update([
                'email' => 'deleted@' . date('Y/m/d H:i:s') ,
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return UserResource::MsgResponse($type, $msg);
        } catch (\Exception $exception) {
            return UserResource::MsgResponse(true, $exception->getMessage());
        }
    }

    public function firstResetPassword($id)
    {
        $user = User::find($id);
        if (empty($user->email_verified_at)) {
            return (new UserResource($user))->additional(['verified' => false]);
        } else {
            return response()->json(['verified' => true]);
        }
    }

    /**
     * Verify
     *
     * @param Request $request
     *
     * @return void
     */
    public function verifyUser(Request $request)
    {
        $user = User::find($request->id);
        if (password_verify($request->password, $user->password)) {
            return response()->json(['verified' => true]);
        }

        return response()->json(['verified' => false]);
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        if($user && (Hash::check($request->old_password, $user->password) && $request->new_password == $request->confirm_password)){
            $user_query = User::findOrFail($user->id);
            $user_query->password = Hash::make($request->new_password);
            $user_query->save();

            if($user){
                return response()->json([
                    'success' => true,
                ]);
            }
        }
        if($request->new_password == $request->confirm_password){
            return response()->json([
                'success' => false,
                'description' => "Password Mismatch"
            ]);
        }

        if(!Hash::check($request->old_password, $user->password)){
            return response()->json([
                'success' => false,
                'description' => "Old Password Incorrect"
            ]);
        }

    }

    public function searchUser(Request $request)
    {
        $users = User::where('name', 'LIKE', '%' . $request->query('search') . '%')
                    ->orWhere('email', 'LIKE', '%' . $request->query('search') . '%')
                    ->role($request->query('role'))
                    ->permission($request->query('permission'))
                    ->orderBy('name', 'ASC')->get();
        return UserResource::collection($users);
    }
}

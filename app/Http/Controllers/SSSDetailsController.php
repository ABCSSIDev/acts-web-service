<?php

namespace App\Http\Controllers;

use App\SSSDetails;
use Illuminate\Http\Request;

class SSSDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SSSDetails  $sSSDetails
     * @return \Illuminate\Http\Response
     */
    public function show(SSSDetails $sSSDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SSSDetails  $sSSDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SSSDetails $sSSDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SSSDetails  $sSSDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(SSSDetails $sSSDetails)
    {
        //
    }
}

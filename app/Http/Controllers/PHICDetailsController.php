<?php

namespace App\Http\Controllers;

use App\PHICDetails;
use Illuminate\Http\Request;

class PHICDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PHICDetails  $pHICDetails
     * @return \Illuminate\Http\Response
     */
    public function show(PHICDetails $pHICDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PHICDetails  $pHICDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PHICDetails $pHICDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PHICDetails  $pHICDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(PHICDetails $pHICDetails)
    {
        //
    }
}

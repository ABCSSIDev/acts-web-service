<?php

namespace App\Http\Controllers;

use App\Designation;
use App\Employee;
use App\Department;
use Illuminate\Http\Request;
use App\Http\Resources\Designation as DesignationResource;
use App\Http\Resources\Log as LogResource;
use function App\Log\log_created;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $designation = Designation::all();
        return DesignationResource::collection($designation);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function create(){
        $response = array();
        $departments = Department::all();
        $response['departments'] = $departments;

        return response()->json($response);
    }
    public function store(Request $request)
    {
        //try{
            $request->validate([
                'name' => 'unique:hr_designations'
            ]);
            $Designation = New Designation;
            $Designation->department_id = $request->input('department_id');
            $Designation->name = $request->input('name');
            $Designation->description = $request->input('description');
            $Designation->status = 1;
            $Designation->save();
            log_created($Designation); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new DesignationResource($Designation);
        // }catch(\Exception $exception){
        //     return DesignationResource::MsgResponse(true,$exception->getMessage());
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $Designation = Designation::findOrFail($id);
            return (new DesignationResource($Designation))->additional([
                'approver' => check_approver($Designation), //option if user is approver and already approved
                'approved_status' =>  model_approval_status(check_approver($Designation)),   //pending, waiting order approver,approved 
                'history_logs' => LogResource::collection(Designation::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
            ]);
        }catch(\Exception $exception){
            return DesignationResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = array();
        $Department = Department::all();
        $Designation = Designation::findOrFail($id);  
        $response['department'] = $Department;          
        $response['data'] = new DesignationResource($Designation);
        return response()->json($response);
    }
    public function update(Request $request, $id)
    {
        try{
            // $request->validate([
            //     'name' => 'unique:hr_designations'
            // ]);
            $Designation = Designation::where('id', $id)->update([ 
                'department_id' => $request->input('department_id'),
                'description' => $request->input('description'),
                'name' => $request->input('name'),
                'status' => 1,
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            // log_created($Designation); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            
            if (!empty($Designation)) {
                $query = Designation::where('id', $id)->first();
                return new DesignationResource($query);
            }
        }catch(\Exception $exception){
            return DesignationResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        try {
            $DepartmentDelete = Designation::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return DesignationResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return DesignationResource::MsgResponse(true,$exception->getMessage());
        }
    }
    
}

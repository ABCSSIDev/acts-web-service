<?php

namespace App\Http\Controllers;

use App\Http\Resources\Notification as NotificationResource;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationController extends Controller
{
    /**
     * __construct
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Get Auth User Notifications
     *
     * @return JsonResource
     */
    public function getAllNotification(): JsonResource
    {
        return NotificationResource::collection(Auth::User()->notifications->take(20));
    }

    /**
     * Get unread Notification
     *
     * @return void
     */
    public function getUnreadNotificationCount()
    {
        return Auth::User()->unreadNotifications->count();
    }
    
    /**
     * Get read all Notifications
     *
     * @return void
     */
    public function readAllNotification()
    {
        return Auth::User()->unreadNotifications->markAsRead();
    }

    public function markAsViewNotification(string $id)
    {
        $notification = Auth::User()->notifications->where('id', $id)->first();
        return $notification->markAsView();
    }
}

<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Http\Resources\Onboarding as OnboardingResource;
use App\Http\Resources\Log as LogResource;
use App\Http\Resources\OnboardingRequirement as OnboardingRequirementResource;
use App\Onboarding;
use App\OnboardingRequirement;
use Illuminate\Http\Request;
use function App\Log\log_created;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class OnboardingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index()
    {
        $onboarding = Onboarding::all();
        return OnboardingResource::collection($onboarding);

    }

    public function create(){
        $candidates = Candidate::select('id', 'name')->get();
        return response()->json(['candidates'=>$candidates]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'candidate_id' => 'unique:hr_onboardings'
            ]);

            $onboard = new Onboarding;
            $onboard->candidate_id = $request->input('candidate_id');
            $onboard->salary_offered = $request->input('salary_offered');
            $onboard->status = $request->input('status') == null ? 0 : $request->input('status');
            $onboard->starting_date = $request->input('starting_date');
            $onboard->hired_date = $request->input('hired_date');
            $onboard->save();
            log_created($onboard); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new OnboardingResource($onboard);
        } catch (\Exception $exception) {
            return OnboardingResource::errorResponse($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Onboarding  $onboarding
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $onboarding = Onboarding::find($id);
        return (new OnboardingResource($onboarding))->additional([
            'approver' => check_approver($onboarding), //option if user is approver and already approved
            'approved_status' =>  model_approval_status(check_approver($onboarding)),   //pending, waiting order approver,approved 
            'history_logs' => LogResource::collection(Onboarding::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Onboarding  $onboarding
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $onboarding = Onboarding::findOrFail($id);
        $candidate = Candidate::all();
        $employee_onboarding = new OnboardingResource($onboarding);
        $collections = [
            'onboarding' => $employee_onboarding,
            'candidate' => $candidate,
        ];
        return $collections;
    }
    public function update(Request $request, $id)
    {
        try{
            $onboarding = Onboarding::where('id', $id)->update([
                'candidate_id' => $request->input('candidate_id'),
                'salary_offered' => $request->input('salary_offered'),
                'status' => $request->input('status') == null ? 0 : $request->input('status'),
                'starting_date' => $request->input('starting_date'),
                'hired_date' => $request->input('hired_date'),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            $query = Onboarding::where('id', $id)->first();
            log_created($query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new OnboardingResource($query);
            
        }catch(\Exception $exception){
            return OnboardingResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Onboarding  $onboarding
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $OnboardingDelete = Onboarding::where('id', $id)->update([
            'deleted_at' => date('Y/m/d H:i:s')
        ]);
        $type = false;
        $msg = 'Successfully Deleted!';
        return OnboardingResource::MsgResponse($type,$msg);
    }
    public function massdelete(Request $request,$id)
    {        
        try {
            $OnboardingDelete = Onboarding::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return OnboardingResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return OnboardingResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function convertstore(Request $request)
    {

    }
}

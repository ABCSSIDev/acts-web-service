<?php

namespace App\Http\Controllers;

use App\Interview;
use App\InterviewType;
use App\Employee;
use App\Candidate;
use App\InterviewAttachment;
use Illuminate\Http\Request;
use function App\Log\log_created;
use App\Http\Resources\Interview as InterviewResource;
use App\Http\Resources\Log as LogResource;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $interview = Interview::all();
        return InterviewResource::collection($interview);
    }
    public function create(){
        $response = array();
        $interviewer_id = Employee::all();
        $candidate_id = Candidate::all();
        $interview_type = InterviewType::all();
        $response['interviewer_id'] = $interviewer_id;
        $response['candidate_id'] = $candidate_id;
        $response['interview_type'] = $interview_type;

        return response()->json($response);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $Interview = New Interview;
            $Interview->interview_type_id = $request->input('interview_type_id');
            $Interview->interview_from = $request->input('interview_from');
            $Interview->interview_to = $request->input('interview_to');
            $Interview->candidate_id = $request->input('candidate_id');
            $Interview->interviewer_id = $request->input('interviewer_id');
            $Interview->location = $request->input('location');
            $Interview->comments = ($request->input('comments') == Null?'':$request->input('comments'));
            $Interview->save();            
            $type = false;
            $msg = 'Successfully New Interview Created!';
            log_created($Interview); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new InterviewResource($Interview);
        }catch(\Exception $exception){
            return InterviewResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $Interview = Interview::findOrFail($id);
            return (new InterviewResource($Interview))->additional([
                'approver' => check_approver($Interview), //option if user is approver and already approved
                'history_logs' => LogResource::collection(Interview::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get()),
                'approved_status' =>  model_approval_status(check_approver($Interview))   //pending, waiting order approver,approved 
            ]);
        }catch(\Exception $exception){
            return InterviewResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $interview = Interview::findOrFail($id);
        $interview_type = InterviewType::all();
        $interviewer = Employee::all();
        $candidate = Candidate::all();
        $employee_interview = new InterviewResource($interview);
        $collections = [
            'interview' => $employee_interview,
            'interview_type' => $interview_type,
            'interviewer' => $interviewer,
            'candidate' => $candidate,
        ];
        return $collections;
    }
    public function update(Request $request, $id)
    {
        try{
            $interview = Interview::where('id', $id)->update([
                'interview_type_id' => $request->input('interview_type_id'),
                'interview_from' => $request->input('interview_from'),
                'interview_to' => $request->input('interview_to'),
                'candidate_id' => $request->input('candidate_id'),
                'interviewer_id' => $request->input('interviewer_id'),
                'location' => $request->input('location'),
                'comments' => ($request->input('comments') == '' ? '' : $request->input('comments')),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            $query = Interview::where('id', $id)->first();
            log_created($query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return new InterviewResource($query);
            
        }catch(\Exception $exception){
            return InterviewResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $InterviewDelete = Interview::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return InterviewResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return InterviewResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

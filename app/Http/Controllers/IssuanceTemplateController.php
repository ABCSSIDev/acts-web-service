<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IssuanceTemplatesItems;
use App\Inventories;
use App\ReturnUsedMaterials;
use App\IssuanceTemplates;
use function App\Log\log_created;
use App\Http\Resources\IssuanceTemplates as IssuanceTemplatesResource;
use App\Http\Resources\ReturnUsedMaterials as ReturnUsedMaterialsResource;
use App\Unit;

class IssuanceTemplateController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $IssuanceTemplates = IssuanceTemplates::all();
        return IssuanceTemplatesResource::collection($IssuanceTemplates);
    }
    public function create(){
        $response = array();
        $Inventories = Inventories::where('type',0)->get();
        $response['Inventories'] = $Inventories;
        return response()->json($response);
    }
    public function store(Request $request)
    {
        try {
            $IssuanceTemplates = New IssuanceTemplates;
            $IssuanceTemplates->template_name = $request->input('template_name');
            $IssuanceTemplates->default = $request->input('default');
            $IssuanceTemplates->save();
            $data =  $request->input('item');
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    // dd($IssuanceTemplates->id);
                    $IssuanceTemplatesItems = new IssuanceTemplatesItems();
                    $IssuanceTemplatesItems->template_id = $IssuanceTemplates->id;
                    $IssuanceTemplatesItems->inventory_id = $value['inventory_id'];
                    $IssuanceTemplatesItems->quantity = $value['quantity'];
                    $IssuanceTemplatesItems->save();
                }
            }
            // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            log_created($IssuanceTemplates);
            return new IssuanceTemplatesResource($IssuanceTemplates);
        }
        catch (\Exception $exception) {
            return IssuanceTemplatesResource::MsgResponse(true, $exception->getMessage());
        }
    }
    public function show($id)
    {
        $IssuanceTemplates = IssuanceTemplates::findOrFail($id);
        return new IssuanceTemplatesResource($IssuanceTemplates);
    }
    public function destroy($id)
    {
        try {
            $IssuanceTemplatesDelete = IssuanceTemplates::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return IssuanceTemplatesResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return IssuanceTemplatesResource::MsgResponse(true,$exception->getMessage());
        }
    }
    public function returnusedmaterials(Request $request)
    {
        // return $request->input('Used');
        try{
            $data = $request->input('Used');
            $data = $request->input('Returned');
            if(!empty($data)){
                foreach ($data as $key => $value){
                    $validation = ReturnUsedMaterials::where('issuance_item_id',$value['id'])->where('status',$value['status'])->first();
                    if(!$validation){
                        $ReturnUsedMaterials = New ReturnUsedMaterials;
                        $ReturnUsedMaterials->issuance_item_id = $value['id'];
                        $ReturnUsedMaterials->status = $value['status'];
                        $ReturnUsedMaterials->quantity = $value['quantity'];
                        $ReturnUsedMaterials->remarks = $value['remarks'];
                        $ReturnUsedMaterials->save();
                    }else{
                        $ReturnUsedMaterials = ReturnUsedMaterials::where('issuance_item_id', $value['id'])->where('status', $value['status'])->update([ 
                            'issuance_item_id' => $value['id'],
                            'status' => $value['status'],
                            'quantity' => $value['quantity'],
                            'remarks' => $value['remarks'],
                            'updated_at' => date('Y/m/d H:i:s')
                        ]);
                    }
                }
            }
            // log_created($ReturnUsedMaterials); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            return ReturnUsedMaterialsResource::MsgResponse(false,'Successfully Created!');
        }catch(\Exception $exception){
            return ReturnUsedMaterialsResource::MsgResponse(true,$exception->getMessage());
        }
    }
}

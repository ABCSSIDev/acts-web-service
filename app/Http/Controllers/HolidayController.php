<?php

namespace App\Http\Controllers;

use App\Holiday;
use Illuminate\Http\Request;
use App\Http\Resources\Holiday as HolidayResource;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Holiday = Holiday::all();
        return HolidayResource::collection($Holiday);
    }

    public function create(){

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         try{
            $Holiday = New Holiday;
            $Holiday->name = $request->input('name');
            $Holiday->interview_from = $request->input('interview_from');
            $Holiday->holiday_date = $request->input('holiday_date');
            $Holiday->holiday_type = $request->input('holiday_type');
            $Holiday->is_recurring = $request->input('is_recurring');
            $Holiday->description = $request->input('description');
            $Holiday->save();
            
            $type = false;
            $msg = 'Successfully New Holiday Created!';
           
            return HolidayResource::MsgResponse($type,$msg);
        }catch(\Exception $exception){
            return HolidayResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $Holiday = Holiday::findOrFail($id);
            return new HolidayResource($Holiday);
        }catch(\Exception $exception){
            return HolidayResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Holiday $holiday)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Holiday $holiday)
    {
        //
    }
}

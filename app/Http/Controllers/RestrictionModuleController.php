<?php

namespace App\Http\Controllers;

use App\RestrictionModule;
use Illuminate\Http\Request;
use App\Http\Resources\RestrictionModule as RestrictionModuleResource;
use App\System;

class RestrictionModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $system_module = $this->systemModuleRestriction();
        return $system_module;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RestrictionModule  $restrictionModule
     * @return \Illuminate\Http\Response
     */
    public function show(RestrictionModule $restrictionModule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RestrictionModule  $restrictionModule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RestrictionModule $restrictionModule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RestrictionModule  $restrictionModule
     * @return \Illuminate\Http\Response
     */
    public function destroy(RestrictionModule $restrictionModule)
    {
        //
    }
    public function systemModuleRestriction()
    {
        $system = System::where('is_enabled', '1')->get();
        $system_module = array();
        foreach ($system as $key => $application_name) {
            $system_module[] =
            [
                'title' => $application_name->name,
                'data' => RestrictionModuleResource::collection($application_name->getSubModules)
            ];
        }
        return $system_module;
    }
}

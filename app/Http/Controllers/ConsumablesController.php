<?php

namespace App\Http\Controllers;

use App\Http\Resources\Inventories as ResourcesInventories;
use App\Inventories;
use App\InventoriesNames;
use Illuminate\Http\Request;

class ConsumablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ResourcesInventories::collection(Inventories::where('type', 3)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $InventoriesNames = new InventoriesNames();
            $InventoriesNames->category_id = $request->input('category_id');
            $InventoriesNames->description = $request->input('description');
            $InventoriesNames->save();

            $Inventories = new Inventories();
            $Inventories->inventory_description_id =  $InventoriesNames->id;
            $Inventories->unit_id = $request->input('unit_id');
            $Inventories->category_id = $request->input('category_id');
            $Inventories->quantity = $request->input('quantity');
            $Inventories->min_stock = $request->input('min_stock');
            $Inventories->type = 3;
            $Inventories->save();

            return new ResourcesInventories($Inventories);

        } catch (\Exception $exception) {
            return ResourcesInventories::MsgResponse(true, $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

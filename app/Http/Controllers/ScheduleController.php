<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\Log as LogResource;
use App\Http\Resources\Schedule as ScheduleResource;
use App\Http\Resources\ScheduleBreak as ScheduleBreakResource;
use App\Schedule;
use App\ScheduleBreak;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use function App\Log\log_created;

class ScheduleController extends Controller
{
    /**
     * list
     *
     * @return ResourceCollection
     */
    public function index(): ResourceCollection
    {
        $Schedule = Schedule::all();
        return ScheduleResource::collection($Schedule);
    }

    /**
     * store data
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function store(Request $request): JsonResource
    {
        $schedule = Schedule::create($this->payload($request));

        log_created($schedule);

        return new ScheduleResource($schedule);
    }

    /**
     * payload for update and create
     *
     * @param Request $request
     *
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'name' => 'required',
                'schedule_date_start' => 'required',
                'schedule_date_end' => 'required'
            ]
        );
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     try {
    //         $id = array();
    //         $employeeid = array();
    //         foreach ($request->input('departments') as $key => $value) {
    //             $id[] = (string)$value['id'];
    //         }

    //         if ($request->input('exceptions') != []) {
    //             foreach ($request->input('exceptions') as $key => $value) {
    //                 $employeeid[] = (string)$value['id'];
    //             }
    //         }

    //         $schedule = new Schedule();
    //         $schedule->name = $request->input('name');
    //         $schedule->schedule_date_start = $request->input('schedule_start');
    //         $schedule->schedule_date_from = $request->input('schedule_end') ;
    //         $schedule->departments = json_encode($id);
    //         $schedule->exceptions = json_encode($employeeid);
    //         $schedule->working_days = json_encode($request->input('working_days'));
    //         $schedule->start_full_day = $request->input('start_full_day');
    //         $schedule->end_full_day = $request->input('end_full_day');
    //         $schedule->start_half_day = $request->input('start_half_day');
    //         $schedule->end_half_day = $request->input('end_half_day');
    //         $schedule->save();

    //         log_created($schedule);

    //         if ($request->has('break_time')) {
    //             foreach ($request->input('break_time') as $key => $value) {
    //                 $break = new ScheduleBreak();
    //                 $break->schedule_id = $schedule->id;
    //                 $break->name = $value['break_name'];
    //                 $break->duration =  $value['duration'];
    //                 $break->break_mode =  $value['break_mode'];
    //                 $break->is_billable = ($value['billable'] == 'Billable' ? 1 : 0);
    //                 $break->save();
    //             }
    //         }
    //         return new ScheduleResource($schedule);
    //     } catch (\Exception $exception) {
    //         dd($exception->getMessage());
    //     }
    // }

    public function create()
    {
        return [
            'departments' => DepartmentResource::collection(Department::all()),
            'employees'   => EmployeeResource::collection(Employee::all())
        ];
    }
    
    /**
     * show data
     *
     * @param $scheduleId
     *
     * @return JsonResource
     */
    public function show($scheduleId): JsonResource
    {
        $schedule = Schedule::findOrFail($scheduleId);

        return (new ScheduleResource($schedule))->additional([
            // 'approver' => check_approver($schedule),
            // 'approved_status' =>  model_approval_status(check_approver($schedule)),
            'history_logs' => LogResource::collection(
                Schedule::findOrFail($scheduleId)->getLogs()->orderBy('created_at', 'DESC')->get()
            )
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Department = Department::all();
        $Employee = Employee::all();
        $sched = Schedule::findOrFail($id);
        $schedule = new ScheduleResource($sched);
        $collections = [
            'schedule' => $schedule,
            'departments' => $Department,
            'employees' => $Employee,
            'last_id' => count(ScheduleBreak::where('schedule_id', $sched->id)->get())
        ];
        return $collections;
    }
    public function update(Request $request, $id)
    {
        try{
            $ids = array();
            $employeeid = array();
            if(request('departments') != []){
                foreach (\request('departments') as $key => $value){
                    $Department = Department::where('name','LIKE', '%'. $value. '%')->first();
                    $ids[] = (string)$Department->id;
                }
            }
            if(request('exceptions') != []){
                foreach (\request('exceptions') as $key => $value){
                    $Department = Employee::where(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"),'LIKE', '%'. $value. '%')->first();
                    $employeeid[] = (string)$Department->id;
                }
            }
            $schedule = Schedule::where('id', $id)->update([
                'name' => $request->input('name'),
                'schedule_date_start' => date('Y-m-d', strtotime($request->input('schedule_start'))),
                'schedule_date_from' => $request->input('schedule_end') == null ? NULL : date('Y-m-d', strtotime($request->input('schedule_end'))),
                'departments' => json_encode($ids),
                'exceptions' => json_encode($employeeid),
                'working_days' => json_encode(\request('working_days')),
                'start_full_day' => $request->input('start_full_day'),
                'end_full_day' => $request->input('end_full_day'),
                'start_half_day' => $request->input('start_half_day'),
                'end_half_day' => $request->input('end_half_day'),
                'updated_at' => date('Y/m/d H:i:s')
            ]);
            $array = array();
            if ($request->input('break_time') != []){
                foreach ($request->input('break_time') as $key => $value){
                    if($value['id'] == ''){
                        $schedulebreak = ScheduleBreak::create([
                            'schedule_id'   => $id,
                            'name'          => $value['name'],
                            'duration'      => $value['edit_duration'],
                            'break_mode'    => $value['break_mode'],
                            'is_billable'   => ($value['is_billable'] == 1 ? 1 : 0)
                        ]);
                        $array[] = $schedulebreak->id;
                    }else{
                        $array[] = $value['id'];
                        $schedule_break = ScheduleBreak::where('id', $value['id'])->update([
                            'schedule_id' => $id,
                            'name' => $value['name'],
                            'duration' =>  $value['edit_duration'],
                            'break_mode' =>  $value['break_mode'],
                            'is_billable' => $value['is_billable'],
                            'updated_at' => date('Y/m/d H:i:s')
                        ]);
                    }
                }
                if(count($array) > 0){
                    $check_schedule = ScheduleBreak::where('schedule_id', $id)->whereNotIn('id',$array)->update([
                        'deleted_at' => date('Y/m/d H:i:s')
                    ]);
                }
                $query = Schedule::where('id', $id)->first();
                log_created($query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
                return new ScheduleResource($query);
            }
            else {
                $check_schedule = ScheduleBreak::where('schedule_id', $id)->get();
                foreach ($check_schedule as $key => $data){
                    if (!in_array($data->id, $array)){
                        $schedule_breakdlt = ScheduleBreak::where('id',$data->id)->update([
                            'deleted_at' => date('Y/m/d H:i:s')
                        ]);
                    }
                }
                $query = Schedule::where('id', $id)->first();
                log_created($query); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
                return new ScheduleResource($query);
            }



        } catch (\Exception $exception){
            dd($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Schedule = Schedule::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return ScheduleResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return ScheduleResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Get shift by Employee
     *
     * @param [int] $id
     *
     * @return void
     */
    public function getShiftSchedulebyEmployee($id)
    {
        $schedule = Schedule::getSchedulebyDepartment($id);
        return new ScheduleResource($schedule);
    }
}

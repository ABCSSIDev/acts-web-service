<?php

namespace App\Http\Controllers;

use App\Employee;
use App\FileManager;
use App\Http\Resources\Employee as ResourcesEmployee;
use App\Http\Resources\FileManager as FileManagerResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FileManagerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function uploadFile(Request $request){
        //variables
        try{
            
            if(!empty($request->file('file'))){
                $file = $request->file('file');
                $path = $request->input('path');
                $file_name = $file->getClientOriginalName();
                $employee_id = $request->input('employee_id');
                $fileUrl = $path . '\\' . now()->timestamp . "_". $path . "_" . $file->getClientOriginalName().'.'.$file->getClientOriginalExtension();
                
                //insert to database
                $attachment = new FileManager;
                $attachment->file_name = $file_name;
                $attachment->url = $fileUrl;
                $attachment->attached_by = $employee_id;
                $attachment->reference_id = $request->input('id');
                $attachment->reference_type = 'App\\' . $request->input('path');
                
                //store to storage path
                $file_for_upload = File::get($file);
                // $realPath = $file->getRealPath();
                $make_directory = true;
                if( !file_exists( storage_path('app/public/'.$path) ) ){
                    $make_directory = mkdir(storage_path('app/public/'.$path), 0777, true);
                }
                if($make_directory){
                    $upload = Storage::disk('public')->put( $fileUrl, $file_for_upload);
                }
    
                if($upload){
                    $attachment->save();
                    return new FileManagerResource($attachment);
                }else{
                    return FileManagerResource::errorResponse("Upload Failed");
                }
            }else{
                return FileManagerResource::errorResponse('No File Received for Uploading');
            }
           
           
        }catch(\Exception $exception){
            return FileManagerResource::errorResponse($exception->getMessage());
        }
        
    }
    
    public function deleteFile($id){
        try{
            $fileManager = FileManager::find($id);
            $deleteFile = unlink(storage_path('app/public/'.$fileManager->url));
            // $deleteFile = Storage::disk('public')->delete($fileManager->url);
            if($deleteFile){
                $delete = FileManager::destroy($id);
                return response()->json(['message' => 'Successfully Delete File']);
            }
        }catch(\Exception $exception){
            return FileManagerResource::errorResponse($exception->getMessage());
        }
        
    }

    public function downloadFile($id){
        try{
            $fileManager = FileManager::find($id);
            // $deleteFile = Storage::disk('public')->delete($fileManager->url);
            if($fileManager){
                // $delete = FileManager::destroy($id);
                // return Storage::download($fileManager->url, $fileManager->file_name);
                return response()->download(storage_path('app/public/' . $fileManager->url), $fileManager->file_name, [], 'inline');
            }
        }catch(\Exception $exception){
            return FileManagerResource::errorResponse($exception->getMessage());
        }
    }

    public function getAttachments($id){
       
        try{
            $employee = Employee::find($id);
            return FileManagerResource::collection($employee->getAttachments);
        }catch(\Exception $exception){
            return FileManagerResource::errorResponse($exception->getMessage());
        }

    }
}

<?php

namespace App\Http\Controllers;

use App\Reimbursement;
use App\Employee;
use App\ReimbursementItem;
use Illuminate\Http\Request;
use App\Http\Resources\Reimbursement as ReimbursementResource;
use App\Http\Resources\Log as LogResource;
use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use function App\Log\log_created;
class ReimbursementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
       $this->middleware('auth:api');
    }
    public function index()
    {
        $reimbursement = Reimbursement::role(Reimbursement::class)->get();
        return ReimbursementResource::collection($reimbursement);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function create(){
        $response = array();
        $employee = Employee::role(Reimbursement::class)->get();
        $response['employee_list'] = $employee;

        return response()->json($response);
    }
    public function store(Request $request)
    {
        try{
            if(!empty(request('item'))){
            $Reimbursement = New Reimbursement;
            $Reimbursement->subject = $request->input('subject');
            $Reimbursement->employee_id = $request->input('employee_id');
            $Reimbursement->filed_date = $request->input('filed_date');
            $Reimbursement->description = $request->input('description');
            $Reimbursement->approved_status = $request->input('approved_status');
            $Reimbursement->approved_by = $request->input('approved_by');
            $Reimbursement->approved_date = $request->input('approved_date');
            $Reimbursement->save();
            log_created($Reimbursement); // Logs have to Parameter available(param1,param2) param1 = reference module, param2 = reference relate module
            
                foreach (request('item') as $key => $value){
                    $ReimbursementItem = New ReimbursementItem;
                    $ReimbursementItem->reimbursement_id = $Reimbursement->id;
                    $ReimbursementItem->date = $value['date'];
                    $ReimbursementItem->type = $value['type'];
                    $ReimbursementItem->particulars = $value['particulars'];
                    $ReimbursementItem->receipt_number = $value['receipt_number'];
                    $ReimbursementItem->amount = $value['amount'];
                    $ReimbursementItem->save();
                }
            
            $type = false;
            return new ReimbursementResource($Reimbursement);
            }else{
                $msg = 'Reimbursements Items required';
                return ReimbursementResource::MsgResponse(true,$msg);
            }
        }catch(\Exception $exception){
            return ReimbursementResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $reimbursement = Reimbursement::findOrFail($id);
            return (new ReimbursementResource($reimbursement))->additional([
                'approver' => check_approver($reimbursement), //option if user is approver and already approved
                'approved_status' =>  model_approval_status(check_approver($reimbursement)),   //pending, waiting order approver,approved
                'history_logs' => LogResource::collection(Reimbursement::findOrFail($id)->getLogs()->orderBy('created_at','DESC')->get())
            ]);
        }catch(\Exception $exception){
            return ReimbursementResource::MsgResponse(true,$exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = array();
        $Employee = Employee::role(Reimbursement::class)->get();
        $reimbursement = Reimbursement::findOrFail($id);
        $response['employee'] = $Employee;
        $response['data'] = new ReimbursementResource($reimbursement);
        return response()->json($response);
    }
    public function update(Request $request, Reimbursement $reimbursement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        try {
            $ReimbursementDelete = Reimbursement::whereIn('id', explode(',', $id))->update([
                'deleted_at' => date('Y/m/d H:i:s')
            ]);
            $type = false;
            $msg = 'Successfully Deleted!';
            return ReimbursementResource::MsgResponse($type,$msg);
        }
        catch(\Exception $exception){
            return ReimbursementResource::MsgResponse(true,$exception->getMessage());
        }
    }
    
    // public function ApproveReim(Request $request,$id)
    // {
    //     try{
    //         $approve = Reimbursement::where('id', $id)->update([
    //             'approved_status' => $request->input('approved_status'),
    //             'approved_by' => $request->input('approved_by'),
    //             'approved_date' => date('Y/m/d H:i:s')
    //         ]);
    //         $type = false;
    //         $msg = 'Successfully Approved Reimbursement!';
    //         return ReimbursementResource::MsgResponse($type,$msg);
    //     }catch(\Exception $exception){
    //         return ReimbursementResource::MsgResponse(true,$exception->getMessage());
    //     }
    // }
}

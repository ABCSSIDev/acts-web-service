<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\AttendanceBreak;
use App\Employee;
use App\Http\Resources\QRTimelog as QRTimelogResource;
use App\Http\Resources\Timelog as TimelogResource;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class TimelogController extends Controller
{
    /**
     * verify Employee ID
     *
     * @param [type] $employee_no
     *
     * @return collection
     */
    public function verifyEmployee($employee_no)
    {
        $employee_details = $this->employeeNoToEmployee($employee_no);
        if ($employee_details) {
            return new TimelogResource($employee_details);
        } else {
            return response()->json('Not Found on the records. Please Try Again', 422);
            // return TimelogResource::MsgResponse(true, "Employee ID '" . $employee_no .
            // "' not found on the records. Please Try again.");
        }
    }

    public function store(Request $request)
    {
        if ($request->attendance_id != null) {
            return $this->timeinBreakAttendance($request->attendance_id);
        } else {
            $employee = $this->employeeNoToEmployee($request->employee_id);
            $attendance = new Attendance();
            $attendance->employee_id = $employee->id;
            $attendance->project_site_id = '1';
            $attendance->start_time = date('Y-m-d H:i:s');
            $attendance->longitude = $request->longitude;
            $attendance->latitude = $request->latitude;
            $attendance->save();
            
            return new TimelogResource($employee);
        }
    }

    public function timeOut(Request $request)
    {
        $employee = $this->employeeNoToEmployee($request->employee_id);
        $attendance = Attendance::where('employee_id', $employee->id)
        ->where('id', $request->attendance_id)
        ->update([
            'end_time' => date('Y-m-d H:i:s')
        ]);

        return new TimelogResource($employee);
    }

    public function employeeNoToEmployee($employee_no)
    {
        return Employee::where('employee_no', $employee_no)->first();
    }

    public function addBreakAttendance(Request $request)
    {
        $employee = $this->employeeNoToEmployee($request->employee_id);

        $break = new AttendanceBreak();
        $break->attendance_id = $request->attendance_id;
        $break->break_id = null;
        $break->type = 0;
        $break->time = date('Y-m-d H:i:s');
        $break->save();

        return new TimelogResource($employee);
    }

    public function timeinBreakAttendance(Request $request)
    {
        $employee = $this->employeeNoToEmployee($request->employee_id);
        $attendance_user = Attendance::findOrFail($request->attendance_id);
        foreach ($attendance_user->getAttendanceBreak as $key => $value) {
            if ($value->type == 0) {
                //$break_type = AttendanceBreak::where('break_id', $value->break_id)
                            //->where('attendance_id', $value->attendance_id)->where('type', 1)->first();
                //if (!$break_type) {
                    $break = new AttendanceBreak();
                    $break->attendance_id = $request->attendance_id;
                    $break->break_id = null;
                    $break->type = 1;
                    $break->time = date('Y-m-d H:i:s');
                    $break->save();
                //}
            }
        }
        return new TimelogResource($employee);
    }
    

    /**
     * time-in QR
     *
     * @param Request $request
     * @param string $employeeNo
     *
     * @return void
     */
    public function qrTimeIn(Request $request, string $employeeNo)
    {
        $date_today = date('Y-m-d H:i:s');

        $employee = $this->employeeNoToEmployee($employeeNo);

        if (!$employee) {
            return response()->json('Not Found on the records. Please Try Again', 422);
        }

        if ($this->checkIfExistTimein(date('Y-m-d', strtotime($date_today)), $employee->id)) {
            return response()->json('You already have Time-in today. Time-out first before you Time-in again.', 422);
        }

        $request->merge([
            'start_time' => $date_today,
            'employee_id' => $employee->id,
            'project_site_id' => '1'
        ]);

        $attendance = Attendance::create($this->payload($request));

        return new QRTimelogResource($attendance);
    }

    /**
     * time-out QR
     *
     * @param Request $request
     * @param string $employeeNo
     *
     * @return void
     */
    public function qrTimeOut(Request $request, string $employeeNo)
    {
        $employee = $this->employeeNoToEmployee($employeeNo);

        if (!$employee) {
            return response()->json('Not Found on the records. Please Try Again', 422);
        }

        if ($this->checkExistAttendance($employee->id) == false) {
            return response()->json('Sorry you dont have any occuring Attendance. Please Time-in now.', 422);
        }

        $attendance = $this->getCurrentAttendance($employee->id);
        
        if ($attendance->end_time != null) {
            return response()->json('You already Time-out!', 422);
        }
        
        $attendance->update([
            'end_time' => date('Y-m-d H:i:s')
        ]);

        return new QRTimelogResource($attendance);
    }

    /**
     * get Current Attendance
     *
     * @param int $employee_id
     *
     * @return void
     */
    public function getCurrentAttendance(int $employee_id)
    {
        $attendance = Attendance::where('employee_id', $employee_id)
            ->orderBy('created_at', 'DESC')
            ->first();
        
        return $attendance;
    }

    /**
     * check Exist Attendance
     *
     * @param int $employee_id
     *
     * @return bool
     */
    public function checkExistAttendance(int $employee_id): bool
    {
        $attendance = $this->getCurrentAttendance($employee_id);
        
        if ($attendance) {
            return true;
        }
        
        return false;
    }

    /**
     * check if existTimeIn
     *
     * @param string $date
     * @param int $employee_id
     *
     * @return bool
     */
    public function checkIfExistTimein(
        string $date,
        int $employee_id
    ): bool {
        $attendance = Attendance::where('employee_id', $employee_id)
            ->whereDate('start_time', $date)
            ->where('end_time', null)->first();

        if ($attendance) {
            return true;
        }

        return false;
    }

    /**
     * Validation Rules for Store Attendance
     *
     * @param Request $request
     *
     * @return array
     */
    private function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'employee_id' => 'required',
                'project_site_id' => 'required',
                'start_time' => 'required',
                'longitude' => 'nullable',
                'latitude' => 'nullable'
            ]
        );
    }
}

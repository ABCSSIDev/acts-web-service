<?php

namespace App\Http\Controllers;

use App\Http\Resources\OvertimeType as OvertimeTypeResource;
use App\OvertimeType;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OvertimeTypeController extends Controller
{
    /**
     * Construct API
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return OvertimeTypeResource::collection(
            OvertimeType::all()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OvertimeType  $overtimeType
     * @return \Illuminate\Http\Response
     */
    public function show(OvertimeType $overtimeType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OvertimeType  $overtimeType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OvertimeType $overtimeType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OvertimeType  $overtimeType
     * @return \Illuminate\Http\Response
     */
    public function destroy(OvertimeType $overtimeType)
    {
        //
    }

    /**
     * get Ot rates per Type
     *
     * @param string $type
     *
     * @return ResourceCollection
     */
    public function otPerType(string $type): ResourceCollection
    {
        return OvertimeTypeResource::collection(
            OvertimeType::where('type', $type)
            ->get()
        );
    }
}

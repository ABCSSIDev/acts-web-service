<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IssuanceTemplates extends Model
{
    use SoftDeletes;

    protected $table = 'kamalig_issuance_templates';
    protected $primaryKey = 'id';
    protected $fillable = ['template_name', 'default'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['getIssuanceTemplatesItems'];

    public function getIssuanceTemplatesItems() {
        return $this->hasMany('App\IssuanceTemplatesItems','template_id', 'id');
    }

    public function getLogs() {
        return $this->morphMany(Log::class,"reference");
    }
}

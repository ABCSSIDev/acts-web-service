<?php

namespace App;

use App\Scopes\RoleScope;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "roles";
    protected $primaryKey = 'id';
    protected $fillable = ['role_id', 'role_name', 'role_description', 'is_shared'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getChildren(){
        return $this->hasMany('App\Role','role_id');
    }

    public function getUserAccess(){
        return $this->hasMany('App\UserAccess', 'role_id', 'id');
    }

    public static function getRoleIDByName($name){
        $role = self::where('role_name','like',$name)->first();
        if($role){
            return $role->id;
        }
        return null;
    }

    public function logs()
    {
        return $this->morphMany(Log::class, "reference");
    }
}

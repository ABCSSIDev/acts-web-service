<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class IncidentStatement extends Model
{
    protected $table = 'kamalig_incident_statements';
    protected $primaryKey = 'id';
    protected $fillable = ['incident_id', 'statement_type', 'statement', 'injury_statement'];
    protected $dates = ['created_at', 'updated_at'];
    // use SoftDeletes;
    
    public function getIncidentReports(){
        return $this->hasOne('App\IncidentReports','id', 'incident_id');
    }
    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }
}

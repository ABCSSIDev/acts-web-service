<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApprovalProcess extends Model
{
    use SoftDeletes;

    protected $table = "approval_processes";
    protected $primaryKey = 'id';
    protected $fillable = [
        'reference_type',
        'name',
        'description',
        'execute',
        'status',
        'approved_setup',
        'order',
        'department'
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Approver user relationship
     *
     * @return HasMany
     */
    public function getApprovalUser(): HasMany
    {
        return $this->hasMany(ApprovalUser::class, 'approval_process_id');
    }

    public function logs()
    {
        return $this->morphMany(Log::class, "reference");
    }
}

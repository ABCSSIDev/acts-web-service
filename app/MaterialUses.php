<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaterialUses extends Model
{
    protected $table = 'kamalig_material_uses';
    protected $primaryKey = 'id';
    protected $fillable = ['inventory_id', 'service_id', 'type', 'qty_used', 'brandandmodel', 'serial_no'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getInventories(){
        return $this->belongsTo(Inventories::class,'inventory_id');
    }
    public function getServiceOrders(){
        return $this->belongsTo(ServiceOrders::class,'service_id');
    }


}

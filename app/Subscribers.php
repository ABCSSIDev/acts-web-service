<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscribers extends Model
{
    protected $table = 'kamalig_subscribers';
    protected $primaryKey = 'id';
    protected $fillable = ['subscriber_name', 'email', 'home_no','other_no', 'mobile_no', 'gender', 'birthdate', 'civil_status', 'household_held', 'service_address', 'billing_address'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;
    
   


}

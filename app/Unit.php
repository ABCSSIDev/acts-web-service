<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'kamalig_units';
    protected $primaryKey = 'id';
    protected $fillable = ['unit_name', 'unit_code', 'enabled'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}

<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Technicians extends Model
{
    protected $table = 'kamalig_team_technicians';
    protected $primaryKey = 'id';
    protected $fillable = ['team_id', 'employee_id','technician'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getTechnicians(){
        return $this->belongsTo(Employee::class,'employee_id');
    }
}

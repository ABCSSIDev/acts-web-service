<?php

namespace App;

use App\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    use SoftDeletes;

    protected $table = 'hr_schedules';

    protected $guarded = [];

    /**
     * Get Logs relationship
     *
     * @return MorphMany
     */
    public function getLogs(): MorphMany
    {
        return $this->morphMany(Log::class, "reference");
    }

    /**
     * get schedule items relationship
     *
     * @return HasMany
     */
    public function scheduleItems(): HasMany
    {
        return $this->hasMany(ScheduleItem::class, 'schedule_id');
    }

    public static function getSchedulebyDepartment($user_id)
    {
        $shift = Employee::findORFail($user_id);
        $department_id = $shift->getJobDetails->getDesignation->department_id;
        return (new static)::whereJsonContains('departments', [$department_id])->first();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deduction extends Model
{
    use SoftDeletes;

    protected $table = 'hr_deductions';
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getEmployee()
    {
        return $this->hasOne('App\Employee', 'id', 'employee_id');
    }

    public function getLogs()
    {
        return $this->morphMany(Log::class, "reference");
    }

    public function getAsset()
    {
        return $this->hasOne(Assets::class, 'id', 'asset_id');
    }
}

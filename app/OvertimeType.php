<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OvertimeType extends Model
{
    protected $table = 'hr_overtime_types';
    use SoftDeletes;
}

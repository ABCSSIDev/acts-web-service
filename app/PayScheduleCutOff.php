<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayScheduleCutOff extends Model
{
    protected $table = "hr_pay_schedule_cut_offs";
    protected $primaryKey = 'id';

    public function getPaySchedule()
    {
        return $this->belongsTo(PaySchedule::class, 'pay_schedule_id');
    }
}

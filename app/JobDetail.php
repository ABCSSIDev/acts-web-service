<?php

namespace App;

use App\Scopes\RoleScope;
use Illuminate\Database\Eloquent\Model;

class JobDetail extends Model
{
    protected $table = 'hr_job_details';

    public function getDesignation(){
        return $this->belongsTo(Designation::class,'designation_id');
    }

    public function getEmployeeType(){
        return $this->belongsTo(EmployeeType::class,'employee_type_id');
    }

    public function getEmployee(){
        return $this->belongsTo(Employee::class,'employee_id', 'id')->withoutGlobalScope(RoleScope::class);
    }

}

<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Accountfilter
{
    /**
     * Filter
     *
     * @param Builder $query
     * @param array $filter
     * @return Builder
     */
    public function scopeFilter(Builder $query, array $filter): Builder
    {
        if ($filter['year'] ?? false) {
            $query = $query->vat($filter['year']);
        }

        return $query;
    }
}

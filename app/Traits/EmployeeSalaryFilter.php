<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait EmployeeSalaryFilter
{
    /**
     * Filter
     *
     * @param Builder $query
     * @param array $filter
     * @return Builder
     */
    public function scopeFilter(Builder $query, array $filter): Builder
    {
        if ($filter['search'] ?? false) {
            $query = $query->name($filter['search']);
        }

        if (isset($filter['rate_type'])) {
            $query = $query->rateType($filter['rate_type']);
        }

        return $query;
    }

    /**
     * scope filter by name
     *
     * @param Builder $query
     * @param string $search
     *
     * @return Builder
     */
    public function scopeName(
        Builder $query,
        string $search
    ): Builder {
        return $query->whereHas('employee', function ($q) use ($search) {
            $q->where('first_name', 'LIKE', '%' . $search . '%')
                ->orWhere('last_name', 'LIKE', '%' . $search . '%');
        });
    }

    /**
     * scope filter by Rate Type
     *
     * @param Builder $query
     * @param int $rate
     *
     * @return Builder
     */
    public function scopeRateType(
        Builder $query,
        int $rate
    ): Builder {
        return $query->where('type', $rate);
    }
}

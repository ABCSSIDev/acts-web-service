<?php

namespace App\Traits;

use App\ScheduleEmployeeItem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

trait ScheduleEmployeeFilter
{
    /**
     * Filter
     *
     * @param Builder $query
     * @param array $filter
     * @return Builder
     */
    public function scopeFilter(Builder $query, array $filter): Builder
    {
        if ($filter['search'] ?? false) {
            $query = $query->search($filter['search']);
        }

        if ($filter['date_from'] ?? false && $filter['date_to'] ?? false) {
            $query = $query->dateRange($filter['date_from'], $filter['date_to']);
        }

        if ($filter['verify'] ?? false) {
            $query = $query->verify($filter['verify']);
        }

        return $query;
    }

    /**
     * scope filter by date Range
     *
     * @param Builder $query
     * @param string $dateFrom
     * @param string $dateTo
     *
     * @return Builder
     */
    public function scopeDateRange(
        Builder $query,
        string $dateFrom,
        string $dateTo
    ): Builder {
        return $query->whereHas('scheduleEmployee', function ($q) use ($dateFrom, $dateTo) {
            $q->whereBetween(
                DB::raw('date(date)'),
                [date('Y-m-d', strtotime($dateFrom)),
                date('Y-m-d', strtotime($dateTo))]
            );
        });
    }

    /**
     * scope filter by employee name
     *
     * @param Builder $query
     * @param string $search
     *
     * @return Builder
     */
    public function scopeSearch(
        Builder $query,
        string $search
    ): Builder {
        return $query->whereHas('employee', function ($q) use ($search) {
            $q->where('first_name', 'LIKE', '%' . $search . '%')
                ->orWhere('last_name', 'LIKE', '%' . $search . '%');
        });
    }

    /**
     * scope filter for verify status
     *
     * @param Builder $query
     * @param string $verify
     *
     * @return Builder
     */
    public function scopeVerify(
        Builder $query,
        string $verify
    ): Builder {
        return $query->where('verified_attendance', ScheduleEmployeeItem::STATUS[$verify]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class BirTemplate extends Model
{
    use SoftDeletes;

    protected $table = 'bir_templates';

    protected $fillable = [
        'date_effective_from',
        'date_effective_to',
        'is_activate'
    ];

    /**
     * Bir Template Items relationship
     *
     * @return HasMany
     */
    public function birTemplateItems(): HasMany
    {
        return $this->hasMany(BirTemplateItem::class, 'bir_template_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SSSTemplates extends Model
{
    use SoftDeletes;

    protected $table = 'hr_sss_templates';
    protected $dates = ['created_at','deleted_at','updated_at'];
    protected $fillable = ['template_name','active','date_effective_from','date_effective_to'];

    public function SSSTemplateContribution()
    {
        return $this->hasOne(SSSTemplateContributions::class, 'sss_template_id', 'id');
    }
}

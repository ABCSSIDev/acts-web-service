<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OfficeSuppliesAndEquipment extends Model
{
    protected $table = 'kamalig_assets';
    protected $primaryKey = 'id';
    protected $fillable = ['inventory_id', 'inventory_status_id', 'asset_tag', 'assigned_to', 'serial_model_no'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getInventories(){
        return $this->belongsTo(Inventories::class,'inventory_id');
    }
    public function getInventoryStatus(){
        return $this->belongsTo(InventoryStatus::class,'inventory_status_id');
    }
    public function getEmployee(){
        return $this->belongsTo(Employee::class,'assigned_to');
    }
    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }

    public function issuanceItem()
    {
        return $this->hasMany(IssuanceItems::class, 'asset_id', 'id');
    }


}

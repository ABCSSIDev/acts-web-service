<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ScheduleEmployee extends Model
{
    
    protected $table = 'hr_schedule_employees';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * schedule relationship
     *
     * @return BelongsTo
     */
    public function schedule(): BelongsTo
    {
        return $this->belongsTo(Schedule::class, 'schedule_id');
    }

    /**
     * schedule employee item relationship
     *
     * @return HasMany
     */
    public function scheduleEmployeeItems(): HasMany
    {
        return $this->hasMany(ScheduleEmployeeItem::class, 'schedule_employee_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidate extends Model
{
    protected $table = 'hr_candidates';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'contact_number', 'email', 'address', 'source', 'recruiter_id', 'potential_designation', 'job_opening_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getRecruiter(){
        return $this->belongsTo('App\Employee', 'recruiter_id', 'id');
    }

    public function getJobOpening(){
        return $this->belongsTo('App\JobOpening', 'job_opening_id', 'id');
    }

    
    public function getDesignation(){
        return $this->hasOne('App\Designation','id', 'potential_designation');
    }
    
    public function getCandidateAttachment(){
        return $this->hasMany('App\CandidateAttachment','candidate_id', 'id');
    }

    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }
}

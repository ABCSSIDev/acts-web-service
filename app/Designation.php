<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Designation extends Model
{
    protected $table = 'hr_designations';
    protected $primaryKey = 'id';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['department_id', 'name', 'description', 'status'];
    use SoftDeletes;

    public function getDepartment(){
        return $this->belongsTo(Department::class,'department_id');
    }
    
    public static function getDesignationIDbyName($name){
        $designation = self::where('name','like',$name)->first();
        if($designation){
            return $designation->id;
        }
        return null;
    }

    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }
}

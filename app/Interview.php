<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interview extends Model
{
    protected $table = 'hr_interviews';
    protected $primaryKey = 'id';
    protected $fillable = ['interview_type_id', 'interview_from', 'interview_to', 'candidate_id', 'interviewer_id', 'location', 'comments'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;
    
    public function getInterviewType(){
        return $this->hasOne('App\InterviewType','id', 'interview_type_id');
    }
    public function getCandidate(){
        return $this->hasOne('App\Candidate','id', 'candidate_id');
    }
    public function getInterviewer(){
        return $this->hasOne('App\Employee','id', 'interviewer_id');
    }
    public function getInterviewAttachment(){
        return $this->hasMany('App\InterviewAttachment','interview_id', 'id');
    }

    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }
}


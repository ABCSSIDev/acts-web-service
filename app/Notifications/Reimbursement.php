<?php

namespace App\Notifications;

use App\Reimbursement as ReimbursementRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Reimbursement extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        //Angelo Zamora Created Reimbursement. Transportation going to ACTS
        return [
            'notif_type' => 'reimbursement_request',
            'id'         => $this->request->id,
            'user_name'  => $this->request->getEmployee->first_name . ' ' .
                            $this->request->getEmployee->last_name,
            'other_info' => ' ' . $this->request->subject,
            'description' => 'File Reimbursement',
            'route' => 'reimbursements.view',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = ReimbursementRecord::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

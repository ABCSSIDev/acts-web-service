<?php

namespace App\Notifications;

use App\EmployeeSalary as EmployeeSalaryRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmployeeSalary extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        //Angelo Zamora Added Salary Effective Date (Jan 14, 2021) Daily Rate.
        return [
            'notif_type' => 'employee_salary_request',
            'id'         => $this->request->id,
            'user_name'  => $this->request->employee->first_name . ' ' .
                            $this->request->employee->last_name,
            'other_info' => 'Effective Date (' . date('M d, Y', strtotime($this->request->effective_date)) . ') '
                . ($this->request->type == 0 ? 'Fixed Rate' : 'Daily Rate'),
            'description' => ' Added Salary ',
            'route' => 'employee_salaries.view',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = EmployeeSalaryRecord::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Candidate as CandidateRecord;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Candidate extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        //Angelo Zamora Created for Candidates. potential HR Supervisor via Employee Referral
        return [
            'notif_type' => 'candidate_request',
            'id'         => $this->request->id,
            'user_name'  => $this->request->name,
            'other_info' => $this->request->getDesignation->name . ' via ' .
                ($this->request->source == 1 ? 'Walk-in' :
                    ($this->request->source == 2 ? 'Employee Referral' : 'Via-Website')),
            'description' => 'Created for Candidates. Potential ',
            'route' => 'candidates.view',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = CandidateRecord::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

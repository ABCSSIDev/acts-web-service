<?php

namespace App\Notifications;

use App\Interview as InterviewRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Interview extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        //Angelo Zamora Interviewed by Alyssa Marie. position for Application Development
        return [
            'notif_type' => 'interview_request',
            'id'         => $this->request->id,
            'user_name'  => $this->request->getCandidate->name,
            'other_info' => $this->request->getInterviewer->first_name . ' '
                . $this->request->getInterviewer->last_name . '. position for '
                    . $this->request->getCandidate->getJobOpening->name . '.',
            'description' => ' Interviewed by ',
            'route' => 'interview.view',
            'created_at' => $this->getCreatedAt($this->request->id),
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = InterviewRecord::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

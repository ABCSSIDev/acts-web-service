<?php

namespace App\Notifications;

use App\Employee as EmployeeRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Employee extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notif_type' => 'employee_request',
            'id'         => $this->request->id,
            'user_name'  => $this->request->first_name . ' ' . $this->request->last_name,
            'other_info' => '(' . $this->request->getJobDetails->getDesignation->name . ')',
            'description' => 'created Employee Records. ',
            'route' => 'employees.view',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = EmployeeRecord::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

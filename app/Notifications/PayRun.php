<?php

namespace App\Notifications;

use App\PayRun as PayRunRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use function App\Datetime\dateFormatRange;

class PayRun extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notif_type' => 'payrun_request',
            'id'         => $this->request->id,
            'user_name'  => null,
            'other_info' => '(' . dateFormatRange($this->request->start_period, $this->request->end_period) . ')',
            'description' => 'Payroll Created!. Cut-off period from ',
            'route' => 'pay_runs.view',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = PayRunRecord::findOrFail($id)
            ->logs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

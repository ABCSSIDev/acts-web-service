<?php

namespace App\Notifications;

use App\Onboarding as OnboardingRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Onboarding extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        //Angelo Zamora Onboarded as Employee. Starting Date (Jan 14, 2021) Date Hired (Jan 14, 2021)
        return [
            'notif_type' => 'onboarding_request',
            'id'         => $this->request->id,
            'user_name'  => $this->request->getCandidate->name,
            'other_info' => 'Starting Date (' . date('M d, Y', strtotime($this->request->starting_date))
                . '), Date Hired (' . date('M d, Y', strtotime($this->request->hired_date)) . ')',
            'description' => ' Onboarded as Employee. ',
            'route' => 'onboarding.view',
            'created_at' => $this->getCreatedAt($this->request->id)
            
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = OnboardingRecord::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

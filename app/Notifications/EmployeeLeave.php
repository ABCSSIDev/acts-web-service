<?php

namespace App\Notifications;

use App\EmployeeLeave as Leave;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmployeeLeave extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notif_type' => 'employee_leave_request',
            'id'         => $this->request->id,
            'user_name'  => $this->request->getEmployee->first_name . ' ' .
                            $this->request->getEmployee->last_name,
            'other_info' => number_format($this->request->total_days) . ' Day/s ' .
                '(' . date('F d, Y', strtotime($this->request->start_date)) . ' - ' .
                date('F d, Y', strtotime($this->request->end_date)) . ')',
            'description' => 'File ' . $this->request->getLeaveType->name,
            'route' => 'leave_tracker.view',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = Leave::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

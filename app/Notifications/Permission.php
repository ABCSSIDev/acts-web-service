<?php

namespace App\Notifications;

use App\Permission as PermissionRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use function App\Datetime\dateFormatRange;

class Permission extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notif_type' => 'permission',
            'id'         => $this->request->id,
            'user_name'  => null,
            'other_info' => null,
            'description' => 'Permission Created!',
            'route' => 'permissions.view',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = PermissionRecord::findOrFail($id)
            ->logs()->orderBy('created_at', 'DESC')->first();

        return $log->user->name;
    }
}

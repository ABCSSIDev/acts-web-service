<?php

namespace App\Notifications;

use App\EmployeeOvertime as Overtime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmployeeOvertime extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notif_type' => 'overtime_request',
            'id'         => $this->request->id,
            'user_name'  => $this->request->getEmployee->first_name . ' ' .
                            $this->request->getEmployee->last_name,
            'other_info' => '(' . date('F d, Y', strtotime($this->request->overtime_date)) .
                ' ' . $this->request->rendered_hours . ' Hours' . ')',
            'description' => 'File Overtime',
            'overtime_date' => $this->request->overtime_date,
            'route' => 'overtime.view',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = Overtime::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

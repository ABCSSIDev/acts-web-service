<?php

namespace App\Notifications;

use App\OB as OBRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OB extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        //Angelo Zamora File OB Form (Jan 14, 2021)

        return [
            'notif_type' => 'ob_request',
            'id'         => $this->request->id,
            'user_name'  => $this->request->getEmployee->first_name . ' ' .
                            $this->request->getEmployee->last_name,
            'other_info' => ' (' . date('M d, Y', strtotime($this->request->date)) . ')',
            'description' => ' File OB Form ',
            'route' => 'official_business.view',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = OBRecord::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

<?php

namespace App\Notifications;

use App\Schedule as ScheduleRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use function App\Datetime\dateFormatRange;

class Schedule extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        //New Shift Scheduled Created. Field Engineer Schedule (Jan 13 - Jan 14, 2021).

        return [
            'notif_type' => 'shift_schedule_request',
            'id'         => $this->request->id,
            'user_name'  => null,
            'other_info' => $this->request->name
                . '(' . dateFormatRange($this->request->schedule_date_start, $this->request->schedule_date_from) . ')',
            'description' => 'New Shift Schedule Created!. ',
            'route' => 'shift_schedules.view',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = ScheduleRecord::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();
        
        return $log->user->name;
    }
}

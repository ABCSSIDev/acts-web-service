<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationalDetails extends Model
{
    protected $table = 'hr_employee_educational_details';

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterviewAttachment extends Model
{
    protected $table = 'hr_interview_attachments';
    protected $primaryKey = 'id';
    protected $fillable = ['interview_id', 'attached_by', 'filepath', 'description'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}

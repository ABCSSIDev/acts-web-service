<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function getParentSystem(){
        return $this->belongsTo(System::class,'system_id');
    }

    public function getParentModule(){
        return $this->belongsTo(Module::class,'module_id');
    }

    public function getSubmodules(){
        return $this->hasMany(Module::class,'module_id')->where('is_enabled', 1);
    }

    public function getRestrictionSubModule(){
        return $this->hasOne(RestrictionModule::class,'module_id');
    }

    public function getFilteredModules($modules){
        return $this->getSubmodules()->whereIN('id', $modules)->where('is_enabled', 1);
    }
    
}

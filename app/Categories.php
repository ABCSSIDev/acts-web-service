<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends Model
{
    use SoftDeletes;
    protected $table = 'kamalig_categories';
    protected $primaryKey = 'id';
    protected $fillable = ['category_id', 'category_name', 'meta_title', 'enabled'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['parent_category'];

    public function parent_category(){
        return $this->hasOne(Categories::class, 'id', 'category_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeSalaryAllowance extends Model
{
    use SoftDeletes;

    protected $table = 'hr_employee_salary_allowances';

    protected $fillable = [
        'salary_id',
        'allowance_name',
        'amount'
    ];

    /**
     * employee salary relationship
     *
     * @return Builder
     */
    public function employeeSalary(): Builder
    {
        return $this->belongsTo(EmployeeSalary::class, 'salary_id');
    }
}

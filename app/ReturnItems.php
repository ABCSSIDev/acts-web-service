<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnItems extends Model
{
    protected $table = "kamalig_return_items";
    protected $primaryKey = 'id';
    protected $fillable = ['return_id', 'inventory_id', 'asset_id', 'remarks', 'qty_return', 'qty_recieved'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['inventory', 'asset'];
    public function returnSlips(){
        return $this->belongsTo(ReturnSlip::class, 'return_id', 'id');
    }

    public function inventory(){
        return $this->belongsTo(Inventories::class, 'inventory_id', 'id');
    }

    public function asset(){
        return $this->belongsTo(Assets::class, 'asset_id', 'id');
    }
}


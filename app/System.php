<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    protected $table = "systems";
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'description', 'icon', 'route', 'is_enabled'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getModules(){
        return $this->hasMany(Module::class, 'system_id')->where('module_id', null)->where('is_enabled', 1);
    }
    public function getSubModules(){
        return $this->hasMany(Module::class, 'system_id')->where('module_id', '!=', null)->where('is_enabled', 1);
    }
}

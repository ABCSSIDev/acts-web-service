<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnboardingRequirement extends Model
{
    protected $table = "hr_onboarding_requirements";
    protected $primaryKey = 'id';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}

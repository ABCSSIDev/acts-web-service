<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HDMFDetails extends Model
{
    protected $table = 'hr_employee_hdmf_details';
}

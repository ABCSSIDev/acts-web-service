<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OB extends Model
{
    protected $table = "hr_ob";
    protected $primaryKey = 'id';
    protected $fillable = ['employee_id', 'date', 'time-in', 'time-out', 'purpose'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getEmployee(){
        return $this->belongsTo(Employee::class,'employee_id')->withoutGlobalScope(RoleScope::class);
    }

    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }

    public function getObTotalDays($employee_id, $start, $end) {
        return $this->whereBetween(
            DB::raw('date(date)'),
            [date('Y-m-d', strtotime($start)), date('Y-m-d', strtotime($end))]
        )
        ->where('employee_id', $employee_id)
        ->whereExists(function ($query) {
            $query->select(DB::raw(1))
            ->from('approvals')
            ->whereColumn('approvals.reference_id', 'hr_ob.id')
            ->where('approvals.reference_type', 'App\\OB');
        })
        ->get();
    }

    public function getEmployeeOBTotalHours($employee_id, $start, $end) {
        $ob = $this->getObTotalDays($employee_id, $start, $end);
        $total = 0;
        foreach ($ob as $o) {
            $department_id = $o->getEmployee->getJobDetails->getDesignation->department_id;
            $schedule = Schedule::whereJsonContains('departments', [$department_id])->first();
            $schedule_break = ScheduleBreak::where('schedule_id', $schedule->id)->first();
            $break_hours = 1;
            if ($schedule_break) {
                $break_hours = $schedule_break->duration / 60;
            }
            $total += Carbon::parse($o->time_in)->floatDiffInHours($o->time_out, false) - $break_hours;
        }
        return number_format($total, 2);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReturnSlip extends Model
{
    use SoftDeletes;
    protected $table = "kamalig_return_slips";
    protected $primaryKey = 'id';
    protected $fillable = ['employee_id', 'date'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $with = ["employee", "returnedItems", "logs"];
    /**
     * Get Employee
     *
     * @return employee
     */
    public function employee(){
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function returnedItems(){
        return $this->hasMany(ReturnItems::class, 'return_id', 'id');
    }

    public function logs(){
        return $this->morphMany(Log::class,"reference");
    }

    public function infoOfCreate(){
        return $this->logs()->oldest()->first();
    }
}
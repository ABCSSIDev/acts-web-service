<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class BirTemplateItem extends Model
{
    use SoftDeletes;

    protected $table = 'bir_template_items';

    protected $fillable = [
        'bir_template_id',
        'rate_type',
        'salary_range_from',
        'salary_range_to',
        'withholding_tax_value',
        'withholding_tax_percent'
    ];

    /**
     * Bir Template relationship
     *
     * @return BelongsTo
     */
    public function birTemplate(): BelongsTo
    {
        return $this->belongsTo(BirTemplate::class, 'bir_template_id');
    }
}

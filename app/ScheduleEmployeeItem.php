<?php

namespace App;

use App\Traits\ScheduleEmployeeFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ScheduleEmployeeItem extends Model
{
    use ScheduleEmployeeFilter;

    const STATUS = [
        'VERIFY' => 1,
        'NOT_VERIFY' => 0,
    ];

    protected $table = "hr_schedule_employee_items";

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * schedule employee relationship
     *
     * @return BelongsTo
     */
    public function scheduleEmployee(): BelongsTo
    {
        return $this->belongsTo(ScheduleEmployee::class, 'schedule_employee_id');
    }

    /**
     * employee relationship
     *
     * @return BelongsTo
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    /**
     * employee relationship
     *
     * @return BelongsTo
     */
    public function reschedEmployee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'resched_employee_id');
    }
}

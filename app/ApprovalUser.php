<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ApprovalUser extends Model
{
    protected $table = "approval_users";
    protected $primaryKey = 'id';
    protected $fillable = [
        'approval_process_id',
        'user_id',
        'seq_order'
    ];

    /**
     * user relationship
     *
     * @return BelongsTo
     */
    public function getUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

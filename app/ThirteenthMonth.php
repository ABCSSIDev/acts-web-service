<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThirteenthMonth extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $table = 'hr_thirteenth_month_employees';
    protected $primaryKey = 'id';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getThirteenthMonthEmployees()
    {
        return $this->hasMany('App\ThirteenthMonthEmployee', 'id', 'thirteenth_month_id');
    }
    
    /**
     * logs polymorphic
     *
     * @return MorphMany
     */
    public function logs(): MorphMany
    {
        return $this->morphMany(Log::class, "reference");
    }

    /**
     * employee
     *
     * @return BelongsTo
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}

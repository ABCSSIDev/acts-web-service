<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobOpening extends Model
{
    protected $table = 'hr_job_openings';

    protected $fillable = [
        'name','manpower_required', 'designation_id', 'assigned_recruiter', 'target_date', 'job_opening_status','expected_salary','hiring_manager_id','opened_date','job_type','work_experience','job_description','job_requirements',
    ];
    use SoftDeletes;

    public function getDesignation(){
        return $this->belongsTo(Designation::class,'designation_id');
    }

    public function getRecruiter(){
        return $this->belongsTo(Employee::class,'assigned_recruiter');
    }

    public function getHiringManager(){
        return $this->belongsTo(Employee::class,'hiring_manager_id');
    }

    public function getCandidates(){
        return $this->hasMany(Candidate::class,'job_opening_id');
    }

    public function getInterviews(){
        return $this->hasManyThrough(Interview::class,Candidate::class);
    }

    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }

}

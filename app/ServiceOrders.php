<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceOrders extends Model
{
    protected $table = 'kamalig_service_orders';
    protected $primaryKey = 'id';
    protected $fillable = ['team_id','subscribers_id', 'team_member', 'account_no', 'service_no', 'payment_mode', 'check_or_no',
                            'bank', 'branch', 'so_no', 'type', 'completion_date', 'time_start',
                            'time_end', 'remarks', 'with_pob', 'without_pob', 'type_pob'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getTeam(){
        return $this->belongsTo(Team::class,'team_id');
    }
    public function getSubscribers(){
        return $this->belongsTo(Subscribers::class,'subscribers_id');
    }
    public function logs(){
        return $this->morphMany(Log::class,"reference");
    }
    // public function getSubscribers(){
    //     return $this->hasMany(Subscribers::class, 'id', 'id');
    // }
    public function getServiceCharges(){
        return $this->hasMany(ServiceCharges::class,'service_id');
    }
    public function getMaterialUses(){
        return $this->hasMany(MaterialUses::class,'service_id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function getPermissionModule(){
        return $this->hasMany(PermissionModule::class,'permission_id');
    }

    public static function getPermissionIdByName($name){
        $permission = self::where('permission_name','like',$name)->first();
        if($permission){
            return $permission->id;
        }
        return null;
    }

    public function logs()
    {
        return $this->morphMany(Log::class, "reference");
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateAttachment extends Model
{
    protected $table = 'hr_candidate_attachments';
    protected $primaryKey = 'id';
    protected $fillable = ['candidate_id', 'attached_by', 'filepath', 'description'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getAttachedby(){
        return $this->belongsTo('App\Employee', 'attached_by', 'id');
    }
    public function getCandidateid(){
        return $this->belongsTo('App\Employee', 'candidate_id', 'id');
    }

}

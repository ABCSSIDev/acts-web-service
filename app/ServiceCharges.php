<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceCharges extends Model
{
    protected $table = 'kamalig_service_charges';
    protected $primaryKey = 'id';
    protected $fillable = ['type', 'service_id', 'description', 'amount'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;
    
    public function getServiceOrders(){
        return $this->belongsTo(ServiceOrders::class,'service_id');
    }


}

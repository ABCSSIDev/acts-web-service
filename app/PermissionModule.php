<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PermissionModule extends Model
{
    use SoftDeletes;

    public function getPermission(){
        return $this->belongsTo(Permission::class,'permission_id');
    }

    public function getModule(){
        return $this->belongsTo(Module::class,'module_id');
    }
}

<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaterialIssuancesItems extends Model
{
    protected $table = 'kamalig_material_issuance_items';
    protected $primaryKey = 'id';
    protected $fillable = ['issuance_id', 'inventory_id', 'quantity'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getInventories(){
        return $this->belongsTo(Inventories::class,'inventory_id');
    }
    
    public function getReturnUsedMaterials(){
        return $this->hasOne(Inventories::class,'issuance_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{

    protected $table = 'hr_departments';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'description', 'employee_id', 'status'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;


    public function getEmployee(){
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function getDesignations(){
        return $this->hasMany(Designation::class,'department_id');
    }

    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }

    public function scopeApprovals($query, $model_name){
        $model = 'App\\' . $model_name;
        $approvals = ApprovalProcess::where('reference_type', $model)->get();
        $departments_arr = array();
        foreach ($approvals as $approval) {
            $departments = json_decode($approval->departments);
            for ($i = 0; $i < count($departments); $i++) {
                array_push($departments_arr, $departments[$i]);
            }
        }
        $unique_department = array_unique($departments_arr);
        return $query->whereNotIn('id', $unique_department);
    }


}

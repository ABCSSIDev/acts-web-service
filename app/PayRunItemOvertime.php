<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayRunItemOvertime extends Model
{
    protected $table = "hr_pay_run_item_overtimes";
    protected $primaryKey = 'id';
    protected $fillable = [
        'pay_run_item_id',
        'ot_type_id',
        'billable_hours',
        'non_billable_hours'
    ];

    public function getPayRun()
    {
        return $this->belongsTo(PayRun::class, 'pay_run_item_id');
    }
    public function getOvertimeType()
    {
        return $this->belongsTo(OvertimeType::class, 'ot_type_id');
    }
}

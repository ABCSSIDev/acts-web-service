<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssuanceItems extends Model
{
    protected $table = "kamalig_issuance_items";
    protected $primaryKey = 'id';
    protected $fillable = ['issuance_id', 'asset_id'];
    protected $dates = ['created_at', 'updated_at'];
    protected $with = [ 'asset', 'issuance'];

    public function issuance()
    {
        return $this->belongsTo(AccountabilityRequisition::class, 'issuance_id', 'id');
    }

    public function asset()
    {
        return $this->belongsTo(Assets::class, 'asset_id', 'id');
    }
}

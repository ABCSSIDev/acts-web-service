<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SSSTemplateContributions extends Model
{
    use SoftDeletes;

    protected $table = 'hr_sss_template_contributions';
    protected $dates = ['created_at','deleted_at','updated_at'];
    protected $fillable = ['salary_from',
                           'salary_to',
                           'monthly_salary_credit',
                           'employer_contribution',
                           'employee_contribution',
                           'employee_compensation'];
}

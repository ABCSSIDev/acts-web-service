<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryStatus extends Model
{
    protected $table = 'kamalig_inventory_statuses';
    protected $primaryKey = 'id';
    protected $fillable = ['status_name', 'status_type', 'description', 'enabled'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;
}

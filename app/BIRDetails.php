<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BIRDetails extends Model
{
    protected $table = 'hr_employee_bir_details';
}

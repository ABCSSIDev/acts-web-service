<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkDetails extends Model
{
    protected $table = 'hr_employee_work_details';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

}

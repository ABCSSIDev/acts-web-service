<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceBreak extends Model
{
    protected $table = 'hr_attendance_breaks';

    public function getAttendance(){
        return $this->belongsTo(Attendance::class,'attendance_id');
    }
    public function getBreak(){
        return $this->belongsTo(ScheduleBreak::class,'break_id');
    }
}

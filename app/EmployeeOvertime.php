<?php

namespace App;

use App\Scopes\RoleScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class EmployeeOvertime extends Model
{
    use SoftDeletes;

    const OT_TYPES = [
        'NORMAL_DAY_OT' => 'NDOT',
        'REST_DAY_OT' => 'RDOT',
        'SPE_NON_WORKING_OT' => 'SpeOT',
        'SPE_NON_WORKING_REST_DAY_OT' => 'Spe&RDOT',
        'REG_HOLIDAY_OT' => 'RHOT',
        'REG_HOLIDAY_REST_DAY_OT' => 'RH&RDOT'
    ];

    protected $table = 'hr_employee_overtimes';

    private static $employee_array = [];

    protected $fillable = ['file_date',
                           'overtime_date',
                           'start_time',
                           'end_time',
                           'ot_id',
                           'rate',
                           'rendered_hours',
                           'purpose',
                           'reason'];

    public function getEmployee()
    {
        return $this->belongsTo(Employee::class, 'employee_id')
            ->withoutGlobalScope(RoleScope::class);
    }

    public function getOtType()
    {
        return $this->belongsTo(OvertimeType::class, 'ot_id');
    }

    public function getLogs()
    {
        return $this->morphMany(Log::class, "reference");
    }

    public function scopeRole($query, $model_name)
    {
        $user_id = Auth::id();
        if ($user_id != 1) {
            $user_access = UserAccess::where('user_id', $user_id)->first();
            if (self::checkModule($user_access->bypass_role_systems, $model_name) && self::checkIfApprover($model_name)) {
                $roles = Role::where('id', $user_access->role_id)->get();
                self::loopRoles($roles, $user_access->role_id);
                return $query->whereIn('employee_id', self::$employee_array);
            }
        }

        return $query;
    }

    public static function loopRoles($roles, $parentRoleId = null, $child = false)
    {
        foreach ($roles as $role) {
            if (( !empty($parentRoleId) && $role->is_shared != null ) || $role->id == $parentRoleId || !empty($child)) {
                if (!empty($role->getUserAccess)) {
                    foreach ($role->getUserAccess as $access) {
                        $employee = Employee::where('user_id', $access->user_id)->first();
                        if (!empty($employee)) {
                            array_push( self::$employee_array, $employee->id);
                        }
                    }
                }
                if (!empty($role->getChildren)) {
                    self::loopRoles($role->getChildren, null, true);
                }
            }
        }
    }

    public static function checkModule($system_list, $module_name)
    {
        $system_count = 0;
        if (!empty($system_list)) {
            $systems = json_decode($system_list);
            foreach ($systems as $system) {
                $modules = Module::where('system_id', '=', $system)->where('model_type', $module_name)->get();
                if ($modules->count() > 0) {
                    $system_count++;
                }
            }
        }

        return $system_count == 0;
    }

    public static function checkIfApprover($model_name)
    {
        $approval_users = [];
        $get_process = ApprovalProcess::where('reference_type', $model_name)->where('status', 1)->first();
        if (!empty($get_process)) {
            $approval_users = ApprovalUser::where('approval_process_id', $get_process->id)->where('user_id', Auth::user()->id)->get();
        }
        return count($approval_users) <= 0;
    }
}

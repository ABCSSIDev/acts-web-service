<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    protected $table = 'kamalig_teams';
    protected $primaryKey = 'id';
    protected $fillable = ['teams_id', 'team_name','team_type','active', 'vehicle_plateno'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getTechnicians(){
        return $this->hasMany(Technicians::class,'teams_id');
    }
    public function logs(){
        return $this->morphMany(Log::class,"reference");
    }
}

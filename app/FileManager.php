<?php

namespace App;

use App\Employee;
use Illuminate\Database\Eloquent\Model;

class FileManager extends Model
{
    protected $table = "file_managers";
    protected $primaryKey = 'id';
    protected $fillable = ['reference_id', 'reference_type', 'file_name', 'url', 'attached_by'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getAttacher(){
        return $this->belongsTo(Employee::class,'attached_by');
    }

    public function reference(){
        return $this->morphTo();
    }
}

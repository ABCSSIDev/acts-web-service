<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReimbursementItem extends Model
{
    protected $table = 'hr_reimbursement_items';
    protected $primaryKey = 'id';
    protected $fillable = ['reimbursement_id', 'date', 'type', 'particulars', 'receipt_number', 'amount'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}

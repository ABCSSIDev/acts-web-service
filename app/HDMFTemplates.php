<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HDMFTemplates extends Model
{
    use SoftDeletes;

    protected $table = 'hr_hdmf_templates';
    
    protected $dates = [
        'created_at',
        'deleted_at'
    ];

    protected $fillable = [
        'salary_from',
        'salary_to',
        'employer_percent',
        'employee_percent'
    ];
}

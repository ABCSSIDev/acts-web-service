<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestdayHoliday extends Model
{
    use SoftDeletes;

    const RESTDAY_TYPES = [
        'REST_DAY' => 'RD',
        'SPE_NON_WORKING' => 'Spe',
        'SPE_NON_WORKING_REST_DAY' => 'Spe&RD',
        'REG_HOLIDAY' => 'RH',
        'REG_HOLIDAY_REST_DAY' => 'RH&RD'
    ];

    protected $table = 'hr_employee_restday_holiday_works';

    protected $fillable = [
        'id',
        'employee_id',
        'ot_id',
        'attendance_id',
        'holidays',
        'rate',
        'restday_holiday_date',
        'file_date',
        'start_time',
        'rendered_hours',
        'purpose'
    ];

    /**
     * employee relationship
     *
     * @return Builder
     */
    public function employees(): BelongsTo
    {
        return $this->belongsTo(
            Employee::class,
            'employee_id'
        )->withoutGlobalScope(RoleScope::class);
    }

    /**
     * restday holiday type relationship
     *
     * @return BelongsTo
     */
    public function overtimeType(): BelongsTo
    {
        return $this->belongsTo(OvertimeType::class, 'ot_id');
    }

    /**
     * logs polymorphic
     *
     * @return MorphMany
     */
    public function logs(): MorphMany
    {
        return $this->morphMany(Log::class, "reference");
    }
}

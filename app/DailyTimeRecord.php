<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DailyTimeRecord extends Model
{
    protected $table = 'hr_daily_time_records';

    /**
     * @var array
     */
    protected $guarded = [];
}

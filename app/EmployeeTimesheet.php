<?php

namespace App;

use App\Scopes\RoleScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class EmployeeTimesheet extends Model
{
    protected $table = "hr_employee_timesheets";
    protected $primaryKey = 'id';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getEmployee(){
        return $this->belongsTo(Employee::class,'employee_id')->withoutGlobalScope(RoleScope::class);
    }

    public function getApprover(){
        return $this->hasOne('App\Employee', 'id', 'approved_by');
    }

    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }
}

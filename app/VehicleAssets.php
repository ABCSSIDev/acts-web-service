<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleAssets extends Model
{
    protected $table = 'kamalig_vehicle_assets';
    protected $dates = ['plate_number', 'driver', 'fleet_card_number', 'tools', 'status'];
}

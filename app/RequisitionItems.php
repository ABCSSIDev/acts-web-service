<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequisitionItems extends Model
{
    protected $table = "kamalig_requisition_items";
    protected $primaryKey = 'id';
    protected $fillable = ['requisition_id', 'asset_id'];
    protected $dates = ['created_at', 'updated_at'];
    protected $with = ['asset', 'inventory'];

    public function requisition()
    {
        return $this->belongsTo(AccountabilityRequisition::class, 'requisition_id', 'id');
    }

    public function asset()
    {
        return $this->belongsTo(Assets::class, 'asset_id', 'id');
    }

    public function inventory()
    {
        return $this->belongsTo(Inventories::class, 'inventory_id', 'id');
    }
}

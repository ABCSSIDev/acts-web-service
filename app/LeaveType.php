<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{
    protected $table = "hr_leave_types";
    protected $primaryKey = 'id';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function leaves(){
        return $this->hasMany('App\EmployeeLeave', 'leave_id', 'id');
    }
}

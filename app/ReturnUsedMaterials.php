<?php

namespace App;

use App\Scopes\RoleScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReturnUsedMaterials extends Model
{
    protected $table = 'kamalig_return_used_materials';
    protected $primaryKey = 'id';
    protected $fillable = ['issuance_item_id', 'status', 'quantity', 'remarks'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;

    public function getIssuanceTemplatesItems(){
        return $this->hasOne('App\IssuanceTemplatesItems','id', 'issuance_item_id');
    }
    public function getLogs(){
        return $this->morphMany(Log::class,"reference");
    }
}

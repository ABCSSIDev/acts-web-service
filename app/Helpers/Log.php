<?php
namespace App\Log;

use App\Employee;
use App\Log;
use App\Role;
use App\User;
use App\UserAccess;
use Illuminate\Support\Facades\Auth;

//function setup for create logs
if (!function_exists('log_created')) {
    function log_created($param, $related = null) {
        $log = new Log();
        $log->reference_id = $param->id;
        $log->reference_type = 'App\\' . class_basename($param);
        ($related != null ? $log->reference_relate_id = $related->id : '');
        ($related != null ? $log->reference_relate_type = 'App\\' . class_basename($related) : '');
        $log->user_id = Auth::user()->id;
        $log->description = class_basename($param) . ' created';
        $log->save();

        notifyUser($param);
    }

    function notifyUser($model)
    {
        $ids = getIdNotifyUser(); // getting of users need to notify for this action
        if (count($ids) > 0) {
            $users = User::whereIn('id', $ids)->get();
            $notif = 'App\\Notifications\\' . class_basename($model);
            //Insert to Database Notification
            if (\Notification::send($users, new $notif($model))) {
                return back();
            }
        }
    }

    function getIdNotifyUser()
    {
        if (Auth::user()->id == 1) {
            $userId = array();
            $roles = Role::where('role_id', null)->get();
            foreach ($roles as $key => $role) {
                $user = UserAccess::where('role_id', $role->role_id)->select('user_id')->get();
                if (count($user) > 0) {
                    array_push($userId, $user);
                }
            }
        } else {
            $userId = array('1'); // adding superadmin id for notifying
            $userRole = Auth::user()->getUserAccess->getRole->id;
            $role_subordinate = $userRole;

            while ($role_subordinate != null) {
                $role = Role::findorFail($role_subordinate);
                $getUser = UserAccess::where('role_id', $role->role_id)->get();
                foreach ($getUser as $key => $user) {
                    array_push($userId, $user->user_id);
                }

                $role_subordinate = $role->role_id;
            }
        }
        return $userId;
    }
}
//function setup for approval logs
if (!function_exists('log_approval')){
    function log_approval($ref_id,$ref_type,$approver,$status){
        $log = new Log;
        $log->reference_id = $ref_id;
        $log->reference_type = 'App\\'.class_basename($ref_type);
        $log->reference_relate_id = $approver;
        $log->reference_relate_type = 'App\\Employee';
        $log->user_id = $approver;
        $log->description = '<strong>' . approver_name($approver) . '</strong> ' . approved_stats($status) . ' <strong>' . class_basename($ref_type) . '</strong> request';
        $log->save();
    }

    function approved_stats($stats){
        if($stats == 0){
            $status = 'Rejected';
        }else{
            $status = 'Approved';
        }
        return $status;
    }

    function approver_name($id){
        $name = Employee::where('user_id',$id)->first();
        if(!$name){
            $approver_name = User::select('name')->where('id',$id)->first();
        }
        else{
            $approver_name = $name->fullNameWithoutMiddleName();
        }
        return $approver_name;
    }
}


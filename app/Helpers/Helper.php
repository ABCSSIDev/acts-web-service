<?php
namespace App\Helpers;
use App\PHICDetails;
use App\Restriction;
use App\User;
use App\UserAccess;
use function foo\func;
use Image;
use App\Employee;
use App\EmployeeContact;
use App\JobDetail;
use App\EducationalDetails;
use App\WorkDetails;
use App\SSSDetails;
use App\BIRDetails;
use App\HDMFDetails;
use App\Mail\SignUp;
use Illuminate\Support\Facades\Mail;

if (!function_exists('save_image')){
    function save_image($file, $file_name){
        $img = Image::make($file->getRealPath());
        $path = explode('/', $file_name);
        if (!file_exists(storage_path('app/public/'.$path[0]))) {
            $make_directory = mkdir(storage_path('app/public/'.$path[0]), 0777, true);
        }

        if ($img->save(storage_path('app/public/'.$file_name) , 60)){
            return true;
        };
        return false;
    }
}

if (!function_exists('save_employee')) {
    function save_employee($data)
    {
        $employee = new Employee();
        // if (array_key_exists("employee_id",$data)){
        //     $employee->id = $data['employee_id'];
        // }
        $employee->employee_no = $data['employee_no'];
        $employee->first_name = $data['first_name'];
        $employee->middle_name = $data['middle_name'];
        $employee->last_name = $data['last_name'];
        $employee->gender = $data['gender'];
        $employee->birth_date = $data['birth_date'];
        $employee->current_address = $data['current_address'];
        $employee->permanent_address = $data['current_address'];
        $employee->marital_status = $data['marital_status'];
        if ($employee->save()) {
            return $employee;
        }
        return null;
    }
}
if (!function_exists('edit_employee')){
   
    function edit_employee($data){
        // return 1;
        $edit_employee = Employee::where('id', $data['id'])->update([
            'employee_no' => $data['employee_no'],
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'last_name' => $data['last_name'],
            'gender' => $data['gender'],
            'birth_date' => $data['birth_date'],
            'current_address' => $data['current_address'],
            'permanent_address' => $data['permanent_address'],
            'marital_status' => $data['marital_status'],
            'updated_at' => date('Y/m/d H:i:s')
        ]);
        return $edit_employee?$data:'';
    }
}
if (!function_exists('save_employee_contacts')){
    function save_employee_contacts($data){
        $employee_contacts = new EmployeeContact();
        $employee_contacts->employee_id = $data['employee_id'];
        $employee_contacts->email = $data['email'];
        $employee_contacts->phone_number = $data['phone_number'];
        $employee_contacts->telephone_number = $data['telephone_number'];
        $employee_contacts->work_phone = $data['work_phone'];
        $employee_contacts->emergency_contact = $data['emergency_contact'];
        $employee_contacts->emergency_contact_number = $data['emergency_contact_number'];
        if ($employee_contacts->save()){
            return $employee_contacts;
        }
        return null;
    }
}

if (!function_exists('edit_employee_contacts')){
    function edit_employee_contacts($data){
            $employee_contacts = EmployeeContact::where('employee_id', $data['employee_id'])->update([ 
                'email' => $data['email'],
                'phone_number' => $data['phone_number'],
                'telephone_number' => $data['telephone_number'],
                'work_phone' => $data['work_phone'],
                'emergency_contact' => $data['emergency_contact'],
                'emergency_contact_number' => $data['emergency_contact_number'],
                'updated_at' => date('Y/m/d H:i:s')
            ]);
        
        
        return $employee_contacts?$data:'';
    }
}
if (!function_exists('save_job_details')){
    function save_job_details($data){
        $job_detail = new JobDetail();
        $job_detail->employee_id = $data['employee_id'];
        $job_detail->designation_id = $data['designation_id'];
        $job_detail->employee_type_id = $data['employee_type_id'];
        $job_detail->employee_status = $data['employee_status'];
        $job_detail->source_of_hire = $data['source_of_hire'];
        $job_detail->joined_date = $data['joined_date'];
        if ($job_detail->save()){
            return $job_detail;
        }
        return null;
    }
}
if (!function_exists('edit_job_details')){
    function edit_job_details($data){
       
            $job_detail = JobDetail::where('employee_id', $data['employee_id'])->update([ 
                'designation_id' => $data['designation_id'],
                'employee_type_id' => $data['employee_type_id'],
                'employee_status' => $data['employee_status'],
                'source_of_hire' => $data['source_of_hire'],
                'joined_date' => $data['joined_date'],
                'reporting_to' => $data['reporting_to'],
                'updated_at' => date('Y/m/d H:i:s')
            ]);
        return $job_detail?$data:'';
    }
}
if (!function_exists('save_educational_background')){
    function save_educational_background($data){
        $educational_details = new EducationalDetails();
        $educational_details->employee_id = $data['employee_id'];
        $educational_details->level = $data['level'];
        $educational_details->start_date = $data['start_date'];
        $educational_details->end_date = $data['end_date'];
        $educational_details->school = $data['school'];
        if ($educational_details->save()){
            return $educational_details;
        }
        return null;
    }
}

if (!function_exists('edit_educational_background')){
    function edit_educational_background($data){
        $educational_details = EducationalDetails::where('id', $data['id'])->update([ 
            'level' => $data['level'],
            'start_date' => $data['start_date'],
            'end_date' => $data['end_date'],
            'school' => $data['school'],
            'updated_at' => date('Y/m/d H:i:s')
        ]);
        
        return $educational_details?$data:'';
    }
}
if (!function_exists('save_employment_background')){
    function save_employment_background($data){
        
        $work_details = new WorkDetails();
        $work_details->employee_id = $data['employee_id'];
        $work_details->company = $data['company'];
        $work_details->start_date = $data['start_date'];
        $work_details->end_date = $data['end_date'];
        $work_details->description = $data['description'];
        if ($work_details->save()){
            return $work_details;
        }
        return null;
    }
}
if (!function_exists('edit_employment_background')){
    function edit_employment_background($data){
        $work_details = WorkDetails::where('id', $data['id'])->update([ 
            'company' => $data['company'],
            'start_date' => $data['start_date'],
            'end_date' => $data['end_date'],
            'description' => $data['description'],
            'updated_at' => date('Y/m/d H:i:s')
        ]);
        
        return $work_details?$data:'';
    }
}
if (!function_exists('save_employee_sss')){
    function save_employee_sss($data){
        $sss = new SSSDetails();
        $sss->employee_id = $data['employee_id'];
        $sss->membership_code = $data['membership_code'];
        $sss->joined_date = $data['joined_date'];
        $sss->separation_date = $data['separation_date'];
//        $sss->status = $data['status'];
        if ($sss->save()){
            return $sss;
        }
        return null;
    }
}

if (!function_exists('edit_employee_sss')){
    function edit_employee_sss($data){
        $sss = SSSDetails::where('employee_id', $data['employee_id'])->update([ 
            'membership_code' => $data['membership_code'],
            'joined_date' => $data['joined_date'],
            'separation_date' => $data['separation_date'],
            'status' => $data['status'],
            'updated_at' => date('Y/m/d H:i:s')
        ]);
    }
}
if (!function_exists('save_employee_bir')){
    function save_employee_bir($data){
        $bir = new BIRDetails();
        $bir->employee_id = $data['employee_id'];
        $bir->membership_code = $data['membership_code'];
        $bir->account_number = 'NA';
        $bir->joined_date = $data['joined_date'];
        $bir->separation_date = $data['separation_date'];
//        $bir->status = $data['status'];
        if ($bir->save()){
            return $bir;
        }
        return null;
    }
}

if (!function_exists('edit_employee_bir')){
    function edit_employee_bir($data){
        $bir = BIRDetails::where('employee_id', $data['employee_id'])->update([ 
            'membership_code' => $data['membership_code'],
            'joined_date' => $data['joined_date'],
            'separation_date' => $data['separation_date'],
            'account_number' => $data['account_number'],
            'status' => $data['status'],
            'updated_at' => date('Y/m/d H:i:s')
        ]);
        
    }
}
if (!function_exists('save_employee_phic')){
    function save_employee_phic($data){
        $phic = new PHICDetails();
        $phic->employee_id = $data['employee_id'];
        $phic->membership_code = $data['membership_code'];
        $phic->joined_date = $data['joined_date'];
        $phic->separation_date = $data['separation_date'];
//        $phic->status = $data['status'];
        if ($phic->save()){
            return $phic;
        }
        return null;
    }
}

if (!function_exists('edit_employee_phic')){
    function edit_employee_phic($data){
        $phic = PHICDetails::where('employee_id', $data['employee_id'])->update([ 
            'membership_code' => $data['membership_code'],
            'joined_date' => $data['joined_date'],
            'separation_date' => $data['separation_date'],
            'status' => $data['status'],
            'updated_at' => date('Y/m/d H:i:s')
        ]);
    }
}
if (!function_exists('save_employee_hdmf')){
    function save_employee_hdmf($data){
        $hdmf = new HDMFDetails();
        $hdmf->employee_id = $data['employee_id'];
        $hdmf->membership_code = $data['membership_code'];
        $hdmf->account_number = 'NA';
        $hdmf->joined_date = $data['joined_date'];
        $hdmf->separation_date = $data['separation_date'];
//        $hdmf->status = $data['status'];
        if ($hdmf->save()){
            return $hdmf;
        }
        return null;
    }
}

if (!function_exists('edit_employee_hdmf')){
    function edit_employee_hdmf($data){
        $phic = HDMFDetails::where('employee_id', $data['employee_id'])->update([ 
            'membership_code' => $data['membership_code'],
            'joined_date' => $data['joined_date'],
            'separation_date' => $data['separation_date'],
            'account_number' => $data['account_number'],
            'status' => $data['status'],
            'updated_at' => date('Y/m/d H:i:s')
        ]);
    }
}
if (!function_exists('save_user_information')){
    function save_user_information($data){
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['username'];
        $user->password = $data['password'];
        $user->save();

        Employee::where('id',$data['employee_id'])->update([
            'user_id' => $user->id
        ]);

        $user_access = new UserAccess();
        $user_access->user_id = $user->id;
        $user_access->role_id = $data['role'];
        $user_access->permission_id = $data['permission'];
        $user_access->save();

        return $user;
    }
}
if (!function_exists('edit_user_information')){
    function edit_user_information($data){
        $user = User::where('id', $data['employee_id'])->update([ 
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'updated_at' => date('Y/m/d H:i:s')
        ]);
        $user_access = UserAccess::where('user_id', $data['employee_id'])->update([ 
            'role_id' => $data['role_id'],
            'permission_id' => $data['permission'],
            'updated_at' => date('Y/m/d H:i:s')
        ]);
    }
}
if (!function_exists('change_date_format')){
    function change_date_format($format,$date){
        return date($format,strtotime($date));
    }
}

if (!function_exists('get_restrictions')){
    function get_restrictions($data){
        $data_array = json_decode($data);
        $restriction_array = array();
        foreach ($data_array as $key => $value){
            $restriction = Restriction::findOrFail($value);
            if ($restriction){
                $restriction_array[] = array(
                    'id' => $value,
                    'description' => $restriction->description
                );
            }
        }
        return $restriction_array;
    }
}

if (!function_exists('send_email')){
    function send_email($user){
        Mail::to($user->email)->send(new SignUp($user));
    }
}
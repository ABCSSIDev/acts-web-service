<?php

namespace App\TimeAttendance;

use App\Attendance;
use App\Schedule;
use App\ScheduleEmployee;
use App\ScheduleEmployeeItem;
use App\ScheduleItem;
use Illuminate\Support\Facades\DB;

if (!function_exists('get_shift_employee')) {
    /**
     * get Schedule Employee
     *
     * @param int $employee_id
     * @param string $dept_id
     *
     * @return Schedule
     */
    function get_shift_employee(int $employee_id, string $date)
    {
        $_date = date('Y-m-d', strtotime($date));

        $schedule = Schedule::where(DB::raw('date(schedule_date_start)'), '<=', $date)
                ->where(DB::raw('date(schedule_date_end)'), '>=', $date)
                ->where('is_activate', '1')
                ->first();
            
        if ($schedule) {
            $schedule_item = ScheduleItem::where('schedule_id', $schedule->id)
                ->where('employee_id', $employee_id)->first();
            
            return $schedule_item ? $schedule_item : null;
        }
        
        return null;
    }
}

if (!function_exists('check_dtr_verified')) {
    /**
     * check_dtr_verified
     *
     * @param string $date
     * @param int $employee_id
     *
     * @return bool
     */
    function check_dtr_verified(string $date, int $employee_id): bool
    {
        $_date = date('Y-m-d', strtotime($date));

        $schedule = ScheduleEmployee::where(DB::raw('date(date)'), $_date)->first();

        if ($schedule) {
            $check_schedule = ScheduleEmployeeItem::where('schedule_employee_id', $schedule->id)
                ->where('employee_id', $employee_id)
                ->where('verified_attendance', 1)
                ->first();

            if ($check_schedule) {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('check_shift_schedule')) {

    /**
     * check Shift Schedule
     *
     * @param string $date
     * @param int $employee_id
     *
     * @return bool
     */
    function check_shift_schedule(string $date, int $employee_id): bool
    {
        $_date = date('Y-m-d', strtotime($date));

        $schedule = ScheduleEmployee::where(DB::raw('date(date)'), $_date)->first();

        if ($schedule) {
            $schedule_check = Schedule::where('id', $schedule->schedule_id)->where('is_activate', '1')->first();

            if ($schedule_check) {
                $check_schedule = ScheduleEmployeeItem::where('schedule_employee_id', $schedule->id)
                    ->where('employee_id', $employee_id)->first();
    
                if ($check_schedule) {
                    return true;
                }
            }
        }

        return false;
    }
}

<?php

namespace App\Helpers\Payruns;

use App\Attendance;
use App\Employee;
use App\EmployeeLeave;
use App\EmployeeTimesheet;
use App\Helpers\Payruns\AttendanceHelper;
use App\Helpers\Payruns\OBHelper;
use App\Holiday;
use App\OB;
use DateTime;
use Illuminate\Support\Facades\DB;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use function App\TimeAttendance\check_shift_schedule;
use function App\TimeAttendance\get_shift_employee;

class LateAbsenceHelper
{
    /**
     * @var AttendanceHelper
     */
    protected $attendanceHelper;

    /**
     * @var OBHelper
     */
    protected $obHelper;

    public function __construct(
        AttendanceHelper $attendanceHelper,
        OBHelper $obHelper
    ) {
        $this->attendanceHelper = $attendanceHelper;
        $this->obHelper = $obHelper;
    }

    /**
     * Get Late and Absent
     * @param string $start
     * @param string $end
     * @return array
     */
    public function getLatesAbsence(string $start, string $end): array
    {
        $merges = array();
        $data = array();
        $employee_id = array();
        $employee_attendance = $this->attendanceHelper->getPeriodAttendance($start, $end);

        foreach ($employee_attendance->groupBy('employee_id')->get() as $key => $attendance) {
            $per_employee_att = Attendance::where('employee_id', $attendance->employee_id)->get();

            $shift_schedule = get_shift_employee($attendance->employee_id, $attendance->start_time);

            if ($shift_schedule != null) {
                $data[] = [
                    'employee_name'    => $attendance->getEmployee->first_name . ' '
                        . $attendance->getEmployee->last_name,
                    'employee_id'      => $attendance->employee_id,
                    'lates_absences'   => $this->computelateAbsence(
                        $attendance->employee_id,
                        $per_employee_att,
                        $shift_schedule,
                        $start,
                        $end
                    )
                ];

                $employee_id[] = $attendance->employee_id;
            }
        }

        $merges = $this->computeAbsentOb($start, $end, $employee_id);

        return array_merge($data, $merges);
    }

    public function computeAbsentOb($start, $end, $employee_id)
    {
        $officialBusiness = $this->obHelper->ob($start, $end);

        $data_ob = array();
        $data = array();
        foreach ($officialBusiness->groupBy('employee_id')->get() as $key => $ob) {
            if (
                check_approver($ob) == null
                || model_approval_status(check_approver($ob)) == 'Approved'
            ) {
                $data_ob[] = $ob->employee_id;
            }
        }

        $added_employee = array();

        foreach ($data_ob as $key => $id) {
            if (!in_array($id, $employee_id)) {
                $added_employee[] = $id;
            }
        }

        foreach ($added_employee as $employee) {
            $employees = Employee::where('id', $employee)->first();

            $shift_schedule = get_shift_employee(
                $employee,
                $start
            );

            if ($shift_schedule != null) {
                $checkAbsent = $this->checkAbsent($employee, null, $start, $end);
                $data[] = [
                    'employee_name' => $employees->first_name . ' '
                        . $employees->last_name,
                    'employee_id' => $employee,
                    'lates_absences' => [
                        'days_late'   => 0,
                        'hrs_late'    => 0,
                        'hrs_absent'  => number_format($checkAbsent * 8, 2),
                        'days_absent' => $checkAbsent
                    ]
                ];
            }
        }

        return $data;
    }

    /**
     * Activity Absence
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function activityLateAbsence(string $start, string $end): array
    {
        $data = array();
        $holiday = false; // initialize holiday for upcoming holiday module
        $employee_attendance = $this->attendanceHelper->getPeriodAttendance($start, $end);

        foreach ($employee_attendance->groupBy('employee_id')->get() as $key => $attendance) {
            $shift = get_shift_employee(
                $attendance->employee_id,
                $attendance->start_time
            );

            $begin = new DateTime($start);

            $ends = new DateTime($end);

            $timekeeping = Attendance::where('employee_id', $attendance->employee_id)->get();
            
            if ($shift != null) {
                for ($i = $begin; $i <= $ends; $i->modify('+1 day')) {
                        $no_attendance = true;
                    $dateStart = false;
                    foreach ($timekeeping as $key => $att) {
                        $no_absent = 0;
                        $no_lates = 0;
                        $hr_lates = 0;

                        $date = date('Y-m-d', strtotime($att->start_time));

                        $check = Attendance::where(DB::raw('date(start_time)'), $date)
                            ->where('employee_id', $attendance->employee_id)->get();

                        if (count($check) > 1) {
                            if ($dateStart) {
                                continue;
                            }

                            $dateStart = true;
                            $oneAttendance = Attendance::where(DB::raw('date(start_time)'), $date)
                                ->where('employee_id', $attendance->employee_id)->orderBy('start_time', 'ASC')->first();

                            $start_time = $oneAttendance->start_time;
                        } else {
                            $start_time = $att->start_time;
                        }

                        if (date('Y-m-d', strtotime($att->start_time)) == $i->format("Y-m-d")) {
                            if ($att->end_time != null) {
                                $no_attendance = false;
                                if (
                                    date("H:i", strtotime($shift->time_in)) <
                                    date("H:i", strtotime($start_time))
                                ) {
                                    $no_lates += 1;
                                    $a = date("H:i", strtotime($shift->time_in));
                                    $b = date("H:i", strtotime($start_time));
                                    $hr_lates += (strtotime($b) - strtotime($a)) / 3600;
                                    $data[] = [
                                        'employee_name' => $attendance->getEmployee->first_name . ' ' .
                                            $attendance->getEmployee->last_name,
                                        'date' => date('F d,Y', strtotime($start_time)),
                                        'status' => 'Late',
                                        'time-in' => date('H:i A', strtotime($start_time)),
                                        'hrs_late' => round($hr_lates, 2)
                                    ];
                                }
                            }
                        }
                    }

                    

                    if (
                        $this->holidays($i->format('Y-m-d')) == false
                        && $no_attendance
                        && check_shift_schedule($i->format("Y-m-d"), $attendance->employee_id)
                        && !$this->checkLeave($attendance->employee_id, $i->format("Y-m-d"))
                    ) {
                        $data[] = [
                            'employee_name' => $attendance->getEmployee->first_name . ' '
                                . $attendance->getEmployee->last_name,
                            'date' => $i->format("F d, Y"),
                            'status' => $this->timesheetCovered($i->format("Y-m-d"), $attendance->employee_id),
                            'time-in' => '-',
                            'hrs_late' => '-'

                        ];
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Compute late absent
     * @param string $employee_id
     * @param object $attendance
     * @param string $shift
     * @param string $start
     * @param string $end
     * @return void
     */
    public function computelateAbsence(
        string $employee_id,
        object $attendance,
        object $shift,
        string $start,
        string $end
    ) {
        $no_lates = 0;
        $hr_lates = 0;
        $dateStart = false;
        
        foreach ($attendance as $key => $att) {
            $date = date('Y-m-d', strtotime($att->start_time));

            $check = Attendance::where(DB::raw('date(start_time)'), $date)
                ->where('employee_id', $employee_id)->get();

            if (count($check) > 1) {
                if ($dateStart) {
                    continue;
                }

                $dateStart = true;
                $oneAttendance = Attendance::where(DB::raw('date(start_time)'), $date)
                    ->where('employee_id', $employee_id)->orderBy('start_time', 'ASC')->first();

                $start_time = $oneAttendance->start_time;
            } else {
                $start_time = $att->start_time;
            }

            if (check_shift_schedule($date, $employee_id)) {
                if ($att->end_time != null) {
                    if (date("H:i", strtotime($shift->time_in)) < date("H:i", strtotime($start_time))) {
                        $no_lates += 1;
                        $a = date("H:i", strtotime($shift->time_in));
                        $b = date("H:i", strtotime($start_time));
                        $hr_lates += round((strtotime($b) - strtotime($a)) / 3600, 2);
                        $count[] = $hr_lates;
                    }
                }
            }
        }

        $checkAbsent = $this->checkAbsent($employee_id, $attendance, $start, $end);

        return [
            'days_late'   => $no_lates,
            'hrs_late'    => number_format($hr_lates, 2),
            'hrs_absent'  => number_format($checkAbsent * 8, 2),
            'days_absent' => $checkAbsent
        ];
    }

    /**
     * check absent
     * @param string $employee_id
     * @param $attendance
     * @param string $start_date
     * @param string $end_date
     * @return float
     */
    public function checkAbsent(
        string $employee_id,
        $attendance,
        string $start_date,
        string $end_date
    ): float {

        $begin = new DateTime($start_date);
        $end = new DateTime($end_date);
        $no_absent = 0;
        $total_att = array();
        $data = array();
        for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
            $no_attendance = true;
            if ($attendance != null) {
                foreach ($attendance as $key => $att) {
                    if (date('Y-m-d', strtotime($att->start_time)) == $i->format("Y-m-d") && $att->end_time != null) {
                        $no_attendance = false;
                    }
                }
            }

            if (
                $this->holidays($i->format("Y-m-d")) == false
                && $no_attendance
                && check_shift_schedule($i->format("y-m-d"), $employee_id)
                && !$this->checkLeave($employee_id, $i->format("Y-m-d"))
                && !$this->checkOb($employee_id, $i->format("Y-m-d"))
            ) {
                    $no_absent += 1;
            }
        }

        return $no_absent;
    }

    public function checkOb(string $employee_id, string $date)
    {
        $obStatus = false;
        $employee_ob = OB::where(DB::raw('date(date)'), '=', $date)
                        ->where('employee_id', $employee_id)
                        ->first();

        if ($employee_ob) {
            if (
                check_approver($employee_ob) == null
                || model_approval_status(check_approver($employee_ob)) == 'Approved'
            ) {
                $obStatus = true;
            }
        }

        return $obStatus;
    }

    /**
     * check holidays
     * @param string $date
     * @return bool
     */
    public function holidays(string $date): bool
    {

        $yr = date('y', strtotime($date));
        $month = date('m', strtotime($date));
        $day = date('d', strtotime($date));

        $isHoliday = Holiday::whereMonth('holiday_date', $month)->whereDay('holiday_date', $day)->first();

        if ($isHoliday) {
            $checkRecurring = Holiday::whereMonth('holiday_date', $month)
                ->whereDay('holiday_date', $day)
                ->where('is_recurring', 1)
                ->first();
            if (!$checkRecurring) {
                $isHoliday = Holiday::whereDate('holiday_date', date('Y-m-d', strtotime($date)))->first();
            }
        }
        return $isHoliday != null;
    }

    /**
     * Check Leave
     * @param string $employee_id
     * @param string $date
     * @return void
     */
    public function checkLeave(string $employee_id, string $date)
    {
        $employee_leave = EmployeeLeave::where(DB::raw('date(start_date)'), '<=', $date)
                        ->where(DB::raw('date(end_date)'), '>=', $date)
                        ->where('employee_id', $employee_id)
                        ->first();

        return $employee_leave;
    }

    /**
     * timesheet covered lates and absence
     *
     * @param string $date
     * @param string $employeeId
     *
     * @return void
     */
    public function timesheetCovered(string $date, string $employeeId)
    {
        $status = 'Absent';
        $timesheet = EmployeeTimesheet::whereDate('timesheet_date', $date)
            ->where('employee_id', $employeeId)->first();
        if ($timesheet) {
            if (check_approver($timesheet) == null) {
                $status = $timesheet;
            } else {
                if (model_approval_status(check_approver($sheet)) == 'Approved') {
                    $status = $timesheet;
                }
            }
        }
        return $status;
    }
}

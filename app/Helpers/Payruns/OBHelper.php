<?php

namespace App\Helpers\Payruns;

use App\Http\Resources\OB as ResourcesOB;
use App\OB;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;
use function App\TimeAttendance\get_shift_employee;
use function App\TimeAttendance\check_shift_schedule;

class OBHelper
{

    /**
     * @param string $start
     * @param string $end
     * @return void
     */
    public function ob(string $start, string $end)
    {
        $ob = OB::whereBetween(
            DB::raw('date(date)'),
            [date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))]
        );

        return $ob;
    }

    /**
     * Get Ob
     * @param string $start
     * @param string $end
     * @return array
     */
    public function getOfficialBusiness(string $start, string $end): array
    {
        $data = array();
        $officialBusiness = $this->ob($start, $end);

        foreach ($officialBusiness->groupBy('employee_id')->get() as $key => $ob) {
            if (
                check_approver($ob) == null
                || model_approval_status(check_approver($ob)) == 'Approved'
            ) {
                $renderedHours = Carbon::parse($ob->time_in)->floatDiffInHours($ob->time_out, false);
                $data[] = [
                    'employee_id' => $ob->employee_id,
                    'employee_name' => $ob->getEmployee->first_name . ' ' . $ob->getEmployee->last_name,
                    'total_days' => count($this->getObTotalDays($ob, $start, $end)),
                    'rendered_hours'  => $this->computeTotalHoursBySchedule($ob, $start, $end),
                ];
            }
        }
        return $data;
    }

    /**
     * Compute total Hourse by schedule
     * @param $obs
     * @param string $start
     * @param string $end
     *
     * @return void
     */
    public function computeTotalHoursBySchedule($obs, string $start, string $end)
    {
        $ob = $this->getObTotalDays($obs, $start, $end);
        $total = 0;
        $break_hours = 0;
        foreach ($ob as $o) {
            $shift = get_shift_employee(
                $o->employee_id,
                $start
            );
            if ($shift != null) {
                if ($shift->getBreaks) {
                    foreach ($shift->getBreaks as $key => $break) {
                        if ($break->is_billable != 1) {
                            $break_hours += $break->duration;
                        }
                    }
                }
            } else {
                $break_hours = 1;
            }

            $total += Carbon::parse($o->time_in)->floatDiffInHours($o->time_out, false) - $break_hours;
        }

        return number_format($total, 2);
    }

    /**
     * getOB Total Days
     *
     * @param object $ob
     * @param string $start
     * @param string $end
     * @return void
     */
    public function getOBTotalDays(object $ob, string $start, string $end)
    {
        return $ob->whereBetween(
            DB::raw('date(date)'),
            [date('Y-m-d', strtotime($start)), date('Y-m-d', strtotime($end))]
        )
        ->where('employee_id', $ob->employee_id)->get();
    }

    /**
     * actiivity OB
     * @param string $start
     * @param string $end
     * @return array
     */
    public function activityOfficialBusiness(string $start, string $end): array
    {
        $data = array();
        $officialBusiness = $this->ob($start, $end);
        foreach ($officialBusiness->get() as $key => $ob) {
            $stats = check_approver($ob);
            if (
                model_approval_status($stats) == 'Pending'
                || model_approval_status($stats) == 'Rejected'
            ) {
                $data[] = new ResourcesOB($ob);
            }
        }
        return $data;
    }
}

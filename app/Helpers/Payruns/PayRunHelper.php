<?php

namespace App\Helpers\Payruns;

use App\Helpers\Payruns\AttendanceHelper;
use App\PayRun;
use Carbon\Carbon;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class PayRunHelper
{

    /**
     * @var AttendanceHelper
     */
    protected $attendanceHelper;

    public function __construct(AttendanceHelper $attendanceHelper)
    {
        $this->attendanceHelper = $attendanceHelper;
    }
    
    /**
     * Get Payruns
     * @return array
     */
    public function getPayruns(Request $request): array
    {
        $today = Carbon::today();
        
        if ($request->year != null) {
            $year = $request->year;
        } else {
            $year = Carbon::createFromFormat('Y', $today->year)->format('Y');
        }

        $data = array();
        $month = Carbon::createFromFormat('m', 1)->format('m');

        for ($i = 1; $i <= 12; $i++) {
            for ($j = 1; $j <= 2; $j++) {
                $data = $this->listPeriodData($month, $year, $data, $j);
            }
            $month++;
        }
        $data = $this->addCutoffNextYr($data, $year + 1);

        $start_period = array();
        $checkCurrent = false;
        foreach ($data as $key => $row) {
            if ($row['cutoff'] == 'Current') {
                $checkCurrent = true;
            }
            $start_period[$key] = $row['start_period'];
        }

        array_multisort($start_period, SORT_DESC, $data);
        
        $merges = [];
        if (!$checkCurrent) {
            $start = date('Y-m-d', strtotime($data[0]['start_period'] . '-1 year'));
            $end =  date('Y-m-d', strtotime($data[0]['end_period'] . '-1 year'));
            $payment_date = date('Y-m-d', strtotime($data[0]['payment_date'] . '-1 year'));

            $merges[] = [
                "cutoff" => 'Current',
                "month" => 12,
                "approval_status" => $this->getApprovedStatus($start, $end),
                "payroll_period" => date('F d, Y', strtotime($start)) . ' - ' . date('F d, Y', strtotime($end)),
                "start_period" => $start,
                "end_period" => $end,
                "payment_date" => $payment_date,
                "added_info"    => $this->timeSheetTotal($start, $end)
            ];
        }

        return array_merge($data, $merges);
    }

    /**
     * Add Cut-Off by next year
     * @param array $data
     * @param string $yr
     * @return array
     */
    public function addCutoffNextYr(array $data, string $yr): array
    {
        if (count($data) < 10) {
            $count_added = (10 - count($data)) / 2;
            for ($i = 1; $i <= $count_added; $i++) {
                for ($j = 1; $j <= 2; $j++) {
                    $data = $this->listPeriodData($i, $yr, $data, $j);
                }
            }
        }
        return $data;
    }

    /**
     * Payroll Period
     * @param int $month
     * @param string $year
     * @param array $data
     * @param int $index
     * @return array
     */
    public function listPeriodData(
        int $month,
        string $year,
        array $data,
        int $index
    ): array {
        $start = Carbon::createFromFormat('Y-m-d', $year . '-' . $month . '-' . ($index == 1 ? 6 : 20));
        $end = Carbon::createFromFormat('Y-m-d', $year . '-' . ($index == 1 ? $month : $month + 1) . '-'
                . ($index == 1 ? 19 : 5));

        $data[] = [
            "cutoff"         => $this->getCurrentCutoff($start, $end),
            "month"          => $month,
            "approval_status" => $this->getApprovedStatus(
                date("Y-m-d", mktime(0, 0, 0, $month, ($index == 1 ? '6' : '20' ), $year)),
                ($index == 1 ? date("Y-m-d", mktime(0, 0, 0, $month, 19, $year)) :
                    $this->addMonth(date("Y-m", strtotime($year . '-' . $month)) . '-5') )
            ),
            "payroll_period" => $this->periodRange(
                date("F d, Y", mktime(0, 0, 0, $month, ($index == 1 ? '6' : '20' ), $year)),
                ($index == 1 ? date("F d, Y", mktime(0, 0, 0, $month, 19, $year)) :
                date("F d, Y", strtotime('+1 month', strtotime($year . '-' . $month . '-5'))) ),
                $index
            ),
            "start_period"   => date("Y-m-d", mktime(0, 0, 0, $month, ($index == 1 ? '6' : '20' ), $year)),
            "end_period"     => ($index == 1 ? date("Y-m-d", mktime(0, 0, 0, $month, 19, $year)) :
                            $this->addMonth(date("Y-m", strtotime($year . '-' . $month)) . '-5') ),
            "payment_date"   => ($index == 1 ? date("Y-m-t", strtotime($year . '-' . $month)) :
                            date("Y-m-d", strtotime('+1 month', strtotime($year . '-' . $month . '-15'))) ),
            "added_info"    => $this->timeSheetTotal($start, $end)
        ];

        return $data;
    }

    /**
     * Get Current Cut-off Status
     * @param string $from
     * @param string $to
     * @return string
     */
    public function getCurrentCutoff(string $from, string $to): string
    {
        $today = Carbon::today();
        $end_day = date('d', strtotime($to));
        $day_today = date('d', strtotime($today));

        if (date('Y-m', strtotime($today)) == date('Y-m', strtotime($to))) {
            if ($end_day == '05' && $day_today <= '15') {
                return 'Current';
            }

            if ($end_day == '19' && $day_today > '15') {
                return 'Current';
            }
        }
        return 'Pending';
    }

    /**
     * Check if have process payrun already Approved
     * @param string $start
     * @param string $end
     * @return void
     */
    public function getApprovedStatus(string $start, string $end)
    {
        $status = null;
        $payroll = PayRun::where('start_period', $start)->where('end_period', $end)->get();
        foreach ($payroll as $key => $payrun) {
            if (check_approver($payrun) == null || model_approval_status(check_approver($payrun)) == 'Approved') {
                $status = 'Approved';
            } else {
                $status = 'Pending';
            }
        }

        return $status;
    }

    /**
     * Add Month
     * @param string $date
     * @return string
     */
    public function addMonth(string $date)
    {
        $d = new DateTime($date);
        $d->add(new DateInterval('P1M'));

        return $d->format('Y-m-d');
    }

    /**
     * Period range
     * @param string $start
     * @param string $end
     * @return string
     */
    public function periodRange(string $start, string $end, $cutoff): string
    {
        $format = "F d";
        if (date("Y", strtotime($start)) != date("Y", strtotime($end))) {
            $format = "F d, Y";
        }

        return ($cutoff == 1 ? date($format, strtotime($start)) . ' - ' . date('d, Y', strtotime($end)) :
        date($format, strtotime($start)) . ' - ' . date('F d, Y', strtotime($end)));
    }

    /**
     * timesheet total record
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function timeSheetTotal(string $start, string $end)
    {
        $getAttendance = $this->attendanceHelper->getAttendance($start, $end);

        $totalHours = 0;

        foreach ($getAttendance['data'] as $attendance) {
            $totalHours += $attendance['billable_hrs'];
        }

        return [
            'total_hours' => $totalHours,
            'employee_count' => $getAttendance['total_employee']
        ];
    }
}

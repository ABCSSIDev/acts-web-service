<?php

namespace App\Helpers\Payruns;

use App\Attendance;
use App\DailyTimeRecord;
use Illuminate\Support\Facades\DB;

use function App\TimeAttendance\check_shift_schedule;
use function App\TimeAttendance\get_shift_employee;
use function App\TimeAttendance\check_dtr_verified;

class AttendanceHelper
{
    /**
     * @var float
     */
    protected $billable_hours = 0.00;
    
    /**
     * Get Attendances
     * @param string $start
     * @param string $end
     * @return array
     */
    public function getAttendance(string $start, string $end): array
    {
        $data = array();

        $total_employee = 0;

        $employee_attendance = $this->getPeriodAttendance($start, $end);

        $attendances = $employee_attendance->whereNotNull('end_time')->groupBy('employee_id')->get();

        foreach ($attendances as $key => $attendance) {
            $this->billable_hours = 0;
            $data[] = [
                'employee_id' => $attendance->getEmployee->id,
                'employee_name' => $attendance->getEmployee->first_name . ' '
                    . $attendance->getEmployee->last_name,
                'total_days' => $this->computeTotalWorkingDays($start, $end, $attendance->employee_id),
                'rendered_hours' => $this->computeTotalRenderedHours($start, $end, $attendance),
                'billable_hrs'  => $this->billable_hours
            ];
            $total_employee++;
        }

        return [
            'data' => $data,
            'total_employee' => $total_employee
        ];
    }

    /**
     * Activity Attendance
     * @param string $start
     * @param string $end
     * @return array
     */
    public function getActivityAttendance(string $start, string $end): array
    {
        $data = array();
        $employee_attendance = $this->getPeriodAttendance($start, $end);
        foreach ($employee_attendance->groupBy('employee_id')->get() as $key => $attendance) {
            $employee_attendance_employee = $employee_attendance->where('employee_id', $attendance->employee_id)->get();
            foreach ($employee_attendance_employee as $key => $att) {
                if (count($this->statusAttendance($att))) {
                    $data[] = [
                        'id' => $att->id,
                        'employee_name' => $attendance->getEmployee->first_name . ' ' .
                            $attendance->getEmployee->last_name,
                        'date'  => date('F d, Y', strtotime($att->start_time)),
                        'status' => $this->statusAttendance($att)
                    ];
                }
            }
        }

        return $data;
    }
    /**
     * Get Period Attendance
     *
     * @param string $start
     * @param string $end
     *
     * @return object
     */
    public function getPeriodAttendance(string $start, string $end): object
    {
        return Attendance::whereBetween(
            DB::raw('date(start_time)'),
            [date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))]
        );
    }

    /**
     * Compute total working hours
     * @param string $attendance
     * @param string $type
     * @return float
     */
    public function computeTotalRenderedHours(
        string $start,
        string $end,
        object $attendance
    ): float {
        $total_hours = 0;
        $employee_attendance = $this->getPeriodAttendance($start, $end);
        
        $att = Attendance::where('employee_id', $attendance->employee_id)
            ->whereBetween(DB::raw('date(start_time)'), [
                date('Y-m-d', strtotime($start)),
                date('Y-m-d', strtotime($end))
            ])
            ->whereNotNull('end_time')
            ->get();
        
        $dates = array();

        foreach ($att as $key => $time_att) {

            $date = date('Y-m-d', strtotime($time_att->start_time));

            if (!in_array($date, $dates)) {
                $dates[] = $date;

                $attendance_emp = Attendance::where(
                    DB::raw('date(start_time)'),
                    $date
                )->where('employee_id', $attendance->employee_id);
    
                $check = $this->checkSameDayTimeinAttendance($attendance_emp, $date);
                
                if ($check['ifSameDayAttendance']) {
                    $total_hours += $check['total_hours'];
                } else {
                    $total_hours += (strtotime($time_att->end_time) -
                             strtotime($time_att->start_time)) / 3600;
                }

                if (!check_dtr_verified($date, $attendance->employee_id)) {
                    continue;
                }
                
                $this->computeTotalBillableHours($attendance, $total_hours, $date);
            }
        }
        return round($total_hours, 2);
    }


    /**
     * Compute Total Hours
     * @param $attendance
     * @param $total_hours
     * @param $date
     * @return void
     */
    public function computeTotalBillableHours($attendance, $total_hours, $date)
    {
        if (check_shift_schedule($date, $attendance->employee_id)) {
            if ($total_hours > 8) {
                $this->billable_hours += 8.00;
            } else {
                $this->billable_hours += $total_hours;
            }
        }
    }

    /**
     * Check Multiple Same Day Time-in
     * @param $attendance
     * @param $date
     * @return void
     */
    public function checkSameDayTimeinAttendance($attendance, $date)
    {
        $ifSameDayAttendance = false;
        $total_hours = 0;
        $getSameDayTimein = $attendance->where(DB::raw('date(start_time)'), $date)->whereNotNull('end_time');

        if (count($getSameDayTimein->get()) > 0) {
            $ifSameDayAttendance = true;
            foreach ($getSameDayTimein->get() as $key => $getAttendance) {
                $total_hours += (strtotime($getAttendance->end_time) -
                    strtotime($getAttendance->start_time)) / 3600;
            }
        }

        return [
            'total_hours' => $total_hours,
            'ifSameDayAttendance' => $ifSameDayAttendance,
            'attendanceCheck' => $getSameDayTimein
        ];
    }

    /**
     * Compute total working days
     * @param string $start
     * @param string $end
     * @param string $employee_id
     * @return int
     */
    public function computeTotalWorkingDays(
        string $start,
        string $end,
        string $employee_id
    ): int {
        $attendance = Attendance::whereBetween(
            DB::raw('date(start_time)'),
            [date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))]
        )->whereNotNull('end_time')->where('employee_id', $employee_id)
            ->groupBy(DB::raw('date(start_time)'))->get();

        return count($attendance);
    }

    /**
     * status Attendance
     *
     * @param [type] $attendance
     * @param [type] $schedule
     *
     * @return array
     */
    public function statusAttendance($attendance): array
    {
        $statuses = array();

        if ($attendance->end_time == null) {
            array_push($statuses, 'No Time-out'); // check if attendance have time-out
        }
        
        return $statuses;
    }
}

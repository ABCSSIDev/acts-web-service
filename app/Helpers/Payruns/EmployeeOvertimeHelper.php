<?php

namespace App\Helpers\Payruns;

use App\Attendance;
use App\EmployeeOvertime;
use App\Http\Resources\EmployeeOvertime as EmployeeOvertimeResource;
use App\OvertimeType;
use Illuminate\Support\Facades\DB;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class EmployeeOvertimeHelper
{

    /**
     * get Employee Overtime
     * @param string $start
     * @param string $end
     * @return array
     */
    public function getOvertimeEmployee(string $start, string $end): array
    {
        $data = array();
        $employee_overtime = $this->overtime($start, $end);

        foreach ($employee_overtime->groupBy('employee_id')->get() as $key => $overtime) {
            if (
                check_approver($overtime) == null
                || model_approval_status(check_approver($overtime)) == 'Approved'
            ) {
                $checkAttendance = Attendance::where('employee_id', $overtime->employee_id)->whereBetween(
                    DB::raw('date(start_time)'),
                    [
                        date('Y-m-d', strtotime($start)),
                        date('Y-m-d', strtotime($end))
                    ]
                )->first();
                    
                if ($checkAttendance && $checkAttendance->end_time != null) {
                    $data[] = [
                        'employee_id' => $overtime->employee_id,
                        'employee_name' => $overtime->getEmployee->first_name . ' ' .
                            $overtime->getEmployee->last_name,
                        'overtime'  => $this->computeOvertime($overtime)
                    ];
                }
            }
        }
        return $data;
    }

    /**
     * Overtime
     * @param string $start
     * @param string $end
     * @return object
     */
    public function overtime(string $start, string $end): object
    {
        return EmployeeOvertime::whereBetween(
            DB::raw('date(overtime_date)'),
            [date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))]
        );
    }

    /**
     * Compute Overtime
     * @param $overtime
     * @return array
     */
    public function computeOvertime($overtime): array
    {
        $ot_type = OvertimeType::where('type', 2)->get();
        $data = array();
        foreach ($ot_type as $key => $ot) {
            $data[] = [
                'overtime_type_id' => $ot->id,
                'overtime_type' => $ot->code,
                'total_hours' => number_format(round($this->computeOtType($overtime, $ot->id), 2), 2)
            ];
        }

        // foreach ($data as $key => $value) {
        //     $this->total_ot_hrs = $this->total_ot_hrs + $value['total_hours'];
        // }

        return $data;
    }

    public function getTotalOtHours(string $start, string $end)
    {
        $total_ot_hours = 0;
        $data = array();
        $data =  $this->getOvertimeEmployee($start, $end);

        foreach ($data as $key => $value) {
            foreach ($value['overtime'] as $key => $overtime) {
                $total_ot_hours += $overtime['total_hours'];
            }
        }

        return $total_ot_hours;
    }

    /**
     * compute Ot per Type
     *
     * @param [type] $overtime
     * @param [type] $type_id
     *
     * @return float
     */
    public function computeOtType($overtime, $type_id): float
    {
        $hours_ot = 0;
        $break_hours = 0;
        $total = 0;
        $overtime = EmployeeOvertime::where('employee_id', $overtime->employee_id)->where('ot_id', $type_id)->get();
        foreach ($overtime as $key => $ot) {
            $attendance = Attendance::where(
                DB::raw('date(start_time)'),
                date('Y-m-d', strtotime($ot->overtime_date))
            )->orderBy('start_time', 'DESC')->first();

            if ($attendance) {
                $att_start = date('Y-m-d H:i:s', strtotime("$ot->overtime_date $ot->start_time"));
                $attendance_compute = (strtotime($attendance->end_time) - strtotime($att_start)) / 3600;

                if ($attendance_compute > $ot->rendered_hours) {
                    $hours_ot += $ot->rendered_hours;
                } else {
                    $hours_ot += $attendance_compute;
                }
            }
        }
        return round($hours_ot, 2);
    }

    /**
     * activity OT
     *
     * @param string $start
     * @param string $end
     *
     * @return ResourceCollection
     */
    public function getActivityOvertime(string $start, string $end)
    {
        $data = array();
        $overtimes = $this->overtime($start, $end)->get();
        foreach ($overtimes as $key => $ot) {
            $stats = check_approver($ot);
            if (model_approval_status($stats) == 'Pending' || model_approval_status($stats) == 'Rejected') {
                $data[] = new EmployeeOvertimeResource($ot);
            }
        }

        return $data;
    }
}

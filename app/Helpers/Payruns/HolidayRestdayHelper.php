<?php 

namespace App\Helpers\Payruns;

use App\Attendance;
use App\Helpers\Payruns\AttendanceHelper;
use App\Http\Resources\RestdayHoliday as RestdayHolidayResource;
use App\OvertimeType;
use App\RestdayHoliday;
use Illuminate\Support\Facades\DB;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class HolidayRestdayHelper
{
    /**
     * @var AttendanceHelper
     */
    protected $attendanceHelper;

    public function __construct(AttendanceHelper $attendanceHelper)
    {
        $this->attendanceHelper = $attendanceHelper;
    }
    
    /**
     * getRestday
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function getRestDay(string $start, string $end): array
    {
        $data = array();
        $restdayHoliday = RestdayHoliday::whereBetween(
            DB::raw('date(restday_holiday_date)'),
            [date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))]
        );

        foreach ($restdayHoliday->groupBy('employee_id')->get() as $key => $day) {
            if (
                check_approver($day) == null
                || model_approval_status(check_approver($day)) == 'Approved'
            ) {
                $data[] = [
                    'employee_id' => $day->employee_id,
                    'employee_name' => $day->employees->first_name . ' ' .
                        $day->employees->last_name,
                    'restday'  => $this->computeRestdayHoliday($day)
                ];
            }
        }
        return $data;
    }

    /**
     * Compute Restday/holiday
     * @param $overtime
     *
     * @return array
     */
    public function computeRestdayHoliday($restdayHoliday): array
    {
        $ot_type = OvertimeType::where('type', 1)->get();
        $data = array();
        foreach ($ot_type as $key => $ot) {
            $data[] = [
                'restday_type_id' => $ot->id,
                'restday_type' => $ot->code,
                'total_hours' => number_format(round($this->computeRestdayOt($restdayHoliday, $ot->id), 2), 2)
            ];
        }

        return $data;
    }

    /**
     * compute Ot per Type
     *
     * @param $overtime
     * @param $type_id
     *
     * @return float
     */
    public function computeRestdayOt($restdayHoliday, $type_id): float
    {
        $hours_ot = 0;
        $break_hours = 0;
        $total = 0;
        $restday = RestdayHoliday::where('employee_id', $restdayHoliday->employee_id)->where('ot_id', $type_id)->get();
        foreach ($restday as $key => $ot) {
            $attendance = Attendance::where(
                DB::raw('date(start_time)'),
                date('Y-m-d', strtotime($ot->restday_holiday_date))
            )->where('employee_id', $restdayHoliday->employee_id)->first();

            if ($attendance) {
                $date = date('Y-m-d', strtotime($ot->restday_holiday_date));
                $combinedDT = date('Y-m-d H:i:s', strtotime("$ot->restday_holiday_date $ot->start_time"));

                $attendance_emp = Attendance::where(
                    DB::raw('date(start_time)'),
                    date('Y-m-d', strtotime($ot->restday_holiday_date))
                )->where('employee_id', $restdayHoliday->employee_id);

                $check = $this->attendanceHelper->checkSameDayTimeinAttendance($attendance_emp, $date);

                if ($check['ifSameDayAttendance']) {
                    $attendance_compute = $check['total_hours'];
                } else {
                    $attendance_compute = (strtotime($attendance->end_time) -
                            strtotime($combinedDT)) / 3600; // computation of total working hours per employee
                }
                
                // $att_end = date('H:i:s', strtotime($attendance->end_time));
                // $attendance_compute = (
                //     strtotime($att_end) -
                //     strtotime($ot->start_time)) / 3600; // computation of total ot hours per OT type

                if ($attendance_compute > $ot->rendered_hours && $ot->rendered_hours != null) {
                    $hours_ot += $ot->rendered_hours;
                } else {
                    $hours_ot += $attendance_compute;
                }

                //$break_hours = $this->computeBreakHours($attendance->getAttendanceBreak);
                //$hours_ot = $hours_ot - round($break_hours, 2);
            }
        }
        return round($hours_ot, 2);
    }

    /**
     * activity restday/holiday
     *
     * @param string $start
     * @param string $end
     *
     * @return ResourceCollection
     */
    public function activityRestdayHoliday(string $start, string $end)
    {
        $data = array();
        $restday = RestdayHoliday::whereBetween(
            DB::raw('date(restday_holiday_date)'),
            [date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))]
        )->get();
        foreach ($restday as $key => $ot) {
            $stats = check_approver($ot);
            if (model_approval_status($stats) == 'Pending' || model_approval_status($stats) == 'Rejected') {
                $data[] = new RestdayHolidayResource($ot);
            }
        }

        return $data;
    }
}

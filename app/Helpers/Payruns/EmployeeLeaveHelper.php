<?php

namespace App\Helpers\Payruns;

use App\EmployeeLeave;
use Illuminate\Support\Facades\DB;

use function App\Approval\check_approver;
use function App\Approval\model_approval_status;

class EmployeeLeaveHelper
{

     /**
      * get Leave Employee
      * @param string $start
      * @param string $end
      * @return array
      */
    public function getLeaveEmployee(string $start, string $end): array
    {
        $data = array();
        $employee_leave = $this->leaves($start, $end);
        foreach ($employee_leave->get() as $key => $leave) {
            if (
                check_approver($leave) == null
                || model_approval_status(check_approver($leave)) == 'Approved'
            ) {
                $data[] = [
                    'leave_id' => $leave->id,
                    'employee_id' => $leave->employee_id,
                    'employee_name' => $leave->getEmployee->first_name . ' '
                        . $leave->getEmployee->last_name,
                    'leave_date' => date('M d, Y', strtotime($leave->start_date)) . ' - '
                        . date('M d, Y', strtotime($leave->end_date)),
                    'total_days' => $leave->total_days,
                    'billable' => ($leave->getLeaveType->type == 1 ? 'Yes' : 'No'),
                    'leave_type' => $leave->getLeaveType->name,
                    'leave_hours' => ($leave->total_days * 8.00),
                ];
            }
        }

        return $data;
    }

    /**
     * Leave
     *
     * @param string $start_date
     * @param string $end_date
     *
     * @return object
     */
    public function leaves(string $start_date, string $end_date): object
    {
        return EmployeeLeave::whereBetween(
            DB::raw('date(start_date)'),
            [date('Y-m-d', strtotime($start_date)),
            date('Y-m-d', strtotime($start_date))]
        )->orwhereBetween(
            DB::raw('date(end_date)'),
            [date('Y-m-d', strtotime($start_date)),
            date('Y-m-d', strtotime($end_date))]
        )->orderBy('employee_id', 'ASC');
    }

    /**
     * activity Leave
     *
     * @param string $start
     * @param string $end
     *
     * @return array
     */
    public function activityLeave(string $start, string $end): array
    {
        $data = array();
        $leave_pending = $this->leaves($start, $end);
        foreach ($leave_pending->get() as $key => $leave) {
            $stats = check_approver($leave);
            if (model_approval_status($stats) == 'Pending' || model_approval_status($stats) == 'Rejected') {
                $data[] = [
                    'employee_name' => $leave->getEmployee->first_name . ' ' .
                        $leave->getEmployee->last_name,
                    'leave_date' => date('F d, Y', strtotime($leave->start_date)) .
                        ' - ' . date('F d, Y', strtotime($leave->end_date)),
                    'status' => model_approval_status($stats),
                    'total_days' => $leave->total_days,
                    'billable' => ($leave->getLeaveType->type == 1 ? 'Yes' : 'No'),
                    'leave_type' => $leave->getLeaveType->name,
                    'leave_hours' => ($leave->total_days * 8.00),
                ];
            }
        }

        return $data;
    }
}

<?php

namespace App\Approval;

use App\Approval;
use App\ApprovalProcess;
use App\ApprovalUser;
use App\Employee;
use App\Log;
use App\User;
use Auth;

if (!function_exists('check_approver')) {
    function check_approver($param, $submittedUser = null)
    {
        $model = 'App\\' . class_basename($param);
        $user_id = Auth::user()->id;
        $is_approver = false;
        $setup = '';
        $status = array();
        $employee = Employee::where('user_id', $user_id)->first();
        if(!empty($param->employee_id)){
            $employee = Employee::where('id', $param->employee_id)->first();
        }
        $approval_process = ApprovalProcess::where('reference_type', $model)->where('status', 1)->get();
        $get_process = null;
        foreach ($approval_process as $process) {
            $departments = json_decode($process->departments);
            if (!empty($employee->getJobDetails) && in_array($employee->getJobDetails->getDesignation->getDepartment->id, $departments)) {
                $get_process = $process;
            }
        }
        if ($get_process) {
            $total_approver = count($get_process->getApprovalUser);
            //anyone who can officially approved
            if ($get_process->approved_setup == 0) {
                $setup = 'anyone';
                $get_approval = Approval::where('reference_type', $model)
                                ->where('reference_id', $param->id)
                                ->first();
                if ($get_approval) {
                    $get_name = User::where('id', $get_approval->user_id)->first();
                    $status['name'] = $get_name->name;
                    $status['approver'] = ($get_approval ? $get_approval->user_id : null );
                    $status['status'] = ($get_approval ? $get_approval->approved_status : null);
                    $status['remarks'] = $get_approval->approved_status == 0 ?
                                            'Remarks: ' . $get_approval->remarks : null;
                    $status['date_approved'] = ($get_approval ?
                        date('M/d/Y', strtotime($get_approval->created_at)) : null);
                    $status['time_approved'] = ($get_approval ?
                        date('h:i a', strtotime($get_approval->created_at)) : null);
                }
                
                $check_if_approver = ApprovalUser::where('approval_process_id', $get_process->id)
                                    ->where('user_id', $user_id)->first();

                if ($check_if_approver) {
                    if ($get_approval) {
                        $is_approver = false; //already approved by user
                    } else {
                        $check_approver_other = Approval::where('reference_type', $model)
                                ->where('reference_id', $param->id)
                                ->first();
                        if ($check_approver_other) {
                            $is_approver = false; // need for approval
                        } else {
                            $is_approver = true; // need for approval
                        }
                    }
                } else {
                    $employeeSub =  Employee::where('id', $submittedUser)->first();
                    if (!empty($employeeSub)) {
                        $userSub = User::where('id', $employeeSub->user_id)->first();
                        $user_access_role = $userSub->getUserAccess->getRole;
                        $emp = Employee::where('user_id', $user_id)->first();
                        $user_auth = User::where('id', $emp->user_id)->first();
                        if ($user_access_role->role_id == $user_auth->getUserAccess->getRole->id) {
                            $is_approver = true;
                        }
                    }
                }

            } elseif ($get_process->approved_setup == 1) {
                if ($get_process->order == 0) {
                    //sequential
                    $setup = 'sequential';
                    $count_approver = 0;
                    $count_approved = 1;
                    $approval_users = ApprovalUser::where('approval_process_id', $get_process->id)
                                    ->orderBy('seq_order', 'asc')->get();
                    $approval_order = ApprovalUser::where('approval_process_id', $get_process->id)
                                    ->where('user_id', $user_id)->first();
                    if ($approval_order || $user_id == 1) {
                        $exist_approver = 1;
                        foreach ($approval_users as $key => $user) {
                            $count_approver++;
                            
                            $approved_user = Approval::where('reference_type', $model)
                                    ->where('reference_id', $param->id)
                                    ->where('user_id', $user->user_id)
                                    ->first();
                            
                                
                            if ($approved_user  && $approved_user->approved_status == 1) {
                                $count_approved++;
                            }
                                                    
                            if ($approved_user) {
                                $approver_name = User::where('id', $approved_user->user_id)->first();
                                $status[] = [
                                    'approver' => $user->user_id,
                                    'seq_order' => $count_approver,
                                    'status' => $approved_user->approved_status,
                                    'name' => $approver_name->name,
                                    'remarks' => $approved_user->approved_status == 0 ?
                                        'Remarks: ' . $approved_user->remarks : null,
                                    'date_approved' => date('m/d/Y', strtotime($approved_user->created_at)),
                                    'time_approved' => date('h:i a', strtotime($approved_user->created_at))
                                ];
                                $exist_approver = 1;
                            } elseif ($exist_approver == 1) {
                                $status[] = [
                                            'approver' => null,
                                            'seq_order' => null,
                                            'status' => null,
                                            'name' => null,
                                            'date_approved' => null,
                                            'time_approved' => null
                                        ];
                            }
                        }
                        //special case for administrator
                        if ($user_id == 1) {
                            $is_approver = false;
                        } else {
                            $order = $approval_order->seq_order; // user approval order list

                            if ($count_approved == $order) {
                                $is_approver = true;
                            }
                        }
                        
                        //check if theres one rejected the data
                        $reject = Approval::where('reference_type', $model)
                                            ->where('reference_id', $param->id)
                                            ->where('approved_status', 0)
                                            ->get();
                        if (count($reject) != 0) {
                            $is_approver = false;
                        }
                    }
                } elseif ($get_process->order == 1) {
                    //parallel
                    $setup = 'parallel';
                    $check_if_approver = ApprovalUser::where('approval_process_id', $get_process->id)
                                                ->where('user_id', $user_id)->first();
                    if ($check_if_approver) {
                        $approved_user = Approval::where('reference_type', $model)
                                ->where('reference_id', $param->id)
                                ->where('user_id', $user_id)
                                ->first();
                        if ($approved_user) {
                            $is_approver = false;
                        } else {
                            //check if theres one rejected the data
                            $reject = Approval::where('reference_type', $model)
                                ->where('reference_id', $param->id)
                                ->where('approved_status', 0)
                                ->get();
                            if (count($reject) == 0) {
                                $is_approver = true;
                            } else {
                                $is_approver = false;
                            }
                        }
                    }
                    $count_approver = 0;
                    $approval_order = ApprovalUser::where('approval_process_id', $get_process->id)->get();
                    $exist_approver = 1;
                    foreach ($approval_order as $key => $user) {
                        $count_approver++;
                        $approved_user = Approval::where('reference_type', $model)
                                ->where('reference_id', $param->id)
                                ->where('user_id', $user->user_id)
                                ->first();
                        
                        if ($approved_user) {
                            $approver_name = User::where('id', $approved_user->user_id)->first();
                            $status[] = [
                                'approver' => $user->user_id,
                                'seq_order' => $count_approver,
                                'status' => $approved_user->approved_status,
                                'name' => $approver_name->name,
                                'remarks' => $approved_user->approved_status == 0 ?
                                        'Remarks: ' . $approved_user->remarks : null,
                                'date_approved' => date('M/d/Y', strtotime($approved_user->created_at)),
                                'time_approved' => date('h:i a', strtotime($approved_user->created_at))
                            ];
                            $exist_approver = 1;
                        } elseif ($exist_approver == 1) {
                            $status[] = [
                                        'approver' => null,
                                        'seq_order' => null,
                                        'status' => 2,
                                        'name' => null,
                                        'date_approved' => null,
                                        'time_approved' => null
                                    ];
                        }
                    }
                }
            }
            return [
                'approver' => $is_approver,
                'setup' => $setup,
                "model" => $model,
                "approval_status" => $status
            ];
        }
    }
}

if (!function_exists('model_approval_status')) {
    function model_approval_status($param)
    {
        if ($param != null) {
            $status = '';
            //status - pending for approval, rejected, approved
            if ($param['setup'] == 'parallel' || $param['setup'] == 'sequential') {
                $count = 0;
                if (count($param['approval_status']) > 0) {
                    foreach ($param['approval_status'] as $key => $value) {
                        if ($param['setup'] == 'sequential') {
                            if (is_null($value['status'])) {
                                $status = 'Pending';
                                break;
                            } elseif ($value['status'] == 0) {
                                $status = 'Rejected';
                                break;
                            } else {
                                $count = $count + 1;
                            }
                        } elseif ($param['setup'] == 'parallel') {
                            if ($value['status'] == 2) {
                                $status = 'Pending';
                            } elseif ($value['status'] == 0) {
                                $status = 'Rejected';
                                break;
                            } else {
                                $count = $count + 1;
                            }
                        }
                    }
                }
                
                if (count($param['approval_status']) == 0) {
                    $status = 'Pending';
                } elseif ($count == count($param['approval_status'])) {
                    $status = 'Approved';
                }
            } else {
                if (count($param['approval_status']) == 0) {
                    $status = 'Pending';
                } elseif ($param['approval_status']['status'] == 0) {
                    $status = 'Rejected';
                } else {
                    $status = 'Approved';
                }
            }
            return $status;
        }
    }
}

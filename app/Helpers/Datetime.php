<?php

namespace App\Datetime;

use App\EmployeeOvertime;
use App\Holiday;
use App\OvertimeType;
use App\RestdayHoliday;

if (!function_exists('check_rest_day')) {

    function check_rest_day($date)
    {
        $rest_day_array = ['6','7'];
        $date_filed = date('N', strtotime($date));

        return in_array($date_filed, $rest_day_array);
    }
}

if (!function_exists('check_for_holidays')) {
    function check_for_holidays($date)
    {
        $month = date('m', strtotime($date));
        $day = date('d', strtotime($date));
        $holiday = Holiday::whereMonth('holiday_date', $month)
            ->whereDay('holiday_date', $day)->first();

        if ($holiday) {
            if (!$holiday->is_recurring) {
                //if holiday not recurring
                if ($holiday->holiday_date == $date) {
                    return $holiday->holiday_type;
                }
                return 0;
            } else {
                //if holiday recurring
                return $holiday->holiday_type;
            }
        } else {
            return 0;
        }
    }
}

if (!function_exists('get_overtime_rate')) {

    function get_overtime_rate($rest_day, $holiday)
    {
        $ot_code = '';
        if ($rest_day) {
            $ot_code = EmployeeOvertime::OT_TYPES['REST_DAY_OT'];
            $rest_day_code = RestdayHoliday::RESTDAY_TYPES['REST_DAY'];
        } elseif ($holiday == 2) {
            $ot_code = EmployeeOvertime::OT_TYPES['SPE_NON_WORKING_OT'];
            $rest_day_code = RestdayHoliday::RESTDAY_TYPES['SPE_NON_WORKING'];
        } elseif ($rest_day && $holiday == 2) {
            $ot_code = EmployeeOvertime::OT_TYPES['SPE_NON_WORKING_REST_DAY_OT'];
            $rest_day_code = RestdayHoliday::RESTDAY_TYPES['SPE_NON_WORKING_REST_DAY'];
        } elseif ($holiday == 1) {
            $ot_code = EmployeeOvertime::OT_TYPES['REG_HOLIDAY_OT'];
            $rest_day_code = RestdayHoliday::RESTDAY_TYPES['REG_HOLIDAY'];
        } elseif ($holiday == 1 && $rest_day) {
            $ot_code = EmployeeOvertime::OT_TYPES['REG_HOLIDAY_REST_DAY_OT'];
            $rest_day_code = RestdayHoliday::RESTDAY_TYPES['REG_HOLIDAY_REST_DAY'];
        } else {
            $ot_code = EmployeeOvertime::OT_TYPES['NORMAL_DAY_OT'];
            $rest_day_code = '';
        }

        $ot_type = OvertimeType::where('code', $ot_code)->first();

        $rest_day_type = OvertimeType::where('code', $rest_day_code)->first();

        return array(
            'id' => $ot_type ? $ot_type->id : null,
            'rate' => $ot_type ? $ot_type->rate : null,
            'restday_id' => $rest_day_type ? $rest_day_type->id : null,
            'restday_rate' => $rest_day_type ? $rest_day_type->rate : null
        );
    }
}

if (!function_exists('dateFormatRange')) {
    function dateFormatRange($start, $end = null)
    {
        $startTime = strtotime($start);
        if ($end == null) {
            return date('M d, Y', $startTime);
        } else {
            $endTime = strtotime($end);
            if (date('Y', $startTime) != date('Y', $endTime)) {
                return date('M j, Y', $startTime) . " to " . date('M j, Y', $endTime);
            } else {
                if ((date('j', $startTime) == 1 ) && (date('j', $endTime) == date('t', $endTime))) {
                    return date('M', $startTime) . " to " . date('M, Y', $endTime);
                } else {
                    if (date('m', $startTime) != date('m', $endTime)) {
                        return date('M j', $startTime) . " to " . date('M j, Y', $endTime);
                    } else {
                        return date('M j', $startTime) . " to " . date('j, Y', $endTime);
                    }
                }
            }
        }
    }
}

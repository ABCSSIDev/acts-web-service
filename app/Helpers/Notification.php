<?php
namespace App\Notification;

//function setup for date format (1 min ago)
if (!function_exists('notificationDate')) {
    function notificationDate($date_created) 
    {
        date_default_timezone_set('Asia/Manila');
        $str = strtotime($date_created);
        $today = strtotime(date('Y-m-d H:i:s'));
        // It returns the time difference in Seconds...
        $time_difference = $today-$str;
        // To Calculate the time difference in Years...
        $years = 60*60*24*365;
        // To Calculate the time difference in Months...
        $months = 60*60*24*30;
        // To Calculate the time difference in Days...
        $days = 60*60*24;
        // To Calculate the time difference in Hours...
        $hours = 60*60;
        // To Calculate the time difference in Minutes...
        $minutes = 60;

        if (intval($time_difference/$years) > 1) {
            return intval($time_difference/$years)." years ago";
        } else if(intval($time_difference/$years) > 0) {
            return intval($time_difference/$years)." year ago";
        } else if(intval($time_difference/$months) > 1) {
            return intval($time_difference/$months)." months ago";
        } else if(intval(($time_difference/$months)) > 0) {
            return intval(($time_difference/$months))." month ago";
        } else if(intval(($time_difference/$days)) > 1) {
            return intval(($time_difference/$days))." days ago";
        } else if (intval(($time_difference/$days)) > 0) {
            return intval(($time_difference/$days))." day ago";
        } else if (intval(($time_difference/$hours)) > 1) {
            return intval(($time_difference/$hours))." hours ago";
        } else if (intval(($time_difference/$hours)) > 0) {
            return intval(($time_difference/$hours))." hour ago";
        } else if (intval(($time_difference/$minutes)) > 1) {
            return intval(($time_difference/$minutes))." minutes ago";
        } else if (intval(($time_difference/$minutes)) > 0) {
            return intval(($time_difference/$minutes))." minute ago";
        } else if (intval(($time_difference)) > 1) {
            return intval(($time_difference))." seconds ago";
        } else {
            return "few seconds ago";
        }
    }
}

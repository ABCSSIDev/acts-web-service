<?php

namespace App;

use App\Traits\EmployeeSalaryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeSalary extends Model
{
    use SoftDeletes;
    
    use EmployeeSalaryFilter;

    protected $table = 'hr_employee_salaries';
    protected $fillable = [
        'id',
        'employee_id',
        'type',
        'classification',
        'effective_date',
        'monthly_salary',
        'rate_per_day'
    ];
    /**
     * employee relationship
     *
     * @return Builder
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(
            Employee::class,
            'employee_id'
        )->withoutGlobalScope(RoleScope::class);
    }

    /**
     * allowances relationship
     *
     * @return HasMany
     */
    public function allowances(): HasMany
    {
        return $this->hasMany(EmployeeSalaryAllowance::class, 'salary_id');
    }

    /**
     * logs polymorphic
     *
     * @return MorphMany
     */
    public function getLogs(): MorphMany
    {
        return $this->morphMany(Log::class, "reference");
    }
}

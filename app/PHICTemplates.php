<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PHICTemplates extends Model
{
    use SoftDeletes;

    protected $table = 'hr_phic_templates';
    protected $dates = ['created_at','deleted_at'];
    protected $fillable = ['year_effective',
                           'premium_rate'
                        ];
}

<?php

namespace App;

use App\Scopes\RoleScope;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getEmployee()
    {
        return $this->hasOne(Employee::class, 'user_id', 'id')->withoutGlobalScope(RoleScope::class);
    }

    public function getUserAccess()
    {
        return $this->hasOne(UserAccess::class, 'user_id');
    }

    public function logs()
    {
        return $this->morphMany(Log::class, "reference");
    }

    public function scopeRole($query, $role)
    {
        if (!empty($role)) {
            return $query->whereHas('getUserAccess', function ($q) use ($role) {
                $q->whereHas('getRole', function ($role_query) use ($role) {
                    $role_query->where('id', '=', $role);
                });
            });
        }
        return $query;
    }

    public function scopePermission($query, $permission)
    {
        if (!empty($permission)) {
            return $query->whereHas('getUserAccess', function ($q) use ($permission) {
                $q->whereHas('getPermission', function ($permission_query) use ($permission) {
                    $permission_query->where('id', '=', $permission);
                });
            });
        }
        return $query;
    }
}

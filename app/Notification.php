<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Notification extends Model
{
    protected $table = "notifications";

    public function notifiable()
    {
        return $this->morphTo();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ElementsList extends Model
{
    protected $table = 'kamalig_element_list_values';
    protected $primaryKey = 'id';
    protected $fillable = ['element_id','name','value'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    // use SoftDeletes;
    public function getElements(){
        return $this->hasOne('App\Elements','id', 'element_id');
    }
}

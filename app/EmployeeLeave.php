<?php

namespace App;

use App\Scopes\EmployeeFormScope;
use App\Scopes\RoleScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeLeave extends Model
{
    use SoftDeletes;

    protected $table = "hr_employee_leaves";
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'employee_id', 'leave_id', 'start_date', 'end_date', 'total_days', 'reason', 'status'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    private static $employee_array = [];
    
    //Global Scope on Filtering Query to fetch only users/employees that are subordinates or peers of current user 
    // protected static function booted(){
    //     static::addGlobalScope(new EmployeeFormScope());
    // }

    public function getEmployee(){
        return $this->hasOne('App\Employee', 'id', 'employee_id')->withoutGlobalScope(RoleScope::class);
    }

    public function getLeaveType(){
        return $this->hasOne('App\LeaveType', 'id', 'leave_id');
    }

    public function getLogs()
    {
        return $this->morphMany(Log::class,"reference");
    }
    
    public function scopeRole($query, $model_name)
    {
        $user_id = Auth::id();
        if ($user_id != 1) {
            $user_access = UserAccess::where('user_id', $user_id)->first();
            if (self::checkModule($user_access->bypass_role_systems, $model_name) && self::checkIfApprover($model_name)) {
                $roles = Role::where('id', $user_access->role_id)->get();
                self::loopRoles($roles, $user_access->role_id);
                return $query->whereIn('employee_id', self::$employee_array);
            }
        }
        
        return $query;
    }

    public static function loopRoles($roles, $parentRoleId = null, $child = false)
    {
        foreach ($roles as $role) {
            if (( !empty($parentRoleId) && $role->is_shared != null ) || $role->id == $parentRoleId || !empty($child)) {
                if (!empty($role->getUserAccess)) {
                    foreach ($role->getUserAccess as $access) {
                        $employee = Employee::where('user_id', $access->user_id)->first();
                        if (!empty($employee)) {
                            array_push( self::$employee_array, $employee->id);
                        }
                    }
                }
                if (!empty($role->getChildren)) {
                    self::loopRoles($role->getChildren, null, true);
                }
            }

        }

    }

    public static function checkModule($system_list, $module_name)
    {
        $system_count = 0;
        if (!empty($system_list)) {
            $systems = json_decode($system_list);
            foreach ($systems as $system) {
                $modules = Module::where('system_id', '=', $system)->where('model_type', $module_name)->get();
                if ($modules->count() > 0) {
                    $system_count++;
                }
            }
        }

        return $system_count == 0;
    }

    public static function checkIfApprover($model_name)
    {
        $approval_users = [];
        $get_process = ApprovalProcess::where('reference_type', $model_name)->where('status', 1)->first();
        if (!empty($get_process)) {
            $approval_users = ApprovalUser::where('approval_process_id', $get_process->id)->where('user_id', Auth::user()->id)->get();
        }
        return count($approval_users) <= 0;
    }
}

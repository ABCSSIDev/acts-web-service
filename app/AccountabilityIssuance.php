<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountabilityIssuance extends Model
{
    use SoftDeletes;
    protected $table = "kamalig_accountability_issuances";
    protected $primaryKey = 'id';
    protected $fillable = ['requisition_id', 'employee_id', 'from_employee_id', 'type', 'date'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['employee', 'fromEmployee', 'issuanceItems', 'requisition', 'logs'];

    public function employee(){
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function fromEmployee(){
        return $this->belongsTo(Employee::class, 'from_employee_id', 'id');
    }

    public function requisition(){
        return $this->belongsTo(AccountabilityRequisition::class, 'requisition_id', 'id');
    }

    public function issuanceItems(){
        return $this->hasMany(IssuanceItems::class, 'issuance_id', 'id');
    }
    
    public function logs(){
        return $this->morphMany(Log::class,"reference");
    }
}
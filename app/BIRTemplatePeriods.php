<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BIRTemplatePeriods extends Model
{
    //
    use SoftDeletes;

    protected $table = 'hr_bir_template_periods';
    protected $dates = ['created_at','deleted_at','updated_at'];
    protected $fillable = ['period'];

    public function categoryDeductions()
    {
        return $this->hasOne(BIRTemplateCategoryDeductions::class, 'id');
    }

    public function salaries()
    {
        return $this->hasMany(BIRTemplateSalaries::class, 'id');
    }
}

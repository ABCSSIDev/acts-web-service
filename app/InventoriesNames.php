<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoriesNames extends Model
{
    protected $table = 'kamalig_inventory_descriptions';
    protected $primaryKey = 'id';
    protected $fillable = ['category_id', 'description'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    use SoftDeletes;
    public function getCategories(){
        return $this->hasOne('App\Categories','id', 'category_id');
    }

}

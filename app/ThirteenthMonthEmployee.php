<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThirteenthMonthEmployee extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $table = 'hr_thirteenth_month_employees';
    protected $primaryKey = 'id';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getThirteenthMonth()
    {
        return $this->hasOne('App\ThirteenthMonth', 'id', 'thirteenth_month_id');
    }

    public function getEmployee()
    {
        return $this->hasOne('App\Employee', 'id', 'employee_id');
    }
}

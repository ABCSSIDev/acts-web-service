<?php

namespace App\Scopes;

use App\ApprovalProcess;
use App\ApprovalUser;
use App\Employee;
use App\Role;
use App\UserAccess;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Auth;

class RoleScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    private static $user_array = array();
    private static $employee_array = array();

    
    public function apply(Builder $builder, Model $model)
    {
        if (Auth::user()->id == 1){
            $builder->where('deleted_at', null);
        } else {
            $user_access = UserAccess::where('user_id', Auth::user()->id)->first();
            self::pushUserRoles(Role::where('id', $user_access->role_id)->get(), $user_access->role_id);
            $modelname = 'App/'.class_basename($model);
            // $approval_process = ApprovalProcess::where('reference_type', 'LIKE', $modelname)->first();
            $approval_users = [];
            $get_process = ApprovalProcess::where('reference_type', $modelname)->where('status', 1)->first();
            if (!empty($get_process)) {
                $approval_users = ApprovalUser::where('approval_process_id', $get_process->id)->whereIN('user_id', [Auth::user()->id])->get();
            }
            if (count($approval_users) > 0) {
                $builder->where('deleted_at', null);
            } else {
                $builder->whereIN('id', self::$employee_array);
            }
        }
        
        
    }

    public static function pushUserRoles($roles, $parent_id = null, $child = false){
        foreach ($roles as $role) {
            if ( ( !empty($parent_id) && !empty($role->is_shared) ) || $role->id == $parent_id || $child ){
                if (!empty($role->getUserAccess)) {
                    foreach ($role->getUserAccess as $access) {
                        $employee = Employee::where('user_id', $access->getUser->id)->withoutGlobalScope(RoleScope::class)->first();
                        if (!empty($employee)) {
                            array_push( self::$employee_array, $employee->id);
                        }
                    }
                }
                if (!empty($role->getChildren)) {
                    self::pushUserRoles($role->getChildren, null, true);
                }
            }
        }
    }

}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BIRTemplateTaxCriterias extends Model
{
    //
    use SoftDeletes;

    protected $table = 'hr_bir_template_tax_criterias';
    protected $dates = ['created_at','deleted_at','updated_at'];
    protected $fillable = ['tax_category_id','compensation_level','prescribed_tax','percent_over'];

    public function taxCategory()
    {
        return $this->belongsTo(BIRTemplateTaxCategories::class, 'tax_category_id');
    }
}

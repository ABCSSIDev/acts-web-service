<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BIRTemplateSalaries extends Model
{
    //
    use SoftDeletes;

    protected $table = 'hr_bir_template_salaries';
    protected $dates = ['created_at','deleted_at','updated_at'];
    protected $fillable = ['period_id','category_id','exemption_id','salary_value'];

    public function period()
    {
        return $this->belongsTo(BIRTemplatePeriods::class, 'period_id');
    }

    public function category()
    {
        return $this->belongsTo(BIRTemplateCategories::class, 'category_id');
    }

    public function exemption()
    {
        return $this->belongsTo(BIRTemplateExemptions::class, 'exemption_id');
    }
}

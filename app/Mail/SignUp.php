<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SignUp extends Mailable
{

    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Please Verify your account')
            ->markdown('emails.signup')
            ->with([
                'token' => $this->user->id,
                'name' => $this->user->name,
                'email' => $this->user->email,
                'url' => env('APP_URL') . '/signup/' . base64_encode($this->user->id)
            ]);
    }
}

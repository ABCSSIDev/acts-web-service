@component('mail::message')

Hi {{$name}}, 

You recently created a ACTS BayanihanEMSID. Your ACTS BayanihanEMSID is {{$email}}.
To access your account, please click this link:<br>
<a href="{{ url($url)}}">{{url($url)}}</a>

Please note:
If you cannot access this link, copy and paste the entire URL into your browser.

The ACTS Bayanihan EMS Team

Thanks,<br>
ACableworks Technology Solutions Inc.
@endcomponent

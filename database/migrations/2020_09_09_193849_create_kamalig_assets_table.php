<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_assets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('inventory_id');
            $table->unsignedBigInteger('inventory_status_id');
            $table->unsignedBigInteger('assigned_to')->nullable();
            $table->string('image')->nullable();
            $table->string('asset_tag')->unique();
            $table->string('serial_model_no')->unique()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('inventory_id')->references('id')->on('kamalig_inventories')
                  ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('inventory_status_id')->references('id')->on('kamalig_inventory_statuses')
                  ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('assigned_to')->references('id')->on('hr_employees')->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_assets');
    }
}

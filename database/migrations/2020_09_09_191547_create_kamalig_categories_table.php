<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_categories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('category_name');
            $table->string('meta_title')->nullable();
            $table->boolean('enabled')->default(0)->comment('1=active,0=inactive');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('category_id')->references('id')->on('kamalig_categories')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_categories');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayRunItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_pay_run_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('pay_run_id');
            $table->decimal('total_late_absent_hours', 14, 2)->default(0.00);
            $table->decimal('billable_leave_hours', 14, 2)->default(0.00);
            $table->decimal('non_billable_leave_hours', 14, 2)->default(0.00);
            $table->decimal('total_billable_hours', 14, 2)->default(0.00);
            $table->decimal('total_non_billable_hours', 14, 2)->default(0.00);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('employee_id')
                ->references('id')->on('hr_employees')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('pay_run_id')
                ->references('id')->on('hr_pay_runs')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_pay_run_items');
    }
}

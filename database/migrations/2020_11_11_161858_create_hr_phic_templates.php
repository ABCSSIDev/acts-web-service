<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrPhicTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_phic_templates', function (Blueprint $table) {
            $table->id();
            $table->float('start_range');
            $table->float('end_range');
            $table->float('salary_base');
            $table->float('employer_contribution');
            $table->float('employee_contribution');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_phic_templates');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligReceivingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_receiving_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('receiving_id');
            $table->unsignedBigInteger('inventory_id');
            $table->timestamps();
            $table->decimal('quantity', 14, 2);
            $table->decimal('old_quantity', 14, 2);
            $table->foreign('receiving_id')
                ->references('id')
                ->on('kamalig_receivings')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('inventory_id')
                ->references('id')
                ->on('kamalig_inventories')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_receiving_items');
    }
}

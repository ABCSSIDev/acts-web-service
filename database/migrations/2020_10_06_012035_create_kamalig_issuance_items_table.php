<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligIssuanceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_issuance_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('issuance_id');
            $table->unsignedBigInteger('asset_id');
            $table->timestamps();
            $table->foreign('issuance_id')->references('id')
                  ->on('kamalig_accountability_issuances')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_issuance_items');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReimbursementItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_reimbursement_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('reimbursement_id');
            $table->date('date');
            $table->tinyInteger('type');
            $table->string('particulars');
            $table->string('receipt_number');
            $table->decimal('amount',14,2);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('reimbursement_id')->references('id')->on('hr_reimbursements')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_reimbursement_items');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddYearColumnInHrPhicTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hr_phic_templates', function (Blueprint $table) {
            $table->year('year_effective')->after('id');
            $table->decimal('premium_rate')->after('end_range');
            $table->dropColumn('start_range');
            $table->dropColumn('end_range');
            $table->dropColumn('salary_base');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hr_phic_templates', function (Blueprint $table) {
            $table->dropColumn('year_effective');
            $table->dropColumn('premium_rate');
            $table->float('start_range')->after('id');
            $table->float('end_range')->after('start_range');
            $table->float('salary_base')->after('end_range');
        });
    }
}

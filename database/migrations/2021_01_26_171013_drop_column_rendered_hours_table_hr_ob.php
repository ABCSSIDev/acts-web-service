<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnRenderedHoursTableHrOb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hr_ob', function (Blueprint $table) {
            $table->dropColumn('rendered_hours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hr_ob', function (Blueprint $table) {
            $table->decimal('rendered_hours',14,2)->after('time_out');
        });
    }
}

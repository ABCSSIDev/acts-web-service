<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligSubscribers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_subscribers', function (Blueprint $table) {
            $table->id();
            $table->string('subscriber_name');
            $table->string('email')->unique()->nullable();
            $table->string('home_no')->nullable();
            $table->string('other_no')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('gender')->nullable();
            $table->dateTime('birthdate')->nullable();
            $table->string('civil_status')->nullable();
            $table->string('household_held')->nullable();
            $table->longText('service_address');
            $table->longText('billing_address');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_subscribers');
    }
}

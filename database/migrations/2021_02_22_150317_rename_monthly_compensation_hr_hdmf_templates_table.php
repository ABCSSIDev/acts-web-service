<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameMonthlyCompensationHrHdmfTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hr_hdmf_templates', function (Blueprint $table) {
            $table->renameColumn('monthly_compensation', 'salary_from');
            $table->float('salary_to')->after('monthly_compensation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hr_hdmf_templates', function (Blueprint $table) {
            $table->renameColumn('salary_from', 'monthly_compensation');
            $table->dropColumn('salary_to');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligPurchaseRequestItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_purchase_request_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('purchase_request_id');
            $table->unsignedBigInteger('inventory_id');
            $table->decimal('quantity', 14, 2);
            $table->timestamps();
            $table->foreign('purchase_request_id')
                ->references('id')
                ->on('kamalig_purchase_requests')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('inventory_id')
                ->references('id')
                ->on('kamalig_inventories')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_purchase_request_items');
    }
}

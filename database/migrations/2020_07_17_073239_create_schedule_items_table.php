<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_schedule_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('schedule_id');
            $table->unsignedBigInteger('employee_id');
            $table->time('time_in')->nullable();
            $table->time('time_out')->nullable();
            $table->longText('restdays')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('schedule_id')
                ->references('id')
                ->on('hr_schedules')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            
            $table->foreign('employee_id')
                ->references('id')
                ->on('hr_employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_schedule_breaks');
    }
}

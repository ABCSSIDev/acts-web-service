<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrPaySchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_pay_schedules', function (Blueprint $table) {
            $table->id();
            $table->text('department_ids');
            $table->text('exceptions')->nullable();
            $table->string('name');
            $table->date('payroll_start_date');
            $table->boolean('calculate_salary_based')->comment('0=actual_days,1=days_month');
            $table->integer('days_per_month')->nullable();
            $table->boolean('cut_off_period')->comment('0=no_cut_offs,1=cut_offs');
            $table->date('pay_schedule')->nullable();
            $table->boolean('active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_pay_schedules');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveForeignKeyOnHrSssTemplateContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hr_sss_template_contributions', function (Blueprint $table) {
            $table->dropForeign(['sss_template_id']);
            $table->dropColumn('sss_template_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hr_sss_template_contributions', function (Blueprint $table) {
            $table->unsignedBigInteger('sss_template_id');
            $table->foreign('sss_template_id')->references('id')
                  ->on('hr_sss_templates')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}

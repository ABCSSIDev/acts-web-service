<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeExitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_employee_exits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('interviewer_id');
            $table->date('separation_date');
            $table->string('reason');
            $table->tinyInteger('exit_type')->comment('0=resignation,1=termination');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_id')->references('id')->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('interviewer_id')->references('id')->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_employee_exits');
    }
}

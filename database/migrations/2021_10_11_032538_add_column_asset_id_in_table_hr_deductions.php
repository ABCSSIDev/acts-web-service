<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAssetIdInTableHrDeductions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hr_deductions', function (Blueprint $table) {
            $table->string('asset_id')->after('reason');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table_hr_deductions', function (Blueprint $table) {
            $table->dropColumn('asset_id');
        });
    }
}

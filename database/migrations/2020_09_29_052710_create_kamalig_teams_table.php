<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_teams', function (Blueprint $table) {
            $table->id();
            $table->string('team_id');
            $table->string('team_name')->nullable();
            $table->string('team_type')->comment('bau,corpo');
            $table->boolean('active')->default(0);
            $table->string('vehicle_plateno')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_teams');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnBypassRoleUserAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_accesses', function (Blueprint $table) {
            $table->string('bypass_role_systems')->nullable()->after('permission_id');

        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_accesses', function (Blueprint $table) {
            $table->dropColumn('bypass_role_systems');
        });
    }
}

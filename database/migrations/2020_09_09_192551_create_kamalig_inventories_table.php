<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_inventories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('inventory_description_id');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('category_id');
            $table->tinyInteger('type')->comment('0=Operation,1=CPE,2=Accountable,3=Consumable');
            $table->decimal('quantity', 14, 2);
            $table->decimal('unit_price', 14, 2)->nullable();
            $table->decimal('min_stock', 14, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('inventory_description_id')->references('id')
                  ->on('kamalig_inventory_descriptions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('unit_id')->references('id')->on('kamalig_units')->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('kamalig_categories')
                  ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_inventories');
    }
}

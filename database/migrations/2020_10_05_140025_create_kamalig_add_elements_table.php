<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligAddElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_add_elements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('element_type_id');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('inventory_id')->nullable();
            $table->string('caption')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('element_type_id')->references('id')
                  ->on('kamalig_element_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('category_id')->references('id')
                  ->on('kamalig_categories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('inventory_id')->references('id')
                  ->on('kamalig_inventories')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_add_elements');
    }
}

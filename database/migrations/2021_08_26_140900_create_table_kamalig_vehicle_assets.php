<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableKamaligVehicleAssets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_vehicle_assets', function (Blueprint $table) {
            $table->id();
            $table->string('plate_number')->nullable();
            $table->unsignedBigInteger('driver')->nullable();
            $table->string('fleet_card_number')->nullable();
            $table->string('tools')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
            $table->foreign('driver')
                ->references('id')
                ->on('hr_employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_kamalig_vehicle_assets');
    }
}

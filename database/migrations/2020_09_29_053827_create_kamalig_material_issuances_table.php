<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligMaterialIssuancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_material_issuances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('team_id');
            $table->unsignedBigInteger('tech1');
            $table->unsignedBigInteger('tech2');
            $table->unsignedBigInteger('template_id');
            $table->date('date_issue');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('team_id')->references('id')
                  ->on('kamalig_teams')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tech1')->references('id')
                  ->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tech2')->references('id')
                  ->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('template_id')->references('id')
                  ->on('kamalig_issuance_templates')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_material_issuances');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnVerifyToHrScheduleEmployeeItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hr_schedule_employee_items', function (Blueprint $table) {
            $table->boolean('verified_attendance')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hr_schedule_employee_items', function (Blueprint $table) {
            $table->dropColumn('verified_attendance');
        });
    }
}

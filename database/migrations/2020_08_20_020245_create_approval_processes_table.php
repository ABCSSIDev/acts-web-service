<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApprovalProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_processes', function (Blueprint $table) {
            $table->id();
            $table->string('reference_type')->comment('from model');
            $table->string('name');
            $table->text('description');
            $table->tinyInteger('execute')->comment('0=create,1=edit,2=both');
            $table->boolean('status')->comment('0=inactive,1=active');
            $table->boolean('approved_setup')->comment('0-anyone,1=everyone');
            $table->boolean('order')->comment('0-sequential,1=parallel or Null if anyone')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approval_processes');
    }
}

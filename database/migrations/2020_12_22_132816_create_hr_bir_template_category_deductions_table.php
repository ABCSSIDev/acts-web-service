<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrBirTemplateCategoryDeductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_bir_template_category_deductions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('period_id');
            $table->unsignedBigInteger('category_id');
            $table->float('deduction_value');
            $table->float('deduction_percent');
            $table->timestamps();
            $table->foreign('period_id')->references('id')
                ->on('hr_bir_template_periods')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('category_id')->references('id')
                ->on('hr_bir_template_categories')->onUpdate('cascade')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_bir_template_category_deductions');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligIncidentReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_incident_reports', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('reported_by_employee')->nullable();
            $table->unsignedBigInteger('asset_id')->nullable();
            $table->unsignedBigInteger('team_id')->nullable();
            $table->string('reported_by')->nullable();
            $table->dateTime('incident_date');
            $table->dateTime('reported_date');
            $table->tinyInteger('incident_type')->comment('0=Vehicular,1=damage,2=lost');
            $table->text('witnesses');
            $table->string('incident_location')->nullable();
            $table->text('incident_report');
            $table->text('employee_deducted')->nullable();
            $table->float('deduction_amount', 14, 2)->nullable();
            $table->float('deduction_per_period')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('department_id')->references('id')
                  ->on('hr_departments')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('reported_by_employee')->references('id')
                  ->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('asset_id')->references('id')
                  ->on('kamalig_assets')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('team_id')->references('id')
                  ->on('kamalig_teams')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_incident_reports');
    }
}

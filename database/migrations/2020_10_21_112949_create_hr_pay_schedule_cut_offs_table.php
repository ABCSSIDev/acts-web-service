<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrPayScheduleCutOffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_pay_schedule_cut_offs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pay_schedule_id');
            $table->integer('orders');
            $table->date('date_from');
            $table->date('date_to');
            $table->integer('pay_schedule')->comment('0=end of the month');
            $table->timestamps();
            $table->foreign('pay_schedule_id')->references('id')
                ->on('hr_pay_schedules')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_pay_schedule_cut_offs');
    }
}

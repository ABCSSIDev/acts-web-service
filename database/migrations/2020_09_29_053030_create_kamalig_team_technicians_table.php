<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligTeamTechniciansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_team_technicians', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('teams_id');
            $table->unsignedBigInteger('employee_id');
            $table->tinyInteger('technician')->comment('1 = Tech 1, 2= Tech 2')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('teams_id')->references('id')
                  ->on('kamalig_teams')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('employee_id')->references('id')
                  ->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_team_technicians');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligReturnUsedMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_return_used_materials', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('issuance_item_id');
            $table->boolean('status')->comment('0 = return; 1 = used');
            $table->float('quantity', 14, 2);
            $table->string('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('issuance_item_id')->references('id')
                  ->on('kamalig_material_issuance_items')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_return_used_materials');
    }
}

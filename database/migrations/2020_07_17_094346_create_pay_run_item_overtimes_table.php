<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayRunItemOvertimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_pay_run_item_overtimes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pay_run_item_id');
            $table->unsignedBigInteger('ot_type_id');
            $table->decimal('billable_hours')->default(0.00);
            $table->decimal('non_billable_hours')->default(0.00);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pay_run_item_id')
                ->references('id')
                ->on('hr_pay_run_items')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ot_type_id')
                ->references('id')
                ->on('hr_overtime_types')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_pay_run_item_overtimes');
    }
}

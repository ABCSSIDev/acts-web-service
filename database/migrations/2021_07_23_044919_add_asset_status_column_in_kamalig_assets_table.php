<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssetStatusColumnInKamaligAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kamalig_assets', function (Blueprint $table) {
            $table->integer('asset_status')->comment('1 = used, 2 = new, 3 = damaged');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kamalig_assets', function (Blueprint $table) {
            $table->dropColumn('asset_status');
        });
    }
}

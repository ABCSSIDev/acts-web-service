<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligIncidentStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_incident_statements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('incident_id');
            $table->tinyInteger('statement_type')
                ->comment('0=summary,1=supervisor_comment,2=investigation_findings,3=investigation_analysis');
            $table->longText('statement')->nullable();
            $table->string('injury_statement')->nullable();
            $table->foreign('incident_id')->references('id')
                  ->on('kamalig_incident_reports')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_incident_statements');
    }
}

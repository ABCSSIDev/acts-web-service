<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInventoryIdInKamaligRequisitionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kamalig_requisition_items', function (Blueprint $table) {
            $table->unsignedBigInteger('inventory_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kamalig_requisition_items', function (Blueprint $table) {
            $table->dropColumn('inventory_id');
        });
    }
}

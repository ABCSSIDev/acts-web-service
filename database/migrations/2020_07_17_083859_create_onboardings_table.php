<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnboardingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_onboardings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('candidate_id');
            $table->decimal('salary_offered');
            $table->tinyInteger('status');
            $table->date('starting_date');
            $table->date('hired_date');
            $table->longText('requirements')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('candidate_id')->references('id')->on('hr_candidates')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_onboardings');
    }
}

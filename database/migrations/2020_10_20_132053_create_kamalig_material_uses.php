<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligMaterialUses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_material_uses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('inventory_id');
            $table->tinyInteger('type')->comment('0=op,1=cpe');
            $table->float('qty_used', 14, 2);
            $table->string('brandandmodel')->nullable();
            $table->string('serial_no')->nullable();
            $table->foreign('service_id')->references('id')
            ->on('kamalig_service_orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('inventory_id')->references('id')
            ->on('kamalig_inventories')->onUpdate('cascade')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_material_uses');
    }
}

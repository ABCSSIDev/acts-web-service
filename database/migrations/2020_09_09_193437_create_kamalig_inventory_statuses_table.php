<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligInventoryStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_inventory_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('status_name');
            $table->tinyInteger('status_type')->default(1)->comment('1=ready to deploy, 2=deployed,3=undeployable,4=pending,0=archived');
            $table->string('description')->nullable();
            $table->boolean('enabled')->default(0)->comment('1=active,0=inactive');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_inventory_statuses');
    }
}

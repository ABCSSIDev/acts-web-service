<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThirteenthMonthEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_thirteenth_month_employees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->year('thirteenth_month_year');
            $table->unsignedTinyInteger('category');
            $table->string('period', 30);
            $table->string('months_worked');
            $table->unsignedTinyInteger('total_months');
            $table->float('hours_render', 7, 2);
            $table->float('amount', 8, 2);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('employee_id')
                  ->references('id')
                  ->on('hr_employees')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_thirteenth_month_employees');
    }
}

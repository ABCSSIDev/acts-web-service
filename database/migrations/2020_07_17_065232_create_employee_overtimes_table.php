<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeOvertimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_employee_overtimes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('ot_id')->nullable();
            $table->decimal('rate')->nullable()->comment('only has value if ot_id is null');
            $table->date('file_date');
            $table->date('overtime_date');
            $table->time('start_time');
            $table->decimal('rendered_hours',14,2);
            $table->string('purpose');
            $table->tinyInteger('approved_status')->nullable();
            $table->unsignedBigInteger('approved_by')->nullable();
            $table->date('approved_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_id')->references('id')->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ot_id')->references('id')->on('hr_overtime_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('approved_by')->references('id')->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_employee_overtimes');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligMaterialIssuanceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_material_issuance_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('issuance_id');
            $table->unsignedBigInteger('inventory_id');
            $table->float('quantity', 14, 2)->nullable();
            $table->string('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('issuance_id')->references('id')
                  ->on('kamalig_material_issuances')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('inventory_id')->references('id')
                  ->on('kamalig_inventories')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_material_issuance_items');
    }
}

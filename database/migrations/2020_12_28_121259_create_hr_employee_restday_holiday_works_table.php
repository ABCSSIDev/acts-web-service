<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrEmployeeRestdayHolidayWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_employee_restday_holiday_works', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('ot_id')->nullable();
            $table->unsignedBigInteger('attendance_id')->nullable();
            $table->text('holidays')->nullable()->comment('has value if holiday json format');
            $table->decimal('rate')->nullable()->comment('only has value if ot_id is null');
            $table->date('restday_holiday_date');
            $table->date('file_date');
            $table->time('start_time');
            $table->decimal('rendered_hours', 14, 2)->nullable();
            $table->string('purpose')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('employee_id')
                ->references('id')
                ->on('hr_employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('ot_id')
                ->references('id')
                ->on('hr_overtime_types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('attendance_id')
                ->references('id')
                ->on('hr_attendances')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_employee_restday_holiday_works');
    }
}

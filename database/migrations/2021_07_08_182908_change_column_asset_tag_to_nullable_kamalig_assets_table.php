<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnAssetTagToNullableKamaligAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kamalig_assets', function (Blueprint $table) {
            $table->dropUnique('kamalig_assets_asset_tag_unique');
            $table->string('asset_tag')->unique()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kamalig_assets', function (Blueprint $table) {
            $table->string('asset_tag')->unique()->change();
        });
    }
}

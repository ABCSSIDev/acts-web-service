<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrScheduleEmployeeItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_schedule_employee_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('schedule_employee_id');
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('resched_employee_id')->nullable();
            $table->timestamps();

            $table->foreign('schedule_employee_id')
                ->references('id')
                ->on('hr_schedule_employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('employee_id')
                ->references('id')
                ->on('hr_employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('resched_employee_id')
                ->references('id')
                ->on('hr_employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_schedule_employee_items');
    }
}

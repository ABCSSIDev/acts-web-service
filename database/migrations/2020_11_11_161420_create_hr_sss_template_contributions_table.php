<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrSssTemplateContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_sss_template_contributions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sss_template_id');
            $table->float('salary_from');
            $table->float('salary_to');
            $table->float('employer_contribution');
            $table->float('employee_contribution');
            $table->float('employee_compensation');
            $table->foreign('sss_template_id')->references('id')
                  ->on('hr_sss_templates')->onUpdate('cascade')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_sss_template_contributions');
    }
}

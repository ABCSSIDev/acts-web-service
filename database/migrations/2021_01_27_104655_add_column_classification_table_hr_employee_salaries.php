<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnClassificationTableHrEmployeeSalaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hr_employee_salaries', function (Blueprint $table) {
            $table->boolean('classification')->comment('0 = starting_salary, 1 = adjustment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hr_employee_salaries', function (Blueprint $table) {
            $table->dropColumn('classification');
        });
    }
}

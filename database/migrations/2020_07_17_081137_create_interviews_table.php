<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_interviews', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('interview_type_id');
            $table->dateTime('interview_from');
            $table->dateTime('interview_to');
            $table->unsignedBigInteger('candidate_id');
            $table->unsignedBigInteger('interviewer_id');
            $table->string('location');
            $table->string('comments');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('interview_type_id')->references('id')->on('hr_interview_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('candidate_id')->references('id')->on('hr_candidates')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('interviewer_id')->references('id')->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_interviews');
    }
}

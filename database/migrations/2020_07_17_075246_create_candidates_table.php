<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_candidates', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('contact_number');
            $table->string('email');
            $table->string('address');
            $table->tinyInteger('source');
            $table->unsignedBigInteger('recruiter_id')->nullable();
            $table->unsignedBigInteger('potential_designation')->nullable();
            $table->unsignedBigInteger('job_opening_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('recruiter_id')->references('id')->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('potential_designation')->references('id')->on('hr_designations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('job_opening_id')->references('id')->on('hr_job_openings')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_candidates');
    }
}

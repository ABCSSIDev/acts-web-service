<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligIncidentActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_incident_actions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('incident_id');
            $table->text('actions')->nullable();
            $table->text('responsibility')->nullable();
            $table->date('completion_date')->nullable();
            $table->foreign('incident_id')->references('id')
                  ->on('kamalig_incident_reports')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_incident_actions');
    }
}

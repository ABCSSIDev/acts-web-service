<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReimbursementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_reimbursements', function (Blueprint $table) {
            $table->id();
            $table->string('subject');
            $table->unsignedBigInteger('employee_id');
            $table->date('filed_date');
            $table->string('description');
            $table->tinyInteger('approved_status')->nullable();
            $table->unsignedBigInteger('approved_by')->nullable();
            $table->date('approved_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_id')->references('id')->on('hr_employees')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('approved_by')->references('id')->on('hr_employees')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_reimbursements');
    }
}

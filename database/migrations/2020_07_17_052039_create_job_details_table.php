<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_job_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('reporting_to')->nullable();
            $table->unsignedBigInteger('designation_id');
            $table->unsignedBigInteger('employee_type_id');
            $table->tinyInteger('employee_status');
            $table->tinyInteger('source_of_hire');
            $table->dateTime('joined_date');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('employee_id')->references('id')->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('reporting_to')->references('id')->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('designation_id')->references('id')->on('hr_designations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('employee_type_id')->references('id')->on('hr_employee_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_job_details');
    }
}

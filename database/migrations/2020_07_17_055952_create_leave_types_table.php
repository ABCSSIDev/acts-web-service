<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_leave_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code');
            $table->integer('balance')->comment('total balance available');
            $table->boolean('type')->default(1)->comment('1=paid,0=unpaid');
            $table->string('description')->nullable();
            $table->decimal('encashment_percentage',14,2);
            $table->integer('effective_value');
            $table->integer('effective_type')->comment('1=year,2=months,3=days');
            $table->decimal('carry_forward_percentage',14,2);
            $table->boolean('gender')->nullable()->comment('1=male,0=female,null=genderless');
            $table->boolean('marital_status')->nullable()->comment('1=single,0=married,null=genderless');
            $table->longText('department')->nullable();
            $table->longText('designation')->nullable();
            $table->tinyInteger('office_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_leave_types');
    }
}

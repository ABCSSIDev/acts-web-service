<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBirTemplateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bir_template_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bir_template_id');
            $table->string('rate_type');
            $table->float('salary_range_from', 12, 2);
            $table->float('salary_range_to', 12, 2);
            $table->float('withholding_tax_value', 12, 2);
            $table->float('withholding_tax_percent', 12, 2);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('bir_template_id')
                ->references('id')
                ->on('bir_templates')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bir_template_items');
    }
}

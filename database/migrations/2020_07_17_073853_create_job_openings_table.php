<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobOpeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_job_openings', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('posting_title');
            $table->integer('manpower_required')->comment('no. of positions needed');
            $table->unsignedBigInteger('designation_id');
            $table->unsignedBigInteger('assigned_recruiter');
            $table->date('target_date');
            $table->tinyInteger('job_opening_status')->comment('1 = In-progress , 2 = Waiting for approval, 3 = On-Hold, 4 = Filled , 5 = Cancelled , 6 = Declined and  7 = Inactive');
            $table->decimal('expected_salary')->nullable();
            $table->unsignedBigInteger('hiring_manager_id');
            $table->date('opened_date');
            $table->tinyInteger('job_type')->comment('1 = OJT , 2 = Regular , 3 = Probationary , 4 = Freelance , 5 = Resign , 6 = Terminated');
            $table->tinyInteger('work_experience')->comment('1 = Fresh Grad , 2 = 1 to 3 years Expri. , 3  = 3 to 5years Expri.');
            $table->string('job_description')->nullable();
            $table->string('job_requirements')->nullable();
            $table->string('attachments')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('designation_id')->references('id')->on('hr_designations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('assigned_recruiter')->references('id')->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('hiring_manager_id')->references('id')->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_job_openings');
    }
}

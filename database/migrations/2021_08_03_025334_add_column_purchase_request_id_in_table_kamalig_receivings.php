<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPurchaseRequestIdInTableKamaligReceivings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kamalig_receivings', function (Blueprint $table) {
            $table->unsignedBigInteger('purchase_request_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kamalig_receivings', function (Blueprint $table) {
            $table->dropColumn('purchase_request_id');
        });
    }
}

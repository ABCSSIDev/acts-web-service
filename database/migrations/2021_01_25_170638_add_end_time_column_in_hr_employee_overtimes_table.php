<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEndTimeColumnInHrEmployeeOvertimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hr_employee_overtimes', function (Blueprint $table) {
            $table->time('end_time')->after('start_time');
            $table->time('start_time')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hr_employee_overtimes', function (Blueprint $table) {
            $table->dropColumn('end_time');
            $table->time('start_time')->nullable(false)->change();
        });
    }
}

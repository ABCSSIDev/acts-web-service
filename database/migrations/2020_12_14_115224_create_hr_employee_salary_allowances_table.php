<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrEmployeeSalaryAllowancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_employee_salary_allowances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('salary_id');
            $table->string('allowance_name');
            $table->float('amount', 14, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('salary_id')
                ->references('id')->on('hr_employee_salaries')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_employee_salary_allowances');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligAccountabilityIssuancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_accountability_issuances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('requisition_id')->nullable();
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('from_employee_id')->nullable();
            $table->tinyInteger('type')->comment('1=from WH,2=Transfer,3=Return');
            $table->date('date');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_id')->references('id')
                  ->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('from_employee_id')->references('id')
                  ->on('hr_employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('requisition_id')->references('id')
                  ->on('kamalig_accountability_requisitions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_accountability_issuances');
    }
}

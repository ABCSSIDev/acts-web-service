<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrBirTemplateTaxCriteriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_bir_template_tax_criterias', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tax_category_id');
            $table->float('compensation_level');
            $table->float('prescribed_tax');
            $table->float('percent_over');
            $table->timestamps();
            $table->foreign('tax_category_id')->references('id')
                ->on('hr_bir_template_tax_categories')->onUpdate('cascade')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_bir_template_tax_criterias');
    }
}

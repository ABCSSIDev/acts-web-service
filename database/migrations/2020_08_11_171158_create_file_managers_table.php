<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_managers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('reference_id');
            $table->string('reference_type')->comment('from model');
            $table->string('file_name');
            $table->string('url');
            $table->unsignedBigInteger('attached_by');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('attached_by')->on('hr_employees')->references('id')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_managers');
    }
}

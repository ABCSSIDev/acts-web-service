<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligInventoryOtherDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_inventory_other_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('element_id');
            $table->longText('value');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('element_id')->references('id')
                  ->on('kamalig_add_elements')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_inventory_other_details');
    }
}

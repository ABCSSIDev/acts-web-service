<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligReturnItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_return_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('return_id');
            $table->unsignedBigInteger('inventory_id');
            $table->unsignedBigInteger('asset_id')->nullable();
            $table->string('remarks')->nullable();
            $table->float('qty_return', 14, 2);
            $table->float('qty_recieved', 14, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('return_id')->references('id')
                  ->on('kamalig_return_slips')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('inventory_id')->references('id')
                  ->on('kamalig_inventories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('asset_id')->references('id')
                  ->on('kamalig_assets')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_return_items');
    }
}

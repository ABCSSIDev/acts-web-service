<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKamaligServiceOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamalig_service_orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('team_id');
            $table->unsignedBigInteger('subscribers_id');
            $table->text('team_member');
            $table->string('account_no');
            $table->string('service_no');
            $table->string('payment_mode')->comment('cash,check')->nullable();
            $table->string('check_or_no')->nullable();
            $table->string('bank')->nullable();
            $table->string('branch')->nullable();
            $table->string('so_no')->nullable();
            $table->string('type')->nullable();
            $table->dateTime('completion_date')->nullable();
            $table->time('time_start')->nullable();
            $table->time('time_end')->nullable();
            $table->string('remarks')->nullable();
            $table->string('with_pob')->nullable();
            $table->string('without_pob')->nullable();
            $table->string('type_pob')->nullable();
            $table->softDeletes();
            $table->foreign('team_id')->references('id')
                  ->on('kamalig_teams')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('subscribers_id')->references('id')
                  ->on('kamalig_subscribers')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamalig_service_orders');
    }
}

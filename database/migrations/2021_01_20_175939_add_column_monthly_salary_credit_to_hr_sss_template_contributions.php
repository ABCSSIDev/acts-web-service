<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnMonthlySalaryCreditToHrSssTemplateContributions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hr_sss_template_contributions', function (Blueprint $table) {
            $table->float('monthly_salary_credit')->after('salary_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hr_sss_template_contributions', function (Blueprint $table) {
            $table->dropColumn(['monthly_salary_credit']);
        });
    }
}

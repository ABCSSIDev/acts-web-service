<?php

use Illuminate\Database\Seeder;
use App\ApprovalProcess;

class ApprovalProcessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApprovalProcess::create([
            'reference_type' => 'App\EmployeeExit',
            'name' => "standard employee exit approval",
            'description' => "one approver for employee exit",
            'execute' => 2,
            'status' => 1,
            'approved_setup' => 0, // anyone
            'order' => NULL
        ]);

        ApprovalProcess::create([
            'reference_type' => 'App\EmployeeLeave',
            'name' => "standard office base leave approval",
            'description' => "2 approver for employee leave immediate and admin parallel order",
            'execute' => 2,
            'status' => 1,
            'approved_setup' => 1, // everyone
            'order' => 1 // parallel
        ]);

        ApprovalProcess::create([
            'reference_type' => 'App\EmployeeOvertime',
            'name' => "standard office base overtime approval process",
            'description' => "2 approver for employee overtime immediate and admin sequential order",
            'execute' => 2,
            'status' => 1,
            'approved_setup' => 1, // everyone
            'order' => 0 //sequential
        ]);

        ApprovalProcess::create([
            'reference_type' => 'App\EmployeeTimesheet',
            'name' => "standard office base timesheet approval process",
            'description' => "2 approver for employee timesheet hr",
            'execute' => 2,
            'status' => 1,
            'approved_setup' => 0, // anyone
            'order' => NULL
        ]);

        ApprovalProcess::create([
            'reference_type' => 'App\PayRun',
            'name' => "standard office base payroll approval process",
            'description' => "3 approver for employee payroll hr",
            'execute' => 2,
            'status' => 1,
            'approved_setup' => 0, // everyone
            'order' => 0 //sequential
        ]);

        ApprovalProcess::create([
            'reference_type' => 'App\TransmittalSlip',
            'name' => "standard office base transmittal slip reimbursement process",
            'description' => "1 approver for transmittal slip hr",
            'execute' => 2,
            'status' => 1,
            'approved_setup' => 1, // anyone
            'order' => NULL 
        ]);

        ApprovalProcess::create([
            'reference_type' => 'App\Reimbursement',
            'name' => "standard office base payroll reimbursement process",
            'description' => "1 approver for reimbursement hr",
            'execute' => 2,
            'status' => 1,
            'approved_setup' => 1, // anyone
            'order' => NULL 
        ]);


    }
}

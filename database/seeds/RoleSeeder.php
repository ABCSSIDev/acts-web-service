<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'role_id' => null,
                'role_name' => 'President',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 1,
                'role_name' => 'Operations Head',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 2,
                'role_name' => 'PMG Supervisor',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 2,
                'role_name' => 'DSIG Head',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 2,
                'role_name' => 'HR Supervisor',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 2,
                'role_name' => 'QA Supervisor',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 2,
                'role_name' => 'Warehouse',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 3,
                'role_name' => 'Provincial Senior Admin',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 3,
                'role_name' => 'NCR Senior Admin',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 4,
                'role_name' => 'Operations Supervisor',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 10,
                'role_name' => 'Admin Staff',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 5,
                'role_name' => 'HR Assistant',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 6,
                'role_name' => 'QA Officer',
                'role_description' => '',
                'is_shared' => 0
            ],
            [
                'role_id' => 7,
                'role_name' => 'Warehouse Personnel',
                'role_description' => '',
                'is_shared' => 0
            ]
        ];
        Role::insert($roles);
    }
}

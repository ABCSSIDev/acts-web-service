<?php

use App\Categories;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json_category = file_get_contents("database/data/category.json");
        $categories = json_decode($json_category);
        foreach ($categories as $category) {
            $insert = new Categories();
            $insert->category_name = $category->name;
            $insert->enabled = 1;
            $insert->save();
        }
    }
}

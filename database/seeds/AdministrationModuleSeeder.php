<?php

use Illuminate\Database\Seeder;
use App\Module;
use App\RestrictionModule;

class AdministrationModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $management = Module::create([
            'system_id' => 5,
            'module_id' => null,
            'name' => 'User Management',
            'route' => ''
        ]);
        $process = Module::create([
            'system_id' => 5,
            'module_id' => null,
            'name' => 'Process Management',
            'route' => ''
        ]);
        $user = Module::create([
            'system_id' => 5,
            'module_id' => $management->id,
            'name' => 'Users',
            'route' => 'users',
            'icon' => 'fa fa-user-circle'
        ]);
        RestrictionModule::create([
            'module_id' => $user->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $roles = Module::create([
            'system_id' => 5,
            'module_id' => $management->id,
            'name' => 'Roles',
            'route' => 'roles',
            'icon' => 'fa fa-user-plus'
        ]);
        RestrictionModule::create([
            'module_id' => $roles->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $permission = Module::create([
            'system_id' => 5,
            'module_id' => $management->id,
            'name' => 'Permissions',
            'route' => 'permissions',
            'icon' => 'fa fa-lock'
        ]);
        RestrictionModule::create([
            'module_id' => $permission->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $approval = Module::create([
            'system_id' => 5,
            'module_id' => $process->id,
            'name' => 'Approval Processes',
            'route' => 'approval_process',
            'icon' => 'fa fa-lock'
        ]);
        RestrictionModule::create([
            'module_id' => $approval->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $audit = Module::create([
            'system_id' => 5,
            'module_id' => $process->id,
            'name' => 'Audit Trails',
            'route' => 'audit_trails',
            'icon' => 'fa fa-search-plus'
        ]);
        RestrictionModule::create([
            'module_id' => $audit->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
    }
}

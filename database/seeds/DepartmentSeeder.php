<?php

use Illuminate\Database\Seeder;
use App\Department;
use App\Designation;
use App\EmployeeType;
use App\JobDetail;
use App\ProjectSite;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
            [
                "id" => 1,
                "name" => 'DSIG'
            ],
            [
                "id" => 2,
                "name" => 'PMG'
            ],
            [
                "id" => 3,
                "name" => 'Warehouse'
            ],
            [
                "id" => 4,
                "name" => 'Quality Assurance'
            ],
            [
                "id" => 5,
                "name" => 'HR'
            ],
        ];

        $designations = [
            [
                "department_id" => 1,
                "name" => 'Operations Head'
            ],
            [
                "department_id" => 1,
                "name" => 'Operations Assistant Manager'
            ],
            [
                "department_id" => 1,
                "name" => 'Operations Supervisor'
            ],
            [
                "department_id" => 1,
                "name" => 'Technical Support Officer'
            ],
            [
                "department_id" => 1,
                "name" => 'DSIG Technician'
            ],
            [
                "department_id" => 1,
                "name" => 'DSIG Driver Technician'
            ],
            [
                "department_id" => 2,
                "name" => 'Operations Head'
            ],
            [
                "department_id" => 2,
                "name" => 'PMG Supervisor'
            ],
            [
                "department_id" => 2,
                "name" => 'Senior Admin'
            ],
            [
                "department_id" => 2,
                "name" => 'PMG Team Leader'
            ],
            [
                "department_id" => 2,
                "name" => 'PMG Technician'
            ],
            [
                "department_id" => 2,
                "name" => 'PMG Driver Technician'
            ],
            [
                "department_id" => 3,
                "name" => 'Operations Head'
            ],
            [
                "department_id" => 3,
                "name" => 'Warehouse Staff'
            ],
            [
                "department_id" => 4,
                "name" => 'Operations Head'
            ],
            [
                "department_id" => 4,
                "name" => 'Quality Assurance Supervisor'
            ],
            [
                "department_id" => 4,
                "name" => 'Quality Assurance Officer'
            ],
            [
                "department_id" => 5,
                "name" => 'Operations Head'
            ],
            [
                "department_id" => 5,
                "name" => 'HR Supervisor'
            ],
            [
                "department_id" => 5,
                "name" => 'HR Assistant'
            ],
        ];
        $employeeTypes = [
            [
                "name" => 'Regular'
            ],
            [
                "name" => 'Project-Based'
            ],
            [
                "name" => 'Probationary'
            ]
        ];

        $projectSites = [
            [
                "name" => 'Rizal',
                "address" => 'Antipolo, Rizal'
            ],
            [
                "name" => 'Bacolod',
                "address" => 'Bacolod'
            ],
            [
                "name" => 'Davao',
                "address" => 'Davao'
            ],
            [
                "name" => 'Cebu',
                "address" => 'Cebu'
            ],
            [
                "name" => 'Iloilo',
                "address" => 'Iloilo'
            ],
        ];

        // Department::insert($departments);
        // Designation::insert($designations);
        EmployeeType::insert($employeeTypes);
        ProjectSite::insert($projectSites);
    }
}

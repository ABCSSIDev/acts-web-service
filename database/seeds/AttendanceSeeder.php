<?php

use App\Attendance;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttendanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees = DB::table('hr_employees')
                    ->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('hr_employee_salaries')
                            ->whereColumn('hr_employee_salaries.employee_id', 'hr_employees.id');
                    })
                    ->get();
        $end_date = Carbon::createFromFormat('Y-m-d', '2021-02-20');
        foreach ($employees as $employee) {
            $start_date = Carbon::createFromFormat('Y-m-d', '2021-03-05');
            while ($start_date->lessThanOrEqualTo($end_date)) {
                Attendance::create([
                    'employee_id' => $employee->id,
                    'project_site_id' => 1,
                    'start_time' => date('Y-m-d H:i:s', strtotime($start_date->format('Y-m-d') . '08:00:00')),
                    'end_time' => date('Y-m-d H:i:s', strtotime($start_date->format('Y-m-d') . '17:00:00')),
                ]);
                $start_date->add('1 day');
            }
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kamalig_units')->insert([
            'unit_name' => 'kilograms',
            'unit_code' => 'kg',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'pieces',
            'unit_code' => 'pc',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'meter',
             'unit_code' => 'm',
            'enabled' => 1        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'inch',
            'unit_code' => 'inch',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'grams',
             'unit_code' => 'g',
            'enabled' => 1        
            ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'millimeters',
            'unit_code' => 'mm',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'Gallon',
            'unit_code' => 'gall',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'Bags',
            'unit_code' => 'bags',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'Roll',
            'unit_code' => 'roll',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'Pairs',
            'unit_code' => 'pairs',
            'enabled' => 1
        ]);
        
        DB::table('kamalig_units')->insert([
            'unit_name' => 'Ream',
            'unit_code' => 'rm',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'elf',
            'unit_code' => 'elf',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'Can',
            'unit_code' => 'Can',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'pack',
            'unit_code' => 'pack',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'package',
            'unit_code' => 'package',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'tray',
            'unit_code' => 'tray',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'unit',
            'unit_code' => 'unit',
            'enabled' => 1
        ]);

        DB::table('kamalig_units')->insert([
            'unit_name' => 'feet',
            'unit_code' => 'feet',
            'enabled' => 1
        ]);
        DB::table('kamalig_units')->insert([
            'unit_name' => 'box',
            'unit_code' => 'box',
            'enabled' => 1
        ]);
    }
}

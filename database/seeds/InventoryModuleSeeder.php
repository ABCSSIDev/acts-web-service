<?php

use Illuminate\Database\Seeder;
use App\Module;
use App\RestrictionModule;

class InventoryModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $home = Module::create([
            'system_id' => 3,
            'module_id' => null,
            'name' => 'Home',
        ]);
        $inventory = Module::create([
            'system_id' => 3,
            'module_id' => null,
            'name' => 'Asset / Inventory',
        ]);
        $op = Module::create([
            'system_id' => 3,
            'module_id' => null,
            'name' => 'Operations',
        ]);
        $procure = Module::create([
            'system_id' => 3,
            'module_id' => null,
            'name' => 'Procurement',
        ]);
        $dashboard = Module::create([
            'system_id' => 3,
            'module_id' => $home->id,
            'name' => 'Dashboard',
            'icon' => 'fa fa-tachometer',
            'route' => 'inventory.home'
        ]);
        RestrictionModule::create([
            'module_id' => $dashboard->id,
            'restrictions' => '[1]',
        ]);

        $materials = Module::create([
            'system_id' => 3,
            'module_id' => $inventory->id,
            'name' => 'Operations Materials',
            'icon' => 'fa fa-line-chart',
            'route' => 'operation_material'
        ]);
        RestrictionModule::create([
            'module_id' => $materials->id,
            'restrictions' => '[1,2,3,4,5,6]',
        ]);
        $office = Module::create([
            'system_id' => 3,
            'module_id' => $inventory->id,
            'name' => 'Office Supplies and Equipment',
            'icon' => 'fa fa-chain',
            'route' => 'supplies_equipment'
        ]);
        RestrictionModule::create([
            'module_id' => $office->id,
            'restrictions' => '[1,2,3,4,5,6]',
        ]);
        $cpe = Module::create([
            'system_id' => 3,
            'module_id' => $inventory->id,
            'name' => 'Customer Premise Equipment',
            'icon' => 'fa fa-line-chart',
            'route' => 'premise_equipment'
        ]);
        RestrictionModule::create([
            'module_id' => $cpe->id,
            'restrictions' => '[1,2,3,4,5,6]',
        ]);
        $receiving = Module::create([
            'system_id' => 3,
            'module_id' => $inventory->id,
            'name' => 'Receiving',
            'icon' => 'fa fa-laptop',
            'route' => 'receiving'
        ]);
        RestrictionModule::create([
            'module_id' => $receiving->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $accountability_request = Module::create([
            'system_id' => 3,
            'module_id' => $op->id,
            'name' => 'Accountability Request',
            'icon' => 'fa fa-hand-grab-o',
            'route' => 'accountability_request'
        ]);
        RestrictionModule::create([
            'module_id' => $accountability_request ->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $team = Module::create([
            'system_id' => 3,
            'module_id' => $op->id,
            'name' => 'Teams',
            'icon' => 'fa fa-chain',
            'route' => 'team'
        ]);
        RestrictionModule::create([
            'module_id' => $team->id,
            'restrictions' => '[1,2,3,4,5,6]',
        ]);
        $accountability_issue = Module::create([
            'system_id' => 3,
            'module_id' => $op->id,
            'name' => 'Accountability Issuance',
            'icon' => 'fa fa-hand-grab-o',
            'route' => 'accountability_issuance'
        ]);
        RestrictionModule::create([
            'module_id' => $accountability_issue ->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $issuance = Module::create([
            'system_id' => 3,
            'module_id' => $op->id,
            'name' => 'Material Issuance Slip',
            'icon' => 'fa fa-child',
            'route' => 'material_issuance_slip'
        ]);
        RestrictionModule::create([
            'module_id' => $issuance->id,
            'restrictions' => '[1,2,3,4,5]'
        ]);
        $return = Module::create([
            'system_id' => 3,
            'module_id' => $op->id,
            'name' => 'Return Slip',
            'icon' => 'fa fa-file-image-o',
            'route' => 'return_slip'
        ]);
        RestrictionModule::create([
            'module_id' => $return->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $service_order = Module::create([
            'system_id' => 3,
            'module_id' => $op->id,
            'name' => 'Service Order',
            'icon' => 'fa fa-truck',
            'route' => 'service_order'
        ]);
        RestrictionModule::create([
            'module_id' => $service_order->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $transmittal = Module::create([
            'system_id' => 3,
            'module_id' => $op->id,
            'name' => 'Transmittal Slip',
            'icon' => 'fa fa-rotate-left',
            'route' => 'transmittal_slip'
        ]);
        RestrictionModule::create([
            'module_id' => $transmittal->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $incident = Module::create([
            'system_id' => 3,
            'module_id' => $op->id,
            'name' => 'Incident Report',
            'icon' => 'fa fa-clock-o',
            'route' => 'incident_report'
        ]);
        RestrictionModule::create([
            'module_id' => $incident->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $teams = Module::create([
            'system_id' => 3,
            'module_id' => $op->id,
            'name' => 'Teams',
            'icon' => 'fa fa-users',
            'route' => 'teams'
        ]);
        RestrictionModule::create([
            'module_id' => $teams ->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $pr = Module::create([
            'system_id' => 3,
            'module_id' => $procure->id,
            'name' => 'Purchase Request',
            'icon' => 'fa fa-ticket',
            'route' => 'purchase_request'
        ]);
        RestrictionModule::create([
            'module_id' => $pr->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $canvass = Module::create([
            'system_id' => 3,
            'module_id' => $procure->id,
            'name' => 'Canvassing',
            'icon' => 'fa fa-address-book',
            'route' => 'canvassing'
        ]);
        RestrictionModule::create([
            'module_id' => $canvass->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $po = Module::create([
            'system_id' => 3,
            'module_id' => $procure->id,
            'name' => 'Purchase Order',
            'icon' => 'fa fa-file-text',
            'route' => 'purchase_order'
        ]);
        RestrictionModule::create([
            'module_id' => $po->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $acceptance = Module::create([
            'system_id' => 3,
            'module_id' => $procure->id,
            'name' => 'Acceptance',
            'icon' => 'fa fa-shopping-basket',
            'route' => 'acceptance'
        ]);
        RestrictionModule::create([
            'module_id' => $acceptance->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $category = Module::create([
            'system_id' => 3,
            'module_id' => $procure->id,
            'name' => 'Category',
            'icon' => 'fa fa-file-word-o',
            'route' => 'category'
        ]);
        RestrictionModule::create([
            'module_id' => $category->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
    }
}

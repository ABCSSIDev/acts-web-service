<?php

use Illuminate\Database\Seeder;
use App\Module;
use App\RestrictionModule;

class HRMSModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $home = Module::create([
            'system_id' => 1,
            'module_id' => null,
            'name' => 'Home',
        ]);
        $org = Module::create([
            'system_id' => 1,
            'module_id' => null,
            'name' => 'Organization',
        ]);
        $attendance = Module::create([
            'system_id' => 1,
            'module_id' => null,
            'name' => 'Time Attendance',
        ]);
        $payroll = Module::create([
            'system_id' => 1,
            'module_id' => null,
            'name' => 'Payroll',
        ]);
        $recruitment = Module::create([
            'system_id' => 1,
            'module_id' => null,
            'name' => 'Recruitment',
        ]);

        $dashboard = Module::create([
            'system_id' => 1,
            'module_id' => $home->id,
            'name' => 'Dashboard',
            'route' => 'human.home',
            'icon' => 'fa fa-tachometer'
        ]);
        RestrictionModule::create([
            'module_id' => $dashboard->id,
            'restrictions' => '[1]',
        ]);
        $employee = Module::create([
            'system_id' => 1,
            'module_id' => $org->id,
            'name' => 'Employees',
            'route' => 'employees',
            'model_type' => "App\\Employee",
            'icon' => 'fa fa-users'
        ]);
        RestrictionModule::create([
            'module_id' => $employee->id,
            'restrictions' => '[1,2,3,4,5,6]',
        ]);
        $department = Module::create([
            'system_id' => 1,
            'module_id' => $org->id,
            'name' => 'Department',
            'route' => 'departments',
            'model_type' => "App\\Department",
            'icon' => 'fa fa-vcard'
        ]);
        RestrictionModule::create([
            'module_id' => $department->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $designation = Module::create([
            'system_id' => 1,
            'module_id' => $org->id,
            'name' => 'Designation',
            'route' => 'designations',
            'model_type' => "App\\Designation",
            'icon' => 'fa fa-id-badge'
        ]);
        RestrictionModule::create([
            'module_id' => $designation->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $exit = Module::create([
            'system_id' => 1,
            'module_id' => $org->id,
            'name' => 'Employee Exits',
            'route' => 'employee_exits',
            'model_type' => "App\\EmployeeExit",
            'icon' => 'fa fa-sign-out'
        ]);
        RestrictionModule::create([
            'module_id' => $exit->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $tree = Module::create([
            'system_id' => 1,
            'module_id' => $org->id,
            'name' => 'Organizational Tree',
            'route' => 'org_tree',
            'icon' => 'fa fa-sitemap'
        ]);
        RestrictionModule::create([
            'module_id' => $tree->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $birthday = Module::create([
            'system_id' => 1,
            'module_id' => $org->id,
            'name' => 'Birthdays',
            'route' => '',
            'icon' => 'fa fa-birthday-cake'
        ]);
        RestrictionModule::create([
            'module_id' => $birthday->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $event = Module::create([
            'system_id' => 1,
            'module_id' => $org->id,
            'name' => 'Events',
            'route' => '',
            'icon' => 'fa fa-calendar'
        ]);
        RestrictionModule::create([
            'module_id' => $event->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);

        $attendance_employee = Module::create([
            'system_id' => 1,
            'module_id' => $attendance->id,
            'name' => 'Employee Attendances',
            'route' => 'employee_attendance',
            'model_type' => "App\\Attendance",
            'icon' => 'fa fa-calendar-plus-o'
        ]);

        RestrictionModule::create([
            'module_id' => $attendance_employee->id,
            'restrictions' => '[1,2,4,5,6]',
        ]);

        $leave = Module::create([
            'system_id' => 1,
            'module_id' => $attendance->id,
            'name' => 'Leave Tracker',
            'route' => 'leave_tracker',
            'model_type' => "App\\EmployeeLeave",
            'icon' => 'fa fa-crosshairs'
        ]);
        RestrictionModule::create([
            'module_id' => $leave->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $overtime = Module::create([
            'system_id' => 1,
            'module_id' => $attendance->id,
            'name' => 'Overtime',
            'route' => 'overtime',
            'model_type' => "App\\EmployeeOvertime",
            'icon' => 'fa fa-calendar-check-o'
        ]);
        RestrictionModule::create([
            'module_id' => $overtime->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $timesheet = Module::create([
            'system_id' => 1,
            'module_id' => $attendance->id,
            'name' => 'Timesheet',
            'route' => 'timesheet',
            'model_type' => "App\\EmployeeTimesheet",
            'icon' => 'fa fa-clock-o'
        ]);
        RestrictionModule::create([
            'module_id' => $timesheet->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $daily_time_record = Module::create([
            'system_id' => 1,
            'module_id' => $attendance->id,
            'name' => 'Daily Time Record',
            'route' => 'daily_time_record',
            'model_type' => null,
            'icon' => 'fa fa-clock-o'
        ]);
        RestrictionModule::create([
            'module_id' => $daily_time_record->id,
            'restrictions' => '[1,5]',
        ]);
        $ob = Module::create([
            'system_id' => 1,
            'module_id' => $attendance->id,
            'name' => 'Official Business',
            'route' => 'official_business',
            'model_type' => "App\\OB",
            'icon' => 'fa fa-briefcase'
        ]);
        RestrictionModule::create([
            'module_id' => $ob->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $sched = Module::create([
            'system_id' => 1,
            'module_id' => $attendance->id,
            'name' => 'Shift Schedules',
            'route' => 'shift_schedules',
            'model_type' => "App\\Schedule",
            'icon' => 'fa fa-calendar-times-o'
        ]);
        RestrictionModule::create([
            'module_id' => $sched->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $employee_salary = Module::create([
            'system_id' => 1,
            'module_id' => $payroll->id,
            'name' => 'Employee Salaries',
            'route' => 'employee_salaries',
            'icon' => 'fa fa-money'
        ]);
        RestrictionModule::create([
            'module_id' => $employee_salary->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);

        $pay_runs = Module::create([
            'system_id' => 1,
            'module_id' => $payroll->id,
            'name' => 'Pay Runs',
            'route' => 'pay_runs',
            'model_type' => "App\\Payrun",
            'icon' => 'fa fa-credit-card-alt'
        ]);
        RestrictionModule::create([
            'module_id' => $pay_runs->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $contribution = Module::create([
            'system_id' => 1,
            'module_id' => $payroll->id,
            'name' => 'Contribution',
            'route' => 'contribution',
            'icon' => 'fa fa-user-plus'
        ]);
        RestrictionModule::create([
            'module_id' => $contribution->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $adjust = Module::create([
            'system_id' => 1,
            'module_id' => $payroll->id,
            'name' => 'Adjustments',
            'route' => 'adjustments',
            'model_type' => "App\\Adjustment",
            'icon' => 'fa fa-sliders'
        ]);
        RestrictionModule::create([
            'module_id' => $adjust->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $reimburse = Module::create([
            'system_id' => 1,
            'module_id' => $payroll->id,
            'name' => 'Reimbursements',
            'route' => 'reimbursements',
            'model_type' => "App\\Reimbursement",
            'icon' => 'fa fa-exchange'
        ]);
        RestrictionModule::create([
            'module_id' => $reimburse->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $deduction = Module::create([
            'system_id' => 1,
            'module_id' => $payroll->id,
            'name' => 'Deductions',
            'route' => '',
            'icon' => 'fa fa-scissors'
        ]);
        RestrictionModule::create([
            'module_id' => $deduction->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $jo = Module::create([
            'system_id' => 1,
            'module_id' => $recruitment->id,
            'name' => 'Job Openings',
            'route' => 'job_openings',
            'model_type' => "App\\JobOpening",
            'icon' => 'fa fa-envelope-open'
        ]);
        RestrictionModule::create([
            'module_id' => $jo->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $candidates = Module::create([
            'system_id' => 1,
            'module_id' => $recruitment->id,
            'name' => 'Candidates',
            'route' => 'candidates',
            'model_type' => "App\\Candidate",
            'icon' => 'fa fa-user-plus'
        ]);
        RestrictionModule::create([
            'module_id' => $candidates->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $interview = Module::create([
            'system_id' => 1,
            'module_id' => $recruitment->id,
            'name' => 'Interviews',
            'route' => 'interviews',
            'model_type' => "App\\Interview",
            'icon' => 'fa fa-comments'
        ]);
        RestrictionModule::create([
            'module_id' => $interview->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
        $onboarding = Module::create([
            'system_id' => 1,
            'module_id' => $recruitment->id,
            'name' => 'Onboarding',
            'route' => 'onboarding',
            'model_type' => "App\\OnBoarding",
            'icon' => 'fa fa-handshake-o'
        ]);
        RestrictionModule::create([
            'module_id' => $onboarding->id,
            'restrictions' => '[1,2,3,4,5]',
        ]);
    }
}

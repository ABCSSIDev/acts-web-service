<?php

use App\Department;
use App\Employee;
use App\EmployeeContact;
use App\JobDetail;
use App\Designation;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $string = htmlentities('Decoriña', ENT_QUOTES, 'UTF-8');
        // $emp = Employee::create(
        //     [
        //         'employee_no' => '2020-138',
        //         'first_name' => $string,
        //         'last_name' => $string,
        //         'middle_name' => $string,
        //         'gender' => 'female',
        //         'permanent_address' => 'asdf',
        //         'marital_status' => 'married',
        //         'birth_date' => '1993-08-23'
        //     ]
        // );
        //         EmployeeContact::create([
        //             'employee_id' => $emp->id,
        //             'email' => 'asdf@abc.com',
        //             'phone_number' => '+639057125266'
        //         ]);

        //         JobDetail::create([
        //             'employee_id' => $emp->id,
        //             'designation_id' => 1,
        //             'employee_type_id' => 1,
        //             'employee_status' => 1,
        //             'source_of_hire' => 1,
        //             'joined_date' => '2015-01026'
        //         ]);
        // var_dump($emp);
        $file = fopen(database_path('/data/employees_2022.csv'), "r");

        $i = 0;
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($i > 0) {
                if (!isset($column[0])) {
                    continue;
                }
                $firstName = htmlentities(utf8_encode($column[2]), ENT_QUOTES, 'UTF-8');
                $middleName = htmlentities(utf8_encode($column[3]), ENT_QUOTES, 'UTF-8');
                $lastName = htmlentities(utf8_encode($column[1]), ENT_QUOTES, 'UTF-8');
                // var_dump(utf8_encode($column[1]));
                // break;
                $emp = Employee::create(
                    [
                        'employee_no' => str_replace(' ', '', $column[0]),
                        'first_name' => $firstName,
                        'last_name' => $lastName,
                        'middle_name' => $middleName,
                        'gender' => $column[6] == 'M' ? 'male' : 'female',
                        'current_address' => $column[15],
                        'marital_status' => $column[14] == 'M' ? 'married' : 'single',
                        'birth_date' => date('Y-m-d', strtotime($column[4]))
                    ]
                );
                $first = explode(' ', $firstName);
                $f = '';
                foreach ($first as $n) {
                    $f .= substr(strtolower($n), 0, 1);
                }
                $middle = substr(strtolower($middleName), 0, 1);
                $last = strtolower($lastName);
                $email = $f . $middle . $last . '@acableworks.com.ph';

                EmployeeContact::create([
                    'employee_id' => $emp->id,
                    'email' => $email,
                    'phone_number' => str_replace(' ', '', $column[9])
                ]);

                $department_id = $this->getDepartment($column[8]);

                $designation_id = $this->getDesignation($department_id, $column[7]);

                $emp_type = 0;
                if ($column[17] == 'Regular') {
                    $emp_type = 1;
                } else {
                    $emp_type = 3;
                }

                JobDetail::create([
                    'employee_id' => $emp->id,
                    'designation_id' => $designation_id,
                    'employee_type_id' => $emp_type,
                    'employee_status' => 1,
                    'source_of_hire' => 1,
                    'joined_date' => date('Y-m-d', strtotime($column[5]))
                ]);

                $user = User::create([
                    'name' => $firstName . ' ' . $middleName . ' ' . $lastName,
                    'email' => $email,
                    'password' => Hash::make('password123')
                ]);

                Employee::where('id', $emp->id)->update([
                    'user_id' => $user->id
                ]);
            }
            $i++;
        }



        // $json_employees = file_get_contents("database/data/hr_employees.json");
        // $employee_data = json_decode($json_employees);
        // foreach ($employee_data as $obj){
        //     Employee::create([
        //         'id' => $obj->id,
        //         'first_name' => $obj->first_name,
        //         'last_name' => $obj->last_name,
        //         'gender' => $obj->gender,
        //         'permanent_address' => $obj->permanent_address,
        //         'marital_status' => $obj->marital_status
        //     ]);
        // }

        // $json_job_details = file_get_contents("database/data/hr_job_details.json");
        // $job_details_data = json_decode($json_job_details);
        // foreach ($job_details_data as $obj){
        //     JobDetail::create([
        //         'employee_id' => $obj->employee_id,
        //         'designation_id' => $obj->designation_id,
        //         'employee_type_id' => $obj->employee_type_id,
        //         'employee_status' => $obj->employee_status,
        //         'source_of_hire' => $obj->source_of_hire,
        //         'joined_date' => $obj->joined_date,
        //     ]);
        // }
    }

    /**
     * getting department id
     *
     * @param [type] $deptName
     *
     * @return string
     */
    public function getDepartment($deptName): string
    {
        $department = Department::where('name', 'like', $deptName)->first();
        if ($department) {
            return $department->id;
        } else {
            $new_dept = new Department();
            $new_dept->name = $deptName;
            $new_dept->status = 1;
            $new_dept->save();
            return $new_dept->id;
        }
    }
    /**
     * getting designation id with two params
     *
     * @param [type] $deptId
     * @param [type] $designationName
     *
     * @return string
     */
    public function getDesignation($deptId, $designationName): string
    {
        $designation = Designation::where('department_id', $deptId)
                            ->where('name', 'like', $designationName)->first();
        if ($designation) {
            return $designation->id;
        } else {
            $new_designation = new Designation();
            $new_designation->department_id = $deptId;
            $new_designation->name = $designationName;
            $new_designation->status = 1;
            $new_designation->save();
            return $new_designation->id;
        }
    }
}

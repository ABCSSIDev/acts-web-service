<?php

use Illuminate\Database\Seeder;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\System::create([
            'name' => 'Kawani',
            'description' => 'Human Resource Information System (HRIS)',
            'system_theme' => '#55C755'
        ]);

        \App\System::create([
            'name' => 'Kliyente',
            'description' => 'Client Relation Management',
            'system_theme' => '#EC0000',
            'is_enabled' => 0
        ]);

        \App\System::create([
            'name' => 'Kamalig',
            'description' => 'Inventory & Asset Management System',
            'system_theme' => '#EC6E00'
        ]);

        \App\System::create([
            'name' => 'Kartamoneda',
            'description' => 'Acounting System',
            'system_theme' => '#0D4BF6',
            'is_enabled' => 0
        ]);

        \App\System::create([
            'name' => 'Administration',
            'description' => 'General Setup',
            'system_theme' => '#848484'
        ]);
    }
}

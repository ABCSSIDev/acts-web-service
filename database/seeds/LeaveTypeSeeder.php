<?php

use Illuminate\Database\Seeder;
use App\LeaveType;

class LeaveTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeaveType::create([
            'name' => 'Sick Leave',
            'code' => 'L-001',
            'balance' => 1,
            'type' => 1,
            'description' => 'Sick Leave',
            'encashment_percentage' => 10,
            'effective_value' => 10,
            'effective_type' => 3,
            'carry_forward_percentage' => 0,
            'gender' => null,
            'marital_status' => null,
            'department' => null,
            'designation' => null,
            'office_type' => null
        ]);
        LeaveType::create([
            'name' => 'Paternity Leave',
            'code' => 'L-002',
            'balance' => 1,
            'type' => 1,
            'description' => 'Paternity Leave',
            'encashment_percentage' => 10,
            'effective_value' => 10,
            'effective_type' => 3,
            'carry_forward_percentage' => 0,
            'gender' => 1,
            'marital_status' => null,
            'department' => null,
            'designation' => null,
            'office_type' => null
        ]);
        LeaveType::create([
            'name' => 'Maternity Leave ',
            'code' => 'L-003',
            'balance' => 1,
            'type' => 1,
            'description' => 'Maternity Leave ',
            'encashment_percentage' => 10,
            'effective_value' => 10,
            'effective_type' => 3,
            'carry_forward_percentage' => 0,
            'gender' => 0,
            'marital_status' => null,
            'department' => null,
            'designation' => null,
            'office_type' => null
        ]);
        LeaveType::create([
            'name' => 'Leave Without Pay',
            'code' => 'L-004',
            'balance' => 1,
            'type' => 0,
            'description' => 'Leave Without Pay',
            'encashment_percentage' => 10,
            'effective_value' => 10,
            'effective_type' => 3,
            'carry_forward_percentage' => 0,
            'gender' => null,
            'marital_status' => null,
            'department' => null,
            'designation' => null,
            'office_type' => null
        ]);
        LeaveType::create([
            'name' => 'Vacation Leave',
            'code' => 'L-005',
            'balance' => 1,
            'type' => 1,
            'description' => 'Vacation Leave',
            'encashment_percentage' => 10,
            'effective_value' => 10,
            'effective_type' => 3,
            'carry_forward_percentage' => 0,
            'gender' => null,
            'marital_status' => null,
            'department' => null,
            'designation' => null,
            'office_type' => null
        ]);
        LeaveType::create([
            'name' => 'Birthday Leave',
            'code' => 'L-006',
            'balance' => 1,
            'type' => 1,
            'description' => 'Birthday Leave',
            'encashment_percentage' => 10,
            'effective_value' => 10,
            'effective_type' => 3,
            'carry_forward_percentage' => 0,
            'gender' => null,
            'marital_status' => null,
            'department' => null,
            'designation' => null,
            'office_type' => null
        ]);
    }
}

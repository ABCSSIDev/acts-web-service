<?php

use Illuminate\Database\Seeder;
use App\Restriction;

class RestrictionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Restriction::create([
            'description' => 'View',
        ]);
        Restriction::create([
            'description' => 'Create',
        ]);
        Restriction::create([
            'description' => 'Edit',
        ]);
        Restriction::create([
            'description' => 'Delete',
        ]);
        Restriction::create([
            'description' => 'List',
        ]);
        Restriction::create([
            'description' => 'Import',
        ]);
    }
}

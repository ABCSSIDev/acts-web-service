<?php

use Illuminate\Database\Seeder;
use App\InterviewType;
class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InterviewType::create([
            'name' => 'Internal Interview'
        ]);
        InterviewType::create([
            'name' => 'General Interview'
        ]);
        InterviewType::create([
            'name' => 'Online Interview'
        ]);
        InterviewType::create([
            'name' => 'Phone Interview'
        ]);
        InterviewType::create([
            'name' => 'Level 1 Interview'
        ]);
        InterviewType::create([
            'name' => 'Level 2 Interview'
        ]);
        InterviewType::create([
            'name' => 'Level 3 Interview'
        ]);
        InterviewType::create([
            'name' => 'Level 4 Interview'
        ]);
    }
}

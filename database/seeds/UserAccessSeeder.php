<?php

use App\Employee;
use App\Role;
use App\User;
use App\UserAccess;
use Illuminate\Database\Seeder;

class UserAccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // UserAccess::create([
        //     'user_id' => '1',
        //     'role_id' => 1,
        //     'permission_id' => "1"
        // ]);

//        UserAccess::create([
//            'user_id' => 2,
//            'role_id' => 2,
//            'permission_id' => "1"
//        ]);
//
//        
//        UserAccess::create([
//            'user_id' => 3,
//            'role_id' => 4,
//            'permission_id' => 1
//        ]);
//
//        
//        UserAccess::create([
//            'user_id' => 5,
//            'role_id' => 4,
//            'permission_id' => 1
//        ]);
//
//        UserAccess::create([
//            'user_id' => 6,
//            'role_id' => 5,
//            'permission_id' => 1
//        ]);
//
//        UserAccess::create([
//            'user_id' => 7,
//            'role_id' => 4,
//            'permission_id' => 1
//        ]);
//
//        UserAccess::create([
//            'user_id' => 8,
//            'role_id' => 8,
//            'permission_id' => 1
//        ]);
//
//        UserAccess::create([
//            'user_id' => 4,
//            'role_id' => 10,
//            'permission_id' => 1
//        ]);

        $employees = Employee::all();
        foreach($employees as $employee){
            $name = $employee->first_name . ' ' . $employee->middle_name . ' ' . $employee->last_name;
            $role = Role::where('role_name', 'LIKE', '%'. $employee->getJobDetails->getDesignation->name . '%')->first();
            if($role && !empty($employee->user_id)){
                // dump($employee->getJobDetails->getDesignation->name);
                $access = UserAccess::where('user_id', $employee->user_id)->get();
                if($access->count() == 0){
                    $user_access_query = new UserAccess();
                    $user_access_query->user_id = $employee->user_id;
                    $user_access_query->role_id = $role->id;
                    $user_access_query->permission_id = 4;
                    $user_access_query->save();
                }
                    
            }
            // if(!empty($employee->user_id)){
            //     $user_access_query = new UserAccess();
            //     $user_access_query->user_id = $employee->user_id;
            //     $user_access_query->role_id = $role->id;
            //     $user_access_query->permission_id = 4;
            //     $user_access_query->save();
            // }
        }



        // while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
        //     if ($i > 0) {
        //         if (!isset($column[0])) {
        //             continue;
        //         }

        //         $user_id = $column[0];
        //         $role_id = $column[1];

        //         $user = UserAccess::where('user_id', $user_id)->first();
        //         if(!$user){
        //             $user_access = new UserAccess();
        //             $user_access->user_id = $user_id;
        //             $user_access->role_id = $role_id;
        //             $user_access->permission_id = 4;
        //             $user_access->save();
        //         }
        //     }
        // }

    }

}

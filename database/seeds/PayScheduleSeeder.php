<?php

use App\Payschedule;
use App\PayScheduleCutOff;
use Illuminate\Database\Seeder;

class PayScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paySched = PaySchedule::create([
            'department_ids' => '[1]',
            'exceptions' => null,
            'name' => 'OSG Payroll Schedule',
            'payroll_start_date' => '2021-01-01',
            'calculate_salary_based' => '1',
            'days_per_month' => '20',
            'cut_off_period' => 1,
            'active' => 1
        ]);

        PayScheduleCutOff::create([
            'pay_schedule_id' => $paySched->id,
            'orders' => 1,
            'date_from' => '2021-01-06',
            'date_to' => '2021-01-19',
            'pay_schedule' => 0
        ]);

        PayScheduleCutOff::create([
            'pay_schedule_id' => $paySched->id,
            'orders' => 2,
            'date_from' => '2021-01-20',
            'date_to' => '2021-02-05',
            'pay_schedule' => 15
        ]);
    }
}

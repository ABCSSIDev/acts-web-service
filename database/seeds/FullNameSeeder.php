<?php

use App\User;
use Illuminate\Database\Seeder;

class FullNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $file = fopen(database_path('/data/employee_details.csv'), "r");

        $i = 0;
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            if (!isset($column[0])) {
                continue;
            }
            $firstName = htmlentities(utf8_encode($column[2]), ENT_QUOTES, 'UTF-8');
            $middleName = htmlentities(utf8_encode($column[3]), ENT_QUOTES, 'UTF-8');
            $lastName = htmlentities(utf8_encode($column[1]), ENT_QUOTES, 'UTF-8');

            $first = explode(' ', $firstName);
            $f = '';
            foreach ($first as $n) {
                $f .= substr(strtolower($n), 0, 1);
            }
            $middle = substr(strtolower($middleName), 0, 1);
            $last = strtolower($lastName);
            $email = $f . $middle . $last . '@acableworks.com.ph';

            User::where('email', $email)->update([
                'name' => $lastName . ' ' . $firstName . ' ' . $middleName
            ]);
        }
    }
}

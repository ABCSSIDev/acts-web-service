<?php

use Illuminate\Database\Seeder;
use App\Module;

class AccountingModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::create([
            'system_id' => 4,
            'module_id' => null,
            'name' => 'Home',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => null,
            'name' => 'Accountant',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => null,
            'name' => 'Sales',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => null,
            'name' => 'Purchases',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => null,
            'name' => 'Journals',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => null,
            'name' => 'Time Tracking',
        ]);

        Module::create([
            'system_id' => 4,
            'module_id' => 74,
            'name' => 'Dashboard',
            'icon' => 'fa fa-tachometer',
        ]);

        Module::create([
            'system_id' => 4,
            'module_id' => 75,
            'name' => 'Chart of Accounts',
            'icon' => 'fa fa-list-alt',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 75,
            'name' => 'Budget Plan',
            'icon' => 'fa fa-bar-chart',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 75,
            'name' => 'Banking',
            'icon' => 'fa fa-university',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 75,
            'name' => 'Taxes',
            'icon' => 'fa fa-percent',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 75,
            'name' => 'Opening Balance',
            'icon' => 'fa fa-balance-scale',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 75,
            'name' => 'Cost Center',
            'icon' => 'fa fa-clipboard',
        ]);

        Module::create([
            'system_id' => 4,
            'module_id' => 76,
            'name' => 'Customers',
            'icon' => 'fa fa-users',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 76,
            'name' => 'Invoices',
            'icon' => 'fa fa-sticky-note',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 76,
            'name' => 'Collections',
            'icon' => 'fa fa-jpy',
        ]);

        Module::create([
            'system_id' => 4,
            'module_id' => 77,
            'name' => 'Vendors',
            'icon' => 'fa fa-address-book-o',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 77,
            'name' => 'Payment Requests',
            'icon' => 'fa fa-file-text',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 77,
            'name' => 'Cash Disbursements',
            'icon' => 'fa fa-money',
        ]);

        Module::create([
            'system_id' => 4,
            'module_id' => 78,
            'name' => 'Sales Journal',
            'icon' => 'fa fa-bookmark',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 78,
            'name' => 'Cash Receipt Journal',
            'icon' => 'fa fa-file-pdf-o',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 78,
            'name' => 'AP Journal',
            'icon' => 'fa fa-file-text-o',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 78,
            'name' => 'Check Journal',
            'icon' => 'fa fa-th-list',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 78,
            'name' => 'General Journal',
            'icon' => 'fa fa-files-o',
        ]);

        Module::create([
            'system_id' => 4,
            'module_id' => 79,
            'name' => 'Projects',
            'icon' => 'fa fa-legal',
        ]);
        Module::create([
            'system_id' => 4,
            'module_id' => 79,
            'name' => 'Timesheet',
            'icon' => 'fa fa-clock-o',
        ]);
    }
}

<?php

use App\Inventories;
use App\OfficeSuppliesAndEquipment;
use Illuminate\Database\Seeder;

class AssetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $inventories = Inventories::all();

        foreach ($inventories as $inventory) {
            $quantity = $inventory->quantity - 1;
            for ($i = 1; $i < $quantity; $i++) {
                $asset = new OfficeSuppliesAndEquipment();
                $asset->inventory_id = $inventory->id;
                $asset->inventory_status_id = 1;
                $asset->save();
            }
        }
    }
}

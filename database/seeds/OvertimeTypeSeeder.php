<?php

use Illuminate\Database\Seeder;
use App\OvertimeType;

class OvertimeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OvertimeType::create([
            'name' => 'Normal Day OT',
            'code' => 'NDOT',
            'rate' => '1.25',
            'type' => '2',
        ]);

        OvertimeType::create([
            'name' => 'Rest Day OT',
            'code' => 'RDOT',
            'rate' => '1.69',
            'type' => '2',
        ]);

        OvertimeType::create([
            'name' => 'Special Non-working OT',
            'code' => 'SpeOT',
            'rate' => '1.69',
            'type' => '2',
        ]);

        OvertimeType::create([
            'name' => 'Special Non-working and Rest Day OT',
            'code' => 'Spe&RDOT',
            'rate' => '1.95',
            'type' => '2',
        ]);

        OvertimeType::create([
            'name' => 'Regular Holiday OT',
            'code' => 'RHOT',
            'rate' => '2.6',
            'type' => '2',
        ]);

        OvertimeType::create([
            'name' => 'Regular Holiday and Rest Day OT',
            'code' => 'RH&RDOT',
            'rate' => '3.38',
            'type' => '2',
        ]);

        OvertimeType::create([
            'name' => 'Rest Day',
            'code' => 'RD',
            'rate' => '1.30',
            'type' => '1',
        ]);

        OvertimeType::create([
            'name' => 'Double Holiday & Rest Day',
            'code' => 'DH&RD',
            'rate' => '3.90',
            'type' => '1',
        ]);

        OvertimeType::create([
            'name' => 'Double Holiday',
            'code' => 'DH',
            'rate' => '3.00',
            'type' => '1',
        ]);

        OvertimeType::create([
            'name' => 'Special Non-working & Rest Day',
            'code' => 'SpeNon&RD',
            'rate' => '1.50',
            'type' => '1',
        ]);

        OvertimeType::create([
            'name' => 'Regular Holiday',
            'code' => 'LegalH',
            'rate' => '2.00.',
            'type' => '1',
        ]);

        OvertimeType::create([
            'name' => 'Special Non-working',
            'code' => 'SpeH',
            'rate' => '1.30',
            'type' => '1',
        ]);

        OvertimeType::create([
            'name' => 'Regular Holiday and Rest Day',
            'code' => 'RH&RD',
            'rate' => '2.60',
            'type' => '1',
        ]);
    }
}

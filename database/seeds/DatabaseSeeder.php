<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
            UserSeeder::class,
            SystemSeeder::class,
            InventoryModuleSeeder::class,
            HRMSModuleSeeder::class,
            AdministrationModuleSeeder::class,
            RestrictionSeeder::class,
            OvertimeTypeSeeder::class,
            DepartmentSeeder::class,
            InterviewSeeder::class,
            InventoryStatusSeeder::class,
            UnitsSeeder::class,
            LeaveTypeSeeder::class,
            PermissionsSeeder::class,
            //RoleSeeder::class,
            HolidaySeeder::class,
            //ApprovalProcessSeeder::class,
            //ApprovalUserSeeder::class
            PhilhealthSeeder::class,
            SSSContributionSeeder::class,
            PagibigSeeder::class,
            BirTemplateSeeder::class,
            EmployeeSeeder::class,
            //AttendanceSeeder::class
         ]);
    }
}

<?php

use App\Categories;
use App\Inventories;
use App\InventoriesNames;
use App\OfficeSuppliesAndEquipment;
use Illuminate\Database\Seeder;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json_inventory = file_get_contents("database/data/inventory.json");
        $inventories = json_decode($json_inventory);
        foreach ($inventories as $inventory) {
            $category = Categories::where('category_name', $inventory->category)->first();

            $inventory_name = new InventoriesNames();
            $inventory_name->description = $inventory->name;
            $inventory_name->save();

            $inventory_insert = new Inventories();
            $inventory_insert->inventory_description_id = $inventory_name->id;
            $inventory_insert->type = 2;
            $inventory_insert->category_id = $category->id;
            $inventory_insert->quantity = $inventory->quantity;
            $inventory_insert->unit_id = 2;
            $inventory_insert->save();

            $asset = new OfficeSuppliesAndEquipment();
            $asset->inventory_id = $inventory_insert->id;
            $asset->inventory_status_id = 1;
            $asset->save();
        }
    }
}

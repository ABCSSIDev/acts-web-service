<?php

use App\PermissionModule;
use Illuminate\Database\Seeder;

class PermissionModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Administrator Permissions
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 101,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 102,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 103,
            'restriction_id' => "[1,2,3,4,5]"
        ]);


        //HR Permisions
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 54,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 54,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 55,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 56,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 57,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 58,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 59,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 60,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 61,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 62,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 63,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 64,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 65,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 66,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 67,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 8,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 69,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 70,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 71,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 72,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
        PermissionModule::create([
            'permission_id' => '1',
            'module_id' => 73,
            'restriction_id' => "[1,2,3,4,5]"
        ]);
    }
}

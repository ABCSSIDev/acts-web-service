<?php

use App\PHICTemplates;
use Illuminate\Database\Seeder;

class PhilhealthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $start = 2019;
        $premium_rate = [2.75, 3, 3.5, 4, 4.5, 5, 5];
        $i = 0;

        while ($start <= 2025) {
            PHICTemplates::create([
                'year_effective' => $start,
                'premium_rate' => $premium_rate[$i]
            ]);
            $start++;
            $i++;
        }
    }
}

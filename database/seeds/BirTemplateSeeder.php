<?php

use App\BirTemplate;
use App\BirTemplateItem;
use Illuminate\Database\Seeder;

class BirTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = fopen(database_path('/data/BIR.csv'), "r");

        $index = 0;
        while (($column = fgetcsv($file, 10000, ",")) !== false) {
            if ($index > 0) {
                $ifExist = BirTemplate::where(
                    DB::raw('date(date_effective_from)'),
                    date('Y-m-d', strtotime($column[0]))
                )->where(DB::raw('date(date_effective_to)'), date('Y-m-d', strtotime($column[1])))->first();

                if (!$ifExist) {
                    $birTemplate = BirTemplate::create([
                        'date_effective_from' => $column[0],
                        'date_effective_to' => $column[1],
                        'is_activate' => 1
                    ]);
                } else {
                    $birTemplate = $ifExist;
                }

                $birTemplateItem = BirTemplateItem::create([
                    'bir_template_id' => $birTemplate->id,
                    'rate_type' => $column[2],
                    'salary_range_from' => $column[3],
                    'salary_range_to' => $column[4],
                    'withholding_tax_value' => $column[5],
                    'withholding_tax_percent' => $column[6]
                ]);
            }
            $index++;
        }
    }
}

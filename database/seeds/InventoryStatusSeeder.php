<?php

use Illuminate\Database\Seeder;
use App\InventoryStatus;

class InventoryStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents("database/data/inventory_status.json");
        $data = json_decode($json);
        foreach ($data as $object){
            InventoryStatus::create([
                "status_name" => $object->status_name,
                "status_type" => $object->status_type,
                "description" => $object->description,
                "enabled"     => $object->enabled

            ]);
        }
    }
}

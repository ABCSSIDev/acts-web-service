<?php

use App\SSSTemplateContributions;
use App\SSSTemplates;
use Illuminate\Database\Seeder;

class SSSContributionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $start = 3250;
        $salaryCredit = 3000;
        $er = 255;
        $ee = 135;
        $ec = 10;
        $i = 0;

        while ($i < 45) {
            if ($i == 0) {
                SSSTemplateContributions::create([
                    'salary_from' => 0,
                    'salary_to' => 3249.99,
                    'monthly_salary_credit' => $salaryCredit,
                    'employer_contribution' => $er,
                    'employee_contribution' => $ee,
                    'employee_compensation' => $ec
                ]);
            } else {
                $end = $start + 499.99;
                SSSTemplateContributions::create([
                    'salary_from' => $start,
                    'salary_to' => ($start == 24750 ? 99999.99 : $end),
                    'monthly_salary_credit' => $salaryCredit,
                    'employer_contribution' => $er,
                    'employee_contribution' => $ee,
                    'employee_compensation' => $ec
                ]);
                $start += 500;
            }
            if ($salaryCredit >= 15000) {
                $ec = 30;
            } else {
                $ec = 10;
            }
            $salaryCredit += 500;
            if ($start < 20250) {
                $er += 42.5;
                $ee += 22.5;
            }
            $i++;
        }
    }
}

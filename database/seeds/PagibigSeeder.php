<?php

use App\HDMFTemplates;
use Illuminate\Database\Seeder;

class PagibigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HDMFTemplates::create([
            'salary_from' => '0',
            'salary_to' => '1500',
            'employer_percent' => '0.01',
            'employee_percent' => '0.02'
        ]);

        HDMFTemplates::create([
            'salary_from' => '1500',
            'salary_to' => '0',
            'employer_percent' => '0.02',
            'employee_percent' => '0.02'
        ]);
    }
}

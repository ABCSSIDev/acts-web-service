<?php

use App\Permission;
use Illuminate\Database\Seeder;
use App\PermissionModule;
use App\Module;
use App\RestrictionModule;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public $hrStandard = [
        'Overtime',
        'Leave Tracker',
        'Daily Time Record',
        'Official Business'
    ];
    public $hrStandardRestrictions = [
        'Overtime' => [
            1,2,3,5
        ],
        'Leave Tracker' =>
        [
            1,2,3,5
        ],
        'Daily Time Record' => [
            1,5
        ],
        'Official Business' => [
            2,3,5
        ]
    ];

    public $kamaligStandard = [
        'Accountability Request',
        'Accountability Issuance',
        'Material Issuance Slip',
        'Return Slip',
        'Service Order',
        'Incident Report',
        'Transmittal Slip'
    ];

    public $kamaligStandardRestrictions = [
        'Accountability Request' => [
            2,3,5
        ],
        'Accountability Issuance' => [
            2,3,5
        ],
        'Material Issuance Slip' => [
            2,3,5
        ],
        'Return Slip' => [
            2,3,5
        ],
        'Service Order' => [
            2,3,5
        ],
        'Incident Report' => [
            2,3,5
        ],
        'Transmittal Slip' => [
            2,3,5
        ]
    ];

    public function run()
    {

        $this->createAdminSeeder();
        $this->createHRSeeder();
        $this->createInventorySeeder();
        $this->standardHRPermission();
        $this->standardSupervisorPermission();
    }

    /**
     * creating AdminSeeder permission
     *
     * @return void
     */
    public function createAdminSeeder()
    {
        $admin = Permission::create([
            'permission_name' => 'Administrator',
            'description' => 'Grant permission for all modules.',
            'system_id' => "[1,3,5]"
        ]);

        $this->permissionData($admin);
    }

    /**
     * creating HRseeder permission
     *
     * @return void
     */
    public function createHRSeeder()
    {
        $admin = Permission::create([
            'permission_name' => 'HRIS/Kawani',
            'description' => 'Grant permission for all modules in HR/Kawani.',
            'system_id' => "[1]"
        ]);

        $this->permissionData($admin);
    }

    /**
     * creating Inventoryseeder permission
     *
     * @return void
     */
    public function createInventorySeeder()
    {
        $warehouse = Permission::create([
            'permission_name' => 'Warehouse',
            'description' => 'Grant permission for all modules in Inventory.',
            'system_id' => "[3]"
        ]);

        $this->permissionData($warehouse);
    }

    /**
     * inserting data permission per system
     *
     * @param [type] $system_id
     *
     * @return void
     */
    public function permissionData($system)
    {
        foreach (json_decode($system->system_id) as $key => $value) {
            $modules = Module::where('system_id', $value)->get();
            foreach ($modules as $key => $module) {
                if ($module->route != null) {
                    PermissionModule::create([
                        'permission_id' => $system->id,
                        'module_id' => $module->id,
                        'restriction_id' => $module->getRestrictionSubModule->restrictions
                    ]);
                }
            }
        }
    }

    public function standardHRPermission()
    {
        $permission = Permission::create([
            "permission_name" => 'Standard',
            "description" => 'Standard Module Permission of Filing Forms for Employee',
            "system_id" => '[1]'
        ]);
        foreach ($this->hrStandard as $hr) {
            $module = Module::where('name', $hr)->first();
            PermissionModule::create([
                "permission_id" => $permission->id,
                "module_id" => $module->id,
                "restriction_id" => json_encode($this->hrStandardRestrictions[$module->name])
            ]);
        }
    }

    public function standardSupervisorPermission()
    {
        $permission = Permission::create([
            "permission_name" => 'Standard - Supervisor',
            "description" => 'Standard Module Permission for Supervisors',
            "system_id" => '[1,3]'
        ]);
        $standardModules = array_merge($this->hrStandard, $this->kamaligStandard);
        $standardRestrictions = array_merge($this->hrStandardRestrictions, $this->kamaligStandardRestrictions);
        foreach ($standardModules as $modules) {
            $module = Module::where('name', $modules)->first();
            PermissionModule::create([
                "permission_id" => $permission->id,
                "module_id" => $module->id,
                "restriction_id" => json_encode($standardRestrictions[$module->name])
            ]);
        }
    }
}

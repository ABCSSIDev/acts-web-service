<?php

use Illuminate\Database\Seeder;
use App\Categories;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categories::create([
            'category_id' => NULL,
            'category_name' => 'Sky Cable',
            'meta_title' => 'Sky Cable',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => NULL,
            'category_name' => 'ACTS',
            'meta_title' => 'ACTS',
            'enabled' => 1
        ]);
        // category parent 1
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Cable',
            'meta_title' => 'Cable',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Connector',
            'meta_title' => 'Connector',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Grounding',
            'meta_title' => 'Grounding',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Drop Accessories',
            'meta_title' => 'Drop Accessories',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Attenuator/Amplifier',
            'meta_title' => 'Attenuator/Amplifier',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Patch Cord',
            'meta_title' => 'Patch Cord',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Optical Outlet',
            'meta_title' => 'Optical Outlet',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'ONU',
            'meta_title' => 'ONU',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Modem',
            'meta_title' => 'Modem',
            'enabled' => 1
        ]); Categories::create([
            'category_id' => 1,
            'category_name' => 'HD Digibox',
            'meta_title' => 'HD Digibox',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'SOD Box',
            'meta_title' => 'SOD Box',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Digibox Accessories',
            'meta_title' => 'Digibox Accessories',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Remote Control',
            'meta_title' => 'Remote Control',
            'enabled' => 1
        ]);

        Categories::create([
            'category_id' => 1,
            'category_name' => 'DISH',
            'meta_title' => 'DISH',
            'enabled' => 1
        ]);
         Categories::create([
            'category_id' => 1,
            'category_name' => 'LNB',
            'meta_title' => 'LNB',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 1,
            'category_name' => 'Splitter',
            'meta_title' => 'Splitter',
            'enabled' => 1
        ]);
         Categories::create([
            'category_id' => 1,
            'category_name' => 'Amplifier',
            'meta_title' => 'Amplifier',
            'enabled' => 1
        ]);
        //category 2
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Cable',
            'meta_title' => 'Cable',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Connector',
            'meta_title' => 'Connector',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Grounding',
            'meta_title' => 'Grounding',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Drop Accessories',
            'meta_title' => 'Drop Accessories',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Attenuator/Amplifier',
            'meta_title' => 'Attenuator/Amplifier',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Patch Cord',
            'meta_title' => 'Patch Cord',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Optical Outlet',
            'meta_title' => 'Optical Outlet',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'ONU',
            'meta_title' => 'ONU',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Modem',
            'meta_title' => 'Modem',
            'enabled' => 1
        ]); Categories::create([
            'category_id' => 2,
            'category_name' => 'HD Digibox',
            'meta_title' => 'HD Digibox',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'SOD Box',
            'meta_title' => 'SOD Box',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Digibox Accessories',
            'meta_title' => 'Digibox Accessories',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Remote Control',
            'meta_title' => 'Remote Control',
            'enabled' => 1
        ]);

        Categories::create([
            'category_id' => 2,
            'category_name' => 'DISH',
            'meta_title' => 'DISH',
            'enabled' => 1
        ]);
         Categories::create([
            'category_id' => 2,
            'category_name' => 'LNB',
            'meta_title' => 'LNB',
            'enabled' => 1
        ]);
        Categories::create([
            'category_id' => 2,
            'category_name' => 'Splitter',
            'meta_title' => 'Splitter',
            'enabled' => 1
        ]);
         Categories::create([
            'category_id' => 2,
            'category_name' => 'Amplifier',
            'meta_title' => 'Amplifier',
            'enabled' => 1
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'System Administrator',
            'email' => 'admin@acableworks.com.ph',
            'password' => bcrypt('password123')
        ]);
        // \App\Employee::create([
        //     'user_id' => 1,
        //     'first_name' => 'Andrian',
        //     'last_name' => 'Bantero',
        //     'gender' => 'male',
        //     'birth_date' => '1995-06-29',
        //     'permanent_address' => '663 V. Baltazar St. Palatiw Pasig City',
        //     'marital_status' => 'single'
        // ]);
        // \App\EmployeeContact::create([
        //     'employee_id' => 1,
        //     'email' => 'andrianbantero@gmail.com',
        //     'phone_number' => '09275528129',
        //     'emergency_contact' => 'Linda Bantero',
        //     'emergency_contact_number' => '09994568120'
        // ]);
        // \App\WorkDetails::create([
        //     'employee_id' => 1,
        //     'company' => 'Google',
        //     'start_date' => '2016-04-15',
        //     'end_date' => '2020-05-15',
        //     'description' => 'this is a description'
        // ]);

    //    User::create([
    //        'name' => 'Mark Ian Bautista',
    //        'email' => 'mark_bautista@abcsystems.com.ph',
    //        'password' => bcrypt('password123')
    //    ]);
    //    \App\Employee::create([
    //        'user_id' => 2,
    //        'first_name' => 'Mark Ian',
    //        'last_name' => 'Bautista',
    //        'gender' => 'male',
    //        'birth_date' => '1990-06-29',
    //        'permanent_address' => 'Sa Dulo ng Walang Hanggan',
    //        'marital_status' => 'single'
    //    ]);
    //    \App\EmployeeContact::create([
    //         'employee_id' => 2,
    //         'email' => 'markbautista@gmail.com',
    //         'phone_number' => '09986528130',
    //         'emergency_contact' => 'Kath Bautista',
    //         'emergency_contact_number' => '09934546120'
    //     ]);
    //     \App\WorkDetails::create([
    //         'employee_id' => 2,
    //         'company' => 'Yahoo',
    //         'start_date' => '2016-08-20',
    //         'end_date' => '2020-06-21',
    //         'description' => 'this is a description'
    //     ]);

    //    User::create([
    //        'name' => 'Angelo Zamora',
    //        'email' => 'angelo_zamora@abcsystems.com.ph',
    //        'password' => bcrypt('password123')
    //    ]);
    //    \App\Employee::create([
    //        'user_id' => 3,
    //        'first_name' => 'Angelo',
    //        'last_name' => 'Zamora',
    //        'gender' => 'male',
    //        'birth_date' => '1993-06-29',
    //        'permanent_address' => 'Sa Puso Ng Minamahal Ko',
    //        'marital_status' => 'single'
    //    ]);
    //    \App\EmployeeContact::create([
    //         'employee_id' => 3,
    //         'email' => 'angelozamora@gmail.com',
    //         'phone_number' => '09990528130',
    //         'emergency_contact' => 'Mary Glean Argente',
    //         'emergency_contact_number' => '09664706966'
    //     ]);
    //     \App\WorkDetails::create([
    //         'employee_id' => 3,
    //         'company' => 'Accenture',
    //         'start_date' => '2016-03-04',
    //         'end_date' => '2020-07-02',
    //         'description' => 'this is a description'
    //     ]);

    //    User::create([
    //        'name' => 'Jared Bryan S. Espiritu',
    //        'email' => 'jared.espiritu@abcsystems.com.ph',
    //        'password' => bcrypt('password123')
    //    ]);
    //    \App\Employee::create([
    //        'user_id' => 4,
    //        'first_name' => 'Jared Bryan',
    //        'last_name' => 'Espiritu',
    //        'gender' => 'male',
    //        'birth_date' => '1996-05-18',
    //        'permanent_address' => 'Pallet Town, Kanto Region, Pokemon World',
    //        'marital_status' => 'single'
    //    ]);
    //    \App\EmployeeContact::create([
    //         'employee_id' => 4,
    //         'email' => 'jaredespiritu@gmail.com',
    //         'phone_number' => '09183271990',
    //         'emergency_contact' => 'Marco Jaringa',
    //         'emergency_contact_number' => '09123769090'
    //     ]);
    //     \App\WorkDetails::create([
    //         'employee_id' => 4,
    //         'company' => 'Red Core System Solutions',
    //         'start_date' => '2017-07-10',
    //         'end_date' => '2020-03-16',
    //         'description' => 'this is a description'
    //     ]);

    //    User::create([
    //        'name' => 'Melody Dela Cruz',
    //        'email' => 'melodiyacruz@abcsystems.com.ph',
    //        'password' => bcrypt('password123')
    //    ]);
    //    \App\Employee::create([
    //        'user_id' => 5,
    //        'first_name' => 'Melody',
    //        'last_name' => 'Dela Cruz',
    //        'gender' => 'female',
    //        'birth_date' => '1993-06-29',
    //        'permanent_address' => 'BebeShark St. Tarlac, Philippines',
    //        'marital_status' => 'married'
    //    ]);
    //    \App\EmployeeContact::create([
    //         'employee_id' => 5,
    //         'email' => 'melodydelacruz@gmail.com',
    //         'phone_number' => '09194519722',
    //         'emergency_contact' => 'Marjorie Dela Cruz',
    //         'emergency_contact_number' => '09993148599'
    //     ]);
    //     \App\WorkDetails::create([
    //         'employee_id' => 5,
    //         'company' => 'IBM',
    //         'start_date' => '2017-09-15',
    //         'end_date' => '2020-08-15',
    //         'description' => 'this is a description'
    //     ]);

    //    User::create([
    //        'name' => 'John Kenneth Manalo',
    //        'email' => 'kenneth_manalo@abcsystems.com.ph',
    //        'password' => bcrypt('password123')
    //    ]);
    //    \App\Employee::create([
    //        'user_id' => 6,
    //        'first_name' => 'John Kenneth',
    //        'last_name' => 'Manalo',
    //        'gender' => 'male',
    //        'birth_date' => '1993-06-29',
    //        'permanent_address' => 'Execute Hylos St. KS Subdivision, Brgy. MidLane, Land of Dawn',
    //        'marital_status' => 'single'
    //    ]);
    //    \App\EmployeeContact::create([
    //         'employee_id' => 6,
    //         'email' => 'kennethmanalo@gmail.com',
    //         'phone_number' => '09174461892',
    //         'emergency_contact' => 'Shariffa Atienza',
    //         'emergency_contact_number' => '09278813365'
    //     ]);
    //     \App\WorkDetails::create([
    //         'employee_id' => 6,
    //         'company' => 'TADE Infotech Inc.',
    //         'start_date' => '2017-07-20',
    //         'end_date' => '2020-06-13',
    //         'description' => 'this is a description'
    //     ]);

    //    User::create([
    //        'name' => 'Emmanuel Atienza',
    //        'email' => 'emmanuel_atienza@abcsystems.com.ph',
    //        'password' => bcrypt('password123')
    //    ]);
    //    \App\Employee::create([
    //        'user_id' => 7,
    //        'first_name' => 'Emmanuel',
    //        'last_name' => 'Atienza',
    //        'gender' => 'male',
    //        'birth_date' => '1993-06-29',
    //        'permanent_address' => 'Harith St. Lightborn Subdivision, Brgy. MidLane, Land of Dawn',
    //        'marital_status' => 'single'
    //    ]);
    //    \App\EmployeeContact::create([
    //         'employee_id' => 7,
    //         'email' => 'emmanatienza@gmail.com',
    //         'phone_number' => '09155179983',
    //         'emergency_contact' => 'Yna Manalo',
    //         'emergency_contact_number' => '09985614268'
    //     ]);
    //     \App\WorkDetails::create([
    //         'employee_id' => 7,
    //         'company' => 'Infor Manila',
    //         'start_date' => '2019-01-05',
    //         'end_date' => '2020-07-28',
    //         'description' => 'this is a description'
    //     ]);

    //    User::create([
    //        'name' => 'John Kenneth Belmonte',
    //        'email' => 'belmonte@abcsystems.com.ph',
    //        'password' => bcrypt('password123')
    //    ]);
    //    \App\Employee::create([
    //        'user_id' => 8,
    //        'first_name' => 'John Kenneth',
    //        'last_name' => 'Belmonte',
    //        'gender' => 'male',
    //        'birth_date' => '1990-06-29',
    //        'permanent_address' => 'Kalimutan mo na yan St. Sige Sige Subdivision, Brgy. Maglibang, EXB',
    //        'marital_status' => 'single'
    //    ]);
    //    \App\EmployeeContact::create([
        //     'employee_id' => 8,
        //     'email' => 'kennethbelmonte@gmail.com',
        //     'phone_number' => '09174188926',
        //     'emergency_contact' => 'Geraldine Alupay',
        //     'emergency_contact_number' => '09278865240'
        // ]);
        // \App\WorkDetails::create([
        //     'employee_id' => 8,
        //     'company' => 'Concentrix',
        //     'start_date' => '2017-07-10',
        //     'end_date' => '2020-08-21',
        //     'description' => 'this is a description'
        // ]);
    }
}

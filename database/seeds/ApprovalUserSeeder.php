<?php

use Illuminate\Database\Seeder;
use App\ApprovalUser;

class ApprovalUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApprovalUser::create([
            'approval_process_id' => '1',
            'user_id' => '3',
            'seq_order' => NULL
        ]);
        ApprovalUser::create([
            'approval_process_id' => '2',
            'user_id' => '3',
            'seq_order' => NULL
        ]);
        ApprovalUser::create([
            'approval_process_id' => '2',
            'user_id' => '4',
            'seq_order' => NULL
        ]);
        ApprovalUser::create([
            'approval_process_id' => '3',
            'user_id' => '3',
            'seq_order' => '1'
        ]);
        ApprovalUser::create([
            'approval_process_id' => '3',
            'user_id' => '4',
            'seq_order' => '2'
        ]);
        ApprovalUser::create([
            'approval_process_id' => '4',
            'user_id' => '3',
            'seq_order' => NULL
        ]);
        ApprovalUser::create([
            'approval_process_id' => '4',
            'user_id' => '4',
            'seq_order' => NULL
        ]);
        ApprovalUser::create([
            'approval_process_id' => '5',
            'user_id' => '3',
            'seq_order' => '1'
        ]);
        ApprovalUser::create([
            'approval_process_id' => '5',
            'user_id' => '4',
            'seq_order' => '2'
        ]);
        ApprovalUser::create([
            'approval_process_id' => '5',
            'user_id' => '5',
            'seq_order' => '3'
        ]);
        ApprovalUser::create([
            'approval_process_id' => '6',
            'user_id' => '3',
            'seq_order' => NULL
        ]);
    }
}

<?php

use App\Holiday;
use Illuminate\Database\Seeder;

class HolidaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Holiday::create([
            'name' => "New Year’s Day",
            "holiday_date" => "2021-01-01",
            "holiday_type" => 1,
            "is_recurring" => 1,
            "description" => " also simply called New Year, is observed on 1 January, 
            the first day of the year on the modern Gregorian calendar as well as the Julian calendar."
        ]);

        Holiday::create([
            'name' => "Christmas Day",
            "holiday_date" => "2021-12-25",
            "holiday_type" => 1,
            "is_recurring" => 1,
            "description" => "Christmas is an annual festival commemorating
            the birth of Jesus Christ, observed primarily on December 25 as a 
            religious and cultural celebration among billions of people around the world."
        ]);

        Holiday::create([
            'name' => "Labor Day",
            "holiday_date" => "2021-05-01",
            "holiday_type" => 1,
            "is_recurring" => 1,
            "description" => "International Workers' Day, also known as Labour Day in 
            some countries and often referred to as May Day, is a celebration of 
            labourers and the working classes that is promoted by the international labour
             movement and occurs every year on May Day."
        ]);

        Holiday::create([
            'name' => "Independence Day",
            "holiday_date" => "2021-06-12",
            "holiday_type" => 1,
            "is_recurring" => 1,
            "description" => "Independence Day is an annual national holiday in the Philippines
             observed on June 12, commemorating the declaration of Philippine independence from Spain in 1898."
        ]);

        Holiday::create([
            'name' => "National Heroes Day",
            "holiday_date" => "2021-08-30",
            "holiday_type" => 1,
            "is_recurring" => 1,
            "description" => "Heroes' Day or National Heroes' Day may refer to a number
             of commemorations of national heroes in different countries. It is often 
             held on the birthday of a national hero or heroine, or the anniversary
              of their great deeds that made them heroes."
        ]);

        Holiday::create([
            'name' => "Rizal Day",
            "holiday_date" => "2021-12-30",
            "holiday_type" => 1,
            "is_recurring" => 1,
            "description" => "Rizal Day is a Philippine national holiday commemorating the
             life and works of José Rizal, a national hero of the Philippines. 
             It is celebrated every December 30, the anniversary of Rizal's 1896 execution at Bagumbayan in Manila."
        ]);
    }
}
